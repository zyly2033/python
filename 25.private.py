#访问限制 当类内部属性以__开头命名时，通过外部是无法直接访问的  在Python中，实例的变量名如果以__开头，就变成了一个私有变量（private），只有内部可以访问，外部不能访问
class Student1(object):
    def __init__(self,name,score):
        self.__name = name
        self.__score = score

    #当要访问一个属性时，我们可以通过方法实现
    def get_name(self):
        return self.__name
    
    def get_score(self):
        return self.__score

    #当要设置属性时，我们也可通过方法实现，并在方法内部对属性进行检验
    def set_score(self,score):
        if score>100 or score<0:
            return
        else:
            self.__score = score
        
    
    #有些时候，你会看到以一个下划线开头的实例变量名，比如_name，这样的实例变量外部是可以访问的，但是，按照约定俗成的规定，当你看到这样的变量时，意思就是，“虽然我可以被访问，但是，请把我视为私有变量，不要随意访问”。

    #双下划线开头的实例变量是不是一定不能从外部访问呢？其实也不是。不能直接访问__name是因为Python解释器对外把__name变量改成了_Student__name，所以，仍然可以通过_Student__name来访问__name变量

#创建实例
bart = Student1('郑洋',100)
# bart.__name     抛出异常AttributeError: 'Student' object has no attribute '__name'
print(bart.get_name())
bart.set_score(99)
print(bart.get_score())

print(bart._Student1__name)


#请把下面的Student对象的gender字段对外隐藏起来，用get_gender()和set_gender()代替，并检查参数有效性：
class Student(object):
    def __init__(self,name, gender):
        self.name = name
        self.__gender = gender
        
    
    def get_gender(self):
        return self.__gender

    #当要设置属性时，我们也可通过方法实现，并在方法内部对属性进行检验
    def set_gender(self,gender):
        if gender == 'male' or gender == 'female':                
            self.__gender = gender


bart = Student('Bart', 'male')
if bart.get_gender() != 'male':
    print('测试失败!')
else:
    bart.set_gender('female')
    if bart.get_gender() != 'female':
        print('测试失败!')
    else:
        print('测试成功!')
