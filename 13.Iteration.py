#迭代
d = {'a': 1, 'b': 2, 'c': 3}
#迭代key
for key in d:
    print(key)
print('\n')    
#迭代value
for value in d.values():
    print(value)
print('\n')
#迭代key-value
for k,v in d.items():
    print('%s->%s'%(k,v))
print('\n')
for ch in 'ABC':
    print(ch)

#如何判断一个对象是可迭代对象呢？方法是通过collections模块的Iterable类型判断：
from collections import Iterable
print(isinstance('abc',Iterable))
print(isinstance([1,2,3],Iterable))
print(isinstance(123,Iterable))

#Python内置的enumerate函数可以把一个list变成索引-元素对，这样就可以在for循环中同时迭代索引和元素本身：
for i,v in enumerate(['A','B','C']):
    print(i,v)
for x, y in [(1, 1), (2, 4), (3, 9)]:
     print(x, y)

#请使用迭代查找一个list中最小和最大值，并返回一个tuple：
def findMinAndMax(L):
    if L is None:
        raise TypeError('不能传入None')
    if len(L) == 0:
        maxv = None
        minv = None
    else:
        maxv = L[0]
        minv = L[0]
    #获取最大值
    for n in L:
        if n>maxv:
            maxv=n
        if n<minv:
            minv = n;
    return (minv,maxv)

# 测试
if findMinAndMax([]) != (None, None):
    print('测试失败!')
elif findMinAndMax([7]) != (7, 7):
    print('测试失败!')
elif findMinAndMax([7, 1]) != (1, 7):
    print('测试失败!')
elif findMinAndMax([7, 1, 3, 9, 5]) != (1, 9):
    print('测试失败!')
else:
    print('测试成功!')       
        
            
    
