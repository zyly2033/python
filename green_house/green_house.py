from PyQt5.QtGui import QIcon
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QMainWindow, QDialog

# 导入设计的ui界面转换成的py文件
from ui.ui_green_house import Ui_MainWindow
from ui.ui_setting import Ui_Setting
from ui.ui_log import Ui_Log


class GreenHouse(QMainWindow):
    """
     串口行为
    """

    def __init__(self):
        # QMainWindow构造函数初始化
        super().__init__()

        self.ui = Ui_MainWindow()
        # 这个函数本身需要传递一个MainWindow类，而该类本身就继承了这个，所以可以直接传入self
        self.ui.setupUi(self)

        # 设置窗口标题、图标
        self.setWindowTitle("工厂物联网终端")
        self.setWindowIcon(QIcon('./icon/monitor.svg'))
        # 禁用窗体拖拽功能
        self.setFixedSize(616, 761)

        # 加载要显示的图片为 QPixmap 对象
        pixmap = QPixmap("./icon/bg.jpg")

        # 将 QPixmap 对象按比例缩放到 Label 的大小
        scaled_pixmap = pixmap.scaled(self.ui.lb_bg.width(), self.ui.lb_bg.height())

        self.ui.lb_bg.setPixmap(scaled_pixmap)

        # 加载要显示的图片为 QPixmap 对象
        pixmap = QPixmap("./icon/temp.svg")

        # 将 QPixmap 对象按比例缩放到 Label 的大小
        scaled_pixmap = pixmap.scaled(self.ui.lb_tem_bg.width(), self.ui.lb_tem_bg.height())

        self.ui.lb_tem_bg.setPixmap(scaled_pixmap)

        # 加载要显示的图片为 QPixmap 对象
        pixmap = QPixmap("./icon/hum.svg")

        # 将 QPixmap 对象按比例缩放到 Label 的大小
        scaled_pixmap = pixmap.scaled(self.ui.lb_hum_bg.width(), self.ui.lb_hum_bg.height())

        self.ui.lb_hum_bg.setPixmap(scaled_pixmap)

        # 加载要显示的图片为 QPixmap 对象
        pixmap = QPixmap("./icon/noise.svg")

        # 将 QPixmap 对象按比例缩放到 Label 的大小
        scaled_pixmap = pixmap.scaled(self.ui.lb_noise_bg.width(), self.ui.lb_noise_bg.height())

        self.ui.lb_noise_bg.setPixmap(scaled_pixmap)

        # 加载要显示的图片为 QPixmap 对象
        pixmap = QPixmap("./icon/vib.svg")

        # 将 QPixmap 对象按比例缩放到 Label 的大小
        scaled_pixmap = pixmap.scaled(self.ui.lb_vib_bg.width(), self.ui.lb_vib_bg.height())

        self.ui.lb_vib_bg.setPixmap(scaled_pixmap)

        self.ui.btn_setting.clicked.connect(self.open_new_setting)
        self.ui.btn_view_log.clicked.connect(self.open_new_log)

    def open_new_setting(self):
        self.setting_window = QDialog()
        self.setting_ui = Ui_Setting()
        # 显示
        self.setting_ui.setupUi(self.setting_window)
        self.setting_window.show()

        # 设置窗体图标
        self.setting_window.setWindowIcon(QIcon('./icon/monitor.svg'))

        # 加载要显示的图片为 QPixmap 对象
        pixmap = QPixmap("./icon/bg.jpg")

        # 将 QPixmap 对象按比例缩放到 Label 的大小
        scaled_pixmap = pixmap.scaled(self.setting_ui.lb_bg.width(), self.setting_ui.lb_bg.height())

        self.setting_ui.lb_bg.setPixmap(scaled_pixmap)

        # 加载要显示的图片为 QPixmap 对象
        pixmap = QPixmap("./icon/temp.svg")

        # 将 QPixmap 对象按比例缩放到 Label 的大小
        scaled_pixmap = pixmap.scaled(self.setting_ui.lb_tem_bg.width(), self.setting_ui.lb_tem_bg.height())

        self.setting_ui.lb_tem_bg.setPixmap(scaled_pixmap)

        # 加载要显示的图片为 QPixmap 对象
        pixmap = QPixmap("./icon/hum.svg")

        # 将 QPixmap 对象按比例缩放到 Label 的大小
        scaled_pixmap = pixmap.scaled(self.setting_ui.lb_hum_bg.width(), self.setting_ui.lb_hum_bg.height())

        self.setting_ui.lb_hum_bg.setPixmap(scaled_pixmap)

        # 加载要显示的图片为 QPixmap 对象
        pixmap = QPixmap("./icon/noise.svg")

        # 将 QPixmap 对象按比例缩放到 Label 的大小
        scaled_pixmap = pixmap.scaled(self.setting_ui.lb_noise_bg.width(), self.setting_ui.lb_noise_bg.height())

        self.setting_ui.lb_noise_bg.setPixmap(scaled_pixmap)

        # 加载要显示的图片为 QPixmap 对象
        pixmap = QPixmap("./icon/vib.svg")

        # 将 QPixmap 对象按比例缩放到 Label 的大小
        scaled_pixmap = pixmap.scaled(self.setting_ui.lb_vib_bg.width(), self.setting_ui.lb_vib_bg.height())

        self.setting_ui.lb_vib_bg.setPixmap(scaled_pixmap)

    def open_new_log(self):
        self.log_window = QDialog()
        self.log_ui = Ui_Log()
        # 显示
        self.log_ui.setupUi(self.log_window)
        self.log_window.show()

        # 设置窗体图标
        self.log_window.setWindowIcon(QIcon('./icon/monitor.svg'))

        # 加载要显示的图片为 QPixmap 对象
        pixmap = QPixmap("./icon/bg.jpg")

        # 将 QPixmap 对象按比例缩放到 Label 的大小
        scaled_pixmap = pixmap.scaled(self.log_ui.lb_bg.width(), self.log_ui.lb_bg.height())

        self.log_ui.lb_bg.setPixmap(scaled_pixmap)
        style_sheet = f"QTextEdit{{background-image: url(./icon/bg.jpg);color:#fff}}"
        self.log_ui.tet_log.setStyleSheet(style_sheet)

        self.log_ui.tet_log.append("2024/4/1 12:58:20 打开温度传感器")
        self.log_ui.tet_log.append("2024/4/1 13:58:20 关闭温度传感器")

