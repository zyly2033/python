# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_log.ui'
#
# Created by: PyQt5 UI code generator 5.15.9
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Log(object):
    def setupUi(self, Log):
        Log.setObjectName("Log")
        Log.resize(566, 405)
        self.lb_bg = QtWidgets.QLabel(Log)
        self.lb_bg.setGeometry(QtCore.QRect(0, -10, 561, 411))
        self.lb_bg.setText("")
        self.lb_bg.setPixmap(QtGui.QPixmap("../icon/bg.jpg"))
        self.lb_bg.setObjectName("lb_bg")
        self.tet_log = QtWidgets.QTextEdit(Log)
        self.tet_log.setGeometry(QtCore.QRect(10, 10, 541, 381))
        self.tet_log.setObjectName("tet_log")

        self.retranslateUi(Log)
        QtCore.QMetaObject.connectSlotsByName(Log)

    def retranslateUi(self, Log):
        _translate = QtCore.QCoreApplication.translate
        Log.setWindowTitle(_translate("Log", "日志"))
