#请利用Python内置的hex()函数把一个整数转换成十六进制表示的字符串：
para = input('请输入一个整数')
#把获取的字符串转换为整数
num = int(para)
#转换为16进制的字符串
tran = hex(num)
print(tran)

#定义函数
def my_abs(x):
    #参数类型检测
    if not isinstance(x,(int,float)):
        raise TypeError('bad operand type')    
    if x >= 0:
        return x
    else:
        return -x
#调用自己定义的函数
print(my_abs(-99))
#print(my_abs('-99'))


#返回多个值 返回一个元组
import math      #导入math包
def move(x,y,step,angle=0):
    nx = x + step * math.cos(angle)
    ny = y - step * math.sin(angle)
    return nx,ny
#把元组一一对应赋值给变量
x,y = move(100,100,60,math.pi/6)
print(x,y)

#请定义一个函数quadratic(a, b, c)，接收3个参数，返回一元二次方程：
#ax2 + bx + c = 0的两个解。
def quadratic(a,b,c):
    #参数检查
    if not isinstance(a,(int,float)):
        raise TypeError('please intput number a')
    if not isinstance(b,(int,float)):
        raise TypeError('please intput number b')
    if not isinstance(c,(int,float)):
        raise TypeError('please intput number c')
    x1 = b*b - 4*a*c 
    if x1 < 0:
        return '该方程无解'
    elif x1 == 0:
        return -b/(2*a)
    else:
        x2 =( -b + math.sqrt(x1))/(2*a)
        x3 =( -b - math.sqrt(x1))/(2*a)
        return x2,x3
r = quadratic(2, 3, 1)
print(r)

if quadratic(2, 3, 1) != (-0.5, -1.0):
    print('测试失败')
elif quadratic(1, 3, -4) != (1.0, -4.0):
    print('测试失败')
else:
    print('测试成功')


#参考别人写的
a = int(input('请输入a的值：'))
b = int(input('请输入b的值：'))
c = int(input('请输入c的值：'))

def sqr(a, b, c):
    for i in (a, b, c):
        if not isinstance(i, (float, int)):
            print('数据类型输入有误，请重新输入！')
    d = b*b - 4*a*c
    if d < 0:
        print('此方程无实数解！')
        pass
    elif d == 0:
        print('此方程有唯一实数解！')
    else:
        print('此方程有两个实数解！')
    x1 = (0 - b - math.sqrt(d)) / (2*a)
    x2 = (0 - b + math.sqrt(d)) / (2 * a)
    return x1, x2
r = sqr(a, b, c)
print(r)

        


    
        
    
    
    
    
        
