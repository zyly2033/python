#### 一、git使用

#### 1.1 `git`全局设置

```shell
git config --global user.name "大奥特曼打小怪兽"
git config --global user.email "18151521911@163.com"
```

#### 1.2 上传仓库

如果本地没有仓库，初始化：
```shell
git init 
````

```shell 
git add .
git commit -m "python案例"
git remote add origin https://gitee.com/zyly2033/python.git
git push -u origin "master"
```
