# -*- coding: utf-8 -*-
"""
Created on Sat Jun  8 21:13:57 2019

@author: Administrator
"""

import pymysql as mdb


'''
mysql准备工作：
1、pip install pymysql
2、修改mysql配置文件
[mysqld]    
    # The default character set that will be used when a new schema or table is
    # created and no character set is defined
    default-character-set = utf8
    character-set-server = utf8
    collation-server = utf8_general_ci
    init_connect = 'SET collation_connection = utf8_general_ci'
    init_connect = 'SET NAMES utf8'
    
    # The default storage engine that will be used when create new tables when
    default-storage-engine=INNODB
3、重启mysql服务
'''


class Mysql():
    '''
    Mysql数据库操作：https://www.cnblogs.com/conanwang/p/6028110.html
    https://www.runoob.com/python3/python3-mysql.html
    '''
    def __init__(self,user='root',password='123456aa'):   
        self.localhost = 'localhost'
        self.user = user
        self.password = password
        self.database = 'ntit'
        self.table = 'NewsInfo'        
        
        #创建数据库 创建表
        self.create_databases()
        self.create_table()
        pass
     
    def create_databases(self):   
        '''
        重新创建数据库
        '''
        # 打开数据库连接
        conn = mdb.connect(host=self.localhost,port=3306,user=self.user,passwd=self.password,charset='utf8' )
 
        # 使用 cursor() 方法创建一个游标对象 cursor
        cursor = conn.cursor()
        
        # 创建数据库
        cursor.execute('DROP DATABASE IF EXISTS %s' % self.database )
        cursor.execute('CREATE DATABASE IF NOT EXISTS %s ' %self.database )

        # 关闭游标连接
        cursor.close()
        # 关闭数据库连接
        conn.close()
        
        
    def create_table(self):
        '''
        重新创建数据表
        '''
        # 打开数据库连接
        conn = mdb.connect(host=self.localhost,port=3306,user=self.user,passwd=self.password,db=self.database,charset='utf8' )
        
 
        # 使用 cursor() 方法创建一个游标对象 cursor
        cursor = conn.cursor()
        
        
        # 使用 execute() 方法执行 SQL，如果表存在则删除
        cursor.execute("DROP TABLE IF EXISTS %s" % self.table)
 
        # 使用预处理语句创建表
        sql = "CREATE TABLE %s(       \
             ID INT AUTO_INCREMENT PRIMARY KEY,   \
             URL VARCHAR(100),        \
             TITLE  VARCHAR(100),    \
             VIEWS  VARCHAR(100),     \
             RELEASEDATE DATE,            \
             COUNT  INT,             \
             CONTENT TEXT)" % self.table   
 
        cursor.execute(sql)
 
        # 关闭游标连接
        cursor.close()
        # 关闭数据库连接
        conn.close()

    def insert_row(self,url,title,views,update,count,content):
        '''
        向表中插入一行数据
        '''
        # 打开数据库连接
        conn = mdb.connect(host=self.localhost,port=3306,user=self.user,passwd=self.password,db=self.database,charset='utf8' )
         
        # 如果使用事务引擎，可以设置自动提交事务，或者在每次操作完成后手动提交事务conn.commit()
        #conn.autocommit(1)    # conn.autocommit(True) 
        
        # 使用cursor()方法获取操作游标 
        cursor = conn.cursor()
         
        # SQL 插入语句
        sql = "INSERT INTO %s(URL, TITLE, VIEWS, RELEASEDATE, COUNT, CONTENT) \
            VALUES ('%s', '%s',  '%s', '%s',  %s,  '%s')" \
            % (self.table, url, title, views, update, count, content)
        try:
           # 执行sql语句
           cursor.execute(sql)
           # 提交到数据库执行
           conn.commit()
           
        except:
           # 如果发生错误则回滚
           conn.rollback()
           print('Error: unable to write data!')
         
        # 关闭游标连接
        cursor.close()
        # 关闭数据库连接
        conn.close()
        
    def fetch_row(self):
        '''
        取数据表中的第一行数据
        '''
        # 打开数据库连接
        conn = mdb.connect(host=self.localhost,port=3306,user=self.user,passwd=self.password,db=self.database,charset='utf8' )
         
        # 使用cursor()方法获取操作游标 
        cursor = conn.cursor()

        # SQL 查询语句
        sql = 'SELECT * FROM {} WHERE ID < {}'.format(self.table,2)
        try:
            # 执行SQL语句
            cursor.execute(sql)
            # 获取所有记录列表
            results = cursor.fetchall()
            for row in results:
                r_id = row[0]
                r_url = row[1]
                r_title = row[2]
                r_views = row[3]
                r_update = row[4]
                r_count = row[5]
                r_content = row[6]
            # 打印结果
            print('id={},url={},title={},views={},update={},count={},content={}'.format(r_id, r_url, r_title, r_views, r_update,r_count,r_content ))
        except:
            print('Error: unable to fecth data')
        # 关闭游标连接
        cursor.close()
        # 关闭数据库连接
        conn.close()