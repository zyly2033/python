# -*- coding: utf-8 -*-
"""
Created on Sat Jun  8 17:11:40 2019

@author: Administrator
"""

import time
from selenium import webdriver
import urllib.request as urllib  
import os
from  mysql import Mysql
import logging

'''
webdriver使用说明：https://wenku.baidu.com/view/d64005c6af45b307e9719715.html
爬取南通理工学院首页列出的所有新闻（学校新闻、学院动态和媒体报道模块中所列出的新闻）
'''

'''
准备工作：
1、下载火狐浏览器并安装
2、下载geckodriver.exe：下载地址：https://github.com/mozilla/geckodriver/releases请根据系统版本选择下载；（如Windows 64位系统）
3、下载解压后将getckodriver.exe复制到Firefox的安装目录下，
    如（C:\Program Files\Mozilla Firefox），并在环境变量Path中添加路径：C:\Program Files\Mozilla Firefox
4、、安装selenium    pip install selenium
'''

class NtitSpy():
    def __init__(self):  
        #南通理工学院首页网址
        self.herf = 'http://www.ntit.edu.cn/'
        #获取火狐浏览器对象 
        self.browser = webdriver.Firefox(executable_path = r'D:\FF\geckodriver.exe')   
        
        #数据库连接
        self.sql =  Mysql('root','123456aa')

        self.show_msg = False        
        
        '''
        输出日志  https://www.cnblogs.com/bethansy/p/7716747.html
        '''
        '''
        logging.basicConfig(
                level=logging.DEBUG,         # 定义输出到文件的log级别，大于此级别的都被输出
                format='%(asctime)s  %(filename)s : %(levelname)s  %(message)s',  # 定义输出log的格式
                datefmt='%Y-%m-%d %A %H:%M:%S',
                filename='./ntit.log',  # log文件名
                filemode='w')  # 写入模式“w”或“a”'''
        
                                        
        
        
    def get_all_news(self):
        '''
        爬取南通理工学院首页列出的所有新闻（学校新闻、学院动态和媒体报道模块中所列出的新闻）
        '''        
        ###################################     点击学校新闻  更多   ##############################################
        #html请求 网站首页
        self.browser.get(self.herf)
        time.sleep(2) 
        
        #<a href="/79/list.htm" class="w7_more"><span frag="按钮内容"> 更多&gt;&gt;</span></a>
        more_school_news = self.browser.find_element_by_xpath('//a[@class="w7_more"]') 
        more_school_news.click() 
        time.sleep(2)  
        print('Crawing school news.....')
        
        self.get_news()
        
        ###################################     点击学院动态  更多   ##############################################
        #html请求 网站首页
        self.browser.get(self.herf)
        time.sleep(2) 
        
        #<a href="/78/list.htm" class="w10_more"><span frag="按钮内容"> 更多&gt;&gt;</span></a>
        more_school_news = self.browser.find_element_by_xpath('//a[@class="w10_more"]') 
        more_school_news.click() 
        time.sleep(2)  
        print('Crawing academic news.....')
        
        self.get_news()
        
        ###################################     点击媒体报道  更多   ##############################################
        #html请求 网站首页
        self.browser.get(self.herf)
        time.sleep(2) 
        
        #<a href="/80/list.htm" class="w9_more"><span frag="按钮内容">更多&gt;&gt;</span></a>
        more_school_news = self.browser.find_element_by_xpath('//a[@class="w9_more"]') 
        more_school_news.click() 
        time.sleep(2)  
        print('Crawing media report news.....')
        self.get_news()        
        
        ##################################### 测试   #################################################################
        #取第一个新闻进行测试
        print('Get news from mysql.....')
        self.sql.fetch_row()
    
    def get_news(self):
        '''
        获取第一页前4条新闻（即官网首页显示的欣慰）
        '''
        #每页记录数
        #<em class="per_count">14</em>
        #per_count = self.browser.find_element_by_xpath('//em[@class="per_count"]') 
        per_count = 4#int(per_count.text)
        count = 1
        
        #获取当前页面  
        #<em class="curr_page">1</em>
        curr_page = self.browser.find_element_by_xpath('//em[@class="curr_page"]') 
        start_index = int(curr_page.text)    
        
        #获取总页码
        #<em class="all_pages">159</em> 
        #all_pages = self.browser.find_element_by_xpath('//em[@class="all_pages"]') 
        end_index = start_index#int(all_pages.text)        
                
        #遍历每一页
        for i in range(start_index,end_index+1):
            #遍历一页中的每一个新闻 会打开一个新的窗口
            print('Crawing page {}.....'.format(i))       
            
            #获取当前页记录数
            #<div  class="column-news-list clearfix"></div>
            div = self.browser.find_element_by_xpath('//div[@class="column-news-list clearfix"]') 
            #每一个子<div>对应一条新闻
            elements =  div.find_elements_by_tag_name('div') 
    		  #<div class="column-news-item item-1 clearfix" href="/2019/0606/c79a26830/page.htm"><span class="column-news-title"><a href="/2019/0606/c79a26830/page.htm" target="_blank" title="学校召开家长委员会会议">学校召开家长委员会会议</a></span><span class="column-news-date news-date-hide">2019-06-06</span></div>
            for element in elements[0:-1]:                                                
                #<a href="/2019/0606/c79a26830/page.htm" target="_blank" title="学校召开家长委员会会议">学校召开家长委员会会议</a>
                news = self.browser.find_element_by_xpath('//a[@href="{}"]'.format(element.get_attribute('href'))) 
                news.click()
                time.sleep(1)  
                                
                #0是第一个打开的窗口  1是新打开的窗口（新闻窗口） 转到新打开的窗口                                
                self.browser.switch_to.window(self.browser.window_handles[1])
                
                ############ 获取新闻   #########################################
                row = self.get_news_info()
                if self.show_msg:
                    print(row)

                
                # 关闭当前窗口
                self.browser.close()
                #切换回窗口0
                self.browser.switch_to.window(self.browser.window_handles[0]) 
                
                
                count = count+1
                if count > per_count:
                    break
                
            #下一页
            #<a class="next" href="/79/list2.htm" target="_self"><span>下一页&gt;&gt;</span></a>
            #next_page = self.browser.find_element_by_xpath('//a[@class="next"]') 
            #next_page.click()
            #time.sleep(1)  
               
						   
       

                
    def get_news_info(self):
        '''
        获取当前窗口的信息 注意：要求当前窗口必须是新闻页面
        
        args:
        return:
            row:获取当前新闻窗口的数据    并把当前内容保存在文件以及数据库中
            格式为：标题 发布者  发布时间  浏览次数  新闻的完整链接  正文 
        '''
        #链接  http://www.ntit.edu.cn/2019/0606/c79a26830/page.htm    
        current_url = self.browser.current_url   
        
        #新闻标题
        #<h1 class="arti-title">学校召开家长委员会会议</h1>
        title = self.browser.find_element_by_xpath('//h1[@class="arti-title"]').text         
        #logging.info(title)
                
        #发布者
        #<span class="arti-views">发布者：宣传统战部</span>
        text = self.browser.find_element_by_xpath('//span[@class="arti-views"]').text        
        views =text[text.rfind('：')+1:]                       
        #logging.info(views)

        #发布时间
        #<span class="arti-update">发布时间：2019-06-06</span>
        text = self.browser.find_element_by_xpath('//span[@class="arti-update"]').text                        
        update =text[text.rfind('：')+1:]                          
        #logging.info(update)
        
        #浏览次数
        #<span class="WP_VisitCount" url="/_visitcountdisplay?siteId=5&amp;type=3&amp;articleId=26830">211</span>
        count = self.browser.find_element_by_xpath('//span[@class="WP_VisitCount"]').text        
        #logging.info(count)              
                                                             
            
        #创建文件夹2019
        array = current_url.split('/')
        dir1 = array[3]
        dir2 = array[4]
        
        path = dir1
        # 判断路径是否存在  2019
        if not os.path.exists(path):
            # 如果不存在则创建目录        　
            os.makedirs(path) 
 
        path = os.path.join(path,dir2)
        # 判断路径是否存在 0606
        if not os.path.exists(path):
            # 如果不存在则创建目录        　
            os.makedirs(path) 
        
        path = os.path.join(path,title)
        # 判断路径是否存在  以标题为文件夹名，保存图片
        if not os.path.exists(path):
            # 如果不存在则创建目录        　
            os.makedirs(path) 
            
            
        #文章内容部分 class="wp_articlecontent"
        article = self.browser.find_element_by_xpath('//div[@class="wp_articlecontent"]')

        file = '{}.txt'.format(path)        
        #写文件
        with open(file, 'w',encoding='utf-8') as f:          
            f.write(title+'\n')
            f.write(views+'\n')
            f.write(update+'\n')
            f.write(count+'\n')
            f.write(current_url+'\n')
            
            #遍历每一个元素 可能是段落 也可能是图片
            elements =  article.find_elements_by_tag_name('p') 
            i= 0
            content = ''
            #遍历每一项目
            for element in elements:                   
                try:
                    # 如果获取成功说明是段落
                    #正文
                    #<p class="p_text_indent_2" style="text-align:justify;line-height:1.75em;">
                    #   <span style="line-height:1.75em;font-size:16px;">   该元素可能有
                    #      6月6日下午，南通理工学院家长委员会会议在紫琅厅召开。校长、党委书记王宝根、学工处相关负责人及家长代表参加了会议。会议由校长助理赵怀斋主持。
                    #   </span>
                    #</p>
                    p_text = element.find_element_by_tag_name('span')         
                    f.write(p_text.text)
                    content =  '{}{}'.format(content,p_text.text)
                except:
                    try:
                        #如果获取成功说明是图片
                        #<p style="TEXT-ALIGN: center; PADDING-BOTTOM: 0px; WIDOWS: 2; TEXT-TRANSFORM: none; BACKGROUND-COLOR: rgb(255,255,255); TEXT-INDENT: 24px; MARGIN: 0px 0px 6px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; FONT: 400 14px/1.6 'Microsoft YaHei'; WHITE-SPACE: normal; ORPHANS: 2; LETTER-SPACING: normal; COLOR: rgb(51,51,51); WORD-SPACING: 0px; PADDING-TOP: 0px; font-stretch: normal; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial">
                        #   <img style="BORDER-BOTTOM: 0px; BORDER-LEFT: 0px; PADDING-BOTTOM: 0px; MARGIN: 0px; PADDING-LEFT: 0px; WIDTH: 600px; PADDING-RIGHT: 0px; MAX-WIDTH: 940px; HEIGHT: 400px; BORDER-TOP: 0px; BORDER-RIGHT: 0px; PADDING-TOP: 0px" src="http://qcx.ntit.edu.cn/_upload/article/images/f7/ab/6db6a21740eda751937c783ebe54/2f16e552-3ac1-4686-ae06-a1bccde70298.jpg" data-layer="photo" original-src="/_upload/article/images/f7/ab/6db6a21740eda751937c783ebe54/2f16e552-3ac1-4686-ae06-a1bccde70298_d.jpg" sudyfile-attr="{'title':'DSC_1103.JPG'}" width="600" vspace="0" hspace="0" height="400" border="0">
                        #</p>
                        p_image = element.find_element_by_tag_name('img')      
                        dst = os.path.join(path,'{}.jpg'.format(i))
                        self.download(p_image.get_attribute('src'),dst)
                        i= i+1
                    except:
                        #说明是段落
                        f.write(element.text)        
                        content =  '{}{}'.format(content,element.text)

        
        row = '%s %s %s %s %s %s\n'%(current_url,title,views,update,count,content)        
        #  保存到数据库
        self.sql.insert_row(current_url,title,views,update,count,content)
        if self.show_msg:
            print('Download new from {0} finish!'.format(current_url))
        return row
            
      
    
    def download(self,url,dstpath=None):
        '''
        利用urlretrieve()这个函数可以直接从互联网上下载文件保存到本地路径下
        
        args:
            url：网页文件或者图片以及其他数据路径
            dstpath:保存全路况
        '''        
        if dstpath is None:
            dstpath = './code.jpg'
        try:
            urllib.urlretrieve(url,dstpath)                    
            if self.show_msg:
                print('Download from {} finish!'.format(url))
        except Exception as e:
            print('Download from {} fail!'.format(url))
            
        

if __name__ == '__main__':
    #加载页面
    spy = NtitSpy()
    spy.get_all_news()

