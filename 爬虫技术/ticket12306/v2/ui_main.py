﻿# -*- coding: utf-8 -*-
'''Created on Thu Dec 27 15:08:45 2018
@author: Administrator
PyQt5使用：参考https://www.jianshu.com/p/094928ac0b73
推荐pyqt使用：https://blog.csdn.net/qq_31050167/article/details/78924020  很详细
python3+PyQt5+Qt designer+pycharm安装及配置+将ui文件转py文件：https://blog.csdn.net/lyzwjaa/article/details/79429901

安装pyqt ： pip install PyQt5
安装Qt Designer：pip install PyQt5-tools

requests请求卡死问题：https://blog.csdn.net/pilipala6868/article/details/80712195
需要设置DNS域名服务器为;223.5.5.5 / 223.6.6.6
'''

from PyQt5.QtWidgets import QApplication
import sys
from behaviourn_login import  QtLogin
from behaviourn_buyticket import  QtBuyTicket

'''
界面版本刷票系统从这里开始运行
'''


if __name__ == '__main__':
    app = QApplication(sys.argv)

    # 购票界面 ui_login对象中调用ui_buy_ticket
    ui_buy_ticket = QtBuyTicket()

    #登陆界面
    ui_login = QtLogin(ui_buy_ticket)
    # 加载验证码 并尝试自动识别登录，登陆成功用户就会在线
    ui_login.__load_code_image__()
    ui_login.show()
    sys.exit(app.exec_())
