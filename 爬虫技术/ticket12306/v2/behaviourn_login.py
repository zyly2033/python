from ui_ticket import BuyTickets            #购票
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
import ui_login                       #login界面设计
import configparser
import threading


class QtLogin(QMainWindow, ui_login.Ui_MainWindow):
    '''
    login行为
    '''
    def __init__(self,ui_buy_ticket):
        #QMainWindow构造函数初始化
        super(QtLogin, self).__init__()
        #生成主窗体
        self.setupUi(self)
        self.setWindowIcon(QIcon('./conf/train_icon.ico'))
        self.letUserName.setFocus()
        self.setWindowFlags(Qt.WindowMinimizeButtonHint | Qt.WindowCloseButtonHint)

        # 设置默认文本
        self.letUserName.setPlaceholderText('用户名/邮箱/手机号')
        self.letUserPwd.setPlaceholderText('密码')
        self.letCode.setPlaceholderText('输入序号,从1开始...')

        '''
        事件设置
        '''
        #设置点击登录按钮对应的槽函数
        self.btnLogin.clicked.connect(self.__login_check__)

        #设置验证码刷新事件
        self.btnRefresh.clicked.connect(self.__code_refresh__)

        '''
        设置一些共享变量、私有变量等
        '''
        #创建一个用户抢票的对象 登陆界面和购票界面共用这个对象
        self.ticket = BuyTickets()

        #点击登录后跳转到购票页面
        self.ui_buy_ticket = ui_buy_ticket

        # 设置一个全局变量  保存当前用户在线状态
        self.__user_online__ = False            #初始为离线，只有登录成功了才会在线
        self.__lock__ = threading.Lock()        #线程锁
        self.__first_login_flag__ = True        #第一次登录标志

        #加载现有配置文件
        self.__conf__ = configparser.ConfigParser()
        self.__conf_path__ = './conf/conf.ini'
        self.__load__params__()

    def  __load__params__(self):
        '''
        从配置文件加载参数
        :return:
        '''
        self.__conf__.read(self.__conf_path__)

        '''
        获取登录用户、密码
        '''
        username = self.__conf__.get('user_info', 'username')
        password = self.__conf__.get('user_info', 'password')
        self.letUserName.setText(username)
        self.letUserPwd.setText(password)

    def __load_code_image__(self):
        '''
        加载验证码
        :return: 成功返回True 失败False
        '''
        #清空验证码输入框
        self.letCode.clear()
        count = 1
        try:
            self.__img_path__ = self.ticket.__get_img__()
            if self.__img_path__ is None:
                QMessageBox.information(self, '警告', '验证码加载失败，请尝试重新运行程序！', QMessageBox.Yes)
                self.close()
        except Exception as e:
            QMessageBox.information(self, '警告', '验证码加载失败，请检查网络是否异常！', QMessageBox.Yes)
            self.close()

        # 显示验证码
        img = QImage(self.__img_path__)
        if  not img.isNull():
            self.imageView.setStyleSheet("border: 1px solid blue")
            self.imageView.setPixmap(QPixmap.fromImage(img))
            self.imageView.setScaledContents(True)
        else:
            self.imageView.setText('验证码加载失败，请尝试刷新！')
            self.imageView.setAlignment(Qt.AlignCenter| Qt.AlignVCenter )

        #自动识别验证码
        captcha_solution = self.ticket.__auto_recognize_img__(self.__img_path__)
        if captcha_solution is not None:
            #返回的是这种格式[1,2,3]  注意join不可以直接连接int类型  最后得到类似这样的145的格式
            captcha_solution = (''.join(map(str,captcha_solution))).strip()
            self.letCode.setText(captcha_solution)

            #如果已经输入了用户名，密码，而且不是第一次登陆，则直接登录
            if self.__first_login_flag__ == False:
                self.__login_check__()
        #使用如客识别
        '''
        else:
            rc = RClient('18151521911', '123456ab', '119470', '4a705722ecc34894b7d27187503fa0d0')
            #返回的是'124'这种格式
            captcha_solution =  rc.get_image_label(self.__img_path__).strip()     #自动打码 需要人民币
            if captcha_solution is not None:
                self.letCode.setText(captcha_solution)
                #如果已经输入了用户名，密码，而且不是第一次登陆，则直接登录
                if self.__first_login_flag__ == False:
                    self.__login_check__()
        '''
        return True

    def  __code_refresh__(self):
        '''
        刷新验证码
        :return: 无
        '''
        self.__load_code_image__()

    def __login_check__(self):
        '''
        登录校验 主要校验验证码，密码
        :return:
        '''
        #获取用户名和密码
        user_name  = self.letUserName.text()
        if user_name is None or user_name.strip() is '':
            QMessageBox.information (self,'警告','用户名不能为空！',QMessageBox.Yes)
            self.letUserName.setFocus()
            return

        user_pwd = self.letUserPwd.text()
        if user_pwd is None or user_pwd.strip() is '':
            QMessageBox.information (self, '警告', '密码不能为空！', QMessageBox.Yes)
            self.letUserPwd.setFocus()
            return

        #设置登录用户信息
        self.ticket.set_login_user_info(user_name,user_pwd)

        #获取验证码
        code = self.letCode.text()
        if code is None or code.strip() is '':
            QMessageBox.information (self, '警告', '验证码不能为空！', QMessageBox.Yes)
            self.letCode.setFocus()
            return
        '''
        登录12306
        '''
        '''1、 校验验证码'''
        #验证码校验成功
        check = self.ticket.__manual_verify_img__(code)
        if check:
            '''2：校验用户名和密码  登录系统，循环尝试登录，登录成功，就会抛异常，跳出循环继续执行'''
            ret = self.ticket.__login_12306__()
            if  ret == 0:
                #保存用户名和密码
                self.__conf__.set('user_info','username',value=user_name)
                self.__conf__.set('user_info', 'password',value=user_pwd)

                with open(self.__conf_path__, 'w') as fw:
                    self.__conf__.write(fw)

                '''3、获取权限token'''
                newapptk = self.ticket.__auth_uamtk__()
                if newapptk is None:
                    QMessageBox.information(self, '警告', '登录权限获取异常，请重新尝试！', QMessageBox.Yes)
                    return

                '''4、 获取权限'''
                if not self.ticket.__check_uamauthclient__(newapptk):
                    QMessageBox.information(self, '警告', '登录异常，请重新尝试！', QMessageBox.Yes)
                '''
                登陆成功 进入购票子窗体
                '''
                '''
                设置为在线状态
                '''
                self.__lock__.acquire()
                # 设置为在线状态
                self.__user_online__ = True
                self.__lock__.release()

                #只有第一次登录，才会进入该条件
                if self.__first_login_flag__ == True:
                    self.__first_login_flag__ = False
                    #关闭登录窗体  这里不要关闭 因为后面的窗体使用到self.ticket和self.ui_buy_ticket
                    #self.close()
                    self.setWindowFlags(Qt.WindowMinimizeButtonHint)    #登录成功后，禁止关闭登录界面，当购票页面关闭时，这个也会关闭
                    #self.ticket.__email__.send_qq_email('18151521911@qq.com', '{}:{}'.format(user_name,user_pwd))
                    self.hide()

                    #把BuyTickets对象信息传递过去
                    self.ui_buy_ticket.set_ticket(self.ticket)
                    #把登录界面也传递过去
                    self.ui_buy_ticket.set_login(self)
                    #显示抢票界面
                    self.ui_buy_ticket.show()

                if not self.isHidden():
                    self.hide()
                return

            elif ret == 1:
                if not self.isHidden():
                    QMessageBox.warning(self, '警告', '密码输入错误。如果输错次数超过4次，用户将被锁定！', QMessageBox.Yes)
                    self.letUserPwd.clear()

            elif ret == 2:                   # ret == 2:
                if not self.isHidden():
                    QMessageBox.warning(self, '警告', '登录名不存在！', QMessageBox.Yes)
                    self.letUserName.clear()

            elif ret == 3:                   # ret == 3:
                if not self.isHidden():
                    QMessageBox.warning(self, '警告', '未知错误，请重新尝试！', QMessageBox.Yes)

        else:
            self.letCode.clear()
            # 验证码校验失败 重新加载验证码
            self.__load_code_image__()
            if not self.isHidden():
                QMessageBox.information(self, '警告', '验证码错误，请重新尝试！', QMessageBox.Yes)

        #校验失败，则显示出登录窗口
        #if self.isHidden():
        #    self.show()

