#!/usr/bin/env python
# coding:utf-8
#http://wiki.ruokuai.com/%E8%AF%86%E5%88%AB%E5%9B%BE%E5%83%8FPOST.ashx

import requests
from hashlib import md5

'''
使用如快打码平台进行验证码打码识别
'''
class RClient(object):

    def __init__(self, username, password, soft_id, soft_key):
        self.username = username        
        self.password = md5(password.encode('utf-8')).hexdigest()
        self.soft_id = soft_id
        self.soft_key = soft_key
        self.base_params = {
            'username': self.username,
            'password': self.password,
            'softid': self.soft_id,
            'softkey': self.soft_key,
        }
        self.headers = {
            'Connection': 'Keep-Alive',
            'Expect': '100-continue',
            'User-Agent': 'ben',
        }

    def rk_create(self, im, im_type, timeout=60):
        """
        im: 图片字节
        im_type: 题目类型
        """
        params = {
            'typeid': im_type,
            'timeout': timeout,
        }
        params.update(self.base_params)
        files = {'image': ('a.jpg', im)}
        r = requests.post('http://api.ruokuai.com/create.json', data=params, files=files, headers=self.headers)
        return r.json()

    def rk_report_error(self, im_id):
        """
        im_id:报错题目的ID
        """
        params = {
            'id': im_id,
        }
        params.update(self.base_params)
        r = requests.post('http://api.ruokuai.com/reporterror.json', data=params, headers=self.headers)
        return r.json()
    
    def get_image_label(self, img_path, im_type=6113, timeout=60):
        '''
        返回一个list 返回符合要求图片的序号  '123'
        失败返回None
        '''
        im = open(img_path, 'rb').read()
        #{'Result': '8', 'Id': '2af13230-7fdb-4e4d-882a-0941373de2df'}
        re = self.rk_create(im, im_type)
        try:
            if isinstance(re,dict):
                nums = re['Result']
                #nums = ','.join(nums)            
                #nums = list(map(int,nums.split(',')))
        except Exception as e:
            nums = None
        return nums

#rc = RClient('18151521911', '123456ab', '119470', '4a705722ecc34894b7d27187503fa0d0 ')
