# -*- coding: utf-8 -*-
"""
Created on Sat Dec 29 12:11:18 2018

@author: Administrator
"""

import smtplib
from email.mime.text import MIMEText
from urllib import parse

class Email(object):
    def __init__(self):
        pass
    
    def send_163_mail(self, receiver, content,caption='抢票成功通知！'):
        '''
        发送邮件通知
        
        args：
            receiver：收件邮箱
            content：发送内容
        '''        
        # 连接邮箱服务器信息
        host = 'smtp.163.com'
        port = 25
        sender = '18151521911@163.com'   # 发件邮箱号码
        pwd = '123456ab'                 # 不是登陆密码，是客户端授权密码
        # 发件信息
        body = '<h2>温馨提醒：</h2><p>' + content + '</p>'
        msg = MIMEText(body, 'html', _charset="utf-8")
        msg['subject'] = caption
        msg['from'] = sender
        msg['to'] = receiver
        s = smtplib.SMTP(host, port)
        
        # 开始登陆邮箱，并发送邮件        
        s.login(sender, pwd)
        s.sendmail(sender, receiver, msg.as_string())
        s.quit()
        print('邮件已发送~你可以安心去玩了')
    
    def send_qq_email(self,receiver, content,caption='抢票成功通知！'):
        '''
        发送邮件通知
        
        args：
            receiver：收件邮箱
            content：发送内容
            caption：标题
        '''        
        # 连接邮箱服务器信息
        host = 'smtp.qq.com'             #qq邮箱的smtp Sever地址
        port = 465                       #开放的端口
        sender = '975481319@qq.com'   # 发件邮箱号码
        pwd = 'qxvnaqcdvitrbbbb'                 # 不是登陆密码，是客户端授权密码  crlzpfiwgvktbgge
        
        # 发件信息
        body = '<h2>温馨提醒：</h2><p>' + content + '</p>'
        msg = MIMEText(body, 'html', _charset="utf-8")
        msg['subject'] = caption
        msg['from'] = sender
        msg['to'] = receiver
        s =  smtplib.SMTP_SSL()
        s.connect(host, port)
        
        # 开始登陆邮箱，并发送邮件
        s.login(sender, pwd)
        s.sendmail(sender, receiver, msg.as_string())
        s.quit()
        print('邮件已发送~你可以安心去玩了')
    
        
        
if __name__ == '__main__':
    email = Email()
    email.send_qq_email('441952120@qq.com','收到了么')
    #email.send_163_mail('975481319@qq.com','收到了么')