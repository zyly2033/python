# -*- coding: utf-8 -*-
"""
Created on Tue Dec 25 09:01:44 2018

@author: Administrator
"""

'''
抢票程序
使用python + requests
速度更快，不用通过前端
参考：python3.x 购买12306火车票：https://www.jianshu.com/p/7d15b9b989a5
'''


from time import sleep 
from urllib import parse
import requests
#from PIL import Image


import json
import random
from bs4 import BeautifulSoup
#import urllib3
import datetime
import re
import time
from v2.trains_demo import TrainsDemo
from prettytable import PrettyTable
#import sys
#import datetime
#import eventlet
import threading
from collections import Iterable
from v2.conf.proxy  import load_valid_proxies

import os

#发送邮箱
from v2.send_email import Email
from retrying import retry

#图像识别
from v2.image_recognition import ImageRecognition

requests.packages.urllib3.disable_warnings()

'''
如果请求返回的是html
则下面两个一样
print(response.text)     一般针对返回的是文本
print(response.content.decode())   一般针对的是字节码，或者其他

如果是json字符串，则需要respons.json(xx) 或者 json.load(xx)
'''


#配置参数 全局使用
global_output_log_flag = False     #打开输出日志
        
global_is_auto_buy = True         #自动购买

global_is_auto_rec = True         #自动识别验证码


'''
************************************************************************************************
************************************************************************************************
'''
class BuyTickets(object):
    def __init__(self):
        '''
        构造函数
        '''
        #初始化字典
        self.__init_dict__()
        
        #用户
        self.username = ''
        self.userpwd = ''
        
        #车次
        self.numbers = None

        #车次出发时间所在的时间段
        self.start_time = None
        self.end_time = None
            
        self.passengers = None
        self.passenger_cards = None
        self.sexs = None
        
        #起始地 目的地        
        self.from_station = None
        self.to_station = None
        
        #日期
        self.dtime = None
                                                    
        #座位类型 默认顺序
        self.seat_type_names = ['二等座','硬座','软座', '硬卧','软卧','无座',
                               '动卧',
                               '高级软卧',
                               '一等座',
                               '商务座特等座']
                  
        #车次类型
        self.train_type_names = None
        
        #保存余票查询次数
        self.__query_count__ = 1
        #保存验证码识别失败次数  超过5次转为手动识别
        self.__recognize_count = 0        
        #保存余票查询异常次数，连续超过3次，默认被系统禁止查票
        self.__query_exception_count__ = 0
                        
        '''
        模拟发送请求页面 用到的数据
        '''
        #保存查询到的符合要求的车次信息
        self.__trains_data_list__ = None                                 #把每一行原始车次数据使用'|'返回  并返回一个list 每一行对应一个车次信息  [['bJvgWng4bhRNz7UP2ILtKkDh4vLwVHUTXOIvPpm3Igwntr3QDzZDprV7wyjjzxU5gmM5D8O8ip29','D305',...]]
        self.__trains_data_filter_list__ = None                          #过滤一些信息，并返回一个list 每一行对应一个车次信息  [{'trips':'D305',....},{'trips':'D305',....}]
        self.__trains_table__ = None                                    #trains_data_filter_list数据转换为PrettyTable类型
        
        #保存预订的车次信息                             
        self.__preorder_train_index__ = None                     #预订的车次在__trains_table__的索引号
        self.__preorder_train_seat_name__ = None                 #座位类型名
        self.__preorder_train_secret_str__ = None                #构造请求使用
        self.__preorder_train_seat_type__ = None                 #座位类型编码
        
        self.__key_check_isChange__ = ''
        self.__REPEAT_SUBMIT_TOKEN__ = ''

        
        #构造订单提交请求字符串  由乘客信息按一定规则编码
        self.__passenger_ticket_str__ = ''
        self.__old_passenger_str__ = ''        


        # 创建一个网络请求session实现登录验证  由于selenium不能发送post请求，所以登录都通过这个实现
        self.session = requests.session()     
        
        #self.proxies = get_proxies()
        #self.session.get('https://kyfw.12306.cn/otn/resources/login.html', headers=self.headers)

        #抢票成功 邮件通知
        self.__email__ = Email()
        self.receiver = '18151521911@163.com'
        
        #设置请求超时限制  requests 在经过以 timeout 参数设定的秒数时间之后停止等待响应  
        #设置了超时会抛出异常  如果过不指定，当断网时，可能一直等待状态
        self.__timeout__ = 4   #如果为None 不指定

        #余票查询最小刷新时间、
        self.__min_refresh_tine__ = 0.05




    def set_login_user_info(self, name, pwd):
        '''
        设置登录用户信息
        :param name:登录用户名
        :param pwd: 登录用户密码
        :return: 无
        '''
        self.username = name
        self.userpwd = pwd

    def set_passenger_info(self,passengers,passenger_cards,sexs):
        '''
        设置需要购票的乘客信息
        注意：最好把乘客信息先加入该登陆账户的常用联系人  不然可能会购票失败
             如果是特殊人群，必须闲在系统录入信息   学生、儿童等
             如果是成人，则可以不用先在系统录入
        :param passengers: list  乘客名 特殊人群： 杨德龙(学生)   黄旭(残军)  小明(儿童)
        :param passenger_cards:list 乘客名对应的证件号
        :param sexs:list 性别
        :return:无
        '''
        # 判断是不是Iterable对象
        if isinstance(passengers, Iterable) and isinstance(passenger_cards, Iterable) and isinstance(sexs, Iterable):
            self.passengers = passengers
            self.passenger_cards = passenger_cards
            self.sexs = sexs

    def set_from_to_info(self,dtime,from_station,to_station,from_station_code,to_station_code,is_student=False):
        '''
        设置购票信息  出发日期  起始地 目的地
        :param dtime: 出发日期
        :param from_station: 起始地
        :param to_station: 目的地
        :param from_station_code: 起始地编码
        :param to_station_code: 目的地编码
        :param is_student: 是学生么？如果是默认查找学生票
        :return: 如果起始地点有误，返回1
                 目的地有误，返回2
                 正确，返回0
        '''
        # 通过输入的地点，获取到地点-code
        '''if  not  Stations.__contains__(from_station):
            return 1;
        if  not  Stations.__contains__(to_station):
            return 2;'''
        self.dtime = dtime
        self.from_station = from_station
        self.to_station = to_station
        self.__from_station_code__ = from_station_code
        self.__to_station_code__ = to_station_code

        purpose_codes = 'ADULT'
        if is_student:
            purpose_codes = '0X00'
            self.__is_student__ = True
        else:
            self.__is_student__ = False
        # 余票查询地址可能是变动的  如果查询学生票，需要把purpose_codes改为0X00  学生票一般早放几天，在预订的时候也要一个字段需要修改成这个值
        self.__query_tickets_urla__ = 'https://kyfw.12306.cn/otn/leftTicket/query{}?leftTicketDTO.train_date={}&leftTicketDTO.from_station={}&leftTicketDTO.to_station={}&purpose_codes={}'.format(
            'A', self.dtime, self.__from_station_code__, self.__to_station_code__,purpose_codes)

        self.__query_tickets_urlz__ = 'https://kyfw.12306.cn/otn/leftTicket/query{}?leftTicketDTO.train_date={}&leftTicketDTO.from_station={}&leftTicketDTO.to_station={}&purpose_codes={}'.format(
            'Z', self.dtime, self.__from_station_code__, self.__to_station_code__,purpose_codes)

        # 当前使用的
        self.__query_tickets_url__ = self.__query_tickets_urlz__

        return 0;


    def set_train_numbers(self,numbers):
        '''
        设置想要预订的车次
        :param numbers:  list 车次集合
        :return:无
        '''
        #判断是不是Iterable对象
        if isinstance(numbers,Iterable):
            self.numbers = numbers

    def set_train_time_quantum(self, start_time,end_time):
        '''
        设置想要预订的车次出发时间段
        :param start_time:  最小时间
        :param end_time:  最大时间
        :return:无
        '''
        # 判断是不是Iterable对象
        if start_time is not None:
            self.start_time = start_time
        if end_time  is not None:
            self.end_time = end_time

    def set_train_type_names(self,train_type_names=['D','G']):
        '''
        设置预订的车次类型
        :param train_type_names:list 所买列车类型  G高铁/城际  D-动车 Z-直达 T-特快  K-快速  如果是None：所有
        :return: 无
        '''
        if isinstance(train_type_names, Iterable):
            self.train_type_names = train_type_names

    def set_seat_type_names(self,seat_type_names = ['二等座','硬座','硬卧','软卧']):
        '''
        设置预订的座位类型
        :param seat_type_names:list 想买的座位类型 如果是None：表示随意
        :return: 无
        '''
        if isinstance(seat_type_names, Iterable):
            self.seat_type_names = seat_type_names


    def __init_dict__(self):
        '''
        初始化参数
        :return:
        '''

        '''
        加载cdn_ip
        从http://ping.chinaz.com/kyfw.12306.cn获取cnd ip并保存在文件中
        '''
        with open('./conf/cdn_ips.txt','r') as f:
            ips =  f.readlines()
        self.__cdn_ips__ = ips

        # 加载代理
        self.__valid_proxies__ = load_valid_proxies()

        #票种
        self.ticket_ype_dict = {
                 '成人票':'1',
                 '儿童票':'2',
                 '学生票':'3',
                 '伤残军人票':'4'}
        
        #self.ticket_type: {adult: "1", child: "2", student: "3", disability: "4"},
        self.ticket_type_name_dict = {'1': '成人票',
                                 '2': '孩票',
                                 '3': '学生票',
                                 '4': '伤残军人票'}
        
        self.tour_flag_dict = {'dc': 'dc',
                          'wc': 'wc',
                          'fc': 'fc',
                          'gc': 'gc',
                          'lc': 'lc',
                          'lc1': 'l1',
                          'lc2': 'l2'}
        
        #证件类型
        self.passenger_card_type_dict = { 'two': "1", 
                                'one': "2", 
                                'tmp': "3", 
                                'passport': "B", 
                                'work': "H", 
                                'hongkong_macau': "C",
                                'taiwan': "G"}
        
        self.request_flag_dict= {'isAsync': "1"}
        self.ticket_query_flag_dict = {'query_commom': "00", 'query_student': "0X00"}
        self.special_areas_dict = {'lso': "LSO", 'dao': "DAO", 'ado': "ADO", 'nqo': "NQO", 'tho': "THO"}

        
        #座位类型
        self.seat_type_dict = {'商务座特等座':'9',
                          '一等座':'M',
                          '棚改座':'0',
                          '二等座':'O',
                          '高级软卧':'6',
                          '软卧':'4',
                          '动卧':'F',
                          '硬卧':'3',
                          '软座':'2',
                          '硬座':'1',
                          '无座':'1',
                          '其他':'1'}

        #伪装不同的浏览器
        self.user_agent_list = [
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/22.0.1207.1 Safari/537.1",
            "Mozilla/5.0 (X11; CrOS i686 2268.111.0) AppleWebKit/536.11 (KHTML, like Gecko) Chrome/20.0.1132.57 Safari/536.11",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.6 (KHTML, like Gecko) Chrome/20.0.1092.0 Safari/536.6",
            "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.6 (KHTML, like Gecko) Chrome/20.0.1090.0 Safari/536.6",
            "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/19.77.34.5 Safari/537.1",
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.9 Safari/536.5",
            "Mozilla/5.0 (Windows NT 6.0) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.36 Safari/536.5",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1063.0 Safari/536.3",
            "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1063.0 Safari/536.3",
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_0) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1063.0 Safari/536.3",
            "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1062.0 Safari/536.3",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1062.0 Safari/536.3",
            "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1061.1 Safari/536.3",
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1061.1 Safari/536.3",
            "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1061.1 Safari/536.3",
            "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1061.0 Safari/536.3",
            "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.24 (KHTML, like Gecko) Chrome/19.0.1055.1 Safari/535.24",
            "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/535.24 (KHTML, like Gecko) Chrome/19.0.1055.1 Safari/535.24",
            "Mozilla/5.0 (Macintosh; U; Mac OS X Mach-O; en-US; rv:2.0a) Gecko/20040614 Firefox/3.0.0 ",
            "Mozilla/5.0 (Macintosh; U; PPC Mac OS X 10.5; en-US; rv:1.9.0.3) Gecko/2008092414 Firefox/3.0.3",
            "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.5; en-US; rv:1.9.1) Gecko/20090624 Firefox/3.5",
            "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.6; en-US; rv:1.9.2.14) Gecko/20110218 AlexaToolbar/alxf-2.0 Firefox/3.6.14",
            "Mozilla/5.0 (Macintosh; U; PPC Mac OS X 10.5; en-US; rv:1.9.2.15) Gecko/20110303 Firefox/3.6.15",            
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.6; rv:2.0.1) Gecko/20100101 Firefox/4.0.1",
            "Opera/9.80 (Windows NT 6.1; U; en) Presto/2.8.131 Version/11.11",
            "Opera/9.80 (Android 2.3.4; Linux; Opera mobi/adr-1107051709; U; zh-cn) Presto/2.8.149 Version/11.10",
            "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/531.21.8 (KHTML, like Gecko) Version/4.0.4 Safari/531.21.10",
            "Mozilla/5.0 (Windows; U; Windows NT 5.2; en-US) AppleWebKit/533.17.8 (KHTML, like Gecko) Version/5.0.1 Safari/533.17.8",
            "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/533.19.4 (KHTML, like Gecko) Version/5.0.2 Safari/533.18.5",
            'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.2; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0)',
            'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; Media Center PC 6.0; InfoPath.2; MS-RTC LM 8)',
            'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; InfoPath.2)',
            'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0 Zune 3.0)',
            'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; MS-RTC LM 8)',
            'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; InfoPath.3)',
            'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; InfoPath.2; MS-RTC LM 8)',
            'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET CLR 4.0.20402; MS-RTC LM 8)',
            'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET CLR 1.1.4322; InfoPath.2)',
            'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729)',
            'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Win64; x64; Trident/4.0; .NET CLR 2.0.50727; SLCC2; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; Tablet PC 2.0)',
            'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Win64; x64; Trident/4.0; .NET CLR 2.0.50727; SLCC2; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET CLR 3.0.04506; Media Center PC 5.0; SLCC1)',
            'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Win64; x64; Trident/4.0; .NET CLR 2.0.50727; SLCC2; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0)',
            'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Win64; x64; Trident/4.0)',
            'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; Tablet PC 2.0; .NET CLR 3.0.04506; Media Center PC 5.0; SLCC1)',
            'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; FDM; Tablet PC 2.0; .NET CLR 4.0.20506; OfficeLiveConnector.1.4; OfficeLivePatch.1.3)',
            'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET CLR 3.0.04506; Media Center PC 5.0; SLCC1; Tablet PC 2.0)',
            'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET CLR 1.1.4322; InfoPath.2)',
            'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.3029; Media Center PC 6.0; Tablet PC 2.0)',
            'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0; SLCC2)',
            'Mozilla/4.0 (compatible; MSIE 7.0b; Windows NT 6.0)',
            'Mozilla/4.0 (compatible; MSIE 7.0b; Windows NT 5.2; .NET CLR 1.1.4322; .NET CLR 2.0.50727; InfoPath.2; .NET CLR 3.0.04506.30)',
            'Mozilla/4.0 (compatible; MSIE 7.0b; Windows NT 5.1; Media Center PC 3.0; .NET CLR 1.0.3705; .NET CLR 1.1.4322; .NET CLR 2.0.50727; InfoPath.1)',
            'Mozilla/4.0 (compatible; MSIE 7.0b; Windows NT 5.1; FDM; .NET CLR 1.1.4322)',
            'Mozilla/4.0 (compatible; MSIE 7.0b; Windows NT 5.1; .NET CLR 1.1.4322; InfoPath.1; .NET CLR 2.0.50727)',
            'Mozilla/4.0 (compatible; MSIE 7.0b; Windows NT 5.1; .NET CLR 1.1.4322; InfoPath.1)',
            'Mozilla/4.0 (compatible; MSIE 7.0b; Windows NT 5.1; .NET CLR 1.1.4322; Alexa Toolbar; .NET CLR 2.0.50727)',
            'Mozilla/4.0 (compatible; MSIE 7.0b; Windows NT 5.1; .NET CLR 1.1.4322; Alexa Toolbar)',
            'Mozilla/4.0 (compatible; MSIE 7.0b; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)',
            'Mozilla/4.0 (compatible; MSIE 7.0b; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.40607)',
            'Mozilla/4.0 (compatible; MSIE 7.0b; Windows NT 5.1; .NET CLR 1.1.4322)',
            'Mozilla/4.0 (compatible; MSIE 7.0b; Windows NT 5.1; .NET CLR 1.0.3705; Media Center PC 3.1; Alexa Toolbar; .NET CLR 1.1.4322; .NET CLR 2.0.50727)',
            'Mozilla/5.0 (Windows; U; MSIE 7.0; Windows NT 6.0; en-US)',
            'Mozilla/5.0 (Windows; U; MSIE 7.0; Windows NT 6.0; el-GR)',
            'Mozilla/5.0 (MSIE 7.0; Macintosh; U; SunOS; X11; gu; SV1; InfoPath.2; .NET CLR 3.0.04506.30; .NET CLR 3.0.04506.648)',
            'Mozilla/5.0 (compatible; MSIE 7.0; Windows NT 6.0; WOW64; SLCC1; .NET CLR 2.0.50727; Media Center PC 5.0; c .NET CLR 3.0.04506; .NET CLR 3.5.30707; InfoPath.1; el-GR)',
            'Mozilla/5.0 (compatible; MSIE 7.0; Windows NT 6.0; SLCC1; .NET CLR 2.0.50727; Media Center PC 5.0; c .NET CLR 3.0.04506; .NET CLR 3.5.30707; InfoPath.1; el-GR)',
            'Mozilla/5.0 (compatible; MSIE 7.0; Windows NT 6.0; fr-FR)',
            'Mozilla/5.0 (compatible; MSIE 7.0; Windows NT 6.0; en-US)',
            'Mozilla/5.0 (compatible; MSIE 7.0; Windows NT 5.2; WOW64; .NET CLR 2.0.50727)',
            'Mozilla/4.79 [en] (compatible; MSIE 7.0; Windows NT 5.0; .NET CLR 2.0.50727; InfoPath.2; .NET CLR 1.1.4322; .NET CLR 3.0.04506.30; .NET CLR 3.0.04506.648)',
            'Mozilla/4.0 (Windows; MSIE 7.0; Windows NT 5.1; SV1; .NET CLR 2.0.50727)',
            'Mozilla/4.0 (Mozilla/4.0; MSIE 7.0; Windows NT 5.1; FDM; SV1; .NET CLR 3.0.04506.30)',
            'Mozilla/4.0 (Mozilla/4.0; MSIE 7.0; Windows NT 5.1; FDM; SV1)',
            'Mozilla/4.0 (compatible;MSIE 7.0;Windows NT 6.0)',
            'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0)',
            'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0;)',
            'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; YPC 3.2.0; SLCC1; .NET CLR 2.0.50727; Media Center PC 5.0; InfoPath.2; .NET CLR 3.5.30729; .NET CLR 3.0.30618)',
            'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; YPC 3.2.0; SLCC1; .NET CLR 2.0.50727; .NET CLR 3.0.04506)',
            'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; WOW64; SLCC1; Media Center PC 5.0; .NET CLR 2.0.50727)',
            'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; WOW64; SLCC1; .NET CLR 3.0.04506)',
            'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; WOW64; SLCC1; .NET CLR 2.0.50727; Media Center PC 5.0; InfoPath.2; .NET CLR 3.5.30729; .NET CLR 3.0.30618; .NET CLR 1.1.4322)',
            'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36'
        ]


        self.headers = {
            'Accept-Encoding': 'gzip, deflate, br',
            'Accept-Language': 'zh-CN,zh;q=0.8',
            'Cache - Control': 'no-cache',
            'X-Requested-With': 'XMLHttpRequest',
            'Host': 'kyfw.12306.cn',
            'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'}


    def __get_random_header__(self,connection = True):
        '''
        分享12306抢票心得-终极秒杀思路篇：https://www.cnblogs.com/guozili/p/6144561.html
        http://www.cnblogs.com/dudu/archive/2012/07/18/webrequest_dns.html
        这里为什么指定Host：这个技术，不用修改hosts，使用requests能映射不同的IP 在访问12306网址时，不再使用https://kyfw.12306.cn  而是通过https://ip地址 并指定Host来访问 因为存在CDN缓存问题
        生成随机请求头，用于模拟浏览器
        每次登陆时采用不同的模拟器
        :param connection:是否关闭多余连接，即请求一次就关闭连接，如果不保持会话，就设置为False，如果想保持登录状态，就设置为True
        :return:headers
        '''
        if connection == True:
            value = 'keep-alive'
        else:
            value = 'close'
        user_agent = random.choice(self.user_agent_list)
        headers = {
            'Host': 'kyfw.12306.cn',
            'User-Agent': user_agent,
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Accept-Language': 'zh-CN,zh;q=0.8',
            'Accept-Encoding': 'gzip, deflate, br',
            'If - Modified - Since': '0',
            'Cache - Control': 'no-cache',
            'X-Requested-With': 'XMLHttpRequest',
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'Connection': value
            }
        return headers

    def __get_random_cdn_ip__(self):
        '''
        获取随机的目标主机CDN ip
        :return:
        '''
        cdn_ip = random.choice(self.__cdn_ips__)
        return cdn_ip

    def __get_random_proxies__(self):
        '''
        获取随机的目标主机CDN ip
        :return:
        '''
        proxies = random.choice(self.__valid_proxies__)
        return proxies




    def __make_passengerTicketStr(self,passenger,passenger_card):
        '''
        passengerTicketStr 是以下划线"_"分隔当每一个乘客信息组成的字符串，对应每个乘客信息字符串组成如下：
            第一位：座椅类型  0：二等座 1： 2：  3：硬卧  4：软卧  1：硬座 ....             
            第二位：0
            第三位：票种 成年人：1  2：儿童票  学生票为：3  4：残军票
            第四位：乘客名
            第五位: 证件类型  证件类型指的是二代身份证(1)，学生证(1)，签证等的编码.
            第六位：证件号
            第七位：手机号码
            第八位：保存常用联系人(Y或N)
            多个人中间使用_隔开    
            如:            
            1,0,1,刘燕,1,511523199708177148,,N_1,0,3,杨德亮,1,500228199304130311,,N
            1,0,1,刘燕,1,511523199708177148,,N_1,0,3,杨德亮,1,500228199304130311,,N_1,0,1,郑洋,1,340621199411082033,,N
            3,0,1,郑洋,1,340621199411082033,,N_4,0,1,刘燕,1,511523199708177148,,N_1,0,3,杨德亮,1,500228199304130311,,N
            3,0,2,郑洋,1,340621199411082033,,N_4,0,4,刘燕,1,511523199708177148,,N_1,0,3,杨德亮,1,500228199304130311,,N_1,0,1,马资阳,1,340621199408152037,,N
            
            座位编号是指是什么座位类型，硬座，软座...
            票类型指的是，成人票，学生票等的编码
            证件类型指的是二代身份证，学生证，签证等的编码.
        '''
        seat_type = self.__preorder_train_seat_type__
        if '(学生)' in passenger:
            passenger = re.findall(r'^([\u4e00-\u9fa5]+)', passenger)[0]
            ticket_ype = self.ticket_ype_dict['学生票']
        elif '(儿童)' in passenger:
            passenger = re.findall(r'^([\u4e00-\u9fa5]+)', passenger)[0]
            ticket_ype = self.ticket_ype_dict['儿童票']
        elif '(残' in passenger:
            passenger = re.findall(r'^([\u4e00-\u9fa5]+)', passenger)[0]
            ticket_ype = self.ticket_ype_dict['伤残军人票']
        else:
            ticket_ype = self.ticket_ype_dict['成人票']
        passenger_card_type = '1'        
        phone_num = ''
        save = 'N'
        
        return  '{0},{1},{2},{3},{4},{5},{6},{7}'.format(seat_type,
                                                         '0',
                                                         ticket_ype,
                                                         passenger,
                                                         passenger_card_type,
                                                         passenger_card,
                                                         phone_num,
                                                         save)
        
            
    def __make_oldPassengersStr(self,passenger,passenger_card):
        '''
        oldPassengersStr 也是以下划线"_"分隔每个乘客信息组成的字符串，对应每个乘客信息字符串组成如下：            
            第一位：乘客名
            第二位：证件类型  证件类型指的是二代身份证(1)，学生证(1)，签证等的编码.
            第三位：证件类型
            第四位：票种 成年人：1  2：儿童票  学生票为：3  4：残军票            
                
            如：            
            刘燕,1,511523199708177148,1_杨德亮,1,500228199304130311,3_
            刘燕,1,511523199708177148,1_杨德亮,1,500228199304130311,3_郑洋,1,340621199411082033,1_
            郑洋,1,340621199411082033,1_刘燕,1,511523199708177148,1_马资阳,1,340621199408152037,1_杨德亮,1,500228199304130311,3_            
        '''         
        passenger_card_type = '1'
        if '(学生)' in passenger:
            passenger = re.findall(r'^([\u4e00-\u9fa5]+)', passenger)[0]
            ticket_ype = self.ticket_ype_dict['学生票']
        elif '(儿童)' in passenger:
            passenger = re.findall(r'^([\u4e00-\u9fa5]+)', passenger)[0]
            ticket_ype = self.ticket_ype_dict['儿童票']
        elif '(残军' in passenger:
            passenger = re.findall(r'^([\u4e00-\u9fa5]+)', passenger)[0]
            ticket_ype = self.ticket_ype_dict['伤残军人票']
        else:
            ticket_ype = self.ticket_ype_dict['成人票']
        return  '{0},{1},{2},{3}_'.format(passenger,
                                         passenger_card_type,
                                         passenger_card,
                                         ticket_ype)
    

    def __dic_to_table__(self,dic):
        '''
        把dic以表格显示
        '''
        #创建表头
        table = PrettyTable()
        header = []
        values_row = []        
        for key,val in dic.items():            
            header.append(key)
            values_row.append(val)
        table._set_field_names(header)
        #插入数据
        table.add_row(values_row)    
        return table
            
            
    def __xml_to_dict__(self,xml_data):
        """
        xml转换为字典  失败返回None
            param xml_data:
        
        """
        soup = BeautifulSoup(xml_data, features='xml')
        xml = soup.find('HashMap')
        if not xml:
            return None
        # 将 XML 数据转化为 Dict
        data = dict([(item.name, item.text) for item in xml.find_all()])
        if len(data) != 0:
           return data
        else:
            data = None

    '''
    **********************************************************************************************
    点击12306登录页面   登录
    *********************************************************************************************
    '''

    def __get_img__(self):
        '''        
        在你登录12306网站的时候，网页会get一个验证码图片，这个步骤封装方法如下：
        返回下载的验证码路径，失败返回None
        '''
        #这是12306登陆第一个请求 获取一个随机请求头
        self.headers = self.__get_random_header__()

        #登录验证码  get
        code_url =  "https://kyfw.12306.cn/passport/captcha/captcha-image?login_site=E&module=login&rand=sjrand&{}".format(random.random())
        
        #获取验证码图片
        #self.browser.get(self.code_url)
        response = self.session.get(code_url,headers=self.headers,verify=False,timeout = self.__timeout__)
        if (response is not None) and (response.status_code == requests.codes.ok):
            sleep(0.3)
            img_path = './img.jpg'
            #把验证码保存到本地
            with open(img_path,'wb') as f:
                f.write(response.content)

            #验证码下载成功
            if os.path.exists(img_path):
                return img_path
            else:
                return None
        else:
            return None

        #=======================================================================
        # 根据打开的图片识别验证码后手动输入，输入正确验证码对应的位置，例如：2,5
        # ---------------------------------------
        #         |         |         |
        #    0    |    1    |    2    |     3
        #         |         |         |
        # ---------------------------------------
        #         |         |         |
        #    4    |    5    |    6    |     7
        #         |         |         |
        # ---------------------------------------
        #=======================================================================
        
    
    def __read_input__(self,caption,timeout=10):
        '''
        输入超时
        '''
        #用于保持输入的值
        context = {'input':'[1,2,3]'}
        def  thread_input(context):
            '''
            可以设置超时
            caption：标题
            default ：默认值
            '''
            context['input'] = input(caption)          
            
        #如果线程daemon属性为True， 则join里的timeout参数是有效的， 主线程会等待timeout时间后，结束子线程。
        t = threading.Thread(target=thread_input ,args =(context,) )
        t.setDaemon(False)
        t.start( )
        t.join(timeout) #等待10秒
        return context['input']

    def __manual_verify_img__(self,rwa_input):
        '''
        手动输入验证码并开始校验
        成功返回True 失败返回False
        args：
            rwa_input：选中的图片如1234
                 元素范围从1-8
        '''
        # 匹配'1,2,3,4,5'这种格式的字符串
        # print(rwa_input)
        res = re.match(r'^\d{1,8}$', rwa_input)
        if res is None:
        #输入格式不对
            return False
        # 转换成坐标
        #code = ['35,35', '105,35', '175,35', '245,35', '35,105', '105,105', '175,105', '245,105']
        #code = ['40,40','110,40', '180,40', '260,40','40,110', '100,110','180,110', '260,110']
        code =  ['38,48','111,46','190,44','256,45','33,110','109,116','179,116','266,125']
        verify_list = []
        for s in rwa_input:
            a = int(s)
            if a > 8:
                a = 8
            verify_list.append(code[a - 1])

        self.answer = ','.join(verify_list)
        data = {
            'answer': self.answer,  # 验证码对应的坐标，两个为一组，跟选择顺序有关，有几个正确的，输入几个
            'login_site': 'E',  # 固定的
            'rand': 'sjrand'  # 固定的
        }

        # 校验验证码页面  post
        check_code_url = "https://kyfw.12306.cn/passport/captcha/captcha-check"

        # selenium不能发送post请求，所以使用requsts库
        response = self.session.post(url=check_code_url, data=data, headers=self.headers, verify=False)

        # self.browser.get(url=url)
        if (response is not None) and (response.status_code == requests.codes.ok):
            # 返回的是json字节
            # Gets the source of the current page.  json字符串解码成python对象
            # dic = json.loads(self.browser.page_source)
            # print(response.content)
            try:
                dic = json.loads(response.content)
            except Exception as e:
                xml = response.content.decode()
                dic = self.__xml_to_dict__(xml)

            if isinstance(dic,dict):
                if global_output_log_flag:
                    print('验证码校验返回消息:\n', self.__dic_to_table__(dic))
                result_code =  str(dic['result_code'])
                if str(result_code) == '4':
                    return True
                else:
                    return False
            else:
                return False
        else:  # 5失败  7过期
            return False

    def __auto_recognize_img__(self,img_path):
        '''
        自动识别验证码
        返回识别到的验证码 如1,2   识别识别返回None
        '''
        # 超时2s 退出
        #with eventlet.Timeout(0.5, False):
        if global_output_log_flag:
            print('自动识别中...')
        rec = ImageRecognition()
        captcha_solution = rec.get_result(img_path)
        # 识别失败
        if captcha_solution is None:
            self.__recognize_count += 1
            if global_output_log_flag:
                print('第{0}次自动识别失败...'.format(self.__recognize_count))
            return None
        # 识别成功
        else:
            self.__recognize_count = 0
            return captcha_solution

    def  __login_12306__(self):
        '''
        用户信息验证 登录

        密码正确返回0,密码错误返回1，用户不存在返回2  3：未知错误
        '''

        data = {
            'username':self.username,
            'password':self.userpwd,
            'appid':'otn',
            'answer':self.answer
            }   
        
        
        #校验用户信息页面  post
        check_user_url = "https://kyfw.12306.cn/passport/web/login"

        response = self.session.post(url=check_user_url,data=data,headers=self.headers,verify=False )

        #保存cookies
        if (response is not None) and (response.status_code == requests.codes.ok):
            #返回的xml字节
            #Gets the source of the current page.  json字符串解码成python对象
            #dic = json.loads(self.browser.page_source)
            try:
                dic = json.loads(response.content)
            except Exception as e:
                xml = response.content.decode()    
                #print(response.content)
                dic = self.__xml_to_dict__(xml)
                print('密码校验：{0}!'.format(dic))

            if isinstance(dic,dict):
                if global_output_log_flag:
                    print('密码校验返回结果：\n', self.__dic_to_table__(dic))
                result_code = str(dic['result_code'])
                #登录成功
                if  result_code == '0':
                    return 0
                else:
                    if '登录名不存在' in dic['result_message']:
                        return 2
                    else:
                        return 1
            else:
                if global_output_log_flag:
                    print(response.content)
                return 3
        else:
            return 3
        
    def __auth_uamtk__(self):
        '''
        根据登录返回的umatk,得到newapptk      失败返回None
        '''
        url = 'https://kyfw.12306.cn/passport/web/auth/uamtk'
        data = {
            'appid':'otn',
            '_json_att':''
        }            
        response = self.session.post(url=url,data=data,headers=self.headers,verify=False )

        if (response is not None) and (response.status_code == requests.codes.ok):
            try:
                dic = json.loads(response.content)
            except Exception as e:
                print('获取newapptk：{0}!'.format(e))
                xml = response.content.decode()
                dic = self.__xml_to_dict__(xml)

            if isinstance(dic,dict):
                if global_output_log_flag:
                    print('获取newapptk：\n',self.__dic_to_table__(dic))
                #验证通过
                if str(dic['result_code']) == '0':
                    newapptk = dic['newapptk']
                    return newapptk
                else:
                    return None
            else:
                return None
        else:
            return None

    

    def __check_uamauthclient__(self,tk):
        '''
        客户端验证  成功返回True 失败返回False
        '''
        url = 'https://kyfw.12306.cn/otn/uamauthclient'
        data = {
            'tk':tk,
             '_json_att':''
        }            
        response = self.session.post(url=url,data=data,headers=self.headers,verify=False )

        if (response is not None) and (response.status_code == requests.codes.ok):
            try:
                dic = json.loads(response.content)
            except Exception as e:
                print('客户端验证：{0}!'.format(e))
                xml = response.content.decode()
                dic = self.__xml_to_dict__(xml)

            if isinstance(dic, dict):
                if global_output_log_flag:
                    print('客户端验证：\n', self.__dic_to_table__(dic))
                # 验证通过
                if str(dic['result_code'] )== '0':
                    return True
                else:
                    return False
            else:
                return False
        else:
            return False


    def __passenger_add__(self,passenger,passenger_card,sex):
        '''
        追加乘客到常用联系人  sex：男：M  女:'W'
        成功返回True 失败返回False
        '''       
        print('开始追加乘客{0}信息到常用联系人'.format(passenger))
        data = {'passenger_name':passenger,          #姓名
                'sex_code':sex,                      #性别
                'passenger_id_no':passenger_card,    #证件号码
                'mobile_no':'',                      #手机号码（+86）   
                'email':'',                          #电子邮箱
                'address':'',                        #地址   
                'postalcode':'',                     #邮编
                'studentInfoDTO.school_code':'',     #学校所在省份：   
                'studentInfoDTO.school_name':'',     #学校名称 
                'studentInfoDTO.department':'',      #院系 
                'studentInfoDTO.school_class':'',    #班级
                'studentInfoDTO.student_no':'',      #学号
                'studentInfoDTO.preference_card_no':'',   #学制
                'GAT_valid_date_end':'2010-01-01',        #入学年份
                'GAT_born_date':'1990-01-01',
                'old_passenger_name':'',
                'country_code':'CN',
                '_birthDate':'2017-01-05',
                'old_passenger_id_type_code':'',
                'passenger_id_type_code':'1',
                'old_passenger_id_no':'',
                'passenger_type':'1',                #旅客类型
                'studentInfoDTO.province_code':'11',
                'studentInfoDTO.school_system':'1',
                'studentInfoDTO.enter_year':'2018',
                'studentInfoDTO.preference_from_station_name':'简码/汉字',
                'studentInfoDTO.preference_from_station_code':'',   #优惠区间 起始
                'studentInfoDTO.preference_to_station_name':'简码/汉字',
                'studentInfoDTO.preference_to_station_code':''      #优惠区间 终点
                }
        
        #常用联系人追加页面 如果联系人未添加到该登陆账户，无法购票
        passenger_add_url = 'https://kyfw.12306.cn/otn/passengers/add'
        
        response = self.session.post(passenger_add_url, data=data,headers=self.headers, verify=False )

        if (response is not None) and (response.status_code == requests.codes.ok):
            try:
                dic = json.loads(response.content)
            except Exception as e:
                print('追加乘客信息到常用联系人:{0}!'.format(e))
                xml = response.content.decode()
                dic = self.__xml_to_dict__(xml)

            if isinstance(dic,dict):
                if dic['data']['flag'] == True:
                    if global_output_log_flag:
                        print("乘客信息追加成功!")
                    return True
                else:
                    if global_output_log_flag:
                        print("乘客信息追加失败!")
                    return False
            else:
                return False
        else:
            return False

    def __get_common_passenger_info__(self):
        '''
        获取常用联系人信息
        :return: 成功返回一个PrettyTable对象 保存常用联系人信息
                 失败返回None
        '''
        if global_output_log_flag:
            print('常用联系人查询.....')

        index = 1
        page_total = 1
        '''创建表头 并显示常用联系人'''
        table = PrettyTable()
        header = {'code': '序号',  # 0
                  'passenger_name': '姓名',
                  'sex_code': '性别',  # 2
                  'passenger_id_type_name': '证件名',
                  'passenger_id_no': '证件号',  # 4
                  'passenger_type_name': '乘客类型',  # 5
                  'mobile_no': '手机号码',
                  'email': '邮箱'}
        table._set_field_names(header.values())

        while(index <= page_total):
            data = {
                'pageIndex': str(index),
                'pageSize': '10'
            }
            # 查找该账户常用联系人
            passenger_query_url = 'https://kyfw.12306.cn/otn/passengers/query'
            response = self.session.post(passenger_query_url, data=data, headers=self.headers, verify=False)
            index += 1

            if (response is not None) and (response.status_code == requests.codes.ok):
                try:
                    dic = json.loads(response.content)
                except Exception as e:
                    print('常用联系人查询:{0}!'.format(e))
                    xml = response.content.decode()
                    dic = self.__xml_to_dict__(xml)

                if isinstance(dic,dict):
                    # print(self.__dic_to_table__(dic))
                    if dic['status'] == True:
                        # 保存所有常用联系人信息
                        passengers_info = dic['data']['datas']
                        #获取总页数
                        page_total = int(dic['data']['pageTotal'])
                        for passenger_info in passengers_info:
                            values_row = []
                            for key in header.keys():
                                values_row.append(passenger_info[key])
                            # 插入数据
                            table.add_row(values_row)
                    else:
                        return None
                else:
                    return None
        return table

    
    '''
    **********************************************************************************************
    加载余票查询页面
    *********************************************************************************************
    '''
    def __init_buy_page__(self):
        '''
        登录进入后，加载购票页面
        '''
        #登陆成功后 余票查询
        ticket_url = "https://kyfw.12306.cn/otn/leftTicket/init"
        if global_output_log_flag:
            print("购票页面初始化中....")
        response = self.session.get(ticket_url,headers=self.headers)
        if (response is not None) and (response.status_code == requests.codes.ok):
            #print("购票页面初始化成功!")
            pass

    
    def __date_check__(self):
        '''
        检查余票预订时间是否在6-23点之间
        返回距离可以抢票的时间差  如果可以抢票：返回0  不可以：返回秒数
        '''
        d0 = datetime.datetime.now()    #datetime
        year = d0.year
        month = str(d0.month).zfill(2)   #前导0
        day = str(d0.day).zfill(2)       #前导0
        
        #当前时间
        s0 = d0.strftime("%Y-%m-%d %H:%M:%S")    #str
        
        #第二天6点
        s1 = '{}-{}-{} 06:00:00'.format(year,month,day)       
        d1 = datetime.datetime.strptime(s1, "%Y-%m-%d %H:%M:%S")    #datetime
        d1 = d1 + datetime.timedelta(days=1)                        #日期+1
        #d1 = datetime.datetime.strptime(s1, "%Y-%m-%d %H:%M:%S")    #datetime
        s1 = d1.strftime("%Y-%m-%d %H:%M:%S")    #str
        
        #当天6点
        s2 = '{}-{}-{} 06:00:00'.format(year,month,day)
        d2 = datetime.datetime.strptime(s2, "%Y-%m-%d %H:%M:%S")    
        
        #当天23点
        s3 = '{}-{}-{} 23:00:00'.format(year,month,day)
        
        
        if s0 < s2:   #当天6点之前  [)
            rest = d2 - d0
            rest = rest.seconds
        elif s0 <= s3:     #6-23点之间 []
            rest = 0
        else:                    #23点之后s0 > d3
            rest = d1 - d0
            rest = rest.seconds    
        return rest

    def __rest_time__(self):
        '''
        计算当前时间在整点附近时间差  比如当前时间05:59:35 距离6点为25秒
        如果不在时间60s时间差之内，返回100
        '''
        d0 = datetime.datetime.now()  # datetime
        year = d0.year
        month = str(d0.month).zfill(2)  # 前导0
        day = str(d0.day).zfill(2)  # 前导0
        hour = str(d0.hour).zfill(2)  # 前导0

        '''
        往后推
        '''
        # 当前时间
        s0 = d0.strftime("%Y-%m-%d %H:%M:%S")  # str

        # 当天hh:59:00
        s1 = '{}-{}-{} {}:59:00'.format(year, month, day, hour)
        d1 = datetime.datetime.strptime(s1, "%Y-%m-%d %H:%M:%S")  # datetime

        # 当天hh+1:01:00点
        s2 = '{}-{}-{} {}:01:00'.format(year, month, day, hour)
        d2 = datetime.datetime.strptime(s2, "%Y-%m-%d %H:%M:%S")  # datetime
        d2 = d2 + datetime.timedelta(hours=1)
        s2 = d2.strftime("%Y-%m-%d %H:%M:%S")  # str

        s11 = '{}-{}-{} {}:00:00'.format(year, month, day, d2.hour)
        d11 = datetime.datetime.strptime(s11, "%Y-%m-%d %H:%M:%S")  # datetime

        '''
        往前推
        '''
        # 当天hh-1:59:00
        d3 = d1 - datetime.timedelta(hours=1)
        s3 = d3.strftime("%Y-%m-%d %H:%M:%S")  # str

        s33 = '{}-{}-{} {}:00:00'.format(year, month, day, hour)
        d33 = datetime.datetime.strptime(s33, "%Y-%m-%d %H:%M:%S")  # datetime

        # 当天hh:01:00点
        d4 = d2 - datetime.timedelta(hours=1)
        s4 = d4.strftime("%Y-%m-%d %H:%M:%S")  # str

        # 接近整点
        if s0 > s1 and s2 > s0:
            if s0 <= s11:
                return (d11 - d0).seconds
            else:
                return (d0 - d11).seconds
        elif s0 > s3 and s4 > s0:
            if s0 <= s33:
                return (d33 - d0).seconds
            else:
                return (d0 - d33).seconds
        else:
            return 100

    def __query_tickets__(self):
        '''
        查询余票信息  这里不使用登录状态去查询余票
        1  True:有指定车次、指定座位类型的余票  False:没有的话就需要循环继续查询
        2  附加消息 抢票失败的原因
        '''

        # print(self.__query_tickets_url__)
        msg = None
        '''有余票：判断当前时间是否可以抢票'''
        res = self.__date_check__()
        if res > (900 + 60):  # 距离开抢大于15分钟
            msg = '距离开抢时间还有{}分钟!：'.format(res // 60)
            if global_output_log_flag:
                print(msg)
            sleep(900)
            msg = '距离开抢时间还有{}分钟!：'.format(res // 60)
            return False, msg
        elif res > (60 + 30):  # 距离开抢大于1分钟
            msg = '距离开抢时间还有{}分钟!：'.format(self.__date_check__() // 60)
            if global_output_log_flag:
                print(msg)
            sleep(20)
            msg = '距离开抢时间还有{}分钟!：'.format(self.__date_check__() // 60)
            return False, msg
        elif res != 0:  # 1分钟以内  允许抢票
            msg = '距离开抢时间还有{}秒!：'.format(res)
            if global_output_log_flag:
                print(msg)
        else:
            msg = ''

        '''如果是整点附近 刷新时间调高  其他时间调低，因为整点容易卡死           必须等待，连续刷会被禁ip,如果1s刷新20次+可能被禁  刷新间隔要好好设定     '''
        '''
        res = self.__rest_time__()
        if res == 100:  # 不在下午4点和上午6点附近前后1分钟内，刷新可以慢些
            ran = random.randint(1, 10)  # 1,2,3,4,5,6,7
            # 1/2 的概率睡眠
            if ran <= 6:
                num = random.randint(0, 5) / 10.0 + self.__min_refresh_tine__
            # 3/10
            elif ran <= 9:
                num = random.randint(4, 6) / 10.0 +  self.__min_refresh_tine__
            # 1/10的概率睡眠 1s
            else:
                num = 0.6 + self.__min_refresh_tine__
        else:
            # 到整点容易卡死注意刷新时间设置
            num = random.randint(2, 4) / 10.0 + self.__min_refresh_tine__'''
        num = random.randint(0, 2) / 10.0 + self.__min_refresh_tine__
        sleep(num)

        self.__query_count__ += 1
        msg = msg + '第%d次刷新,' % self.__query_count__

        headers = self.__get_random_header__(connection=False)
        cdn_ip = self.__get_random_cdn_ip__()
        proxies = self.__get_random_proxies__()
        ip = cdn_ip.strip()
        try:
            url = self.__query_tickets_url__.replace('kyfw.12306.cn', ip)
            response = requests.get(url, headers = headers, verify=False, timeout = self.__timeout__/2,proxies = proxies)
        except Exception as e:
            #移除无效cdn ip
            #self.__cdn_ips__.remove(cdn_ip)
            #sleep(0.2)
            response = None
            msg = msg + '余票查询异常：{}'.format(e)
            print(msg)

        '''
        解析余票信息
        '''
        if (response is not None) and (response.status_code == requests.codes.ok):
            self.__query_exception_count__ = 0
            # print(response.content.decode())
            response.encoding = 'utf-8'
            # print(response.content)
            try:
                dic = response.json()
            except Exception as e:
                # 查询了当前还没放票日期的票 会返回一个html
                if '﻿<!DOCTYPE' in response.text:
                    return False, msg + '请检查你的购票日期，是否超出一个月之后....'
                else:
                    msg = msg + '查票异常{0},重新查询余票中...'.format(e)
                    print(msg)
                    return False, msg

            try:
                if isinstance(dic, dict):
                    # 得到查询到的列车原始数据
                    raw_trains = dic['data']['result']
                    # 输出所有车次信息 这里进行了车次的过滤：按照发车时间、车次类型
                    self.__trains_data_list__, self.__trains_data_filter_list__, self.__trains_table__ = TrainsDemo(
                        raw_trains, self.train_type_names, self.numbers, self.start_time,
                        self.end_time).get_trian_data()
                    if global_output_log_flag:
                        print(self.__trains_table__)
                else:
                    msg = msg  +  '查票异常，不是Json字符串,重新查询余票中...'
                    return False, msg

            except Exception as e:
                msg = msg + '查票结果{0},重新查询余票中...'.format(e)
                print(response.text)
                sleep(5)
                return False, msg

            '''
            判断余票，是否有符合指定车次，指定座位类型的余票？
            '''
            # 没有符合要求的车次，退出
            if self.__trains_data_list__ is None:
                msg = msg + '没有符合要求的车次.....'
                return False, msg
            # 有符合要求的车次
            else:
                # 是否有余票
                flag = False
                for train_info in self.__trains_data_filter_list__:
                    # 满足以下条件，说明有指定类型座位余票
                    for seat_type_name in self.seat_type_names:
                        # 有票?
                        if train_info[seat_type_name] not in ['*', '--', '无']:
                            flag = True
                            break
                    if flag:
                        break
                # 没有余票
                if not flag:
                    msg = msg + '系统没有满足你指定席次的余票了，持续刷新中...'
                    if global_output_log_flag:
                        print(msg)
                    return False, msg

            # 查票成功
            if dic['status'] == True:
                msg  = msg + '当前还有你指定席次的余票，开始预订...'
                return True, msg
            else:
                msg = msg + "查询车次出现异常:{0}".format(dic['messages'][0])
                if global_output_log_flag:
                    print(msg)
                return False, msg
        else:
            # response.text： {"c_name":"CLeftTicketUrl","c_url":"leftTicket/queryZ","status":false}
            if 'queryZ' in self.__query_tickets_url__:
                self.__query_tickets_url__ = self.__query_tickets_urlz__
            else:
                self.__query_tickets_url__ = self.__query_tickets_urla__

            if response is not None:
                msg = msg +  '状态码：{},{}!'.format(response.status_code,response.text)

            if (self.__query_exception_count__ + 1)% 3 == 0:
                if global_output_log_flag:
                    print(response.text)
                msg = msg + '你已经连续三次查票异常，你可能被12306禁止查票了,请尝试其他用户登录！'

            self.__query_exception_count__ += 1

            if global_output_log_flag:
                print(msg)

            return False, msg


    '''
    **********************************************************************************************
    有余票才可以预订
    *********************************************************************************************
    '''                 
    def __check_user__(self):
        '''
        点击预定后  阶段1：首先检查检查用户是否登录      长时间连接，连接可能断开
        成功 返回True 失败返回Fasle，False需要重新登录
        '''
        if global_output_log_flag:
            print("检查用户....")
        url = 'https://kyfw.12306.cn/otn/login/checkUser'
        data = {
                "_json_att": ""
        }
        response = self.session.post(url, data=data, headers=self.headers)
        # 返回一个验证通过信息
        if (response is not None) and (response.status_code == requests.codes.ok):
            #print(response.text)
            try:
                dic = json.loads(response.content)
            except Exception as e:
                #有时候会返回一个html 但是并不代表用户掉线，所以不用管
                print('检查用户异常:{0}!'.format(e))
                xml = response.content.decode()                
                dic = self.__xml_to_dict__(xml)

            if isinstance(dic,dict):
                if global_output_log_flag:
                    #{"validateMessagesShowId":"_validatorMessage","status":true,"httpstatus":200,"data":{"flag":true},"messages":[],"validateMessages":{}}   #成功登陆
                    #{"validateMessagesShowId":"_validatorMessage","status":true,"httpstatus":200,"data":{"flag":flag},"messages":[],"validateMessages":{}}  #没有登录 需要重新登录
                    print(self.__dic_to_table__(dic))
                #try:
                #用户已登录
                if dic['data']['flag'] == True:
                    return True
                else:   #用户过期
                   return False
                #except Exception as e:
                #    print('检查用户异常:{0}'.format(e))
            else:
                return False
        else:
            return False

            
    def __select_train__(self):
        '''
        根据查询到的余票信息进行车次选择 有满足条件的车次返回True 没有返回False
        '''
        # 清空数据
        self.__preorder_train_index__ = None
        self.__preorder_train_secret_str__ = None
        self.__preorder_train_seat_type__ = None
        self.__preorder_train_seat_name__ = None
        self.__passenger_ticket_str__ = ''
        self.__old_passenger_str__ = ''

        if self.__trains_data_list__ is not  None:
            secret_strs = [parse.unquote(train_data[0]) for train_data in self.__trains_data_list__]
            
            #secret_strs = [parse.unquote(self.__trains_data_list__[i][0]) for i in  range(len(self.__trains_data_list__))]            
                                     
            #遍历指定的座位类型
            for seat_type_name in self.seat_type_names:
                #遍历车次
                for index,train in enumerate(self.__trains_data_filter_list__):
                    train_flag_use = True
                    #车次过滤  
                    if self.numbers is not None:  
                        #查找指定车次
                        if train['trips'] not in self.numbers:
                            train_flag_use = False
                    
                    #也可以过滤时间
                                                
                    #判断用户是否想购买该车次
                    if train_flag_use:                                    
                        if train[seat_type_name] == '--':
                            if global_output_log_flag :
                                print('车次{0}无{1}出售，已结束当前刷票，清重新开启！'.format(train['trips'],seat_type_name))
                        elif train[seat_type_name] == '无':
                            if global_output_log_flag:
                                print('车次{0}{1}没有余票了，继续尝试....'.format(train['trips'],seat_type_name))                            
                        else:
                            if global_output_log_flag:
                                print('车次{0}{1}有余票了，开始预订....'.format(train['trips'],seat_type_name))
                            #保存将要预订的列车所在tickets_data中的索引位置以及解密字符串  
                            self.__preorder_train_index__ = index
                            self.__preorder_train_secret_str__ = secret_strs[self.__preorder_train_index__]
                
                            #选择的座位类型
                            self.__preorder_train_seat_type__ = self.seat_type_dict[seat_type_name] 
                            self.__preorder_train_seat_name__ = seat_type_name
                            
                            #确定两个很重要的数据 后面会用到  1,0,1,刘燕,1,511523199708177148,,N_1,0,3,杨德亮,1,500228199304130311,,N
                            for passenger,passenger_card in zip(self.passengers,self.passenger_cards):
                                __passenger_ticket_str__ = self.__make_passengerTicketStr(passenger,passenger_card)
                                __old_passenger_str__ = self.__make_oldPassengersStr(passenger,passenger_card)
                                self.__passenger_ticket_str__ += __passenger_ticket_str__ + '_'
                                self.__old_passenger_str__ += __old_passenger_str__
                            #去除最后一个'-'
                            self.__passenger_ticket_str__ = self.__passenger_ticket_str__[0:-1]
                            if global_output_log_flag:
                                print('__passenger_ticket_str__:',self.__passenger_ticket_str__)
                                print('__old_passenger_str__:',self.__old_passenger_str__)
                            
                            return True
        else:
            #由于查询余票，没有查询到任何结果，可能是刷新太快了，因此停顿一段时间
            sleep(2)

        if global_output_log_flag:
            print('{0} {1} --> {2}满足你要求的车次没有余票了，刷新中....\n'.format(self.dtime,self.from_station,self.to_station))
        return False
        
        
    
    def __submit_order_request__(self):
        '''
        点击预定后  阶段2：对以下条件检查
        
        这里会进行检查：1.买票时间是否正确
                        2.是否有该车次未处理的订单
                        3.选择指定车次、指定座位类型的余票成功？
                        4 登录用户是否过期
        不满足都返回  不满足2返回-1  不满足其他返回1
        满足返回 0
        '''
        if global_output_log_flag:
            print("开始进行订单检查...")
        url = 'https://kyfw.12306.cn/otn/leftTicket/submitOrderRequest'
    
        '''       
        参数信息
             "secretStr"               :  指定需要预订的车次,这个事通过解码得到的
             "train_date"              :  出发日期
             "back_train_date"         :  返回日期
             "tour_flag": "dc"         :  单程/ 往返(wc)             不用管 对应的就是查询时 勾选单程？往返？
             "purpose_codes"           :  "ADULT"  普通/学生(0X00)   不用管 对应的就是查询时 普通？学生？
             "query_from_station_name" :  出发车站 ，可以在查询车次接口中得到
             "query_to_station_name"   :  返回车站，  可以在查询车次接口中得到
             "undefined"               :  ""  应该是跟返回数据相关
        '''
        #选择车次、座位类型  失败返回1
        if not self.__select_train__():
            return 1

        purpose_codes = 'ADULT'
        if self.__is_student__:
            purpose_codes = '0X00'
        data = {
            #用unquote( )函数将余票查询结果中的字符串解码  https://zhuanlan.zhihu.com/p/48077823?utm_source=qq&utm_medium=social&utm_oi=827947156655718400
            "secretStr": self.__preorder_train_secret_str__,   
            "train_date": self.dtime,
            "back_train_date": datetime.datetime.now().strftime('%Y-%m-%d'),
            "tour_flag": "dc",
            "purpose_codes": purpose_codes,                    #如果在查询余票时使用的是学生票，这里预订需要改为0X00
            "query_from_station_name": self.from_station,
            "query_to_station_name": self.to_station,
            "undefined": ""
        }        
        response = self.session.post(url, data=data, headers=self.headers)
                
        # 反回一个验证通过信息
        if (response is not None) and (response.status_code == requests.codes.ok):
            #{"validateMessagesShowId":"_validatorMessage","status":true,"httpstatus":200,"messages":[],"validateMessages":{}}
            #成功
            
            #{"validateMessagesShowId":"_validatorMessage","status":false,"httpstatus":200,"messages":["当前时间不可以订票"],"validateMessages":{}}
            #预订时间检查
            
            #{"validateMessagesShowId":"_validatorMessage","status":false,"httpstatus":200,"messages":["您还有未处理的订单，请您到<a href=\"../view/train_order.html\">[未完成订单]</a>进行处理!"],"validateMessages":{}}                 
            #是否有该次未处理消息？
            
            #{"validateMessagesShowId":"_validatorMessage","status":false,"httpstatus":200,"messages":["车票信息已过期，请重新查询最新车票信息"],"validateMessages":{}}                 
            #登录的用户过期，重新登录后，返回信息
            
            #print(response.content)
            try:
                dic = json.loads(response.content)
            except Exception as e:
                print('订单异常:{0}!'.format(e))
                xml = response.content.decode()
                dic = self.__xml_to_dict__(xml)

            if isinstance(dic,dict):
                if global_output_log_flag:
                    print(data)
                    print(self.__dic_to_table__(dic))
                if dic['status'] == True:
                    return 0
                else:
                    if '您还有未处理的订单' in dic['messages'][0]:
                        #sys.exit(dic['messages'][0],'请先取消订单....')
                        return -1
                    if global_output_log_flag:
                        print("订单异常：", dic['messages'][0])
                    return 1
            else:
                return 1
        else:
            if global_output_log_flag:
                print("订单异常！\n")
            return 1
         

    def __init_dc__(self):
        '''
        点击预定后  阶段3:  页面跳转  获取两个加密字符串，后面请求会使用到
        '''
        if global_output_log_flag:
            print("开始初始化订单数据...")
        dc_url = 'https://kyfw.12306.cn/otn/confirmPassenger/initDc'
        data = {
            '_json_att': ''
        }
        response = self.session.post(dc_url, data=data, headers=self.headers)
        # print(resp.text)
        # 反回一个验证通过信息
        if (response is not None) and (response.status_code == requests.codes.ok):
            # var globalRepeatSubmitToken = '9c4ae60bf6a8457be2214daf95e0d8ce'
            a1 = re.search(r'globalRepeatSubmitToken.+', response.text).group()
            globalRepeatSubmitToken = re.sub(r'(globalRepeatSubmitToken)|(=)|(\s)|(;)|(\')', '', a1)
    
            #'key_check_isChange':'4359BC56A42D9B7C7655EDD91010902B0C8A636DDDD361B011C1B842'
            b1 = re.search(r'key_check_isChange.+', response.text).group().split(',')[0]
            key_check_isChange = re.sub(r'(key_check_isChange)|(\')|(:)', '', b1)
            
            if global_output_log_flag:
                print('得到校验uuid:', globalRepeatSubmitToken,key_check_isChange)
            self.__REPEAT_SUBMIT_TOKEN__, self.__key_check_isChange__ = globalRepeatSubmitToken, key_check_isChange
            

    def __get_passenger__(self):
        '''
        点击预定后  阶段4: 加载乘客信息
        
        获取到用户的乘车人信息                
        '''
        if global_output_log_flag:
            print("获取登陆用户所有常用联系人信息...")
        url = 'https://kyfw.12306.cn/otn/confirmPassenger/getPassengerDTOs'
        data = {
            '_json_att': '',
            "REPEAT_SUBMIT_TOKEN": self.__REPEAT_SUBMIT_TOKEN__  #9c4ae60bf6a8457be2214daf95e0d8ce
        }
        response = self.session.post(url, data=data, headers=self.headers)
        if (response is not None) and (response.status_code == requests.codes.ok):
            #print(response.text)
            try:
                dic = json.loads(response.content)
            except Exception as e:
                print('获取常用联系人信息异常:{0}!'.format(e))
                xml = response.content.decode()
                dic = self.__xml_to_dict__(xml)

            if isinstance(dic, dict):
                normal_passengers = dic['data']['normal_passengers']  #list
                '''
                normal_passengers":[{"code":"4",
                                       "passenger_name":"郑洋",
                                       "sex_code":"M",
                                       "sex_name":"男",
                                       "born_date":"1980-01-01 00:00:00",
                                       "country_code":"CN",
                                       "passenger_id_type_code":"1",
                                       "passenger_id_type_name":"中国居民身份证",
                                       "passenger_id_no":"340621199411082033",
                                       "passenger_type":"1",
                                       "passenger_flag":"0",
                                       "passenger_type_name":"成人",
                                       "mobile_no":"",
                                       "phone_no":"",
                                       "email":"",
                                       "address":"",
                                       "postalcode":"",
                                       "first_letter":"ZY",
                                       "recordCount":"4",
                                       "total_times":"99",
                                       "index_id":"0",
                                       "gat_born_date":"",
                                       "gat_valid_date_start":"",
                                       "gat_valid_date_end":"",
                                       "gat_version":""},
                                       {"code":"1","passenger_name":"刘燕","sex_code":"","born_date":"2017-07-06 00:00:00","country_code":"CN","passenger_id_type_code":"1","passenger_id_type_name":"中国居民身份证","passenger_id_no":"511523199708177148","passenger_type":"1","passenger_flag":"0","passenger_type_name":"成人","mobile_no":"","phone_no":"","email":"","address":"","postalcode":"","first_letter":"LY","recordCount":"4","total_times":"99","index_id":"1","gat_born_date":"","gat_valid_date_start":"","gat_valid_date_end":"","gat_version":""},
                                       {"code":"2","passenger_name":"马资阳","sex_code":"","born_date":"2017-07-05 00:00:00","country_code":"CN","passenger_id_type_code":"1","passenger_id_type_name":"中国居民身份证","passenger_id_no":"340621199408152037","passenger_type":"1","passenger_flag":"0","passenger_type_name":"成人","mobile_no":"","phone_no":"","email":"","address":"","postalcode":"","first_letter":"MZY","recordCount":"4","total_times":"99","index_id":"2","gat_born_date":"","gat_valid_date_start":"","gat_valid_date_end":"","gat_version":""},
                                       {"code":"3","passenger_name":"杨德亮","sex_code":"","born_date":"2018-02-22 00:00:00","country_code":"CN","passenger_id_type_code":"1","passenger_id_type_name":"中国居民身份证","passenger_id_no":"500228199304130311","passenger_type":"3","passenger_flag":"0","passenger_type_name":"学生","mobile_no":"","phone_no":"","email":"","address":"","postalcode":"","first_letter":"YDL","recordCount":"4","total_times":"99","index_id":"3","gat_born_date":"","gat_valid_date_start":"","gat_valid_date_end":"","gat_version":""}
                                       ],
                '''
                if global_output_log_flag:
                    #创建表头
                    table = PrettyTable()
                    header  = ['code','passenger_name','sex_code','country_code','passenger_id_type_code','passenger_id_type_name',
                               'passenger_id_no','passenger_type','passenger_flag','passenger_type_name','mobile_no','phone_no',
                                'email','address','postalcode','recordCount','total_times']
                    table._set_field_names(header)
                    for pnormal_passenger in normal_passengers:
                        values_row = []
                        for key in header:
                            values_row.append(pnormal_passenger[key])
                        #插入数据
                        table.add_row(values_row)
                    if global_output_log_flag:
                        print(table)

    '''
    **********************************************************************************************
    点击提交订单
    *********************************************************************************************
    '''
    def __check_order_info__(self):
        '''
        点击提交订单  阶段1  成功返回True 失败返回False
        '''
        '''
        参数信息
            cancel_flag        : 2  默认
            bed_level_order_num: 000000000000000000000000000000  默认
            passengerTicketStr : 选择的乘客信息                                             
            oldPassengerStr    : 选择的乘客信息                                                                      
            tour_flag          : 单程/ 往返(wc)      不用管 对应的就是查询时 勾选单程？往返？
            randCode           : 需要重新获取验证码，为空
            whatsSelect        : 1 是否是常用联系人中选择的需要购买车票的人
            _json_att:
            REPEAT_SUBMIT_TOKEN:9c4ae60bf6a8457be2214daf95e0d8ce
        '''
        if global_output_log_flag:
            print("检查订单信息...")
        data = {
            'cancel_flag': '2',
            'bed_level_order_num': "000000000000000000000000000000",
            'passengerTicketStr': self.__passenger_ticket_str__,
            'oldPassengerStr': self.__old_passenger_str__,
            'tour_flag': 'dc',            
            'randCode': '',
            'whatsSelect': '1',
            '_json_att': '',
            'REPEAT_SUBMIT_TOKEN': self.__REPEAT_SUBMIT_TOKEN__
        }        
        url = 'https://kyfw.12306.cn/otn/confirmPassenger/checkOrderInfo'
        response = self.session.post(url, data=data, headers=self.headers)
        if (response is not None) and (response.status_code == requests.codes.ok):
            #print(response.text)
            try:
                dic = json.loads(response.content)
            except Exception as e:
                print('订单信息检查异常:{0}!'.format(e))
                xml = response.content.decode()
                dic = self.__xml_to_dict__(xml)

            if isinstance(dic,dict):
                # {'validateMessagesShowId':'_validatorMessage','status':True,'httpstatus':200,'data':{'checkSeatNum':True,'errMsg':'您选择了2位乘车人，但本次列车二等座仅剩1张。','submitStatus': False}, 'messages': [], 'validateMessages': {}}
                #{"validateMessagesShowId":"_validatorMessage","status":true,"httpstatus":200,"data":{"ifShowPassCode":"N","canChooseBeds":"N","canChooseSeats":"N","choose_Seats":"MOP9","isCanChooseMid":"N","ifShowPassCodeTime":"1791","submitStatus":true,"smokeStr":""},"messages":[],"validateMessages":{}}
                ##{"validateMessagesShowId":"_validatorMessage","status":true,"httpstatus":200,"data":{'checkSeatNum': True, 'errMsg': '您选择了1位乘车人，但本次列车棚车仅剩0张。', 'submitStatus': False},"messages":[],"validateMessages":{}}
                ##{"validateMessagesShowId":"_validatorMessage","url":"/leftTicket/init","status":False,"httpstatus":200,messages":[系统忙，请稍后重试],"validateMessages":{}}
                if global_output_log_flag:
                    print(data)
                    print(self.__dic_to_table__(dic))

                if dic['status']:
                    if dic.__contains__('data'):
                        return dic['data']['submitStatus']
                    else:
                        return True
                else:
                    if global_output_log_flag:
                        print("订单信息检查异常：", dic['messages'][0])
                    return False
            else:
                return False
    
        return False
    
    def __trance_date__(self,param):
        '''
        将传递的字符串转化为时间
            :param param: 时间： 2017-12-29
            :return: Fri Dec 29 2017 00:00:00 GMT+0800 (中国标准时间)
        '''
        ts = time.mktime(time.strptime(param, "%Y-%m-%d"))
        s = time.ctime(ts)
        t1 = s[0:11] + s[20:] + " 00:00:00 GMT+0800 (中国标准时间)"
        return t1

    
    def __get_queue_count__(self):
        '''
        判断是都有余票     阶段1
        '''
        if global_output_log_flag:
            print('检查余票中...')

        purpose_codes = '00'
        if self.__is_student__:
            purpose_codes = '0X00'
        url = 'https://kyfw.12306.cn/otn/confirmPassenger/getQueueCount'
        # 将字符串转化为需要的时间
        train_data = self.__trance_date__(self.dtime)
        data = {
                # 时间
                'train_date': train_data,
                # 车次编号
                'train_no': self.__trains_data_list__[self.__preorder_train_index__][2],
                # 火车代码
                'stationTrainCode': self.__trains_data_list__[self.__preorder_train_index__][3],
                # 座位类型 
                'seatType': self.__preorder_train_seat_type__,
                # 出发点，终止地址
                'fromStationTelecode': self.__trains_data_list__[self.__preorder_train_index__][6],
                'toStationTelecode': self.__trains_data_list__[self.__preorder_train_index__][7],
                'leftTicket': self.__trains_data_list__[self.__preorder_train_index__][12],
                'purpose_codes': purpose_codes,
                'train_location': self.__trains_data_list__[self.__preorder_train_index__][15],
                '_json_att': '',
                'REPEAT_SUBMIT_TOKEN': self.__REPEAT_SUBMIT_TOKEN__
            }                  
        response = self.session.post(url, data=data, headers=self.headers)
        if (response is not None) and (response.status_code == requests.codes.ok):
            # 有余票，返回值将会是True
            #print(response.text)
            if global_output_log_flag:
                print(data)

                try:
                    dic = json.loads(response.content)
                except Exception as e:
                    print('检查余票中异常:{0}!'.format(e))
                    xml = response.content.decode()
                    dic = self.__xml_to_dict__(xml)

                if isinstance(dic, dict):
                    print(self.__dic_to_table__(dic))
    
    '''
    **********************************************************************************************
    点击确认提交
    *********************************************************************************************
    '''
    def __confirm_single__(self):
        '''
        确认提交
        最后一次确认订单
        返回购票结果  成功返回True 失败返回False
        '''
        '''
        参数信息：
            passengerTicketStr:
            oldPassengerStr   :            
            randCode          :
            purpose_codes     : 00
            leftTicketStr
            key_check_isChange: 4359BC56A42D9B7C7655EDD91010902B0C8A636DDDD361B011C1B842
            train_location    : QZ
            choose_seats:
            seatDetailType:000
            whatsSelect:1
            roomType:00
            dwAll:N
            _json_att:
                REPEAT_SUBMIT_TOKEN:9c4ae60bf6a8457be2214daf95e0d8ce
        '''
        if global_output_log_flag:
            print('确认订单...')

        purpose_codes = '00'
        if self.__is_student__:
            purpose_codes = '0X00'
        url = "https://kyfw.12306.cn/otn/confirmPassenger/confirmSingleForQueue"
        data = {
            'passengerTicketStr': self.__passenger_ticket_str__,
            'oldPassengerStr':self.__old_passenger_str__,
            'randCode': '',
            'purpose_codes': purpose_codes,
            'key_check_isChange': self.__key_check_isChange__,
            'leftTicketStr': self.__trains_data_list__[self.__preorder_train_index__][12],
            'train_location': self.__trains_data_list__[self.__preorder_train_index__][15],
            'choose_seats': '',
            'seatDetailType': '000',
            'whatsSelect': '1',
            'roomType': '00',
            'dwAll': 'N',
            '_json_att': '',
            'REPEAT_SUBMIT_TOKEN': self.__REPEAT_SUBMIT_TOKEN__
        }
        #print(data)
        response = self.session.post(url, data=data, headers=self.headers)
        if (response is not None) and (response.status_code == requests.codes.ok):
            #{"validateMessagesShowId":"_validatorMessage","status":true,"httpstatus":200,"data":{"errMsg":"系统繁忙，请稍后重试！","submitStatus":false},"messages":[],"validateMessages":{}}
            #print(response.text)            
            try:
                dic = json.loads(response.content)
            except Exception as e:
                print('确认订单异常:{0}!'.format(e))
                xml = response.content.decode()
                dic = self.__xml_to_dict__(xml)
            if isinstance(dic, dict):
                if global_output_log_flag:
                    print(data)
                    print(self.__dic_to_table__(dic))

                # 返回购票结果
                if dic['status'] and dic['data']['submitStatus']:
                    #输出购买车次信息
                    train_info = self.__trains_data_filter_list__[self.__preorder_train_index__]

                    if global_output_log_flag:
                        print(self.__dic_to_table__(train_info))
                    return True
                else:
                    if global_output_log_flag:
                        print("确认订单检查异常：", dic['data'])
                    return False
            else:
                return False
        else:
            return False

    def  __query_order_no_complete__(self,train_trips,train_dtime,passengers):
        '''
        判断确认订单后的车次是否出现在未完成订单中    有时候可能进入等待队列中，因此购票结果，可能是失败的
        0：出票成功
        -1：出票失败
        1：没有出票
        :param train_trips: 车次
        :param train_dtime: 出发日期
        :param passengers：乘客 list
        '''
        if global_output_log_flag:
            print("开始检查未完成订单...")
        dc_url = 'https://kyfw.12306.cn/otn/queryOrder/queryMyOrderNoComplete'
        data = {
            '_json_att':''
        }
        response = self.session.post(dc_url, data=data, headers=self.headers)

        if (response is not None) and (response.status_code == requests.codes.ok):
            try:
                #<class 'dict'>: {'validateMessagesShowId': '_validatorMessage',
                #                 'status': True,
                #                 'httpstatus': 200,
                #                 'data':
                #                       {'orderDBList': [
                #                                           {
                #                                              'sequence_no': 'E780549925','order_date': '2019-01-02 14:12:10', 'ticket_totalnum': 1,'ticket_price_all': 24400.0, 'cancel_flag': 'Y','resign_flag': '4','return_flag': 'N','print_eticket_flag': 'N','pay_flag': 'Y','pay_resign_flag': 'N', 'confirm_flag': 'N',
                #                                             'tickets': [
                #                                                            {
                #                                                                 'stationTrainDTO': {
                #                                                                                       'trainDTO': {'train_no': '650000K67508', 'train_code': 'K675', 'start_station_telecode': 'GGQ', 'end_station_telecode': 'XCH', 'start_date_str': '20190131'},
                #                                                                                       'station_train_code': 'K675', 'from_station_telecode': 'GGQ','from_station_name': '广州东','start_time': '1970-01-01 07:44:00', 'to_station_telecode': 'OXH','to_station_name': '宿州','arrive_time': '1970-01-01 09:05:00', 'distance': '1770'
                #                                                                                     },
                #                                                                      'passengerDTO':
                #                                                                                   {
                #                                                                                      'passenger_name': '郑洋','passenger_id_type_code': '1','passenger_id_type_name': '中国居民身份证','passenger_id_no': '340621199411082033', 'total_times': '98', 'gat_born_date': '','gat_valid_date_start': '','gat_valid_date_end': '','gat_version': ''
                #                                                                                   },
                #                                                                      'ticket_no': 'E7805499251103005','sequence_no': 'E780549925','batch_no': '1','train_date': '2019-01-31 00:00:00','coach_no': '10','coach_name': '10','seat_no': '3005','seat_name': '无座',seat_flag': '1','seat_type_code': '1',
                #                                                                      'seat_type_name': '硬座', 'ticket_type_code': '1','ticket_type_name': '成人票','reserve_time': '2019-01-02 14:12:10','limit_time': '2019-01-02 14:12:10','lose_time': '2019-01-02 14:42:10','pay_limit_time': '2019-01-02 14:42:10',
                #                                                                      'ticket_price': 24400.0, 'print_eticket_flag': 'N', 'resign_flag': '4','return_flag': 'N', 'confirm_flag': 'N','pay_mode_code': 'Y', 'ticket_status_code': 'i', 'ticket_status_name': '待支付','cancel_flag': 'Y', 'amount_char': 0,
                #                                                                       'trade_mode': '','start_train_date_page': '2019-01-31 07:44','str_ticket_price_page': '244.0','come_go_traveller_ticket_page': 'N', 'return_deliver_flag': 'N', 'deliver_fee_char': '', 'is_need_alert_flag': False, 'is_deliver': 'N', 'dynamicProp': '',
                #                                                                       'fee_char': '', 'insure_query_no': '','column_nine_msg': '', 'lc_flag': '8', 'integral_pay_flag': 'N',  'trms_price_rate': '',  'trms_price_number': '','trms_service_code': '','ext_ticket_no': '', 'sale_mode_type': 'E', 'if_cash': '0', 'alternate_flag': 'N'
                #                                                                }
                #                                                            ],  </tickets>
                #                                                'reserve_flag_query': 'p',
                #                                                'if_show_resigning_info': 'N',
                #                                                 'recordCount': '1',
                #                                                 'isNeedSendMailAndMsg': 'N', '
                #                                                 array_passser_name_page': ['郑洋'],
                #                                                 'from_station_name_page': ['广州东'],
                #                                                 'to_station_name_page': ['宿州'],
                #                                                 'start_train_date_page': '2019-01-31 07:44',
                #                                                 'start_time_page': '07:44',
                #                                                 'arrive_time_page': '09:05',
                #                                                  'train_code_page': 'K675',
                #                                                  'ticket_total_price_page': '244.0', '
                #                                                  'come_go_traveller_order_page': 'N',
                #                                                  'canOffLinePay': 'N',
                #                                                  'if_deliver': 'N',
                #                                                  'insure_query_no': ''
                #                                                }
                #                                         ],  </orderDBList>
                #                           'to_page': 'db'    #如果出票失败这里可能是to_page:"cache"
                #                        },    </data>
                #                 'messages': [],
                #                 'validateMessages': {}
                #             }

                #或者 没有为完成订单 <class 'dict'>: {'validateMessagesShowId': '_validatorMessage', 'status': True, 'httpstatus': 200, 'messages': [], 'validateMessages': {}}
                dic = json.loads(response.content)
            except Exception as e:
                print('检查未完成订单异常:{0}!'.format(e))
                xml = response.content.decode()
                dic = self.__xml_to_dict__(xml)

            if isinstance(dic, dict):
                if global_output_log_flag:
                    print(self.__dic_to_table__(dic))

                #可能存在返回dic不包含'data'的情况   即未完成订单中没有任何订单
                if dic['status']  and  not dic.__contains__('data'):
                    return 1

                # 返回购票结果  状态 True  并且to_page:db  包含购票信息，即购票成功
                if dic['status'] and dic['data']['to_page']=='db':
                    order_db_list = dic['data']['orderDBList']

                    # 输出未完成订单信息
                    if global_output_log_flag:
                        print(self.__dic_to_table__(order_db_list))

                    for order_db in order_db_list:
                        #train_code_page:"K729"
                        train_code_page = order_db['train_code_page']
                        #start_train_date_page:"2019-01-31 14:50"
                        start_train_date_page = order_db['start_train_date_page']
                        if train_trips in train_code_page and train_dtime in start_train_date_page:
                            #检查乘客
                            #array_passser_name_page: Array  0:"陈彩梅"
                            array_passser_name_page =  order_db['array_passser_name_page']
                            for passenger in passengers:
                                #过滤掉特殊票种
                                if ')' in passenger:
                                    passenger = re.match(r'([\u4e00-\u9fa5]+)', passenger)
                                    if passenger:
                                        passenger = passenger[0]
                                #如果没找到 表示没购票成功
                                if not array_passser_name_page.__contains__(passenger):
                                    return -1
                            return 0
                    return -1
                else:
                    #出票失败，没有足够的余票   需要前往12306取消，然后重新购买，
                    if dic['status'] and  dic['data']['to_page']=='cache':
                        if global_output_log_flag:
                            print("检查未完成订单异常：", dic['data'])
                    return -1
            else:
                return -1

    '''
    **********************************************************************************************
    退出登录
    *********************************************************************************************
    '''
    def __login_out__(self):
        '''
        登录进入后，加载购票页面
        '''
        # 登陆成功后 余票查询
        url = "https://kyfw.12306.cn/otn/login/loginOut"

        print("退出登录中....")
        response = self.session.get(url, headers=self.headers)
