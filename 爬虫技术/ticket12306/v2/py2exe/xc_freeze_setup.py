'''xc_freeze制作exe文件： 错误指南：https://www.jianshu.com/p/0babfd73fa95
			 1、pip install cx_Freeze
			 2、在程序所在目录下，写setup.py文件内容如下  https://www.jianshu.com/p/0babfd73fa95
			 3、cmd进入程序所在路径，python xc_freeze_setup.py bdist_msi
			 4、会在setup.py所在的目录中生成两个文件夹，dist文件夹中包含一个msi文件，点击安装后会生成build文件中的内容，build文件中包含exe文件和所依赖的各种文件。'''
import os
os.environ['TCL_LIBRARY']=r'D:\ProgramData\Anaconda3\tcl\tcl8.6'
os.environ['TK_LIBRARY']=r'D:\ProgramData\Anaconda3\tcl\tk8.6'
from cx_Freeze import setup,Executable
options = {'build_exe':
               {
                   'excludes':['gtk','Tkinter'],
                   #依赖的包
                   'packages':['dns','PyQt5'],
                   'includes': ['idna.idnadata'],
                    # 额外添加的文件
                   'include_files':['conf.ini','train_icon.ico','cdn_ips.txt']
               }
            }
#options = {'build_exe': { 'excludes':['gtk','Tkinter'], 'packages': ['PyQt5'], 'includes': ['idna.idnadata','dns'],} }
executables = [Executable(
                # 工程的 入口
                script=r'C:\Users\Administrator\Desktop\buy_ticket12306\UI_main.py',base = 'Win32GUI',
                #exe文件图标
                icon='train_icon.ico',
                #exe文件名
                targetName='12306GT.exe')]
setup(name='JNU12306GT', version = '1.0', description = '12306 grab ticket', options=options, executables = executables)
