﻿'''
方法一
使用 py2exe 打包 Python 程序 :https://blog.csdn.net/bruce_6/article/details/82906444
https://www.cnblogs.com/hanson1/articles/7101578.html
1、pip install py2exe
2、cmd到程序所在路径 python py2exe_setup.py py2exe

方法二
pyinstaller打包    打包python文件为exe文件（PyInstaller工具使用方法）：https://blog.csdn.net/weixin_42052836/article/details/82315118
1、pyinstaller.py -F  ui_main.py
2、可以改变图标 pyinstaller -F --icon=my.ico ui_main.py
最后使用   pyinstaller  -w  -F  --icon=train_icon.ico ui_main.spec
'''


from distutils.core import setup
import py2exe

INCLUDES = []
options = { "py2exe" : { "compressed" : 1,  # 压缩
                         "optimize" : 2,
                         #依赖的包
                         'packages':['dns','PyQt5'],
                         "bundle_files" : 1,  # 所有文件打包成一个 exe 文件
                         "includes" : ['idna.idnadata'],
                         "dll_excludes" : ["MSVCR100.dll"]
                         }
            }
setup( options=options,
       description = "12306 geab ticket",
       zipfile=None,
       windows = ['ui_main.py'],
       data_files = [('.',['./conf.ini','./train_icon.icon'])]
       )
