# -*- mode: python -*-

block_cipher = None


a = Analysis(['UI_main.py'],
             pathex=['C:\\Users\\Administrator\\Desktop\\buy_ticket12306'],
             binaries=[],
             datas=[('./conf.ini','./conf.ini'),('./train_icon.ico','./train_icon.ico'),('./cdn_ips.txt','./cdn_ips.txt'),('./valid_proxies.pkl','./valid_proxies.pkl')],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='UI_main',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=True,
          icon = 'train_icon.ico')
