# -*- coding: utf-8 -*-
"""
Created on Thu Dec 27 18:21:18 2018

@author: Administrator
"""
'''
利用百度图像识别api做文字识别，目的是为了做12306的图片校验。
安装：
    pip install baidu-aip
'''
from aip import AipOcr
import requests
import urllib3
from PIL import Image, ImageFilter, ImageEnhance
from bs4 import BeautifulSoup
import re

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


# 定义常量
APP_ID = '10508877'
API_KEY = 'kpFtUgtOaxmKkNa2C0x7Q7mN'
SECRET_KEY = 'TTX5ginIXZyfGtdH8UTO4kF5M41lf3fb '

# 初始化AipFace对象
client = AipOcr(APP_ID, API_KEY, SECRET_KEY)
# 定义参数变量
options = {
    'detect_direction': 'true',
    'language_type': 'CHN_ENG',
}


class RecoginitionContainer(object):
    '''
    识别方式1 主要采用百度识图，准确率太低
    '''
    
    def __init__(self):
        
        #基路径 保存中间产生的文件
        self.base_img_path = '.'

        #百度识图的路径
        self.url = 'http://image.baidu.com/pictureup/uploadshitu?fr=flash&fm=index&pos=upload'

        #换了一个头有问题
        self.headers = {
                 "User-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36"}


    def __upload_pic__(self, img, filex):
        '''
        上传图片，得到图片地址
        
        args：
            img ： 图片数据
            files：00 01 02 03 10 11 12 13
            urls：
        '''
        #保存小图片 
        img.save('./query_temp_img{0}.jpg'.format(filex))
        
        #读取
        raw = open('./query_temp_img{0}.jpg'.format(filex), 'rb').read()        
        
        
        files = {
            'fileheight': "0",
            'newfilesize': str(len(raw)),
            'compresstime': "0",
            'Filename': "image.png",
            'filewidth': "0",
            'filesize': str(len(raw)),
            'filetype': 'image/png',
            'Upload': "Submit Query",
            'filedata': ("image.png", raw)
        }
        response = requests.post(self.url, files=files, headers=self.headers, verify=False)
        redirect_url = "http://image.baidu.com" + response.text
        
        
        return redirect_url

    def __get_query_content__(slef, query_url):
        '''
        得到图片内容
        '''
        response = requests.get(query_url, headers=slef.headers, verify=False)
        
        li = list()
        
        if response.status_code == requests.codes.ok:
            bs = BeautifulSoup(response.text, "html.parser")
            re = bs.find_all("a", class_="guess-info-word-link guess-info-word-highlight")
            for link in re:
                li.append(link.get_text())
                # print(link.get_text())
            # 再次获取图中动物可能是
            te = bs.find_all("ul", class_="shituplant-tag")
            for l in te:
                # 得到有可能包含的数据
                for child in l.children:
                    try:
                        li.append(child.get_text())
                    except:
                        pass
            # 再次获取相似图片，其中在有一个图片描述，进行获取就知道,重新掉了一个接口
            # pc_search 替换成similar在搜索一次
            if len(li) == 0:
                once_url = query_url.replace("pc_search", "similar")
                resp = requests.get(once_url, headers=slef.headers, verify=False)
                js = resp.json()
                #print("查找相似图片")
                for fromTitle in js['data']:
                    li.append(fromTitle['fromPageTitle'])
            return "|".join(x for x in li)

    def get_img_content(self, img_path):
        '''
        识别图片中的文字
        '''
        box = (120,0,290,25)
        Image.open(img_path).crop(box).save('text.jpg')   
        with open('text.jpg', 'rb') as fp:
            img = fp.read()
            result = client.basicGeneral(img, options)
        return result['words_result'][0]['words']
        

    def get_text(self, img_path):
        '''
        识别图片的内容
        
        args：
            img_path：图片路径
        '''
        content = self.get_img_content(img_path)
        print('识别内容:',content)
        
        res = {}
        res['0'] = content
        
        #提高图片的对比度 并保存
        #ImageEnhance.Contrast(Image.open(img_path)).enhance(1.3).save(img_path)        
        
        #加载图片
        img = Image.open(img_path)
        img.filter(ImageFilter.BLUR).filter(ImageFilter.MaxFilter(23))
        
        #图片格式转换
        img.convert('L')
        #plt.imshow(img)
        #plt.show()
        
        #获取图片款和高
        x_width, y_heigth = img.size
        
        # 得到每一张图片应该的大小  划分为8小块
        width = x_width / 4
        heigth = y_heigth / 2
        for x_ in range(0, 2):                  #两行
            for y_ in range(0, 4):              #四列
                left = y_ * width
                right = (y_ + 1) * width
                # 得到图片位置
                index = (x_ * 4) + y_
                
                if x_ == 0:
                    box = (left, x_ * heigth + 21, right, (x_ + 1) * heigth + 21)
                else:
                    box = (y_ * width, x_ * heigth + 21, (y_ + 1) * width, (x_ + 1) * heigth)
                    
                #上传图片，得到图片地址
                query_url = self.__upload_pic__(img.crop(box), str(x_) + str(y_))
                
                #进行查询，返回结果
                text = self.__get_query_content__(query_url)
                
                # 计算坐标 保存每一个位置对应的图片内容
                res[str(index+1)] = text                            
                #print("识别结果:")
                #print(text)        
        #print(res)
        return res
    
    def get_result(self,img_path):
        '''
        传入一张图片，返回符合要求的图片索引 list类型,元素从1-8 例如[1,8]
        '''
        res = self.get_text(img_path)
        query_content = res['0']
        li = []            
        #print(query_content)
        for key,val in res.items():
            for i in query_content:           
                if i in val:
                    li.append(key)
                    break
            
            #if query_content in val:
            #     li.append(key)                    
        return li[1:]




class ImageRecognition(object):
    '''
    参考文章：用深度学习破解12306图片验证码,识别率超96%- web效果版
    用来识别页面：http://littlebigluo.qicp.net:47720/
    '''
    def __init__(self):
        #换了一个头可能有问题
        self.headers = {
                 "User-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36"}
        
        #url
        self.url = 'http://littlebigluo.qicp.net:47720/'
        self.url = 'http://103.46.128.47:47720/'

    def get_result(self,img_path):
        '''
        传入一张图片，返回符合要求的图片索引 list类型,元素从1-8 例如[1,8]
        '''
        files = {'file':(img_path,open(img_path,'rb'),'image/jpeg)')}
        
        try:
            response = requests.post(self.url,files=files,headers=self.headers,timeout=1, proxies = None)
            #下面两个一样
            #print(response.text)
            #print(response.content.decode())
            html = response.text
                            
            #取出结果
            p1 = r"<B>.+</B>"
            pattern = re.compile(p1)
            ret1 = pattern.findall(html)
            
            p2 = r"<font.+</font>"
            pattern = re.compile(p2)
            ret2 = pattern.findall(html)
            #print(ret2)
            #print(ret1)
            res = []
            if len(ret1) >= 1:
                index1 = ret1[0].rfind('<')            
                index2 = ret1[0].find('>',0)+1            
                ret = ret1[0][index2:index1]
                res = list(map(int,ret.split()))
            #识别不出来
            else:
                res = None
        except Exception as e:
            res = None
        return res

if __name__ == "__main__":    
    rec = ImageRecognition()  #RecoginitionContainer()
    res = rec.get_result('img.jpg')
    print(res)
    
    '''
    html = '<font color="red"><font size="+2"><B>3 4 7</B></font></font></p><p><font size="1">第一排图片从左到右编号依次为:1 2 3 4</font></p><p><font size="1">第二排图片从左到右编号依次为:5 6 7 8</font></p><p><font size="1">耗时:339毫秒!觉得俺bigluo眼力如何??</font></p><p><font size="1">如果不确定结果是否正确，不妨登陆一下12306试试！！</font></p><p><font size="1">有意见或建议？？欢迎交流:3490699170@qq.com</font>'
    p1 = r"<B>.+</B>"
    pattern = re.compile(p1)
    ret1 = pattern.findall(html)
    
    p2 = r"<font.+</font>"
    pattern = re.compile(p2)
    ret2 = pattern.findall(html)
    
    print(ret2)
    print(ret1)
    
    res = []
    if len(ret1) >= 1:
        index1 = ret1[0].rfind('<')
        print(index1)
        index2 = ret1[0].find('>',0)+1
        print(index2)
        ret = ret1[0][index2:index1]
        print(ret)
        '''




