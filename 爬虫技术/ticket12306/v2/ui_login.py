# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'UI_login.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    '''
    login界面设计
    '''
    def setupUi(self, MainWindow):
        #主窗体设置
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(379, 452)
        font = QtGui.QFont()
        font.setFamily("楷体")
        font.setPointSize(12)
        font.setBold(False)
        font.setWeight(50)
        MainWindow.setFont(font)

        #centralwidget设置
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")

        #用户名标签设置
        self.lbUserName = QtWidgets.QLabel(self.centralwidget)
        self.lbUserName.setGeometry(QtCore.QRect(40, 30, 54, 12))
        font = QtGui.QFont()
        font.setFamily("楷体")
        font.setPointSize(12)
        self.lbUserName.setFont(font)
        self.lbUserName.setObjectName("lbUserName")

        #用户密码标签设置
        self.lbUserPwd = QtWidgets.QLabel(self.centralwidget)
        self.lbUserPwd.setGeometry(QtCore.QRect(40, 70, 60, 16))
        font = QtGui.QFont()
        font.setFamily("楷体")
        font.setPointSize(12)
        self.lbUserPwd.setFont(font)
        self.lbUserPwd.setObjectName("lbUserPwd")

        #用户名编辑框设置
        self.letUserName = QtWidgets.QLineEdit(self.centralwidget)
        self.letUserName.setGeometry(QtCore.QRect(110, 24, 221, 24))
        font = QtGui.QFont()
        font.setFamily("楷体")
        font.setPointSize(9)
        self.letUserName.setFont(font)
        self.letUserName.setObjectName("letUserName")

        #用户密码编辑框设置
        self.letUserPwd = QtWidgets.QLineEdit(self.centralwidget)
        self.letUserPwd.setGeometry(QtCore.QRect(110, 70, 221, 24))
        font = QtGui.QFont()
        font.setFamily("楷体")
        font.setPointSize(9)
        self.letUserPwd.setFont(font)
        self.letUserPwd.setEchoMode(QtWidgets.QLineEdit.Password)
        self.letUserPwd.setObjectName("letUserPwd")

        #验证码显示设置
        #self.pbxCode = QtWidgets.QGraphicsView(self.centralwidget)
        #self.pbxCode.setGeometry(QtCore.QRect(40, 170, 293, 190))
        #self.pbxCode.setObjectName("pbxCode")
        self.imageView = QtWidgets.QLabel(self.centralwidget)
        self.imageView.setGeometry(QtCore.QRect(40, 170, 293, 190))
        self.imageView.setObjectName("imageView")

        #验证码标签设置
        self.lbCode = QtWidgets.QLabel(self.centralwidget)
        self.lbCode.setGeometry(QtCore.QRect(40, 120, 54, 12))
        font = QtGui.QFont()
        font.setFamily("楷体")
        font.setPointSize(12)
        self.lbCode.setFont(font)
        self.lbCode.setObjectName("lbCode")

        #验证码编辑框设置
        self.letCode = QtWidgets.QLineEdit(self.centralwidget)
        self.letCode.setGeometry(QtCore.QRect(110, 120, 120, 24))
        font = QtGui.QFont()
        font.setFamily("楷体")
        font.setPointSize(9)
        self.letCode.setFont(font)
        self.letCode.setObjectName("letCode")

        #刷新按钮设置
        self.btnRefresh = QtWidgets.QPushButton(self.centralwidget)
        self.btnRefresh.setGeometry(QtCore.QRect(250, 120, 80, 23))
        font = QtGui.QFont()
        font.setFamily("楷体")
        font.setPointSize(12)
        self.btnRefresh.setFont(font)
        self.btnRefresh.setObjectName("btnRefresh")
        MainWindow.setCentralWidget(self.centralwidget)

        #登录按钮设置
        self.btnLogin = QtWidgets.QPushButton(self.centralwidget)
        self.btnLogin.setGeometry(QtCore.QRect(40, 380, 291, 23))
        font = QtGui.QFont()
        font.setFamily("楷体")
        font.setPointSize(12)
        self.btnLogin.setFont(font)
        self.btnLogin.setObjectName("btnLogin")

        #设置主窗口的centralwidget
        MainWindow.setCentralWidget(self.centralwidget)

        #主窗口菜单栏设置
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 379, 23))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)

        #主窗口状态栏设置
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "12306抢票系统-江南大学"))
        self.lbUserName.setText(_translate("MainWindow", "用户名："))
        self.lbUserPwd.setText(_translate("MainWindow", "密  码："))
        self.lbCode.setText(_translate("MainWindow", "验证码："))
        self.btnRefresh.setText(_translate("MainWindow", "刷新"))
        self.btnLogin.setText(_translate("MainWindow", "立即登录"))
        self.imageView.setText(_translate("MainWindow", "验证码加载失败："))


