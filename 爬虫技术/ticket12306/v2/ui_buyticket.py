# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'UI_BuyTicket.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class UI_BuyTicket(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(824, 910)
        font = QtGui.QFont()
        font.setFamily("楷体")
        font.setPointSize(12)
        Form.setFont(font)

        '''
        第一行控件
        '''
        self.lbFromStation = QtWidgets.QLabel(Form)
        self.lbFromStation.setGeometry(QtCore.QRect(40, 30, 54, 12))
        font = QtGui.QFont()
        font.setFamily("楷体")
        font.setPointSize(12)
        self.lbFromStation.setFont(font)
        self.lbFromStation.setObjectName("lbFromStation")

        #self.cbxFromStation = QtWidgets.QLineEdit(Form)
        self.cbxFromStation = QtWidgets.QComboBox(Form)
        self.cbxFromStation.setGeometry(QtCore.QRect(111, 24, 120, 24))
        font = QtGui.QFont()
        font.setFamily("楷体")
        font.setPointSize(12)
        self.cbxFromStation.setFont(font)
        self.cbxFromStation.setObjectName("cbxFromStation")



        self.lbToStation = QtWidgets.QLabel(Form)
        self.lbToStation.setGeometry(QtCore.QRect(261, 30, 54, 12))
        font = QtGui.QFont()
        font.setFamily("楷体")
        font.setPointSize(12)
        self.lbToStation.setFont(font)
        self.lbToStation.setObjectName("lbToStation")

        self.cbxToStation = QtWidgets.QComboBox(Form)
        self.cbxToStation.setGeometry(QtCore.QRect(330, 24, 120, 24))
        font = QtGui.QFont()
        font.setFamily("楷体")
        font.setPointSize(12)
        self.cbxToStation.setFont(font)
        self.cbxToStation.setObjectName("cbxToStation")


        self.lbTicketType = QtWidgets.QLabel(Form)
        self.lbTicketType.setGeometry(QtCore.QRect(480, 24, 40, 24))
        font = QtGui.QFont()
        font.setFamily("楷体")
        font.setPointSize(12)
        self.lbTicketType.setFont(font)
        self.lbTicketType.setObjectName("lbTicketType")

        self.gbxTicketType = QtWidgets.QGroupBox(Form)
        self.gbxTicketType.setGeometry(QtCore.QRect(538, 22, 220, 30))
        self.gbxTicketType.setTitle("")
        self.gbxTicketType.setObjectName("gbxTicketType")

        self.rbnAdult = QtWidgets.QRadioButton(self.gbxTicketType)
        self.rbnAdult.setGeometry(QtCore.QRect(10, 10, 65, 16))
        font = QtGui.QFont()
        font.setFamily("楷体")
        font.setPointSize(12)
        self.rbnAdult.setFont(font)
        self.rbnAdult.setChecked(True)
        self.rbnAdult.setObjectName("rbnAdult")

        self.rbnStudent = QtWidgets.QRadioButton(self.gbxTicketType)
        self.rbnStudent.setGeometry(QtCore.QRect(75, 10, 65, 16))
        font = QtGui.QFont()
        font.setFamily("楷体")
        font.setPointSize(12)
        self.rbnStudent.setFont(font)
        self.rbnStudent.setObjectName("rbnStudent")

        self.rbnOther = QtWidgets.QRadioButton(self.gbxTicketType)
        self.rbnOther.setGeometry(QtCore.QRect(150, 10, 65, 16))
        font = QtGui.QFont()
        font.setFamily("楷体")
        font.setPointSize(12)
        self.rbnOther.setFont(font)
        self.rbnOther.setObjectName("rbnOther")

        '''
        第二行 乘客信息
        '''
        self.groupBox = QtWidgets.QGroupBox(Form)
        self.groupBox.setGeometry(QtCore.QRect(30, 60, 731, 261))
        font = QtGui.QFont()
        font.setFamily("楷体")
        font.setPointSize(12)
        self.groupBox.setFont(font)
        self.groupBox.setObjectName("groupBox")

        self.tablePassenger = QtWidgets.QTableView(self.groupBox)
        self.tablePassenger.setGeometry(QtCore.QRect(10, 20, 700, 180))
        font = QtGui.QFont()
        font.setFamily("楷体")
        self.tablePassenger.setFont(font)
        self.tablePassenger.setObjectName("tablePassenger")

        self.lbName = QtWidgets.QLabel(self.groupBox)
        self.lbName.setGeometry(QtCore.QRect(10, 225, 54, 12))
        font = QtGui.QFont()
        font.setFamily("楷体")
        font.setPointSize(12)
        self.lbName.setFont(font)
        self.lbName.setObjectName("lbName")

        self.letName = QtWidgets.QLineEdit(self.groupBox)
        self.letName.setGeometry(QtCore.QRect(70, 220, 120, 24))
        font = QtGui.QFont()
        font.setFamily("楷体")
        font.setPointSize(10)
        self.letName.setFont(font)
        self.letName.setObjectName("letName")

        self.lbIdCard = QtWidgets.QLabel(self.groupBox)
        self.lbIdCard.setGeometry(QtCore.QRect(210, 225, 54, 12))
        font = QtGui.QFont()
        font.setFamily("楷体")
        font.setPointSize(12)
        self.lbIdCard.setFont(font)
        self.lbIdCard.setObjectName("lbIdCard")

        self.letIdCard = QtWidgets.QLineEdit(self.groupBox)
        self.letIdCard.setGeometry(QtCore.QRect(270, 220, 120, 24))
        font = QtGui.QFont()
        font.setFamily("楷体")
        font.setPointSize(10)
        self.letIdCard.setFont(font)
        self.letIdCard.setObjectName("letIdCard")

        self.gbxSex = QtWidgets.QGroupBox(self.groupBox)
        self.gbxSex.setGeometry(QtCore.QRect(480, 215, 111, 31))
        self.gbxSex.setTitle("")
        self.gbxSex.setObjectName("gbxSex")

        self.lbSex = QtWidgets.QLabel(self.groupBox)
        self.lbSex.setGeometry(QtCore.QRect(420, 220, 54, 24))
        font = QtGui.QFont()
        font.setFamily("楷体")
        font.setPointSize(12)
        self.lbSex.setFont(font)
        self.lbSex.setObjectName("lbSex")

        self.rbnMan = QtWidgets.QRadioButton(self.gbxSex)
        self.rbnMan.setGeometry(QtCore.QRect(10, 10, 41, 16))
        font = QtGui.QFont()
        font.setFamily("楷体")
        font.setPointSize(12)
        self.rbnMan.setFont(font)
        self.rbnMan.setChecked(True)
        self.rbnMan.setObjectName("rbnMan")

        self.rbnWoman = QtWidgets.QRadioButton(self.gbxSex)
        self.rbnWoman.setGeometry(QtCore.QRect(50, 10, 41, 16))
        font = QtGui.QFont()
        font.setFamily("楷体")
        font.setPointSize(12)
        self.rbnWoman.setFont(font)
        self.rbnWoman.setObjectName("rbnWoman")


        self.btnAddPassenger = QtWidgets.QPushButton(self.groupBox)
        self.btnAddPassenger.setGeometry(QtCore.QRect(600, 220, 111, 23))
        font = QtGui.QFont()
        font.setFamily("楷体")
        font.setPointSize(12)
        self.btnAddPassenger.setFont(font)
        self.btnAddPassenger.setObjectName("btnAddPassenger")

        '''
        座位类型
        '''
        self.gbxSeatType = QtWidgets.QGroupBox(Form)
        self.gbxSeatType.setGeometry(QtCore.QRect(31, 530, 151, 351))
        font = QtGui.QFont()
        font.setFamily("楷体")
        font.setPointSize(12)
        self.gbxSeatType.setFont(font)
        self.gbxSeatType.setObjectName("gbxSeatType")

        self.cbxSpecialSeat = QtWidgets.QCheckBox(self.gbxSeatType)
        self.cbxSpecialSeat.setGeometry(QtCore.QRect(20, 40, 121, 16))
        self.cbxSpecialSeat.setObjectName("cbxSpecialSeat")
        self.cbxFirstSeat = QtWidgets.QCheckBox(self.gbxSeatType)
        self.cbxFirstSeat.setGeometry(QtCore.QRect(20, 70, 101, 16))
        self.cbxFirstSeat.setObjectName("cbxFirstSeat")
        self.cbxSecondSeat = QtWidgets.QCheckBox(self.gbxSeatType)
        self.cbxSecondSeat.setGeometry(QtCore.QRect(20, 100, 81, 16))
        self.cbxSecondSeat.setObjectName("cbxSecondSeat")
        self.cbxSoftSleeper = QtWidgets.QCheckBox(self.gbxSeatType)
        self.cbxSoftSleeper.setGeometry(QtCore.QRect(20, 160, 111, 16))
        self.cbxSoftSleeper.setObjectName("cbxSoftSleeper")
        self.cbxActionSleeper = QtWidgets.QCheckBox(self.gbxSeatType)
        self.cbxActionSleeper.setGeometry(QtCore.QRect(20, 190, 91, 16))
        self.cbxActionSleeper.setObjectName("cbxActionSleeper")
        self.cbxHardSleeper = QtWidgets.QCheckBox(self.gbxSeatType)
        self.cbxHardSleeper.setGeometry(QtCore.QRect(20, 220, 91, 16))
        self.cbxHardSleeper.setObjectName("cbxHardSleeper")
        self.cbxHardSeat = QtWidgets.QCheckBox(self.gbxSeatType)
        self.cbxHardSeat.setGeometry(QtCore.QRect(20, 280, 91, 16))
        self.cbxHardSeat.setObjectName("cbxHardSeat")
        self.cbxNoSeat = QtWidgets.QCheckBox(self.gbxSeatType)
        self.cbxNoSeat.setGeometry(QtCore.QRect(20, 310, 91, 16))
        self.cbxNoSeat.setObjectName("cbxNoSeat")
        self.cbxSoftSeat = QtWidgets.QCheckBox(self.gbxSeatType)
        self.cbxSoftSeat.setGeometry(QtCore.QRect(20, 250, 91, 16))
        self.cbxSoftSeat.setObjectName("cbxSoftSeat")
        self.cbxHigherSoftSleeper = QtWidgets.QCheckBox(self.gbxSeatType)
        self.cbxHigherSoftSleeper.setGeometry(QtCore.QRect(20, 130, 111, 16))
        self.cbxHigherSoftSleeper.setObjectName("cbxHigherSoftSleeper")


        '''
        车次类型
        '''
        self.gbxTrainType = QtWidgets.QGroupBox(Form)
        self.gbxTrainType.setGeometry(QtCore.QRect(31, 330, 151, 181))
        font = QtGui.QFont()
        font.setFamily("楷体")
        font.setPointSize(12)
        self.gbxTrainType.setFont(font)
        self.gbxTrainType.setObjectName("gbxTrainType")
        self.cbxG = QtWidgets.QCheckBox(self.gbxTrainType)
        self.cbxG.setGeometry(QtCore.QRect(20, 30, 121, 16))
        self.cbxG.setObjectName("cbxG")
        self.cbxD = QtWidgets.QCheckBox(self.gbxTrainType)
        self.cbxD.setGeometry(QtCore.QRect(20, 60, 101, 16))
        self.cbxD.setObjectName("cbxD")
        self.cbxZ = QtWidgets.QCheckBox(self.gbxTrainType)
        self.cbxZ.setGeometry(QtCore.QRect(20, 90, 81, 16))
        self.cbxZ.setObjectName("cbxZ")
        self.cbxT = QtWidgets.QCheckBox(self.gbxTrainType)
        self.cbxT.setGeometry(QtCore.QRect(20, 120, 111, 16))
        self.cbxT.setObjectName("cbxT")
        self.cbxK = QtWidgets.QCheckBox(self.gbxTrainType)
        self.cbxK.setGeometry(QtCore.QRect(20, 150, 91, 16))
        self.cbxK.setObjectName("cbxK")

        '''
        第三行  时间设定
        '''
        self.lbStartTime = QtWidgets.QLabel(Form)
        self.lbStartTime.setGeometry(QtCore.QRect(190, 330, 120, 24))
        font = QtGui.QFont()
        font.setFamily("楷体")
        font.setPointSize(12)
        self.lbStartTime.setFont(font)
        self.lbStartTime.setObjectName("lbStartTime")

        self.dtStartTime = QtWidgets.QTimeEdit(Form)
        self.dtStartTime.setGeometry(QtCore.QRect(325, 330, 135, 24))
        font = QtGui.QFont()
        font.setFamily("楷体")
        font.setPointSize(12)
        self.dtStartTime.setFont(font)
        self.dtStartTime.setObjectName("dtStartTime")

        self.lbEndTime = QtWidgets.QLabel(Form)
        self.lbEndTime.setGeometry(QtCore.QRect(490, 330, 130, 24))
        font = QtGui.QFont()
        font.setFamily("楷体")
        font.setPointSize(12)
        self.lbEndTime.setFont(font)
        self.lbEndTime.setObjectName("lbEndTime")

        self.dtEndTime = QtWidgets.QTimeEdit(Form)
        self.dtEndTime.setGeometry(QtCore.QRect(625, 330, 135, 24))
        font = QtGui.QFont()
        font.setFamily("楷体")
        font.setPointSize(12)
        self.dtEndTime.setFont(font)
        self.dtEndTime.setObjectName("dtEndTime")

        '''第四行 
        '''
        self.lbDate = QtWidgets.QLabel(Form)
        self.lbDate.setGeometry(QtCore.QRect(190, 374, 54, 12))
        font = QtGui.QFont()
        font.setFamily("楷体")
        font.setPointSize(12)
        self.lbDate.setFont(font)
        self.lbDate.setObjectName("lbDate")

        self.dateEdit = QtWidgets.QDateEdit(Form)
        self.dateEdit.setGeometry(QtCore.QRect(255, 370, 150, 24))
        font = QtGui.QFont()
        font.setFamily("楷体")
        font.setPointSize(12)
        self.dateEdit.setFont(font)
        self.dateEdit.setObjectName("dateEdit")

        self.lbEmail = QtWidgets.QLabel(Form)
        self.lbEmail.setGeometry(QtCore.QRect(430, 374, 81, 16))
        font = QtGui.QFont()
        font.setFamily("楷体")
        font.setPointSize(12)
        self.lbEmail.setFont(font)
        self.lbEmail.setObjectName("lbEmail")

        self.letEmailAddress = QtWidgets.QLineEdit(Form)
        self.letEmailAddress.setGeometry(QtCore.QRect(470, 370, 131, 24))
        font = QtGui.QFont()
        font.setFamily("楷体")
        font.setPointSize(10)
        self.letEmailAddress.setFont(font)
        self.letEmailAddress.setObjectName("letEmailAddress")


        '''
        第五行  车次显示
        '''
        self.tableShowTrains = QtWidgets.QTableView(Form)
        self.tableShowTrains.setGeometry(QtCore.QRect(190, 410, 570, 252))
        font = QtGui.QFont()
        font.setFamily("楷体")
        self.tableShowTrains.setFont(font)
        self.tableShowTrains.setObjectName("tableShowTrains")

        '''
        第六行 消息显示
        '''
        self.tbxShowMsg = QtWidgets.QTextEdit(Form)
        self.tbxShowMsg.setGeometry(QtCore.QRect(190, 680, 571, 191))
        font = QtGui.QFont()
        font.setPointSize(9)
        self.tbxShowMsg.setFont(font)
        self.tbxShowMsg.setObjectName("tbxShowMsg")

        self.btnStartBuy = QtWidgets.QPushButton(Form)
        self.btnStartBuy.setGeometry(QtCore.QRect(640, 366, 121, 31))
        font = QtGui.QFont()
        font.setFamily("楷体")
        font.setPointSize(12)
        self.btnStartBuy.setFont(font)
        self.btnStartBuy.setObjectName("btnStartBuy")



        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "12306抢票系统-江南大学"))
        self.lbFromStation.setText(_translate("Form", "出发地："))
        self.lbToStation.setText(_translate("Form", "目的地："))
        self.lbDate.setText(_translate("Form", "出发日："))
        self.dateEdit.setDisplayFormat(_translate("Form", "yyyy-MM-dd"))
        self.gbxSeatType.setTitle(_translate("Form", "座位类型"))
        self.cbxSpecialSeat.setText(_translate("Form", "商务座/特等座"))
        self.cbxFirstSeat.setText(_translate("Form", "一等座"))
        self.cbxSecondSeat.setText(_translate("Form", "二等座"))
        self.cbxHigherSoftSleeper.setText(_translate("Form", "高级软卧"))
        self.cbxSoftSleeper.setText(_translate("Form", "软卧"))
        self.cbxActionSleeper.setText(_translate("Form", "动卧"))
        self.cbxHardSleeper.setText(_translate("Form", "硬卧"))
        self.cbxHardSeat.setText(_translate("Form", "硬座"))
        self.cbxNoSeat.setText(_translate("Form", "无座"))
        self.cbxSoftSeat.setText(_translate("Form", "软座"))
        self.gbxTrainType.setTitle(_translate("Form", "车次类型"))
        self.cbxG.setText(_translate("Form", "GC-高铁/城际"))
        self.cbxD.setText(_translate("Form", "D-动车"))
        self.cbxZ.setText(_translate("Form", "Z-直达"))
        self.cbxT.setText(_translate("Form", "T-特快"))
        self.cbxK.setText(_translate("Form", "K-快速"))
        self.groupBox.setTitle(_translate("Form", "乘客信息"))
        self.lbName.setText(_translate("Form", "姓 名："))
        self.lbIdCard.setText(_translate("Form", "证件号："))
        self.lbSex.setText(_translate("Form", "性 别："))
        self.rbnMan.setText(_translate("Form", "男"))
        self.rbnWoman.setText(_translate("Form", "女"))
        self.lbStartTime.setText(_translate("Form", "出发时间(min)："))
        self.dtStartTime.setDisplayFormat(_translate("Form", "HH:mm:ss"))
        self.lbEndTime.setText(_translate("Form", "出发时间(max)："))
        self.dtEndTime.setDisplayFormat(_translate("Form", "HH:mm:ss"))
        self.lbTicketType.setText(_translate("Form", "票种："))
        self.rbnAdult.setText(_translate("Form", "成人"))
        self.rbnStudent.setText(_translate("Form", "学生"))
        self.rbnOther.setText(_translate("Form", "其它"))
        self.lbEmail.setText(_translate("Form", "邮箱："))
        self.btnAddPassenger.setText(_translate("Form", "添加"))
        self.btnStartBuy.setText(_translate("Form", "开始抢票"))

