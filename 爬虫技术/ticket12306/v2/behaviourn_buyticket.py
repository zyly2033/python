import threading

import ui_buyticket                               #BuyTicket主界面设计
from ui_ticket import BuyTickets
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from time import sleep,time
import datetime
import re
from prettytable import PrettyTable
import configparser
from station import global_stations
import logging
import threadpool

class QtBuyTicket(QMainWindow, ui_buyticket.UI_BuyTicket):
    '''
    BuyTicket行为
    '''
    # 通过类成员对象定义信号  子线程用来通知主线程数据(主要包括子线程查询到的余票信息、通知信息等)已经处理好了，然后主线程负责把数据写入主界面中
    __update_main_ui_date__ = pyqtSignal(int,str)

    def __init__(self):
        #QMainWindow构造函数初始化
        super(QtBuyTicket, self).__init__()
        #生成窗体 并设置样式
        self.setupUi(self)
        self.setWindowIcon(QIcon('train_icon.ico'))
        #最小化、关闭
        self.setWindowFlags(Qt.WindowCloseButtonHint | Qt.WindowMinimizeButtonHint)
        # 禁止拉伸窗口大小
        #self.setFixedSize(self.width(), self.height())

        '''
        QT - QTableView表格视图的列宽设置;https://blog.csdn.net/wyansai/article/details/53018045
        '''
        self.tablePassenger.setAlternatingRowColors(True)
        self.tablePassenger.setShowGrid(False)
        #根据内容调整列宽
        self.tableShowTrains.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeToContents)
        #选设置成行选中
        self.tablePassenger.setSelectionBehavior(QAbstractItemView.SelectRows)

        self.tableShowTrains.setAlternatingRowColors(True)
        self.tableShowTrains.setShowGrid(False)
        self.tableShowTrains.horizontalHeader().setSectionResizeMode(QHeaderView.ResizeToContents)

        '''
        时间设定：实例一：QDateTimeEdit-1：https://blog.csdn.net/jia666666/article/details/81589442
        '''
        self.dateEdit.setDisplayFormat('yyyy-MM-dd')
        self.dtStartTime.setDisplayFormat('HH:mm')
        self.dtEndTime.setDisplayFormat('HH:mm')
        # 设置日历控件允许弹出
        self.dateEdit.setCalendarPopup(True)
        # 设置日期最大值与最小值，在当前日期-下一个月
        self.dateEdit.setMinimumDate(QDate.currentDate())
        self.dateEdit.setMaximumDate(QDate.currentDate().addDays(29))

        #默认选中7天以后的票
        now_day = datetime.datetime.now()
        now_day = now_day + datetime.timedelta(days=7)    # 日期+28  str
        now_day = now_day.strftime('%Y-%m-%d')
        self.dateEdit.setDate(QDate.fromString(now_day, 'yyyy-MM-dd'))
        self.dtStartTime.setTime(QTime.fromString('06:00:00','HH:mm:ss'))
        self.dtEndTime.setTime(QTime.fromString('20:00:00','HH:mm:ss'))

        '''
        控件默认值设置
        '''
        self.rbnMan.setChecked(True)
        #self.cbxSecondSeat.setChecked(True)
        #self.cbxHardSeat.setChecked(True)
        #self.cbxHardSleeper.setChecked(True)
        #self.cbxNoSeat.setChecked(True)
        #self.cbxG.setChecked(True)
        #self.cbxD.setChecked(True)

        '''
        设置下拉列表自动补全  https://blog.csdn.net/xuleisdjn/article/details/51434118
        '''
        #获取所有站台信息
        self.__stations__ = global_stations
        #self.cbxFromStation.addItems(Stations.keys())
        self.cbxFromStation.setEditable(True)
        self.cbxFromStation.setMaxVisibleItems(10)  # 设置最大显示下列项 超过要使用滚动条拖拉
        self.cbxFromStation.setInsertPolicy(QComboBox.InsertAfterCurrent)  # 设置插入方式
        self.cbxToStation.setEditable(True)
        self.cbxToStation.setMaxVisibleItems(10)  # 设置最大显示下列项 超过要使用滚动条拖拉
        self.cbxToStation.setInsertPolicy(QComboBox.InsertAfterCurrent)  # 设置插入方式
        for index,(key,val) in enumerate(self.__stations__.items()):
            self.cbxFromStation.insertItem(index,key,val)
            self.cbxToStation.insertItem(index, key, val)

        self.qcFromStation =  QCompleter()
        #列出包含输入的项
        self.qcFromStation.setFilterMode(Qt.MatchContains)
        self.qcFromStation.setCompletionMode(QCompleter.PopupCompletion)
        self.qcFromStation.setModelSorting(QCompleter.CaseSensitivelySortedModel)
        self.qcFromStation.setModel(self.cbxFromStation.model())
        self.cbxFromStation.setCompleter(self.qcFromStation)

        self.qcToStation =  QCompleter()
        #列出包含输入的项
        self.qcToStation.setFilterMode(Qt.MatchContains)
        self.qcToStation.setCompletionMode(QCompleter.PopupCompletion)
        self.qcToStation.setModelSorting(QCompleter.CaseSensitivelySortedModel)
        self.qcToStation.setModel(self.cbxFromStation.model())
        self.cbxToStation.setCompleter(self.qcToStation)

        '''
        事件设置
        '''
        #点击追加按钮事件
        self.btnAddPassenger.clicked.connect(self.__add_passenger__)

        #开始抢票按钮点击事件  停止抢票
        self.btnStartBuy.clicked.connect(self.__start_buy_ticket__)

        #设置点击事件，主要用来确定需要购票的乘客
        self.tablePassenger.clicked.connect(self.__select_passenger__)

        #用于更新主界面信息
        self.__update_main_ui_date__.connect(self.__update_main_ui__)

        #当出发时间(min)改变时
        self.dtStartTime.timeChanged.connect(self.__dt_start_time_changed__)

        #当出发时间(max)改变时
        self.dtEndTime.timeChanged.connect(self.__dt_end_time_changed__)

        #选择的是学生 更改余票查询日期范围
        self.rbnStudent.toggled.connect(self.__update_query_date__)

        '''
        注意区分__passengers_info_table__：查询好的常用联系人信息，数据类型是PrettyTable
                __passenger_model__：这是数据模型实例 保存的就是常用联系人信息，用于在tablePassenger可视化控件显示
        '''
        # 建立数据模型实例  和 __passengers_info_table__ 保存的数据是一致的 只不过这个第一列保存的不是序号而是checkbox
        self.__passenger_model__ = QStandardItemModel()
        self.__passengers_info_table__ = None


        '''
        一些私有变量
        '''
        #线程对象，用于单独开一个子线程抢票
        self.__thread__ = None

        #用于加载现有配置文件
        self.__conf__ = configparser.ConfigParser()
        self.__conf_path__ = './conf.ini'

        '''
        输出日志  https://www.cnblogs.com/bethansy/p/7716747.html
        '''
        logging.basicConfig(level=logging.DEBUG,
                            format='%(asctime)s %(filename)s[line:%(lineno)d] %(levelname)s %(message)s',
                            datefmt='%a, %d %b %Y %H:%M:%S',
                            filename='./ticket.log',
                            filemode='w')


    def closeEvent(self,QCloseEvent):
        '''
        窗体关闭触发事件
        :param a0:
        :return:
        '''
        reply = QMessageBox.question(self, '本程序', "是否要退出程序？",
                                               QMessageBox.Yes | QMessageBox.No,
                                               QMessageBox.No)
        if reply == QMessageBox.Yes:
            #关闭会话
            self.ticket.__login_out__()
            self.ticket.session.close()
            # 重新保存cdn ips
            #with open('cdn_ips.txt', 'w') as f:
            #    for ip in self.ticket.__cdn_ips__:
            #        f.write(ip)
            QCloseEvent.accept()
        else:
            QCloseEvent.ignore()

    def __update_query_date__(self):
        if self.rbnStudent.isChecked():
            self.dateEdit.setMaximumDate(QDate.currentDate().addDays(58))
        else:
            self.dateEdit.setMaximumDate(QDate.currentDate().addDays(28))

    def  __load__params__(self):
        '''
        从配置文件加载参数  必须放在self.ticket赋值之后
        :return:
        '''
        self.__conf__.read(self.__conf_path__)

        '''
        获取出发地、目的地、日期
        '''
        from_station = self.__conf__.get('ticket_info', 'from_station')
        from_station_index = self.__conf__.get('ticket_info', 'from_station_index')
        to_station = self.__conf__.get('ticket_info', 'to_station')
        to_station_index = self.__conf__.get('ticket_info', 'to_station_index')
        self.cbxFromStation.setCurrentIndex(int(from_station_index))
        self.cbxToStation.setCurrentIndex(int(to_station_index))

        # 余票查询刷新时间设置
        min_refresh_tine = self.__conf__.get('refresh_time', 'min_refresh_tine')
        min_refresh_tine = float(min_refresh_tine)
        if min_refresh_tine >= 0:
            self.ticket.__min_refresh_tine__ = min_refresh_tine

        date = self.__conf__.get('ticket_info', 'date')
        self.dateEdit.setDate(QDate.fromString(date, 'yyyy-MM-dd'))

        start_time = self.__conf__.get('ticket_info', 'start_time')
        self.dtStartTime.setTime(QTime.fromString(start_time, 'HH:mm'))

        end_time = self.__conf__.get('ticket_info', 'end_time')
        self.dtEndTime.setTime(QTime.fromString(end_time, 'HH:mm'))

        '''
        获取通知邮箱、票种
        '''
        email = self.__conf__.get('user_info', 'email')
        self.letEmailAddress.setText(email)

        special_ticket_type = self.__conf__.get('buy_trcket_info', 'special_ticket_type')
        if special_ticket_type == 'True':
            self.rbnOther.setChecked(True)

        '''
        加载座位类型
        '''
        seat_type = self.__conf__.get('seat_type', 'special_seat')
        if seat_type == 'True':
            self.cbxSpecialSeat.setChecked(True)

        seat_type = self.__conf__.get('seat_type', 'first_seat')
        if seat_type == 'True':
            self.cbxFirstSeat.setChecked(True)

        seat_type = self.__conf__.get('seat_type', 'second_seat')
        if seat_type == 'True':
            self.cbxSecondSeat.setChecked(True)

        seat_type = self.__conf__.get('seat_type', 'higher_soft_sleeper')
        if seat_type == 'True':
            self.cbxHigherSoftSleeper.setChecked(True)

        seat_type = self.__conf__.get('seat_type', 'soft_sleeper')
        if seat_type == 'True':
            self.cbxSoftSleeper.setChecked(True)

        seat_type = self.__conf__.get('seat_type', 'action_sleeper')
        if seat_type == 'True':
            self.cbxActionSleeper.setChecked(True)

        seat_type = self.__conf__.get('seat_type', 'soft_seat')
        if seat_type == 'True':
            self.cbxSoftSeat.setChecked(True)

        seat_type = self.__conf__.get('seat_type', 'hard_sleeper')
        if seat_type == 'True':
            self.cbxHardSleeper.setChecked(True)

        seat_type = self.__conf__.get('seat_type', 'hard_seat')
        if seat_type == 'True':
            self.cbxHardSeat.setChecked(True)

        seat_type = self.__conf__.get('seat_type', 'no_seat')
        if seat_type == 'True':
            self.cbxNoSeat.setChecked(True)

        '''
        加载车次类型
        '''
        train_type = self.__conf__.get('train_type', 'train_g')
        if train_type == 'True':
            self.cbxG.setChecked(True)

        train_type = self.__conf__.get('train_type', 'train_d')
        if train_type == 'True':
            self.cbxD.setChecked(True)

        train_type = self.__conf__.get('train_type', 'train_z')
        if train_type == 'True':
            self.cbxZ.setChecked(True)

        train_type = self.__conf__.get('train_type', 'train_t')
        if train_type == 'True':
            self.cbxT.setChecked(True)

        train_type = self.__conf__.get('train_type', 'train_k')
        if train_type == 'True':
            self.cbxK.setChecked(True)



    def set_login(self,ui_login):
        '''
        设置登录界面对象
        :param QtLogin: 登录界面对象
        :return:
        '''
        self.ui_login = ui_login


    def set_ticket(self,ticket):
        '''
        :param ticket:BuyTickets对象 从登录界面传递过来的
        :return:
        '''
        if isinstance(ticket,BuyTickets):
            self.ticket = ticket
            #从配置文件加载参数
            self.__load__params__()
        else:
            QMessageBox.warning(self, '警告', '登陆异常！', QMessageBox.Yes)
            self.close()
        #加载常用联系人信息
        self.__load__passenger()

    def __dt_start_time_changed__(self):
        '''
        实时更新出发时间(min)
        :return:
        '''
        start_time = self.dtStartTime.text()
        num = re.match(r'^\d{2}:\d{2}$', start_time)
        if num is  None:
            QMessageBox.information(self, '警告', '出发时间(min)输入有误！', QMessageBox.Yes)
            return
        self.ticket.set_train_time_quantum(start_time,None)

    def __dt_end_time_changed__(self):
        '''
        实时更新出发时间(max)
        :return:
        '''
        end_time = self.dtEndTime.text()
        num = re.match(r'^\d{2}:\d{2}$', end_time)
        if num is  None:
            QMessageBox.information(self, '警告', '出发时间(max)输入有误！', QMessageBox.Yes)
            return
        self.ticket.set_train_time_quantum(None,end_time)

    def __load__passenger(self):
        '''
        读取常用联系人信息到QTableView控件显示
        :return:
        '''
        #成功返回一个PrettyTable对象 保存常用联系人信息
        self.__passengers_info_table__ = self.ticket.__get_common_passenger_info__()
        if self.__passengers_info_table__ is not None:
            #获取表头
            headers = self.__passengers_info_table__.field_names

            cols_num = len(headers)
            rows_num = len(self.__passengers_info_table__._rows)

            '''
            PyQt5高级界面控件之QTableView（一） : https://blog.csdn.net/jia666666/article/details/81624259
                                                    https://blog.csdn.net/cloveses/article/details/80943496
            pyqt5 QTableView表格复选框案例       : https://www.cnblogs.com/kennyhip/articles/10020711.html
            '''

            #清空
            self.__passenger_model__ = None
            self.__passenger_model__ = QStandardItemModel()

            #设置水平方向表头
            self.__passenger_model__.setHorizontalHeaderLabels(headers)
            for row,row_val in enumerate(self.__passengers_info_table__._rows):
                for col in range(cols_num):
                    '''设置文本'''
                    item = QStandardItem(row_val[col])
                    # 设置单元不可编辑
                    item.setEditable(False)
                    # 设置每个位置的文本值
                    self.__passenger_model__.setItem(row,col,item)

            '''第一列，设置复选框'''
            for row in range(rows_num):
                item_checked = QStandardItem()
                item_checked.setCheckState(Qt.Unchecked)
                item_checked.setCheckable(False)
                #不可编辑
                item_checked.setEnabled(False)
                self.__passenger_model__.setItem(row, 0, item_checked)

            # 实例化表格视图，设置模型为自定义的模型
            self.tablePassenger.setModel(self.__passenger_model__)

            #更新
            self.tablePassenger.update()


    def __add_passenger__(self):
        '''
        添加乘客
        :return:
        '''
        #获取乘客名、密码、性别
        passenger = self.letName.text()
        passenger_card = self.letIdCard.text()
        sex = 'M'
        if self.rbnWoman.isChecked():
            sex = 'W'
        if passenger is None or passenger.strip() is '':
            QMessageBox.information (self,'警告','姓名不能为空！',QMessageBox.Yes)
            self.letName.setFocus()
            return

        if passenger_card is None or len(passenger_card.strip()) != 18:
            QMessageBox.information (self,'警告','请检查你输入的证件号是否有误！',QMessageBox.Yes)
            self.letIdCard.setFocus()
            return

        #检查该乘客是否已经存在
        for row in  self.__passengers_info_table__._rows:
            #该证件号已经存在
            if passenger_card in row[4]:
                QMessageBox.information(self, '警告', '该乘客信息已经存在于常用联系人中了！', QMessageBox.Yes)

        # 如果不存在,则追加常用联系人
        if self.ticket.__passenger_add__(passenger, passenger_card, sex):    #成功刷新
            sleep(2)
            QMessageBox.information(self, '信息', '该乘客信息追加成功！', QMessageBox.Yes)
            # 加载常用联系人信息
            self.__load__passenger()
        else:
            QMessageBox.information(self, '警告', '追加常用联系人失败，请检查你输入信息是否有误！', QMessageBox.Yes)

    def __select_passenger__(self):
        '''
        选择一行  打钩
        PYQT笔记——QTableView;https://blog.csdn.net/xqf1528399071/article/details/52623678
        :param item:
        :return:
        '''
        #获取表格控件当前选中行,列
        row = self.tablePassenger.currentIndex().row()
        col = self.tablePassenger.currentIndex().column()
        #获取第row行第0列元素值
        index = self.__passenger_model__.index(row,0,QModelIndex())
        data = index.data(Qt.CheckStateRole)

        #打钩->取消  无打钩->打钩
        item_checked = QStandardItem()
        if  data == Qt.Checked:
            item_checked.setCheckState(Qt.Unchecked)
            self.__passenger_model__.setItem(row, 0,item_checked)
        else:
            item_checked.setCheckState(Qt.Checked)
            self.__passenger_model__.setItem(row, 0, item_checked)

    def  __get_selected_rows__(self):
        '''
        获取所有打钩的行中乘客的姓名、证件号、性别
        :return:selected_passengers,selected_passenger_card,selected_sex
        '''
        if  self.__passengers_info_table__ is None:
            return

        '''
        检查是不是特殊票种
        '''
        special_ticket_type = False
        if self.rbnOther.isChecked() or self.rbnStudent.isChecked():
            special_ticket_type = True

        selected_passengers = []
        selected_passenger_card = []
        selected_sex = []
        selected_ticket_type_name = []
        for row, row_val in enumerate(self.__passengers_info_table__._rows):
            # 获取第row行第0列元素值
            index = self.__passenger_model__.index(row, 0, QModelIndex())
            data = index.data(Qt.CheckStateRole)
            if data == Qt.Checked:
                val = row_val[1]
                if  special_ticket_type and  row_val[5]  not in '成人':                         #特殊票种
                    val = val + '({0})'.format( row_val[5])
                selected_passengers.append(val)              #姓名
                selected_passenger_card.append(row_val[4])  # 证件号
                selected_sex.append(row_val[2])              #性别
        return selected_passengers,selected_passenger_card,selected_sex


    def __start_buy_ticket__(self):
        '''
        开始抢票
        :return:
        '''
        if self.btnStartBuy.text() == '停止抢票':
            self.btnStartBuy.setText('开始抢票')
            #设置控件可用
            self.__enable_control__(True)
            #如果线程没有关闭，手动关闭
            #while self.__thread__.is_alive():
            #    self.tbxShowMsg.append('等待上一次查询线程结束!')
            #    sleep(5)
            return

        '''获取出发日期，起始地点、目的地点'''
        from_station_index = self.cbxFromStation.currentIndex()     #获取当前索引
        from_station_code = self.cbxFromStation.currentData()       #获取当前数据
        from_station = self.cbxFromStation.currentText()   #获取当前内容
        real_from_station = self.cbxFromStation.itemText(from_station_index)  #获取索引实际对应的内容
        if from_station != real_from_station:
            QMessageBox.information(self, '警告', '请检查你输入的起始地点是否有误！', QMessageBox.Yes)
            self.cbxFromStation.setFocus()
            return

        to_station_index = self.cbxToStation.currentIndex()
        to_station_code = self.cbxToStation.currentData()
        to_station = self.cbxToStation.currentText()
        real_to_station = self.cbxToStation.itemText(to_station_index)
        if  real_to_station != to_station :
            QMessageBox.information(self, '警告', '请检查你输入的目的地是否有误！', QMessageBox.Yes)
            self.cbxToStation.setFocus()
            return

        date = self.dateEdit.text()
        if date is None or date.strip() is '':
            QMessageBox.information(self, '警告', '出发日期不能为空！', QMessageBox.Yes)
            self.letCode.setFocus()
            return

        num = re.match(r'^\d{4}-\d{2}-\d{2}$', date)
        if num is  None:
            QMessageBox.information(self, '警告', '出发日期格式输入有误！', QMessageBox.Yes)
            return
        '''
        不需要对时间范围以及输出格式进行判断了，因为在初始化的时候设置了QDateEdit的输出格式、以及最小最大日期了
        else:
            # 设置为前导0格式
            nums = date.split('-')
            date = '{}-{}-{}'.format(nums[0].zfill(4), nums[1].zfill(2), nums[2].zfill(2))

            min_date = datetime.datetime.now()
            min_date_s = min_date.strftime('%Y-%m-%d')  # 起始日期
            max_date = min_date + datetime.timedelta(days=31)
            max_date_s = max_date.strftime('%Y-%m-%d')  # 最大日期
            if  date < min_date_s or date >= max_date_s:
                QMessageBox.information(self, '警告', '请检查你的购票日期，是否超出一个月之后....！', QMessageBox.Yes)
                return
        '''
        is_student = False
        if self.rbnStudent.isChecked():
            is_student =True
        # 设置登录用户信息
        ret  = self.ticket.set_from_to_info(date,from_station, to_station,from_station_code,to_station_code,is_student)

        '''获取需要购票的用户名，身份证号'''
        selected_passengers, selected_passenger_card, selected_sex = self.__get_selected_rows__()
        if len(selected_passengers) == 0:
            QMessageBox.information(self,'警告','请在需要购票的乘客前面打勾！',QMessageBox.Yes)
            return

        self.ticket.set_passenger_info(selected_passengers,selected_passenger_card,selected_sex)

        '''获取车次类型'''
        train_type_names = []
        if  self.cbxG.isChecked():
            train_type_names.append('G')
        if self.cbxD.isChecked():
            train_type_names.append('D')
        if  self.cbxZ.isChecked():
            train_type_names.append('Z')
        if  self.cbxT.isChecked():
            train_type_names.append('T')
        if  self.cbxK.isChecked():
            train_type_names.append('K')
        if len(train_type_names) == 0:
            QMessageBox.information(self, '警告', '请选择列车类型！', QMessageBox.Yes)
            return
        self.ticket.set_train_type_names(train_type_names)

        '''获取座位类型'''
        seat_type_names = []
        if self.cbxSpecialSeat.isChecked():
            seat_type_names.append('商务座特等座')
        if self.cbxFirstSeat.isChecked():
            seat_type_names.append('一等座')
        if self.cbxSecondSeat.isChecked():
            seat_type_names.append('二等座')
        if self.cbxHigherSoftSleeper.isChecked():
            seat_type_names.append('高级软卧')
        if self.cbxSoftSleeper.isChecked():
            seat_type_names.append('软卧')
        if self.cbxActionSleeper.isChecked():
            seat_type_names.append('动卧')
        if self.cbxHardSleeper.isChecked():
            seat_type_names.append('硬卧')
        if self.cbxSoftSeat.isChecked():
            seat_type_names.append('软座')
        if self.cbxHardSeat.isChecked():
            seat_type_names.append('硬座')
        if self.cbxNoSeat.isChecked():
            seat_type_names.append('无座')
        if len(seat_type_names) == 0:
            QMessageBox.information(self, '警告', '请选择座位类型！', QMessageBox.Yes)
            return
        self.ticket.set_seat_type_names(seat_type_names)

        '''
        获取出发时间(min)格式和终止时间
        '''
        start_time = self.dtStartTime.text()
        num = re.match(r'^\d{2}:\d{2}$', start_time)
        if num is  None:
            QMessageBox.information(self, '警告', '出发时间(min)输入有误！', QMessageBox.Yes)
            return
        end_time = self.dtEndTime.text()
        num = re.match(r'^\d{2}:\d{2}$', end_time)
        if num is  None:
            QMessageBox.information(self, '警告', '出发时间(max)输入有误！', QMessageBox.Yes)
            return
        self.ticket.set_train_time_quantum(start_time,end_time)

        '''
        检查邮箱
        '''
        email_address = self.letEmailAddress.text()
        if email_address is  not None:
            if not re.match(r'^[0-9a-zA-Z_]{0,19}@[0-9a-zA-Z]{1,13}\.[com,cn,net]{1,3}$',email_address):
                QMessageBox.information(self, '警告', '请检查你的邮箱地址是否有误！', QMessageBox.Yes)
                return
            else:
                self.ticket.receiver = email_address


        if self.btnStartBuy.text() == '开始抢票':
            self.btnStartBuy.setText('停止抢票')
            '''
            开启子线程抢票，并把消息传递给主线程显示
            '''
            #设置当前余票查询次数为0
            self.ticket.__query_count__ = 0
            msg = '{} {} {} -- > {}抢票中...'.format(selected_passengers,date,from_station,to_station)
            self.tbxShowMsg.append(msg)
            self.__thread__ = threading.Thread(target=self.__thread_buying__)
            #这个函数的特性，特性如下：主线程A中，创建了子线程B，并且在主线程A中调用了B.setDaemon(), 这个的意思是，把主线程A设置为守护线程，这时候，要是主线程A执行结束了，就不管子线程B是否完成, 一并和主线程A退出。
            self.__thread__.setDaemon(True)
            # 开始线程
            self.__thread__.start()
            #设置控件不可用
            self.__enable_control__(False)

            '''
            保存信息
            '''
            self.__conf__.set('ticket_info', 'from_station',value=from_station)
            self.__conf__.set('ticket_info', 'to_station',value=to_station)
            self.__conf__.set('ticket_info', 'from_station_index', value = str(from_station_index))
            self.__conf__.set('ticket_info', 'to_station_index', value= str(to_station_index))
            self.__conf__.set('ticket_info', 'date',value=date)
            self.__conf__.set('ticket_info', 'start_time', value=start_time)
            self.__conf__.set('ticket_info', 'end_time', value=end_time)

            self.__conf__.set('user_info', 'email',email_address)
            if  self.rbnOther.isChecked():
                self.__conf__.set('buy_trcket_info', 'special_ticket_type',value='True')
            else:
                self.__conf__.set('buy_trcket_info', 'special_ticket_type', value='False')

            if  self.cbxSpecialSeat.isChecked():
                self.__conf__.set('seat_type', 'special_seat',value='True')
            else:
                self.__conf__.set('seat_type', 'special_seat', value='False')


            if  self.cbxFirstSeat.isChecked():
                self.__conf__.set('seat_type', 'first_seat',value='True')
            else:
                self.__conf__.set('seat_type', 'first_seat', value='False')


            if  self.cbxSecondSeat.isChecked():
                self.__conf__.set('seat_type', 'second_seat',value='True')
            else:
                self.__conf__.set('seat_type', 'second_seat', value='False')

            if  self.cbxHigherSoftSleeper.isChecked():
                self.__conf__.set('seat_type', 'higher_soft_sleeper',value='True')
            else:
                self.__conf__.set('seat_type', 'higher_soft_sleeper', value='False')

            if  self.cbxSoftSleeper.isChecked():
                self.__conf__.set('seat_type', 'soft_sleeper',value='True')
            else:
                self.__conf__.set('seat_type', 'soft_sleeper', value='False')

            if  self.cbxActionSleeper.isChecked():
                self.__conf__.set('seat_type', 'action_sleeper',value='True')
            else:
                self.__conf__.set('seat_type', 'action_sleeper', value='False')

            if  self.cbxSoftSeat.isChecked():
                self.__conf__.set('seat_type', 'soft_seat',value='True')
            else:
                self.__conf__.set('seat_type', 'soft_seat', value='False')

            if  self.cbxHardSleeper.isChecked():
                self.__conf__.set('seat_type', 'hard_sleeper',value='True')
            else:
                self.__conf__.set('seat_type', 'hard_sleeper', value='False')

            if  self.cbxHardSeat.isChecked():
                self.__conf__.set('seat_type', 'hard_seat',value='True')
            else:
                self.__conf__.set('seat_type', 'hard_seat', value='False')

            if  self.cbxNoSeat.isChecked():
                self.__conf__.set('seat_type', 'no_seat',value='True')
            else:
                self.__conf__.set('seat_type', 'no_seat', value='False')

            if  self.cbxG.isChecked():
                self.__conf__.set('train_type', 'train_g',value='True')
            else:
                self.__conf__.set('train_type', 'train_g', value='False')

            if  self.cbxD.isChecked():
                self.__conf__.set('train_type', 'train_d',value='True')
            else:
                self.__conf__.set('train_type', 'train_d', value='False')

            if self.cbxZ.isChecked():
                self.__conf__.set('train_type', 'train_z', value='True')
            else:
                self.__conf__.set('train_type', 'train_z', value='False')

            if self.cbxT.isChecked():
                self.__conf__.set('train_type', 'train_t', value='True')
            else:
                self.__conf__.set('train_type', 'train_t', value='False')

            if self.cbxK.isChecked():
                self.__conf__.set('train_type', 'train_k', value='True')
            else:
                self.__conf__.set('train_type', 'train_k', value='False')

            with open(self.__conf_path__,'w') as fw:
                self.__conf__.write(fw)



    def __update_main_ui__(self,flag,msg):
        '''
        更新UI主界面
        :param flag: 标志位 用来标志传递过来的消息类型   1：表示传递的是车次信息，保存在self.ticket.__trains_table__中    3:：表示用户掉线，需要重新登录  ....
        :param msg: 传递过来的消息
        :return:
        '''
        #显示查询到的车次信息
        #print(self.tbxShowMsg.toPlainText().__len__())
        #清空
        if self.tbxShowMsg.toPlainText().__len__() > 50000:
            self.tbxShowMsg.clear()
        if flag == 1:
            trains_table = self.ticket.__trains_table__
            #查询到车次信息
            if isinstance(trains_table,PrettyTable):
                # 获取表头
                headers = trains_table.field_names

                cols_num = len(headers)
                rows_num = len(trains_table._rows)

                '''
                PyQt5高级界面控件之QTableView（一） : https://blog.csdn.net/jia666666/article/details/81624259
                                                        https://blog.csdn.net/cloveses/article/details/80943496
                pyqt5 QTableView表格复选框案例       : https://www.cnblogs.com/kennyhip/articles/10020711.html
                '''
                trains_modes = QStandardItemModel()
                # 设置水平方向表头
                trains_modes.setHorizontalHeaderLabels(headers)
                for row, row_val in enumerate(trains_table._rows):
                    for col in range(cols_num):
                        '''设置文本'''
                        item = QStandardItem(row_val[col])
                        # 设置单元不可编辑
                        item.setEditable(False)
                        # 设置每个位置的文本值
                        trains_modes.setItem(row, col, item)

                # 实例化表格视图，设置模型为自定义的模型
                self.tableShowTrains.setModel(trains_modes)
                # 更新
                #self.tableShowTrains.update()
            else:
                #没有车次信息，移除内容，保留表头
                if self.tableShowTrains.model() is not None:
                    for i in range(self.tableShowTrains.model().rowCount()):
                        self.tableShowTrains.model().removeRow(i)

        elif flag == 3:
            '''
            用户已经掉线了，需要重新登录
            '''
            if self.ui_login is not None:
                # 加载登陆界面验证码
                # 1、如果自动识别出来验证码则自动登录  不显示登录界面
                # 2、识别不出来则不会尝试登录  不显示登录界面
                self.ui_login.__load_code_image__()
                # 1、没有识别出来，不显示登录界面
                # 2、能识别出来，但是登录失败 则显示登录界面
                if self.ui_login.__user_online__ == False:
                    #  1、如果自动识别出来验证码则自动登录  不显示登录界面
                    #  2、识别不出来则不会尝试登录  不显示登录界面
                    self.ui_login.__load_code_image__()
                    # 1、没有识别出来，不显示登录界面
                    # 2、能识别出来，但是登录失败 则显示登录界面
                    if self.ui_login.__user_online__ == False:
                        #显示登录界面
                        self.ui_login.show()
                        #邮件提醒你已经掉线
                        self.ticket.__email__.send_qq_email(self.ticket.receiver, '12306登录失效，请重新登录！','掉线通知，当前有余票请你速度登陆12306官网购票！')

        elif flag == 4:
            if self.ui_login.__user_online__ == True:
                msg = msg + '\n自动登录成功！'

        #正在抢票过程中输出消息
        #if self.btnStartBuy.text() == '停止抢票':
        # 追加文本消息
        self.tbxShowMsg.append(msg)

    def __enable_control__(self,flag):
        '''
        设置控件可用还是不可用
        :param flag: True 可用 False 不可用
        :return:
        '''
        if isinstance(flag,bool):
            self.cbxFromStation.setEnabled(flag)
            self.cbxToStation.setEnabled(flag)
            self.letEmailAddress.setEnabled(flag)
            self.dateEdit.setEnabled(flag)

            self.gbxSeatType.setEnabled(flag)
            self.gbxTrainType.setEnabled(flag)
            self.gbxTicketType.setEnabled(flag)

            self.tablePassenger.setEnabled(flag)



    def __thread_query_tickets__(self,thread_num):
        '''
        使用线程池来查询余票 这里没用到，代码也没写完
        :param thread_num: 线程个数  https://blog.csdn.net/ljp1919/article/details/70767486
        :return:
        '''
        # 创建线程池
        task_pool = threadpool.ThreadPool(thread_num)

        # list 表示任务 一个ip对应一个任务
        tasks = threadpool.makeRequests(self.ticket.__query_tickets__(), self.__ips__)
        # 将任务放到线程池中
        [task_pool.putRequest(req) for req in tasks]
        while (True):
            sleep(0.5)
            #等待查询到余票，或者任务执行完毕
            if self.ticket.__have_ticket__ == True or task_pool.poll():
                break


    def __thread_buying__(self):
        '''
        线程函数  开始抢票
        :return:
        '''
        '''1、加载购票页面'''
        self.ticket.__init_buy_page__()

        #保存连续订单检查失败的次数，如果超过3次，则自动退出，购票
        submit_order_failed_count = 0

        #开启自动查询购票
        while (self.btnStartBuy.text() == '停止抢票'):
            try:

                '''2、登录之后、查询票信息 有余票？'''
                to = time()   ##获取秒级时间戳

                 #每将近十分钟使用登录的状态去加载一下购票页面
                if  int(to) % 580 == 0:
                    self.ticket.__init_buy_page__()

                have_tickets,msg = self.ticket.__query_tickets__()

                msg = msg + '{}秒'.format(round(time() - to,2))
                self.__update_main_ui_date__.emit(1,msg)
                #保存日志
                logging.info(msg)

                #有余票
                if  have_tickets == True:

                    '''3、点击预定后，首先检查检查用户是否登录,没有登录则会自动登录'''
                    if self.ticket.__check_user__():
                        self.__update_main_ui_date__.emit(2, '用户检查成功......')
                    else:
                        '''
                        需要重新登录程序 在主线程中重新登录 循环等待主线程登录成功
                        '''
                        self.ui_login.__lock__.acquire()
                        #设置为离线状态
                        self.ui_login.__user_online__ = False
                        self.ui_login.__lock__.release()

                        self.__update_main_ui_date__.emit(3, '用户检查失败,重新登录中......')

                        #等待用户上线成功
                        while(self.ui_login.__user_online__ == False):
                            self.__update_main_ui_date__.emit(4, '等待用户上线中......')
                            sleep(5)

                    # 开始进行订单检查 预订成功？ 如果在23-11点禁止买票、选择车次、座位失败等均返回1，有未处理订单返回-1
                    state = self.ticket.__submit_order_request__()
                    if  state != 0:
                        self.__update_main_ui_date__.emit(5, '订单检查失败......')
                        #异常：有未处理的订单
                        if state == -1:
                            self.__update_main_ui_date__.emit(5, '你有未处理的订单，请登录12306尽快取消，开始停止抢票...')
                            self.ticket.__email__.send_qq_email(self.ticket.receiver, '你有未处理的订单，请登录12306尽快取消，开始停止抢票...','抢票停止通知')
                        # 异常：其他类型异常
                        else:
                            submit_order_failed_count += 1
                            if submit_order_failed_count >=3:
                                self.__update_main_ui_date__.emit(5, '订单检查连续三次失败，开始停止抢票...')
                                self.ticket.__email__.send_qq_email(self.ticket.receiver, '订单检查连续三次失败，清尽快登录12306检查失败原因，开始停止抢票...','抢票停止通知')
                        break
                    else:
                        # 说明订单成功，需要确认订单即可  此时进入了订单页面，即选择乘客
                        self.__update_main_ui_date__.emit(6, '订单检查成功......')
                        submit_order_failed_count = 0
                        # 初始化订单数据,获取到REPEAT_SUBMIT_TOKEN,key_check_isChange,leftTicketStr
                        self.ticket.__init_dc__()
                        # 获取该用户下的乘车人信息
                        self.ticket.__get_passenger__()

                        '''4、进行订单提交'''
                        if self.ticket.__check_order_info__() == False:
                            if len(self.ticket.passengers) > 1:
                                self.__update_main_ui_date__.emit(9, '订单提交失败,您选择了多位乘车人，但本次列车余票不足......')
                            else:
                                self.__update_main_ui_date__.emit(9, '订单提交失败,可能是列车余票不足......')
                        else:
                            # 订单提交成功，确认订单
                            # 查询订单队列余票
                            self.ticket.__get_queue_count__()
                            self.__update_main_ui_date__.emit(8, '订单提交成功......')

                            '''5、最后一次确认订单'''
                            # if True:
                            if self.ticket.__confirm_single__() == False:
                                self.__update_main_ui_date__.emit(9, '确认订单失败,可能是列车余票不足......')
                            else:
                                self.__update_main_ui_date__.emit(10, '确认订单成功......')
                                sleep(5)

                                # 输出购买的车次信息
                                train_info = self.ticket.__trains_data_filter_list__[self.ticket.__preorder_train_index__]

                                #检查提交的订单是否出现在未完成订单中
                                state =  self.ticket.__query_order_no_complete__(train_info['trips'],self.ticket.dtime,self.ticket.passengers)
                                #出票成功
                                if state == 0:
                                    self.__update_main_ui_date__.emit(11, '恭喜您,购票成功，请在30分钟内登录官网支付......')
                                    content = '恭喜乘客{}预订的{} {} {} {}  {}购票成功'.format(self.ticket.passengers,  # 乘客名
                                                                                    self.ticket.dtime,  # 日期
                                                                                    train_info['trips'],  # 车次
                                                                                    train_info['from_to_station_name'],
                                                                                    # 起始->目的
                                                                                    train_info['start_arrive_time'],
                                                                                    # 起始->时间
                                                                                    self.ticket.__preorder_train_seat_name__)  # 座位类型
                                    self.ticket.__email__.send_qq_email(self.ticket.receiver, content)
                                    self.__update_main_ui_date__.emit(12,content)
                                    break
                                #出票失败
                                elif state == -1:
                                    self.__update_main_ui_date__.emit(13, '很遗憾,出票失败,请登录12306取消失败订单，重新购票......')
                                    self.ticket.__email__.send_qq_email(self.ticket.receiver,'出票失败,请登录12306取消失败订单，重新购票...', '出票失败通知')
                                #没有购买到票
                                else:
                                    self.__update_main_ui_date__.emit(13, '很遗憾,购票失败，持续刷新中......')

            except Exception as e:
                #print("发生异常:{}，继续购买...".format(e))
                self.__update_main_ui_date__.emit(11, '购票异常{0}......'.format(e))
                sleep(2)
                '''重新加载购票页面'''
                self.ticket.__init_buy_page__()

        #停止抢票了
        self.__update_main_ui_date__.emit(12, '已经停止抢票了...\n')