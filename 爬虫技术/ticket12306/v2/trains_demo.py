# -*- coding: utf-8 -*-
"""
Created on Thu Dec 27 09:19:05 2018

@author: Administrator
"""
'''
需要安装：pip install prettytable
'''
from prettytable import PrettyTable
from station import get_station_name_by_code

class TrainsDemo(object):
    '''
    余票信息显示：数据处理+显示
    '''
    def __init__(self, raw_trains, train_type_names,numbers,start_time,end_time):
        '''
        初始化
        
        args:
             raw_trains：查询到的原始车次信息
             train_type_names list类型   G-高铁/城际  D-动车 Z-直达 T-特快  K-快速  None-所有
             numbers：list类型 保持指定的车次  None：不指定
             start_time：车次出发的最小时间
             end_time：车次出发的最大时间
        
        余票查询：https://kyfw.12306.cn/otn/leftTicket/queryA?leftTicketDTO.train_date=2018-12-29&leftTicketDTO.from_station=WXH&leftTicketDTO.to_station=NIW&purpose_codes=ADULT
                  无锡   - > 遂宁  2018-12-29  有三条记录
                  返回json字符串  其中包含键值为result的数组                  
                  0：XKxRPrejA3sL43LGUNTWR2qc3YhvO0QhH%2BAuA6Ix9nOpRDV%2Fq7XytKC24hEYYwCJd3zq6l166zNq%0AtPI9qU3uuuoLeOQvHYXvD%2BT7DVDf7ldw%2Ba%2B%2Fm9goxLQpA3woqfa82niwbZxdo%2FpIOpKRrJqWSIEP%0APtxb9ZkqVI7Bcv2qC13md%2BLuXl3eg0qiICXkPLZhqRTYmJGe9JfpLI6CUj1WTRpAHH2LbPiCHP8w%0AhxTyXVb8PkDPTQAgziMGdF4rI%2BhGSRLyezT8LRo%2FFSBrYqZBT%2F%2FrolF4rRA4sITtdw%3D%3D
                     |预订           [1]
                     |5l0000D35273   [2] 车次编号 train_no 
                     |D352           [3] 火车代码 车次
                     |AOH            [4]      
                     |ICW            [5]
                     |WXH            [6] 出发地code
                     |NIW            [7] 目的地code 
                     |07:06          [8] 出发时间   
                     |19:27          [9] 到达时间
                     |12:21          [10] 历时 
                     |Y              [11]
                     |hfEtg930T0U%2FOIoFRB2kv8IdSz84dzoz2BRu6jnIIc6VyC3F   [12]  leftTicket
                     |20181229       [13] 日期
                     |3              [14] 
                     |H2             [15] train_location
                     |04             [16]
                     |20             [17]
                     |0              [18] 
                     |0              [19]
                     |               [20]
                     |               [21] 高级软卧
                     |               [22]
                     |               [23] 软卧
                     |               [24] 软座
                     |               [25]
                     |无             [26] 无座
                     |               [27]    
                     |               [28] 硬卧
                     |               [29] 硬座
                     |无             [30]
                     |2              [31] 一等座
                     |               [32] 商务/特等座
                     |               [33] 动卧
                     |O0M0O0         [34]
                     |OMO            [35]
                     |0              [36]
                     |0             [37]
                  1："bJvgWng4bhRNz7UP2ILtKkDh4vLwVHUTXOIvPpm3Igwntr3QDzZDprV7wyjjzxU5gmM5D8O8ip29%0AZFYzD%2F1PZVyOWykmGW5E8XqKzBKYEBC93kgOmRl3whc8mIqaP5HEmliEYdFCICCKcshEWLID2NK5%0AG%2BNmBgZDyRjCwlZhCyHGLRr4rJsceiVtWJqbu2hWtnsv89fjWoDiuXf3PXSKZoLAZQeUN%2FxmKHER%0Avh2vPiUMC%2BdU1Buc1CSDuehMZzBbE6oMJ6d1MNOTibLhhnCnf0oGYtq4jUwCR%2FdkvqUkspfOqhdS%0APhcY8w%3D%3D|预订|550000K29010|K290|SHH|CDW|WXH|NIW|10:31|16:13|29:42|Y|WxRq%2FvIUILZ5XUZxZ2uR%2BkRvOH959QDrnK6CKbmRk%2B8bpdJ7JmZUw%2FcjoDs%3D|20181229|3|H6|03|23|0|0||||1|||有||无|无|||||10401030|1413|1|0"
                  2："SgctRuzaTUcofn%2B1JOAaK01T3%2F38Y3mZKitT5toNX8B1QJRz%2FafLylXCfPOrt%2BVrz5xzdL1IBmF0%0A18lyLczxIlDvJHJonyPKsj96rFJUuJRxp8us4NG6M7YzqUdNzgYZNuhYxXZJ0Qe7Z5TzI9YsPKhG%0AFOZSes%2Fmrd0qwAJx7ydR2h%2FUrTBa3ioBdOoLmoHbPBaiP2qDma%2FFsoGlfJMAyFpPHvozZJW76Np9%0Ak84PzzkM999r96jyA%2BNVgdbdvBgV7W9laXwplhjZ399klZhE2hg%2BioJySgrKPJPPOFW%2Fr0P8HhlX%0AFUCcVg%3D%3D|预订|55000K115651|K1156|SHH|CDW|WXH|NIW|13:04|15:08|26:04|Y|8zpbJFaLOmleax%2FLslc6p9ChCclyXNhNLCcp2sTTD2SVF0Fqlahqv%2BXL%2FRw%3D|20181229|3|H6|04|23|0|0||||无|||无||无|有|||||10401030|1413|0|0"
        '''
        #self.headers = '车次 车站 时间 历时 商务/特等座 一等座 二等座 高级软卧 软卧 动卧 硬卧 软座 硬座 无座 其他'.split()
        self.headers = '车次 车站 时间 历时 商务座特等座 一等座 二等座 高级软卧 软卧 动卧 硬卧 软座 硬座 无座'.split()
        self.raw_trains = raw_trains
        self.train_type_names = train_type_names
        self.numbers = numbers
        self.start_time = start_time
        self.end_time = end_time
	
    def __get_from_to_station(self, data_list):
        '''
        获取出发和到达站
        '''
        self.from_station = data_list[6]               #出发地
        self.to_station = data_list[7]                 #目的地
        self.from_to_station = get_station_name_by_code(self.from_station) + '-->' + get_station_name_by_code(self.to_station)
        return self.from_to_station
	
    
    def __get_start_arrive_time(self, data_list):
        '''
        获得出发和到达时间
        '''
        self.start_arrive_time = data_list[8] + '-->' + data_list[9]
        return self.start_arrive_time
	
    
    def __parse_trains_data(self, data_list):
        '''
        解析trains数据(与headers依次对应)
        '''
        return {
          'trips': data_list[3],         #车次
			'from_to_station_name': self.__get_from_to_station(data_list),   #起始地 --> 目的地
			'start_arrive_time': self.__get_start_arrive_time(data_list),    #出发时间 --> 到达时间
			'duration': data_list[10],                                       #历时
			'商务座特等座': data_list[32] or '--',                            #商务特等座      
			'一等座': data_list[31] or '--',                                 #一等座
			'二等座': data_list[30] or '--',                                 #二等座
			'高级软卧': data_list[21] or '--',                               #高级软卧
			'软卧': data_list[23] or '--',                                   #软卧  
			'动卧': data_list[33] or '--',                                   #动卧
			'硬卧': data_list[28] or '--',                                   #硬卧
			'软座': data_list[24] or '--',                                   #软座
			'硬座': data_list[29] or '--',                                   #硬座
			'无座': data_list[26] or '--',                                   #无座
			#'others': data_list[34] or '--'                                 #其他
			}
        
	
    def __selected(self, train_data):
        '''
        # 判断是否满足筛选条件
        '''
        trips = train_data[3]          #车次
        initial = trips[0].upper()    #获取车次类型  G-高铁/城际  D-动车 Z-直达 T-特快  K-快速            
        time = train_data[8]
        '''没有指定车次'''
        if self.numbers is None:
            #如果指定了车型  过滤掉不符合指定车型的列车
            if self.train_type_names is not None:
                if  initial not in self.train_type_names:
                    return False
            #车型符合，判断时间
            if self.start_time <= self.end_time:
                return     (time>= self.start_time and time <= self.end_time)
            else:
                return (time >= self.start_time or time <= self.end_time)
        else:
            '''如果指定了车次 则忽略车型、出发时间'''
            return (trips in self.numbers)
	
    def get_trian_data(self):
        '''
        对列车原始数据进行分析处理
        trains_data_list：把每一行数据使用'|'返回  并返回一个list 每一行对应一个车次信息  [['bJvgWng4bhRNz7UP2ILtKkDh4vLwVHUTXOIvPpm3Igwntr3QDzZDprV7wyjjzxU5gmM5D8O8ip29','D305',...]]
        trains_data_filter_list：过滤一些信息，并返回一个list 每一行对应一个车次信息  [{'trips':'D305',....},{'trips':'D305',....}]
        trains_table：trains_data_filter_list数据转换为PrettyTable类型
        '''
        #创建表头
        trains_table = PrettyTable()
        trains_table._set_field_names(self.headers)

        trains_data_list = []
        trains_data_filter_list = []

        #遍历每一行原始数据  即每一辆列车
        for train in self.raw_trains:
            #通过'|'分割
            train_data = train.split('|')            
            #判断是否为需要显示的列车类型
            if self.__selected(train_data):                                
                #保存原始车次信息
                trains_data_list.append(train_data)
                values_row = []
                #解析返回dict
                train_data = self.__parse_trains_data(train_data)
                #保解码之后的车次信息(过滤后的)  
                trains_data_filter_list.append(train_data)
                
                #添加数据到表中
                for val in train_data.values():
                    values_row.append(val)    
                    
                #插入数据
                trains_table.add_row(values_row)
        if len(trains_data_list) == 0:
            trains_data_list = None
            trains_data_filter_list = None
            trains_table = None
        return trains_data_list,trains_data_filter_list,trains_table
