# -*- coding: utf-8 -*-
"""
Created on Thu Dec 27 15:08:45 2018

@author: Administrator
PyQt5使用：参考https://www.jianshu.com/p/094928ac0b73
推荐pyqt使用：https://blog.csdn.net/qq_31050167/article/details/78924020  很详细
"""

'''
无界面 12306抢票主程序
'''

from demo_ticket import BuyTickets

if __name__ == '__main__':
    flag = 2
    
    if flag == 1:
         #登录名
         uname = 'xxxxxx'
         #密码
         upwd = 'xxxxxx' 
    
         #用户
         passengers = ['杨德亮(学生)','郑洋','刘燕']
         passenger_cards = ['500228199304xxxxxx','340621199411xxxxxx','51152319970xxxxxx']
    
         passengers = ['杨德亮(学生)','刘燕']
         passenger_cards = ['340621199411xxxxxx','51152319970xxxxxx']
    
         sexs = ['M','W']
    
         #票类型  按重要性依次排序
         seat_type_names = ['二等座','一等座','无座']   #None表示所有类型票 
    
         #车次
         numbers = None #['D2212','D952','D2216','D2206','D636','D352']   #None表示所有
         train_type_names = ['D']
    
         #日期
         dtime = '2019-01-29'    
         start = '无锡'
         end = '重庆'
    
    elif flag == 2:
         uname = '17826110096'
         upwd = 'xxxxxx'
    
         passengers = ['刘燕']
         passenger_cards = ['511523199708xxxxxx']
    
         sexs = ['M']
    
          #票类型  按重要性依次排序
         seat_type_names = ['二等座','一等座']  #None表示所有类型票             
         #车次        
         numbers = None # ['D3070','D3024','D3062','D3066','D638','D354','D2208'] 
    
         train_type_names = ['D']
    
         dtime = '2019-01-31'    
         start = '无锡'
         end = '重庆'
    
    if flag == 3:
         uname = 'xxxxx'
         upwd = 'xxxxxx'
    
         passengers = ['林京京(学生)']
         passenger_cards = ['320305199404xxxxxx']
    

         sexs = ['M']
    
          #票类型  按重要性依次排序
         seat_type_names = ['硬卧','硬座']   #None表示所有类型票             
         #车次        
         numbers = ['K1182','T136','K8432']
    
         dtime = '2019-01-27'    
         start = '无锡'
         end = '徐州'
    
         #车类型    
         train_type_names = ['K','T']
    
    
    
    ticket = BuyTickets(uname,
                         upwd,
                         passengers,
                         passenger_cards,
                         sexs,
                         dtime,
                         start,
                         end,
                         train_type_names= train_type_names,
                         seat_type_names=seat_type_names,
                         numbers=numbers)
    ticket.buy()

