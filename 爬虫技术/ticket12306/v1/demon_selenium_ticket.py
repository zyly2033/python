# -*- coding: utf-8 -*-
"""
Created on Tue Dec 25 09:01:44 2018

@author: Administrator
"""

'''
抢票程序
使用python + selenium
模拟浏览器行为

https://www.jianshu.com/p/a16b0b258c3b
https://blog.csdn.net/qq_23279427/article/details/79071915
https://blog.csdn.net/An_Feng_z/article/details/78631290
selenium.browe是一个开源工具，通过python自动化测试web，即通过电脑自动操作网页

安装步骤：
1、pip install selenium  3.0以上版本

2、安装浏览器的驱动，selenium的brower默认调用的是火狐，所以使用chrome的话需要在初始化brower时指定driver_name="chrome"参数
   建议都明确指定浏览器
   
   火狐驱动下载：
  （1）、下载geckodriver.exe：下载地址：https://github.com/mozilla/geckodriver/releases请根据系统版本选择下载；（如Windows 64位系统）
   (2）、下载解压后将getckodriver.exe复制到Firefox的安装目录下，
    如（C:\Program Files\Mozilla Firefox），并在环境变量Path中添加路径：C:\Program Files\Mozilla Firefox
    
  谷歌驱动是：ChromeDriver 
  
  注意驱动版本要对应，不然可能打不开浏览器，或者未知错误
  
  安装版本问题：https://blog.csdn.net/weixin_42369973/article/details/82715721
  可以使用：selenium 3.14 +火狐52 + geckodriver V 0.11.1
  
3、pip install beautifulsoup4
   pip install lxml
   pip install html5lib

'''
from selenium import webdriver
from selenium.webdriver.support.select import Select
from time import sleep
from PIL import Image
import json
import random
import requests
from bs4 import BeautifulSoup
import matplotlib.pyplot as plt
from prettytable import PrettyTable


class BuyTickets(object):
    
    def __init__(self,uname,upwd,passenger,dtime,start,end,numbers=None,seat_types=['硬座','无座'],print_log=False):
        '''
        number：车次 list类型 None表示所有车次  
        seat_types：座位类型  list类型  None表示所有 
        '''
        #初始化字典
        self.__init_dict()
        
        self.username = uname
        self.userpwd = upwd
        #车次，0代表所有车次 依次从上到下，1代表第一个有余票的车次，以此类推
        self.numbers = numbers
        #乘客名
        self.passengers = passenger
        #起始地 目的地
        self.start = self.__get_city(start)
        self.start_name = start
        self.end = self.__get_city(end)
        self.end_name = end
        #日期
        self.dtime = dtime

        self.print_log = print_log
        #座位索引                
        self.seat_type_indexs = []
        if  seat_types is None:
            self.seat_type_indexs = [9,7,10,8,3,6,5,4,3,2,1,11]
        else:
            for seat_type in seat_types:
                index = self.__get_seat_type_index(seat_type)
                self.seat_type_indexs.append(index)
             
        #保存验证码输入坐标
        self.answer = None
            
        #用户登录初始页面   get
        self.login_url = "https://kyfw.12306.cn/otn/login/init"      
        #登录验证码  get
        self.code_url =  "https://kyfw.12306.cn/passport/captcha/captcha-image?login_site=E&module=login&rand=sjrand&{}".format(random.random())
        #校验验证码页面  post
        self.check_code_url = "https://kyfw.12306.cn/passport/captcha/captcha-check"
        #校验用户信息压面  post
        self.check_user_url = "https://kyfw.12306.cn/passport/web/login"
        #登录成功后 初始页面 
        self.initMy_url = "https://kyfw.12306.cn/otn/index/initMy12306"
        #登陆成功后 余票查询
        self.ticket_url = "https://kyfw.12306.cn/otn/leftTicket/init"
    
        #获取火狐浏览器对象 
        self.browser = webdriver.Firefox(executable_path = r'D:\FF\geckodriver.exe')   
        
        # 创建一个网络请求session实现登录验证  由于selenium不能发送post请求，所以登录都通过这个实现
        self.session = requests.session()

        
    def __init_dict(self):
        #座位类型
        self.seat_types_dict = {'商务座特等座':1,
                          '一等座':2,
                          '二等座':3,
                          '高级软卧':4,
                          '软卧':5,
                          '动卧':6,
                          '硬卧':7,
                          '软座':8,
                          '硬座':9,
                          '无座':10,
                          '其他':11}
                          
        
        
        #拷贝起始地的cookie值，我把几个常用的城市拷出来，放到了字典中：
        self.cities_dict= {'成都':'%u6210%u90FD%2CCDW',
             '重庆':'%u91CD%u5E86%2CCQW',
             '北京':'%u5317%u4EAC%2CBJP',
             '广州':'%u5E7F%u5DDE%2CGZQ',
             '杭州':'%u676D%u5DDE%2CHZH',
             '宜昌':'%u5B9C%u660C%2CYCN',
             '郑州':'%u90D1%u5DDE%2CZZF',
             '深圳':'%u6DF1%u5733%2CSZQ',
             '西安':'%u897F%u5B89%2CXAY',
             '大连':'%u5927%u8FDE%2CDLT',
             '武汉':'%u6B66%u6C49%2CWHN',
             '上海':'%u4E0A%u6D77%2CSHH',             
             '南京':'%u5357%u4EAC%2CNJH',
             '无锡':'%u65E0%u9521%2CWXH',
             '宿州':'%u5BBF%u5DDE%2COXH',
             '遂宁':'%u9042%u5B81%2CNIW',
             '淮北':'%u6DEE%u5317%2CHRH',
             '阜阳':'%u91CD%u5E86%2CCQW',
             '合肥':'%u5408%u80A5%2CHFH'}
        
        self.headers = {
                'Accept-Encoding': 'gzip, deflate, br',
                'Accept-Language': 'zh-CN,zh;q=0.8',
                'X-Requested-With': 'XMLHttpRequest',
                'Origin': 'https://kyfw.12306.cn',
                'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:52.0) Gecko/20100101 Firefox/52.0',
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            }
        
    def __get_seat_type_index(self,seat_type):
        '''
        获取座位类型对应的索引值
        
        args：
            seat_type：座位类型
        '''    
        if self.seat_types_dict.__contains__(seat_type):
            return  self.seat_types_dict[seat_type]
        else:
            print('你输入的座位类型有误，请重新检测....')
            exit()  
            
            
    def __get_city(self,city_name):
        '''
        获取地址对应的cookies值
        
        args：
            city_name：城市名
        '''        
        if self.cities_dict.__contains__(city_name):
            return  self.cities_dict[city_name]
        else:
            print('你输入的城市目前还不支持抢票，请输入其他城市....')
            exit()  
            
            
    def xml_to_dict(self,xml_data):
        """
        xml转换为字典
            param xml_data:
        
        """
        soup = BeautifulSoup(xml_data, features='xml')
        xml = soup.find('HashMap')
        if not xml:
            return {}
        # 将 XML 数据转化为 Dict
        data = dict([(item.name, item.text) for item in xml.find_all()])
        return data

    def __get_img(self):
        '''        
        在你登录12306网站的时候，网页会get一个验证码图片，这个步骤封装方法如下：
        '''
        #获取验证码图片
        #self.browser.get(self.code_url)
        response = self.session.get(url=self.code_url,headers=self.headers,verify=False)        
        sleep(0.5)
        
        #把验证码保存到本地
        with open('img.jpg','wb') as f:
            #f.write(self.browser.page_source)
            f.write(response.content)
            
        try:
            img = Image.open('img.jpg')
            # 展示验证码图片，会调用系统自带的图片浏览器打开图片，线程阻塞
            #im.show()
            # 关闭，只是代码关闭，实际上图片浏览器没有关闭，但是终端已经可以进行交互了(结束阻塞)
            #im.close()
            plt.figure(figsize=(6,6))
            plt.imshow(img)
            plt.show()
        except:
            pass
        #=======================================================================
        # 根据打开的图片识别验证码后手动输入，输入正确验证码对应的位置，例如：2,5
        # ---------------------------------------
        #         |         |         |
        #    0    |    1    |    2    |     3
        #         |         |         |
        # ---------------------------------------
        #         |         |         |
        #    4    |    5    |    6    |     7
        #         |         |         |
        # ---------------------------------------
        #=======================================================================
        

    def __verify(self,):
        '''
        开始校验  
        
        args：
            click_list：选中的图片如[1,2,3,4]
                 元素范围从1-8
        '''        
        try:
            captcha_solution = map(int,input('请输入验证码位置，以","分割例如 2,5：').split(','))  
        except Exception  as e:
            captcha_solution =[1,2,5]
        
        code = ['35,35', '105,35', '175,35', '245,35', '35,105', '105,105', '175,105', '245,105']
        verify_list = []
        for a in captcha_solution:
            verify_list.append(code[int(a)-1])
            
        self.answer = ','.join(verify_list)
        data = {
            'answer': self.answer,        #验证码对应的坐标，两个为一组，跟选择顺序有关，有几个正确的，输入几个
            'login_site': 'E',         #固定的
            'rand': 'sjrand'          #固定的            
        }
        #提交验证码，开始校验  取验证码的验证结果
        #url =self.check_code_url +  '?answer={0}&rand={1}&login_site={2}'.format(data['answer'],data['rand'],data['login_site'])

        #selenium不能发送post请求，所以使用requsts库
        response = self.session.post(url=self.check_code_url,data=data,headers=self.headers,verify=False)        
        #self.browser.get(url=url)
        try:
            #返回的是json字节
            #Gets the source of the current page.  json字符串解码成python对象
            #dic = json.loads(self.browser.page_source)
            dic = json.loads(response.content)
            print('验证码校验返回消息：',dic)
        except:
            #print('验证码解析失败')
            return False
        result_code = dic['result_code']                 
        
        if str(result_code) == '4':            
            return True
        else:          #5失败  7过期            
            return False
   
        
    def __login(self):
     
        self.browser.get(self.login_url)
        
        
        #存放登录前后的cookies
        self.cookies = {'before':'','after':''}
        self.cookies['before'] = self.browser.get_cookies()
        print('登陆前：')
        for cookie in self.cookies['before']:
            print('%s -> %s' % (cookie['name'], cookie['value']))
        

        #  登录系统，循环尝试登录，登录成功，就会抛异常，跳出循环继续执行
        while True:
            try:
             
                
                #获取登录用户名元素                
                username = self.browser.find_element_by_id('username')                           
                #获取登录密码元素
                password = self.browser.find_element_by_id('password')                   
                #输入用户名和密码
                username.clear()  
                username.send_keys(self.username)              
                password.clear()  
                password.send_keys(self.userpwd)      
                
                input('请输入：')
            
                
                #提交登录     
                login_btn = self.browser.find_element_by_id('loginSub')     
                login_btn.click()
                sleep(2)         
                
                
                
            except:
                #cookies = self.browser.get_cookies()          
                print('登录成功！')
                #存放登录前后的cookies
                self.cookies['after'] = self.browser.get_cookies()
                print('登陆后:')
                for cookie in self.cookies['after']:
                    print('%s -> %s' % (cookie['name'], cookie['value']))
                break
        

        
  
    def buy(self):
        '''
        开始买票
        '''
        #用户登录 这里使用request模块登录  
        self.__login()

                                
        #登录成功后，跳转到余票查询页面
        self.browser.get(self.ticket_url )     
        
        sleep(2) 

        print('购票页面开始，余票查询中...')
        # 加载查询信息  注意查询余票时，http请求时，把起始地、终止地、日期通过cookie传递到服务器
        self.browser.add_cookie({'name':'_jc_save_fromStation','value':self.start})
        self.browser.add_cookie({'name':'_jc_save_toStation','value':self.end})
        self.browser.add_cookie({'name':'_jc_save_fromDate','value':self.dtime})
        
        #必须
        self.browser.refresh()
        
        count = 0
    

        #如果一直停留在余票查询页面，点击预订会跳出该循环
        while self.browser.current_url == self.ticket_url:            
            
            try:
            
                #开始查询
                self.browser.find_element_by_id('query_ticket').click()                
                
                count += 1
                
                print('\n第%d次点击查询...' % count)                    
                sleep(0.15)
            
                #获取所有列车
                trains = self.browser.find_elements_by_id('queryLeftTable')
                if trains is not None:
                    trains = trains[0].find_elements_by_tag_name('tr')      
                    
                #没有直达列车
                if trains is None:
                    pass
                else:                        
                    
                    '''保存指定车次的列车对象(要求是可以预定的，即有余票的)'''
                    available_trains = []
                    
                    #保存有余票的"预订"超链接对象
                    pre_order = None
                    
                    #遍历每一辆列车
                    for index in range(0,len(trains),2):                                                                                                                
                        
                        #获取车次名字
                        train_name = trains[index].find_element_by_class_name('number').text    
                            
                        #只预订指定车次
                        if self.numbers is not None:
                        
                            #print(train_name)
                            #查询到指定车次
                            if train_name in self.numbers:           
                            
                                try:
                                    #如果有余票，则保存该车次对象
                                    pre_order = trains[index].find_element_by_link_text('预订')
                                    if pre_order is not None:  
                                        available_trains.append(trains[index])
                                except Exception as e1:
                                    if self.print_log:
                                        print('车次{0}没有余票了，继续尝试....'.format(train_name))        
                                    pass
                                    
                        #所有车次
                        else:
                                try:
                                    #如果有余票，则保存该车次对象
                                    pre_order = trains[index].find_element_by_link_text('预订')                                    
                                    if pre_order is not None:  
                                        if self.print_log:
                                            print('车次{0}还有余票，尝试预订中....'.format(train_name)) 
                                        available_trains.append(trains[index])
                                except Exception as e1:
                                    if self.print_log:
                                        print('车次{0}没有余票了，继续尝试....'.format(train_name))   
                                    pass
                                
                    if len(available_trains) == 0:
                        print('{0} {1}->{2}没有余票了,刷新中....'.format(self.dtime,self.start_name,self.end_name))
                    else:
                        '''遍历每一种座位类型是否有余票  第一层循环为座位类型，第二层循环为有余票的车次'''
                        pre_order = None
                        
                        for seat_type_index in self.seat_type_indexs:
                        
                            #获取座位类型名
                            seat_type = list(self.seat_types_dict.keys())[list(self.seat_types_dict.values()).index(seat_type_index)]
                                
                                
                            #遍历每一辆有余票的车次是否有该座位类型的余票
                            for train in available_trains:
                            
                                #获取车次名字
                                train_name = train.find_element_by_class_name('number').text    
                                #判断是否有余票
                                current_td = train.find_elements_by_tag_name('td')[seat_type_index]                    
                                                          
                                if current_td.text == '--':
                                    if self.print_log:
                                        print('车次{0}无{1}出售，已结束当前刷票，清重新开启！'.format(train_name,seat_type))                                            
                                    if len(self.seat_type_indexs) <= 1:
                                        exit()
                                        
                                elif current_td.text == '无':
                                    if self.print_log:
                                        print('车次{0}{1}没有余票了，继续尝试....'.format(train_name,seat_type))                            
                                    
                                else:                                
                                    print('车次{0}{1}余票预定中....'.format(train_name,seat_type))
                                    #有票，预订
                                    #获取所有可预订的车次
                                    pre_order = train.find_element_by_link_text('预订')
                                    if pre_order is not None:                                            
                                        #点击预订
                                        pre_order.click()                                                
                                        break                
                                       
                            #跳出查询循环  时间不能太短，要等到浏览器跳转到预订页面
                            if pre_order is not None: 
                                sleep(0.6)
                                break
                                
                        
            except Exception as e:
                print(e)
                print('预订失败，重新尝试....')
                
    
        while True:    
            try:
                print('\n开始预订...')  
                print('开始选择用户...')
                for name in self.passengers: 
                    #https://blog.csdn.net/huilan_same/article/details/52541680
                    #https://blog.csdn.net/azsx02/article/details/79032329
                    #通过xpath语法//span[contains(text(),”{}”)]定位到带有当前name的元素。这里格式化字符串时，{}要加双引号再通过/.. 找到父元素。
                    
                    print(self.browser.find_element_by_xpath( '//label[contains(text(),"{}")]'.format(name)).text)
                    ele = self.browser.find_element_by_xpath( '//label[contains(text(),"{}")]/..'.format(name))
                    cbx = ele.find_element_by_tag_name('input')
                    
                    #勾选姓名
                    cbx.click()                                    
                    sleep(0.2)
                    
                    #学生票?
                    if name[-1] == ')':
                        self.browser.find_element_by_id('dialog_xsertcj_ok').click()
                    
                '''
                选择座位类型  https://www.cnblogs.com/imyalost/p/7846653.html
                '''           
                #选择每一行  
                for  row_num in range(1,len(self.passengers)+1):
                    #获取该行select标签对象
                    id_name = 'seatType_' + str(row_num)
                    select = self.browser.find_element_by_id(id_name)
                    options = select.find_elements_by_tag_name('option')                    
                    
                    find_flag = False
                
                    #选择自己想要的座位类型
                    for seat_type_index in self.seat_type_indexs:    
                        #获取座位类型名
                        seat_type = list(self.seat_types_dict.keys())[list(self.seat_types_dict.values()).index(seat_type_index)]     
                                                                    
                        #获取可选座位类型
                        for index,option in enumerate(options):
                            print(option.text)
                            #是自己想要的座位类型？
                            if seat_type in option.text:
                                Select(select).select_by_index(index)
                                find_flag = True
                                break
                        if find_flag:
                            break
                
                print('提交订单...')
                self.browser.find_element_by_id('submitOrder_id').click()
                sleep(0.2)
                
                print('确认选座...')
                self.browser.find_element_by_id('qr_submit_id').click()
                print('预订成功...')
                self.browser.quit()
                break
                
            except Exception as e:
                print(e)
                print('订单提交失败，重新尝试....')


        
        
if __name__ == '__main__':
    #登录名
    uname = 'xxxxxx'
    #密码
    upwd = 'xxxxxx'    
    #票类型  按重要性依次排序
    seat_types = ['硬座','无座','软卧']   #'所有'表示所有类型票  
    #用户
    passengers = ['杨德亮(学生)']
    #车次
    numbers = ['D2212','D952','D2216','D2206','D636','D352']   #None表示所有
    #日期
    dtime = '2019-01-24'
    #出发地 需要填写cookie值
    start = '无锡'
    end = '重庆'
    
    ticket = BuyTickets(uname,upwd,passengers,dtime,start,end,seat_types=seat_types,numbers=numbers,print_log = False)
    ticket.buy()
    bytes1 =b'<HashMap><result_message>\xe9\xaa\x8c\xe8\xaf\x81\xe7\xa0\x81\xe6\xa0\xa1\xe9\xaa\x8c\xe5\xa4\xb1\xe8\xb4\xa5</result_message><result_code>5</result_code></HashMap>'
    print(bytes1.decode())
    print(json.loads(bytes1.decode()))