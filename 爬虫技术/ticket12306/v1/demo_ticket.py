# -*- coding: utf-8 -*-
"""
Created on Tue Dec 25 09:01:44 2018

@author: Administrator
"""

'''
抢票程序
使用python + requests
速度更快，不用通过前端
参考：python3.x 购买12306火车票：https://www.jianshu.com/p/7d15b9b989a5
'''



from time import sleep 
from urllib import parse
import requests
from PIL import Image
import json
import random
from bs4 import BeautifulSoup
import matplotlib.pyplot as plt
import urllib3
import re
import time
from station import global_stations
from trains_demo import TrainsDemo
from prettytable import PrettyTable
import sys
import datetime
import eventlet
import threading

#发送邮箱
from send_email import Email

#图像识别
from image_recognition import ImageRecognition

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

'''
如果请求返回的是html
则下面两个一样
print(response.text)     一般针对返回的是文本
print(response.content.decode())   一般针对的是字节码，或者其他

如果是json字符串，则需要respons.json(xx) 或者 json.load(xx)
'''


#配置参数 全局使用
global_output_log_flag = False     #打开输出日志
        
global_is_auto_buy = True         #自动购买

global_is_auto_rec = True         #自动识别验证码

'''
************************************************************************************************
Python爬虫技巧之设置代理IP
************************************************************************************************
'''
def get_ip_list(url, headers): 
    '''
    函数get_ip_list(url, headers)传入url和headers，最后返回一个IP列表，列表的元素类似42.84.226.65:8888格式，
    这个列表包括国内髙匿代理IP网站首页所有IP地址和端口。
    '''
    web_data = requests.get(url, headers=headers) 
    soup = BeautifulSoup(web_data.text, 'lxml') 
    ips = soup.find_all('tr') 
    ip_list = [] 
    for i in range(1, len(ips)): 
        ip_info = ips[i] 
        tds = ip_info.find_all('td') 
        ip_list.append(tds[1].text + ':' + tds[2].text) 
    return ip_list 

def get_random_ip(ip_list): 
    '''
    函数get_random_ip(ip_list)传入第一个函数得到的列表，返回一个随机的proxies，这个proxies可以传入到requests的get方法中，
    这样就可以做到每次运行都使用不同的IP访问被爬取的网站，有效地避免了真实IP被封的风险。proxies的格式是一个字典：
    {‘http’: ‘http://42.84.226.65:8888‘}。
    '''
    proxy_list = [] 
    for ip in ip_list: 
        proxy_list.append('http://' + ip) 
    proxy_ip = random.choice(proxy_list) 
    proxies = {'http': proxy_ip} 
    return proxies

def get_proxies():
     #IP地址取自国内髙匿代理IP网站：http://www.xicidaili.com/nn/
     url = 'http://www.xicidaili.com/nn/'
     headers = { 'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36' } 
     ip_list = get_ip_list(url, headers=headers) 
     print(ip_list)
     proxies = get_random_ip(ip_list) 
     print(proxies)
     return proxies
 
#使用方法 web_data = requests.get(url, headers=headers, proxies=proxies)
'''
************************************************************************************************
************************************************************************************************
'''
class BuyTickets(object):
    
    def __init__(self,uname,upwd,passengers,passenger_cards,sexs,dtime,start,end,train_type_names=['D','G'],numbers=None,seat_type_names=None):
        '''
        uname：登录用户名
        upwd：登录密码        
        passengers：list 乘客名   特殊人群： 杨德龙(学生)   黄旭(残军)  小明(儿童)
        passenger_cards：list 对应证件号
        sexs：list 性别  'M'：男 'W'：女
        dtime：出发日期
        start：起始地
        end：目的地
        train_type_names：list 所买列车类型  GC-高铁/城际  D-动车 Z-直达 T-特快  K-快速  如果是None：所有
        numbers：list 指定想买的车次 如果是None：所有
        seat_type_names：list 想买的座位类型 如果是None：表示随意
        
        注意：最好把乘客信息先加入该登陆账户的常用联系人  不然可能会购票失败
             如果是特殊人群，必须闲在系统录入信息   学生、儿童等
             如果是成人，则可以不用先在系统录入
             
        '''
        #初始化字典
        self.__init_dict__()
        
        #用户
        self.username = uname
        self.userpwd = upwd
        
        #车次
        self.numbers = numbers
            
        self.passengers = passengers
        self.passenger_cards = passenger_cards
        self.sexs = sexs
        
        #起始地 目的地        
        self.from_station = start        
        self.to_station = end
        
        #日期
        self.dtime = dtime
                                                    
        #座位类型
        self.seat_type_names = seat_type_names
        self.seat_type_codes = []        
        if  self.seat_type_names is None:
            self.seat_type_names = ['二等座','硬座','软座', '硬卧','软卧','无座',
                                   '动卧',
                                   '高级软卧',
                                   '一等座',
                                   '商务座特等座']            
                  
        #车次类型
        self.train_type_names = train_type_names
        
        #保存余票查询次数
        self.__query_count__ = 1
        #保存验证码识别失败次数  超过5次转为手动识别
        self.__recognize_count = 0        
        #保存余票查询异常次数，连续超过3次，默认被系统禁止查票
        self.__query_exception_count__ = 1
                        
        '''
        模拟发送请求页面 用到的数据
        '''
        # 通过输入的地点，获取到地点-code
        self.__from_station_code__ = global_stations[self.from_station]
        self.__to_station_code__ = global_stations[self.to_station]
        
        
        #保存查询到的符合要求的车次信息
        self.__trains_data_list__ = None                                 #|分割之后的  [[],[],...]
        self.__trains_data_filter_list__ = None                                #过滤后的有用信息 [{},{},...]        
        
        #保存预订的车次信息                             
        self.__preorder_train_index__ = None                     #预订的车次在__trains_table__的索引号
        self.__preorder_train_seat_name__ = None                 #座位类型名
        self.__preorder_train_secret_str__ = None                #构造请求使用
        self.__preorder_train_seat_type__ = None                 #座位类型编码
        
        self.__key_check_isChange__ = ''
        self.__REPEAT_SUBMIT_TOKEN__ = ''

        
        #构造订单提交请求字符串  由乘客信息按一定规则编码
        self.__passenger_ticket_str__ = ''
        self.__old_passenger_str__ = ''        

        
        #余票查询地址可能是变动的 
        self.__query_tickets_urla__= 'https://kyfw.12306.cn/otn/leftTicket/query{}?leftTicketDTO.train_date={}&leftTicketDTO.from_station={}&leftTicketDTO.to_station={}&purpose_codes=ADULT'.format(
            'A',self.dtime, self.__from_station_code__, self.__to_station_code__)  
        
        self.__query_tickets_urlz__= 'https://kyfw.12306.cn/otn/leftTicket/query{}?leftTicketDTO.train_date={}&leftTicketDTO.from_station={}&leftTicketDTO.to_station={}&purpose_codes=ADULT'.format(
            'Z',self.dtime, self.__from_station_code__, self.__to_station_code__)  
        
        #当前使用的
        self.__query_tickets_url__ = self.__query_tickets_urla__

        # 创建一个网络请求session实现登录验证  由于selenium不能发送post请求，所以登录都通过这个实现
        self.session = requests.session()     
        
        #self.proxies = get_proxies()
        #self.session.get('https://kyfw.12306.cn/otn/resources/login.html', headers=self.headers)

        #抢票成功 邮件通知
        self.__email__ = Email()
        self.receiver = '18151521911@163.com'
        
        #设置请求超时限制  requests 在经过以 timeout 参数设定的秒数时间之后停止等待响应  
        #设置了超时会抛出异常  如果过不指定，当断网时，可能一直等待状态
        self.__timeout__ = 5   #如果为None 不指定

    def __init_dict__(self):
        
        #票种
        self.ticket_ype_dict = {
                 '成人票':'1',
                 '儿童票':'2',
                 '学生票':'3',
                 '伤残军人票':'4'}
        
        #self.ticket_type: {adult: "1", child: "2", student: "3", disability: "4"},
        self.ticket_type_name_dict = {'1': '成人票',
                                 '2': '孩票',
                                 '3': '学生票',
                                 '4': '伤残军人票'}
        
        self.tour_flag_dict = {'dc': 'dc',
                          'wc': 'wc',
                          'fc': 'fc',
                          'gc': 'gc',
                          'lc': 'lc',
                          'lc1': 'l1',
                          'lc2': 'l2'}
        
        #证件类型
        self.passenger_card_type_dict = { 'two': "1", 
                                'one': "2", 
                                'tmp': "3", 
                                'passport': "B", 
                                'work': "H", 
                                'hongkong_macau': "C",
                                'taiwan': "G"}
        
        self.request_flag_dict= {'isAsync': "1"}
        self.ticket_query_flag_dict = {'query_commom': "00", 'query_student': "0X00"}
        self.special_areas_dict = {'lso': "LSO", 'dao': "DAO", 'ado': "ADO", 'nqo': "NQO", 'tho': "THO"}

        
        #座位类型
        self.seat_type_dict = {'商务座特等座':'9',
                          '一等座':'M',
                          '棚改座':'0',
                          '二等座':'O',
                          '高级软卧':'6',
                          '软卧':'4',
                          '动卧':'F',
                          '硬卧':'3',
                          '软座':'2',
                          '硬座':'1',
                          '无座':'1',
                          '其他':'1'}                         
        
        self.headers = {
                'Accept-Encoding': 'gzip, deflate, br',
                'Accept-Language': 'zh-CN,zh;q=0.8',
                'X-Requested-With': 'XMLHttpRequest',
                'Origin': 'https://kyfw.12306.cn',
                'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:52.0) Gecko/20100101 Firefox/52.0',
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'}
         
        self.headers = { 'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36' } 
        
  
        
    def __make_passengerTicketStr(self,passenger,passenger_card):
        '''
        passengerTicketStr 是以下划线"_"分隔当每一个乘客信息组成的字符串，对应每个乘客信息字符串组成如下：
            第一位：座椅类型  0：二等座 1： 2：  3：硬卧  4：软卧  1：硬座 ....             
            第二位：0
            第三位：票种 成年人：1  2：儿童票  学生票为：3  4：残军票
            第四位：乘客名
            第五位: 证件类型  证件类型指的是二代身份证(1)，学生证(1)，签证等的编码.
            第六位：证件号
            第七位：手机号码
            第八位：保存常用联系人(Y或N)
            多个人中间使用_隔开    
            如:            
            1,0,1,刘燕,1,511523199708177148,,N_1,0,3,杨德亮,1,500228199304130311,,N
            1,0,1,刘燕,1,511523199708177148,,N_1,0,3,杨德亮,1,500228199304130311,,N_1,0,1,郑洋,1,340621199411082033,,N
            3,0,1,郑洋,1,340621199411082033,,N_4,0,1,刘燕,1,511523199708177148,,N_1,0,3,杨德亮,1,500228199304130311,,N
            3,0,2,郑洋,1,340621199411082033,,N_4,0,4,刘燕,1,511523199708177148,,N_1,0,3,杨德亮,1,500228199304130311,,N_1,0,1,马资阳,1,340621199408152037,,N
            
            座位编号是指是什么座位类型，硬座，软座...
            票类型指的是，成人票，学生票等的编码
            证件类型指的是二代身份证，学生证，签证等的编码.
        '''
        seat_type = self.__preorder_train_seat_type__
        if '(学生)' in passenger:
            passenger = passenger[0:-4]
            ticket_ype = self.ticket_ype_dict['学生票']
        elif '(儿童)' in passenger:
            passenger = passenger[0:-4]
            ticket_ype = self.ticket_ype_dict['儿童票']
        elif '(残军)' in passenger:
            passenger = passenger[0:-4]
            ticket_ype = self.ticket_ype_dict['伤残军人票']
        else:
            ticket_ype = self.ticket_ype_dict['成人票']
        passenger_card_type = '1'        
        phone_num = ''
        save = 'N'
        
        return  '{0},{1},{2},{3},{4},{5},{6},{7}'.format(seat_type,
                                                         '0',
                                                         ticket_ype,
                                                         passenger,
                                                         passenger_card_type,
                                                         passenger_card,
                                                         phone_num,
                                                         save)
        
            
    def __make_oldPassengersStr(self,passenger,passenger_card):
        '''
        oldPassengersStr 也是以下划线"_"分隔每个乘客信息组成的字符串，对应每个乘客信息字符串组成如下：            
            第一位：乘客名
            第二位：证件类型  证件类型指的是二代身份证(1)，学生证(1)，签证等的编码.
            第三位：证件类型
            第四位：票种 成年人：1  2：儿童票  学生票为：3  4：残军票            
                
            如：            
            刘燕,1,511523199708177148,1_杨德亮,1,500228199304130311,3_
            刘燕,1,511523199708177148,1_杨德亮,1,500228199304130311,3_郑洋,1,340621199411082033,1_
            郑洋,1,340621199411082033,1_刘燕,1,511523199708177148,1_马资阳,1,340621199408152037,1_杨德亮,1,500228199304130311,3_            
        '''         
        passenger_card_type = '1'
        if '(学生)' in passenger:
            passenger = passenger[0:-4]
            ticket_ype = self.ticket_ype_dict['学生票']
        elif '(儿童)' in passenger:
            passenger = passenger[0:-4]
            ticket_ype = self.ticket_ype_dict['儿童票']
        elif '(残军)' in passenger:
            passenger = passenger[0:-4]
            ticket_ype = self.ticket_ype_dict['伤残军人票']
        else:
            ticket_ype = self.ticket_ype_dict['成人票']
        return  '{0},{1},{2},{3}_'.format(passenger,
                                         passenger_card_type,
                                         passenger_card,
                                         ticket_ype)
    

    def __dic_to_table__(self,dic):
        '''
        把dic以表格显示
        '''
        #创建表头
        table = PrettyTable()
        header = []
        values_row = []        
        for key,val in dic.items():            
            header.append(key)
            values_row.append(val)
        table._set_field_names(header)
        #插入数据
        table.add_row(values_row)    
        return table
            
            
    def __xml_to_dict__(self,xml_data):
        """
        xml转换为字典
            param xml_data:
        
        """
        soup = BeautifulSoup(xml_data, features='xml')
        xml = soup.find('HashMap')
        if not xml:
            return {}
        # 将 XML 数据转化为 Dict
        data = dict([(item.name, item.text) for item in xml.find_all()])
        return data

    '''
    **********************************************************************************************
    点击12306登录页面   登录
    *********************************************************************************************
    '''

    def __get_img__(self):
        '''        
        在你登录12306网站的时候，网页会get一个验证码图片，这个步骤封装方法如下：
        '''
        #登录验证码  get
        code_url =  "https://kyfw.12306.cn/passport/captcha/captcha-image?login_site=E&module=login&rand=sjrand&{}".format(random.random())
        
        #获取验证码图片
        #self.browser.get(self.code_url)
        response = self.session.get(code_url,headers=self.headers,verify=False)        
        sleep(0.3)
        
        #把验证码保存到本地
        with open('img.jpg','wb') as f:
            #f.write(self.browser.page_source)
            f.write(response.content)
            
        try:
            img = Image.open('img.jpg')
            # 展示验证码图片，会调用系统自带的图片浏览器打开图片，线程阻塞
            #im.show()
            # 关闭，只是代码关闭，实际上图片浏览器没有关闭，但是终端已经可以进行交互了(结束阻塞)
            #im.close()
            plt.figure(figsize=(6,6))
            plt.imshow(img)
            plt.show()
        except:
            pass
        #=======================================================================
        # 根据打开的图片识别验证码后手动输入，输入正确验证码对应的位置，例如：2,5
        # ---------------------------------------
        #         |         |         |
        #    0    |    1    |    2    |     3
        #         |         |         |
        # ---------------------------------------
        #         |         |         |
        #    4    |    5    |    6    |     7
        #         |         |         |
        # ---------------------------------------
        #=======================================================================
        
    
    def __read_input__(self,caption,timeout=10):
        '''
        输入超时
        '''
        #用于保持输入的值
        context = {'input':'[1,2,3]'}
        def  thread_input(context):
            '''
            可以设置超时
            caption：标题
            default ：默认值
            '''
            context['input'] = input(caption)          
            
        #如果线程daemon属性为True， 则join里的timeout参数是有效的， 主线程会等待timeout时间后，结束子线程。
        t = threading.Thread(target=thread_input ,args =(context,) )
        t.setDaemon(False)
        t.start( )
        t.join(timeout) #等待10秒
        return context['input']
        


    def __verify_img__(self,):
        '''
        开始校验  
        
        args：
            click_list：选中的图片如[1,2,3,4]
                 元素范围从1-8
        '''   

        #自动识别?  使用全局变量
        global global_is_auto_rec
        if not global_is_auto_rec:
            #超时5s 退出                
            rwa_input = self.__read_input__('请输入验证码位置，以","分割例如 2,5：',15)     
            #无人手动输入，转自动
            if rwa_input is None or rwa_input == '':
                #发送邮箱，高速用户需要手动验证
                self.__email__(self.receiver,'请手动输入12306验证码!')
                captcha_solution = [1,2,5]
                #自动输入
                global_is_auto_rec = True
            else:                            
                #匹配'1,2,3,4,5'这种格式的字符串
                #print(rwa_input)
                num = re.match(r'^((?:\d+,)+)\d+$',rwa_input)
                if num is not None:
                    captcha_solution = map(int,rwa_input.split(','))
                #输入格式不对
                else:
                    print('你输入的验证码格式不对,请重新输入....')
                    return self.__verify_img__()
                    
        else:
            #超时2s 退出
            with eventlet.Timeout(0.5,False):
                print('自动识别中...')
                rec = ImageRecognition()
                captcha_solution = rec.get_result('img.jpg')
            #识别失败
            if captcha_solution is None:
                self.__recognize_count += 1
                print('第{0}次自动识别失败...'.format(self.__recognize_count))                    
                #连续失败2次转为手动
                if self.__recognize_count % 2  == 0:
                    global_is_auto_rec = False
                return False
            #识别成功
            else:
                self.__recognize_count = 0                
                print('自动识别验证码结果为:',captcha_solution)

        #转换成坐标                
        code = ['35,35', '105,35', '175,35', '245,35', '35,105', '105,105', '175,105', '245,105']
        verify_list = []
        for a in captcha_solution:
            if a>8:
                a=8 
            verify_list.append(code[a-1])
            
        self.answer = ','.join(verify_list)
        data = {
            'answer': self.answer,        #验证码对应的坐标，两个为一组，跟选择顺序有关，有几个正确的，输入几个
            'login_site': 'E',         #固定的
            'rand': 'sjrand'          #固定的            
        }

        #校验验证码页面  post
        check_code_url = "https://kyfw.12306.cn/passport/captcha/captcha-check"

        #selenium不能发送post请求，所以使用requsts库
        response = self.session.post(url=check_code_url,data=data,headers=self.headers,verify=False )        
        #self.browser.get(url=url)
        if response.status_code == requests.codes.ok:
            #返回的是json字节
            #Gets the source of the current page.  json字符串解码成python对象
            #dic = json.loads(self.browser.page_source)
            #print(response.content)
            try:
                dic = json.loads(response.content)
            except Exception as e:
                xml = response.content.decode()                
                dic = self.__xml_to_dict__(xml)  
            print('验证码校验返回消息:\n',self.__dic_to_table__(dic))
            result_code = dic['result_code']                 
        
            if str(result_code) == '4':            
                return True
        else:          #5失败  7过期   
            sys.exit('请求失败！')            
   
    
    def  __login_12306__(self):
        '''
        用户信息验证 登录
        '''

        data = {
            'username':self.username,
            'password':self.userpwd,
            'appid':'otn',
            'answer':self.answer
            }   
        
        
        #校验用户信息页面  post
        check_user_url = "https://kyfw.12306.cn/passport/web/login"

        response = self.session.post(url=check_user_url,data=data,headers=self.headers,verify=False )
        #print(response.content)
        #保存cookies
        if response.status_code == requests.codes.ok:
            #返回的xml字节
            #Gets the source of the current page.  json字符串解码成python对象
            #dic = json.loads(self.browser.page_source)
            try:
                dic = json.loads(response.content)
            except Exception as e:
                print('密码校验返回结果{0}!'.format(e))
                xml = response.content.decode()                
                dic = self.__xml_to_dict__(xml)                                      
            print('密码校验返回结果:\n',self.__dic_to_table__(dic))          
            result_code = dic['result_code']                           
            if str(result_code) == '0':            
                return True
        else:
            sys.exit('请求失败！')            
        
    def __auth_uamtk__(self):
        '''
        根据登录返回的umatk,得到newapptk        
        '''
        url = 'https://kyfw.12306.cn/passport/web/auth/uamtk'
        data = {
            'appid':'otn',
            '_json_att':''
        }            
        response = self.session.post(url=url,data=data,headers=self.headers,verify=False )
        if response.status_code == requests.codes.ok:           
            dic = json.loads(response.content)      
            print('获取newapptk：\n',self.__dic_to_table__(dic))
            newapptk = dic['newapptk']
            return newapptk
        else:
            sys.exit('权限token获取失败')

    

    def __check_uamauthclient__(self,tk):
        '''
        检查客户端是否登录
        '''
        url = 'https://kyfw.12306.cn/otn/uamauthclient'
        data = {
            'tk':tk,
             '_json_att':''
        }            
        response = self.session.post(url=url,data=data,headers=self.headers,verify=False )
        #print(response.content)
        if response.status_code == requests.codes.ok: 
            dic = response.json() #json.loads(response.content)      
            print('客户端验证:\n',self.__dic_to_table__(dic))
        else:
            sys.exit('权限获取失败')


    def __passenger_add__(self,passenger,passenger_card,sex):
        '''
        追加乘客到常用联系人  sex：男：M  女:'W'
        '''       
        print('开始追加乘客{0}信息到常用联系人'.format(passenger))
        data = {'passenger_name':passenger,          #姓名
                'sex_code':sex,                      #性别
                'passenger_id_no':passenger_card,    #证件号码
                'mobile_no':'',                      #手机号码（+86）   
                'email':'',                          #电子邮箱
                'address':'',                        #地址   
                'postalcode':'',                     #邮编
                'studentInfoDTO.school_code':'',     #学校所在省份：   
                'studentInfoDTO.school_name':'',     #学校名称 
                'studentInfoDTO.department':'',      #院系 
                'studentInfoDTO.school_class':'',    #班级
                'studentInfoDTO.student_no':'',      #学号
                'studentInfoDTO.preference_card_no':'',   #学制
                'GAT_valid_date_end':'2010-01-01',        #入学年份
                'GAT_born_date':'1990-01-01',
                'old_passenger_name':'',
                'country_code':'CN',
                '_birthDate':'2017-01-05',
                'old_passenger_id_type_code':'',
                'passenger_id_type_code':'1',
                'old_passenger_id_no':'',
                'passenger_type':'1',                #旅客类型
                'studentInfoDTO.province_code':'11',
                'studentInfoDTO.school_system':'1',
                'studentInfoDTO.enter_year':'2018',
                'studentInfoDTO.preference_from_station_name':'简码/汉字',
                'studentInfoDTO.preference_from_station_code':'',   #优惠区间 起始
                'studentInfoDTO.preference_to_station_name':'简码/汉字',
                'studentInfoDTO.preference_to_station_code':''      #优惠区间 终点
                }
        
        #常用联系人追加页面 如果联系人未添加到该登陆账户，无法购票
        passenger_add_url = 'https://kyfw.12306.cn/otn/passengers/add'
        
        response = self.session.post(passenger_add_url, data=data,headers=self.headers, verify=False )        
          
        if response.status_code == requests.codes.ok :

            dic =  response.json()
            
            if dic['data']['flag'] == True:
                print("乘客信息追加成功!")                                     
                pass
            else:
                print("乘客信息追加失败!")                                     
        
    def __passenger_check__(self):
        '''
        登陆进入后，首先判断需要购票的乘客是否已经追加到常用联系人中，如果没有追加
        '''
        print('常用联系人查询.....')
        data = {
                'pageIndex':'1',
                'pageSize':'10'
        }
        #查找该账户常用联系人
        passenger_query_url = 'https://kyfw.12306.cn/otn/passengers/query'
        response = self.session.post(passenger_query_url, data=data,headers=self.headers, verify=False )        
          
        if response.status_code == requests.codes.ok :

            dic =  response.json()
            #print(self.__dic_to_table__(dic))
                                                           
            if dic['status'] == True:
                
                #保存所有常用联系人信息
                passengers_info = dic['data']['datas']
                
                #保存所有常用联系人的证件号
                common_passenger_cards = []
                #保存所有常用联系人的旅客类型
                passenger_type_names = {}
                                
                '''创建表头 并显示常用联系人'''
                table = PrettyTable()
                header  = ['code','passenger_name','sex_code','country_code','passenger_id_type_code','passenger_id_type_name',
                           'passenger_id_no','passenger_type','passenger_flag','passenger_type_name','mobile_no','phone_no',
                            'email','address','postalcode','recordCount','total_times']
                table._set_field_names(header)
                
                for passenger_info in passengers_info:
                    common_passenger_cards.append(passenger_info['passenger_id_no'])                
                    passenger_type_names[passenger_info['passenger_id_no']] = passenger_info['passenger_type_name']                
                    values_row = []
                    for key in header:                                                                                 
                        values_row.append(passenger_info[key])
                    #插入数据
                    table.add_row(values_row)                                                                
                print(table)      
                
                
                #保存不符合要求的乘客序号，用来删除                
                del_indexs = []
                
                #判断需要购票的用户是否在常用联系人中
                for index,passenger_card in enumerate(self.passenger_cards):
                    #首先判断该乘客信息是否已经录入系统 
                    if passenger_card not in common_passenger_cards:  #不在
                        #判断乘客是否为特殊旅客，如果是则需要先登录系统
                        if ')' in  self.passengers[index]:
                            print('乘客{0}购买不了{1}票，请登录系统录入{2}证明信息！'.format(self.passengers[index][0:-4],self.passengers[index][-3:-1],self.passengers[index][-3:-1]))                            
                            del_indexs.append(index)
                        #是成人 直接添加常用联系人
                        else:   
                            #追加
                            self.__passenger_add__(self.passengers[index],passenger_card,self.sexs[index]) 
                    else:   #在系统
                        #在系统 如果是成人 则不能购买其他票
                        if '成人' in passenger_type_names[passenger_card] and ')' in  self.passengers[index]:                            
                            print('乘客{0}系统中录入的是成人,购买不了{1}票，请登录系统录入{2}证明信息！'.format(self.passengers[index][0:-4],self.passengers[index][-3:-1],self.passengers[index][-3:-1]))                            
                            del_indexs.append(index)
                            
                for index in del_indexs[::-1]:
                    #并把该乘客信息从购票序列移除
                    del self.passengers[index]
                    del self.passenger_cards[index]
                    del self.sexs[index]
                
                if len(self.passengers) == 0:
                    sys.exit('当前无用户购票!')
               
            else:
                print("常用联系人查询异常:", dic['messages'])                     
                print('\n')             


    def __login__(self):
        '''
        登录页面  
        首先校验验证码 ： https://kyfw.12306.cn/passport/captcha/captcha-check?callback=jQuery19109896894761037467_1545812116405&answer=125%2C31%2C122%2C114&rand=sjrand&login_site=E&_=1545812116407
        然后校验密码：https://kyfw.12306.cn/passport/web/login
        登录失败：会生成新的验证码 https://kyfw.12306.cn/passport/captcha/captcha-image64?login_site=E&module=login&rand=sjrand&1545812141358&callback=jQuery19109896894761037467_1545812116405&_=1545812116408
        
        登录系统  https://blog.csdn.net/chengxuyuan997/article/details/80781879        
        
        第一步 ：访问登录页面

        第二步：下载验证码

        第三步：校验验证码

        第四步：校验用户名和密码

        第五步：获取权限token

        第六步：获取权限
        
        第七步：确认购票乘客是否在登录用户的常用联系人中
        '''
        '''第一步 打开登录页面'''
        #self.browser.get(self.login_url)
        #response = self.session.post(url=self.login_url,data=data,headers=self.headers,verify=False)

        '''第二步 获取验证码'''
        self.__get_img__()
            
        '''第三步 校验验证码'''
        check = False
        #只有验证成功后才能执行登录操作
        while not check:            
            check = self.__verify_img__()
            if not check:               
                self.__get_img__()
                sleep(2)
                continue

            '''第四步：校验用户名和密码  登录系统，循环尝试登录，登录成功，就会抛异常，跳出循环继续执行'''        
            if  not self.__login_12306__():                            
                break
            
            '''第五步：获取权限token'''
            newapptk = self.__auth_uamtk__()
                
            #print(dic)
            '''第六步 获取权限'''
            self.__check_uamauthclient__(newapptk)
            print('12306登陆成功.........\n')
            
            #第一次查询，打印常用联系人 
            if self.__query_count__ <= 1:
                '''第七步：确认购票乘客是否在登录用户的常用联系人中'''
                self.__passenger_check__()
            
            break
       
    
    '''
    **********************************************************************************************
    加载余票查询页面
    *********************************************************************************************
    '''
    def __init_buy_page__(self):
        '''
        登录进入后，加载购票页面
        '''
        #登陆成功后 余票查询
        ticket_url = "https://kyfw.12306.cn/otn/leftTicket/init"
        
        print("购票页面初始化中....")            
        response = self.session.get(ticket_url,headers=self.headers)
        if response.status_code == requests.codes.ok:
            #print("购票页面初始化成功!")
            pass
        
        
    
    def __date_check__(self):
        '''
        检查余票预订时间是否在6-23点之间
        返回距离可以抢票的时间差  如果可以抢票：返回0  不可以：返回秒数
        '''
        d0 = datetime.datetime.now()    #datetime
        year = d0.year
        month = str(d0.month).zfill(2)   #前导0
        day = str(d0.day).zfill(2)       #前导0
        
        #当前时间
        s0 = d0.strftime("%Y-%m-%d %H:%M:%S")    #str
        
        #第二天6点
        s1 = '{}-{}-{} 06:00:00'.format(year,month,day)       
        d1 = datetime.datetime.strptime(s1, "%Y-%m-%d %H:%M:%S")    #datetime
        d1 = d1 + datetime.timedelta(days=1)                        #日期+1  str
        #d1 = datetime.datetime.strptime(s1, "%Y-%m-%d %H:%M:%S")    #datetime
        s1 = d1.strftime("%Y-%m-%d %H:%M:%S")    #str
        
        #当天6点
        s2 = '{}-{}-{} 06:00:00'.format(year,month,day)
        d2 = datetime.datetime.strptime(s2, "%Y-%m-%d %H:%M:%S")    
        
        #当天23点
        s3 = '{}-{}-{} 23:00:00'.format(year,month,day)
        
        
        if s0 < s2:   #当天6点之前  [)
            rest = d2 - d0
            rest = rest.seconds
        elif s0 <= s3:     #6-23点之间 []
            rest = 0
        else:                    #23点之后s0 > d3
            rest = d1 - d0
            rest = rest.seconds    
        return rest

    def __rest_time__(self):
        '''
        计算当前时间在整点16点、整点6点时的时间差  比如当前时间05:59:35 距离6点为25秒
        如果不在时间60s时间差之内，返回100
        '''
        d0 = datetime.datetime.now()  # datetime
        year = d0.year
        month = str(d0.month).zfill(2)   #前导0
        day = str(d0.day).zfill(2)       #前导0

        # 当前时间
        s0 = d0.strftime("%Y-%m-%d %H:%M:%S")  # str

        # 当天05:59:00
        s1 = '{}-{}-{} 05:59:00'.format(year, month, day)
        d1 = datetime.datetime.strptime(s1, "%Y-%m-%d %H:%M:%S")  # datetime

        s11 = '{}-{}-{} 06:00:00'.format(year, month, day)
        d11 = datetime.datetime.strptime(s11, "%Y-%m-%d %H:%M:%S")  # datetime

        # 当天06:01:00点
        s2 = '{}-{}-{} 06:01:00'.format(year, month, day)
        d2 = datetime.datetime.strptime(s2, "%Y-%m-%d %H:%M:%S")  # datetime

        # 当天15:59:00点
        s3 = '{}-{}-{} 15:59:00'.format(year, month, day)
        d3 = datetime.datetime.strptime(s3, "%Y-%m-%d %H:%M:%S")  # datetime

        s33 = '{}-{}-{} 16:00:00'.format(year, month, day)
        d33 = datetime.datetime.strptime(s33, "%Y-%m-%d %H:%M:%S")  # datetime

        # 当天16:01:00点
        s4 = '{}-{}-{} 16:01:00'.format(year, month, day)
        d4 = datetime.datetime.strptime(s4, "%Y-%m-%d %H:%M:%S")  # datetime

        # 6点，以及下午4点
        if s0 > s1 and s2 > s0:
            if s0 <= s11:
                return (d11 - d0).seconds
            else:
                return (d0 - d11).seconds
        elif s0 > s3 and s4 > s0:
            if s0 <= s33:
                return (d33 - d0).seconds
            else:
                return (d0 - d33).seconds
        else:
            return 100

    def __query_tickets__(self):
        '''
        查询余票信息          
        
        True:有余票 
        False:没有余票 没有余票就需要循环继续查询
        ''' 
        #print(self.__query_tickets_url__)
        
        '''有余票：判断当前时间是否可以抢票'''
        res = self.__date_check__()
        if res > (900 + 60) :   #距离开抢大于15分钟
            print('距离开抢时间还有{}分钟!：'.format(res/60))  
            sleep(900)
            return False
        elif res > (60 + 20) :   #距离开抢大于2分钟
            print('距离开抢时间还有{}分钟!：'.format(res/60)) 
            sleep(30)
            return False
        elif res != 0:                                   #1分钟以内  允许抢票
            print('距离开抢时间还有{}秒!：'.format(res))                                     
        else:
            pass
        
        '''如果是下午4点和6点附近 刷新时间调低  其他时间调高           必须等待，连续刷会被禁ip,如果1s刷新20次+可能被禁      '''
        res = self.__rest_time__()
        if res == 100:               #不在下午4点和上午6点附近前后1分钟内，刷新可以慢些
            ran = random.randint(1,7)              #1,2,3,4,5,6,7
            #6/7的概率睡眠 0.1-1s
            if ran<=6:
                num = random.randint(0,10)/10.0
            #1/7的概率睡眠 2s
            else:
                num = 2
        else:
            num = res/100.0 + 0.05                #0.05作为基准 即最小50ms 剩余时间越大，睡眠时间越长 0.05~(60/100+0.05)
        sleep(num)
        
        print('第%d次点击查询...' % self.__query_count__)
        response = self.session.get(self.__query_tickets_url__, headers=self.headers, verify=False,timeout=5)        
        
        self.__query_count__ += 1

        '''
        解析余票信息
        '''
        if response.status_code == requests.codes.ok :
            #print(response.content.decode())
            response.encoding = 'utf-8'            
            #print(response.content)
            try:
                 dic = response.json()
            except Exception as e:
                #查询了当前还没放票日期的票 会返回一个html 
                if  '﻿<!DOCTYPE' in response.text:
                    sys.exit('请检查你的购票日期，是否超出一个月之后....')
                else:
                    print('查票异常{0},重新查询余票中...\n'.format(e))
                    return False
            
            try:
                # 得到查询到的列车原始数据
                raw_trains = dic['data']['result']        
                
                #输出所有车次信息
                self.__trains_data_list__,self.__trains_data_filter_list__,self.__trains_table__ = TrainsDemo(raw_trains, self.train_type_names,self.numbers,None,None).get_trian_data()
                print(self.__trains_table__)
                
            except Exception as e:
                print('查票结果{0},重新查询余票中...\n'.format(e))
                print(response.text)
                sleep(5)
                return False
            
            '''
            判断余票，是否有符合指定车次，指定座位类型的余票？
            '''
            #没有符合要求的车次，退出  
            if self.__trains_data_list__ is  None:
                sys.exit()('没有符合要求的车次.....')
            #有车票，但是还没开始放票
            else:
                #是否有余票
                flag = False
                for train_info in self.__trains_data_filter_list__:
                    #满足以下条件，说明有指定类型座位余票                    
                    for seat_type_name  in  self.seat_type_names:
                        #有票?
                        if train_info[seat_type_name] not in ['*','--','无']:
                            flag = True
                            break
                    if flag:
                        break                    
                #没有余票
                if not flag:                    
                    print('系统没有满足你指定席次的余票了，持续刷新中...\n')
                    return False                                                                          
                            
            #查票成功
            if dic['status'] == True:
                self.__query_exception_count__ = 0
                print('\n')     
                return True
            else:
                print('\n')     
                print("查询车次出现异常:", dic['messages'][0])                     
                return False            
        else:
            #{"c_name":"CLeftTicketUrl","c_url":"leftTicket/queryZ","status":false}
            if 'leftTicket/queryZ' in response.text:
                self.__query_tickets_url__ = self.__query_tickets_urlz__
            else:
                self.__query_tickets_url__ = self.__query_tickets_urla__
            #如果查询次数太多，可能被进行余票查询
            print('余票查询异常!\n')            
            if self.__query_exception_count__ %3 == 0:
                print(response.text)
                sys.exit('你已经连续三次查票异常，你可能被12306禁止查票了,请尝试其他用户登录！')
            #重新登录            
            #self.__login__()            
            self.__query_exception_count__ += 1
            sleep(2)
            return False


    '''
    **********************************************************************************************
    有余票才可以预订
    *********************************************************************************************
    '''                 
    def __check_user__(self):
        '''
        点击预定后  阶段1：首先检查检查用户是否登录      长时间连接，连接可能断开
        '''        
        print("检查用户....")
        url = 'https://kyfw.12306.cn/otn/login/checkUser'
        data = {
                "_json_att": ""
        }
        response = self.session.post(url, data=data, headers=self.headers)
        # 反回一个验证通过信息
        if response.status_code == requests.codes.ok:
            #print(response.text)
            try:
                dic = json.loads(response.content)
            except Exception as e:
                print('检查用户异常:{0}!'.format(e))
                xml = response.content.decode()                
                dic = self.__xml_to_dict__(xml)  
                
            if global_output_log_flag:                                
                #{"validateMessagesShowId":"_validatorMessage","status":true,"httpstatus":200,"data":{"flag":true},"messages":[],"validateMessages":{}}   #成功登陆
                #{"validateMessagesShowId":"_validatorMessage","status":true,"httpstatus":200,"data":{"flag":flag},"messages":[],"validateMessages":{}}  #没有登录 需要重新登录
                print(self.__dic_to_table__(dic))  
                
            #try:
            #用户已登录
            if dic['data']['flag'] == True:
                pass
            else:   #用户过期
                #重新登录
                self.__login__()
            #except Exception as e:
            #    print('检查用户异常:{0}'.format(e))
                
            print('\n')
        else:
            #重新登录
            self.__login__()

            
    def __select_train__(self):
        '''
        根据查询到的余票信息进行车次选择
        '''                  
        if self.__trains_data_list__ is not  None:            
            
            secret_strs = [parse.unquote(train_data[0]) for train_data in self.__trains_data_list__]
            
            #secret_strs = [parse.unquote(self.__trains_data_list__[i][0]) for i in  range(len(self.__trains_data_list__))]            
                                     
            #遍历指定的座位类型
            for seat_type_name in self.seat_type_names:
                #遍历车次
                for index,train in enumerate(self.__trains_data_filter_list__):
                    
                    train_flag_use = True
                    
                    #车次过滤  
                    if self.numbers is not None:  
                        #查找指定车次
                        if train['trips'] not in self.numbers:
                            train_flag_use = False
                    
                    #也可以过滤时间
                                                
                    #判断用户是否想购买该车次
                    if train_flag_use:                                    
                        if train[seat_type_name] == '--':
                            if global_output_log_flag :
                                print('车次{0}无{1}出售，已结束当前刷票，清重新开启！'.format(train['trips'],seat_type_name))
                        elif train[seat_type_name] == '无':
                            if global_output_log_flag:
                                print('车次{0}{1}没有余票了，继续尝试....'.format(train['trips'],seat_type_name))                            
                        else:
                            print('车次{0}{1}有余票了，开始预订....'.format(train['trips'],seat_type_name))     
                            #保存将要预订的列车所在tickets_data中的索引位置以及解密字符串  
                            self.__preorder_train_index__ = index
                            self.__preorder_train_secret_str__ = secret_strs[self.__preorder_train_index__]
                
                            #选择的座位类型
                            self.__preorder_train_seat_type__ = self.seat_type_dict[seat_type_name] 
                            self.__preorder_train_seat_name__ = seat_type_name
                            
                            #确定两个很重要的数据 后面会用到
                            for passenger,passenger_card in zip(self.passengers,self.passenger_cards):
                                __passenger_ticket_str__ = self.__make_passengerTicketStr(passenger,passenger_card)
                                __old_passenger_str__ = self.__make_oldPassengersStr(passenger,passenger_card)
                                self.__passenger_ticket_str__ += __passenger_ticket_str__ + '_'
                                self.__old_passenger_str__ += __old_passenger_str__
                            #去除最后一个'-'
                            self.__passenger_ticket_str__ = self.__passenger_ticket_str__[0:-1]                            
                            print('__passenger_ticket_str__:',self.__passenger_ticket_str__)
                            print('__old_passenger_str__:',self.__old_passenger_str__)
                            
                            return True
        else:
            #由于查询余票，没有查询到任何结果，可能是刷新太快了，因此停顿一段时间
            sleep(2)
                        
        #没有余票了 清空数据
        self.__preorder_train_index__ = None
        self.__preorder_train_secret_str__ = None
        self.__preorder_train_seat_type__ = None
        self.__passenger_ticket_str__ = None
        self.__old_passenger_str__ = None
        
        print('{0} {1} --> {2}满足你要求的车次没有余票了，刷新中....\n'.format(self.dtime,self.from_station,self.to_station))                            
        return False
        
        
    
    def __submit_order_request__(self):
        '''
        点击预定后  阶段2：对以下条件检查
        
        这里会进行检查：1.买票时间是否正确
                       2.是否有该车次未处理的订单
                       3.是否有余票
        不满足都返回False
        满足返回True
        '''
        print("开始进行订单检查...")
        url = 'https://kyfw.12306.cn/otn/leftTicket/submitOrderRequest'
    
        '''       
        参数信息
             "secretStr"               :  指定需要预订的车次,这个事通过解码得到的
             "train_date"              :  出发日期
             "back_train_date"         :  返回日期
             "tour_flag": "dc"         :  单程/ 往返(wc)             不用管 对应的就是查询时 勾选单程？往返？
             "purpose_codes"           :  "ADULT"  普通/学生(0X00)   不用管 对应的就是查询时 普通？学生？
             "query_from_station_name" :  出发车站 ，可以在查询车次接口中得到
             "query_to_station_name"   :  返回车站，  可以在查询车次接口中得到
             "undefined"               :  ""  应该是跟返回数据相关
        '''
        #选择车次 没有符合指定要求的车次则返回False
        if not self.__select_train__():
            return False
                        
        
        data = {
            #用unquote( )函数将余票查询结果中的字符串解码  https://zhuanlan.zhihu.com/p/48077823?utm_source=qq&utm_medium=social&utm_oi=827947156655718400
            "secretStr": self.__preorder_train_secret_str__,   
            "train_date": self.dtime,
            "back_train_date": datetime.datetime.now().strftime('%Y-%m-%d'),
            "tour_flag": "dc",
            "purpose_codes": "ADULT",
            "query_from_station_name": self.from_station,
            "query_to_station_name": self.to_station,
            "undefined": ""
        }        
        response = self.session.post(url, data=data, headers=self.headers)
                
        # 反回一个验证通过信息
        if response.status_code == requests.codes.ok:
            #{"validateMessagesShowId":"_validatorMessage","status":true,"httpstatus":200,"messages":[],"validateMessages":{}}
            #成功
            
            #{"validateMessagesShowId":"_validatorMessage","status":false,"httpstatus":200,"messages":["当前时间不可以订票"],"validateMessages":{}}
            #预订时间检查
            
            #{"validateMessagesShowId":"_validatorMessage","status":false,"httpstatus":200,"messages":["您还有未处理的订单，请您到<a href=\"../view/train_order.html\">[未完成订单]</a>进行处理!"],"validateMessages":{}}                 
            #是否有该次未处理消息？
            
            #{"validateMessagesShowId":"_validatorMessage","status":false,"httpstatus":200,"messages":["车票信息已过期，请重新查询最新车票信息"],"validateMessages":{}}                 
            #登录的用户过期，重新登录后，返回信息
            
            #print(response.content)
            dic = response.json()
            if global_output_log_flag:
                print(data)
                print(self.__dic_to_table__(dic))
            if dic['status'] == True:
                print('\n') 
                return True
            else:
                if '您还有未处理的订单' in dic['messages'][0]:
                    sys.exit(dic['messages'][0],'请先取消订单....')                
                print("订单异常：", dic['messages'][0])                     
                print('\n') 
                return False
        else:
            print("订单异常！\n")     
            return False
         

    def __init_dc__(self):
        '''
        点击预定后  阶段3:  页面跳转
        '''
        print("开始初始化订单数据...")
        dc_url = 'https://kyfw.12306.cn/otn/confirmPassenger/initDc'
        data = {
            '_json_att': ''
        }
        response = self.session.post(dc_url, data=data, headers=self.headers)
        # print(resp.text)
        # 反回一个验证通过信息
        if response.status_code == requests.codes.ok:
            # var globalRepeatSubmitToken = '9c4ae60bf6a8457be2214daf95e0d8ce'
            a1 = re.search(r'globalRepeatSubmitToken.+', response.text).group()
            globalRepeatSubmitToken = re.sub(r'(globalRepeatSubmitToken)|(=)|(\s)|(;)|(\')', '', a1)
    
            #'key_check_isChange':'4359BC56A42D9B7C7655EDD91010902B0C8A636DDDD361B011C1B842'
            b1 = re.search(r'key_check_isChange.+', response.text).group().split(',')[0]
            key_check_isChange = re.sub(r'(key_check_isChange)|(\')|(:)', '', b1)
            
            if global_output_log_flag:
                print('得到校验uuid:', globalRepeatSubmitToken,key_check_isChange)
            print('\n')
            self.__REPEAT_SUBMIT_TOKEN__, self.__key_check_isChange__ = globalRepeatSubmitToken, key_check_isChange
            

    def __get_passenger__(self):
        '''
        点击预定后  阶段4: 加载乘客信息
        
        获取到用户的乘车人信息                
        '''
        print("获取登陆用户所有常用联系人信息...")
        url = 'https://kyfw.12306.cn/otn/confirmPassenger/getPassengerDTOs'
        data = {
            '_json_att': '',
            "REPEAT_SUBMIT_TOKEN": self.__REPEAT_SUBMIT_TOKEN__  #9c4ae60bf6a8457be2214daf95e0d8ce
        }
        response = self.session.post(url, data=data, headers=self.headers)
        if response.status_code == requests.codes.ok:
            #print(response.text)
            dic = response.json()
            normal_passengers = dic['data']['normal_passengers']  #list
            '''
            normal_passengers":[{"code":"4",
								   "passenger_name":"郑洋",
								   "sex_code":"M",
								   "sex_name":"男",
								   "born_date":"1980-01-01 00:00:00",
								   "country_code":"CN",
								   "passenger_id_type_code":"1",
								   "passenger_id_type_name":"中国居民身份证",
								   "passenger_id_no":"340621199411082033",
								   "passenger_type":"1",
								   "passenger_flag":"0",
								   "passenger_type_name":"成人",
								   "mobile_no":"",
								   "phone_no":"",
								   "email":"",
								   "address":"",
								   "postalcode":"",
								   "first_letter":"ZY",
								   "recordCount":"4",
								   "total_times":"99",
								   "index_id":"0",
								   "gat_born_date":"",
								   "gat_valid_date_start":"",
								   "gat_valid_date_end":"",
								   "gat_version":""},
								   {"code":"1","passenger_name":"刘燕","sex_code":"","born_date":"2017-07-06 00:00:00","country_code":"CN","passenger_id_type_code":"1","passenger_id_type_name":"中国居民身份证","passenger_id_no":"511523199708177148","passenger_type":"1","passenger_flag":"0","passenger_type_name":"成人","mobile_no":"","phone_no":"","email":"","address":"","postalcode":"","first_letter":"LY","recordCount":"4","total_times":"99","index_id":"1","gat_born_date":"","gat_valid_date_start":"","gat_valid_date_end":"","gat_version":""},
								   {"code":"2","passenger_name":"马资阳","sex_code":"","born_date":"2017-07-05 00:00:00","country_code":"CN","passenger_id_type_code":"1","passenger_id_type_name":"中国居民身份证","passenger_id_no":"340621199408152037","passenger_type":"1","passenger_flag":"0","passenger_type_name":"成人","mobile_no":"","phone_no":"","email":"","address":"","postalcode":"","first_letter":"MZY","recordCount":"4","total_times":"99","index_id":"2","gat_born_date":"","gat_valid_date_start":"","gat_valid_date_end":"","gat_version":""},
								   {"code":"3","passenger_name":"杨德亮","sex_code":"","born_date":"2018-02-22 00:00:00","country_code":"CN","passenger_id_type_code":"1","passenger_id_type_name":"中国居民身份证","passenger_id_no":"500228199304130311","passenger_type":"3","passenger_flag":"0","passenger_type_name":"学生","mobile_no":"","phone_no":"","email":"","address":"","postalcode":"","first_letter":"YDL","recordCount":"4","total_times":"99","index_id":"3","gat_born_date":"","gat_valid_date_start":"","gat_valid_date_end":"","gat_version":""}
								   ],
            '''
            if global_output_log_flag:
                #创建表头
                table = PrettyTable()
                header  = ['code','passenger_name','sex_code','country_code','passenger_id_type_code','passenger_id_type_name',
                           'passenger_id_no','passenger_type','passenger_flag','passenger_type_name','mobile_no','phone_no',
                            'email','address','postalcode','recordCount','total_times']
                table._set_field_names(header)
                for pnormal_passenger in normal_passengers:
                    values_row = []
                    for key in header:                                                                                 
                        values_row.append(pnormal_passenger[key])
                    #插入数据
                    table.add_row(values_row)    
                print(table)            
            print('\n')

    '''
    **********************************************************************************************
    点击提交订单
    *********************************************************************************************
    '''
    def __check_order_info__(self):
        '''
        点击提交订单  阶段1  
        '''
        '''
        参数信息
            cancel_flag        : 2  默认
            bed_level_order_num: 000000000000000000000000000000  默认
            passengerTicketStr : 选择的乘客信息                                             
            oldPassengerStr    : 选择的乘客信息                                                                      
            tour_flag          : 单程/ 往返(wc)      不用管 对应的就是查询时 勾选单程？往返？
            randCode           : 需要重新获取验证码，为空
            whatsSelect        : 1 是否是常用联系人中选择的需要购买车票的人
            _json_att:
            REPEAT_SUBMIT_TOKEN:9c4ae60bf6a8457be2214daf95e0d8ce
        '''
        
        print("检查订单信息...")
        data = {
            'cancel_flag': '2',
            'bed_level_order_num': "000000000000000000000000000000",
            'passengerTicketStr': self.__passenger_ticket_str__,
            'oldPassengerStr': self.__old_passenger_str__,
            'tour_flag': 'dc',            
            'randCode': '',
            'whatsSelect': '1',
            '_json_att': '',
            'REPEAT_SUBMIT_TOKEN': self.__REPEAT_SUBMIT_TOKEN__
        }        
        url = 'https://kyfw.12306.cn/otn/confirmPassenger/checkOrderInfo'
        response = self.session.post(url, data=data, headers=self.headers)
        if response.status_code == requests.codes.ok:
            #print(response.text)
            dic = response.json()
            #{"validateMessagesShowId":"_validatorMessage","status":true,"httpstatus":200,"data":{"ifShowPassCode":"N","canChooseBeds":"N","canChooseSeats":"N","choose_Seats":"MOP9","isCanChooseMid":"N","ifShowPassCodeTime":"1791","submitStatus":true,"smokeStr":""},"messages":[],"validateMessages":{}}
            ##{"validateMessagesShowId":"_validatorMessage","status":true,"httpstatus":200,"data":{'checkSeatNum': True, 'errMsg': '您选择了1位乘车人，但本次列车棚车仅剩0张。', 'submitStatus': False},"messages":[],"validateMessages":{}}
            ##{"validateMessagesShowId":"_validatorMessage","url":"/leftTicket/init","status":False,"httpstatus":200,messages":[系统忙，请稍后重试],"validateMessages":{}}
            if global_output_log_flag:
                print(data)
                print(self.__dic_to_table__(dic))
    
            if dic['status']:
                print('\n') 
                return True
            else:
                print("订单信息检查异常：", dic['messages'][0])                     
                print('\n') 
                return False
    
        return False
    
    def __trance_date__(self,param):
        '''
        将传递的字符串转化为时间
            :param param: 时间： 2017-12-29
            :return: Fri Dec 29 2017 00:00:00 GMT+0800 (中国标准时间)
        '''
        ts = time.mktime(time.strptime(param, "%Y-%m-%d"))
        s = time.ctime(ts)
        t1 = s[0:11] + s[20:] + " 00:00:00 GMT+0800 (中国标准时间)"
        return t1

    
    def __get_queue_count__(self):
        '''
        判断是都有余票     阶段1  
        '''
        print('检查余票中...')
        url = 'https://kyfw.12306.cn/otn/confirmPassenger/getQueueCount'
        # 将字符串转化为需要的时间
        train_data = self.__trance_date__(self.dtime)
        data = {
                # 时间
                'train_date': train_data,
                # 车次编号
                'train_no': self.__trains_data_list__[self.__preorder_train_index__][2],
                # 火车代码
                'stationTrainCode': self.__trains_data_list__[self.__preorder_train_index__][3],
                # 座位类型 
                'seatType': self.__preorder_train_seat_type__,
                # 出发点，终止地址
                'fromStationTelecode': self.__trains_data_list__[self.__preorder_train_index__][6],
                'toStationTelecode': self.__trains_data_list__[self.__preorder_train_index__][7],
                'leftTicket': self.__trains_data_list__[self.__preorder_train_index__][12],
                'purpose_codes': "00",
                'train_location': self.__trains_data_list__[self.__preorder_train_index__][15],
                '_json_att': '',
                'REPEAT_SUBMIT_TOKEN': self.__REPEAT_SUBMIT_TOKEN__
            }                  
        response = self.session.post(url, data=data, headers=self.headers)
        if response.status_code == requests.codes.ok:
            # 有余票，返回值将会是True
            #print(response.text)
            if global_output_log_flag:
                print(data)     
                dic = response.json()
                print(self.__dic_to_table__(dic))            
            print('\n')
    
    '''
    **********************************************************************************************
    点击确认提交
    *********************************************************************************************
    '''
    def __confirm_single__(self):
        '''
        确认提交
        最后一次确认订单
        返回购票结果
        '''
        '''
        参数信息：
            passengerTicketStr:
            oldPassengerStr   :            
            randCode          :
            purpose_codes     : 00
            leftTicketStr
            key_check_isChange: 4359BC56A42D9B7C7655EDD91010902B0C8A636DDDD361B011C1B842
            train_location    : QZ
            choose_seats:
            seatDetailType:000
            whatsSelect:1
            roomType:00
            dwAll:N
            _json_att:
                REPEAT_SUBMIT_TOKEN:9c4ae60bf6a8457be2214daf95e0d8ce
        '''
        print('确认订单...')
        url = "https://kyfw.12306.cn/otn/confirmPassenger/confirmSingleForQueue"
        data = {
            'passengerTicketStr': self.__passenger_ticket_str__,
            'oldPassengerStr':self.__old_passenger_str__,
            'randCode': '',
            'purpose_codes': '00',
            'key_check_isChange': self.__key_check_isChange__,
            'leftTicketStr': self.__trains_data_list__[self.__preorder_train_index__][12],
            'train_location': self.__trains_data_list__[self.__preorder_train_index__][15],
            'choose_seats': '',
            'seatDetailType': '000',
            'whatsSelect': '1',
            'roomType': '00',
            'dwAll': 'N',
            '_json_att': '',
            'REPEAT_SUBMIT_TOKEN': self.__REPEAT_SUBMIT_TOKEN__
        }
        #print(data)
        response = self.session.post(url, data=data, headers=self.headers)
        if response.status_code == requests.codes.ok:            
            #{"validateMessagesShowId":"_validatorMessage","status":true,"httpstatus":200,"data":{"errMsg":"系统繁忙，请稍后重试！","submitStatus":false},"messages":[],"validateMessages":{}}
            #print(response.text)            
            dic = response.json()
            if global_output_log_flag:                
                print(data)                
                print(self.__dic_to_table__(dic))                
            # 返回购票结果
            if dic['status'] and dic['data']['submitStatus']:
                #输出购买车次信息
                train_info = self.__trains_data_filter_list__[self.__preorder_train_index__]                
                content = '恭喜乘客{}预订的{} {} {} {}  {}购票成功'.format(self.passengers,    #乘客名
                                                                self.dtime,           #日期
                                                                train_info['trips'],   #车次                                                                                                                  
                                                                train_info['from_to_station_name'],   #起始->目的
                                                                train_info['start_arrive_time'],      #起始->时间                                                                
                                                                self.__preorder_train_seat_name__)    #座位类型
                self.__email__.send_qq_email(self.receiver,content)
                print(self.__dic_to_table__(train_info))                
                print('\n')                 
                return True
            else:
                print("确认订单检查异常：", dic['data'])                     
                print('\n') 
                return False
                     

    def buy(self):
        '''
        开始买票
        '''
        #用户登录 这里使用request模块登录  
        self.__login__()
                
        #加载购票页面
        self.__init_buy_page__()
        
        print('查询车票数据: 出发地:{},目的地:{},出发日期:{}'.format(self.from_station,self.to_station, self.dtime))

        # 开启自动查询购票
        while (global_is_auto_buy):
            try:
                #登录之后、查询票信息 有余票？
                if self.__query_tickets__() == True:   
                     
                    #点击预定后，首先检查检查用户是否登录,没有登录则会自动登录
                    self.__check_user__()
                    
                    #开始进行订单检查 预订成功？ 如果在23-11点禁止买票返回False，有未处理订单也会返回False ，没有余票也会返回False                            
                    if self.__submit_order_request__() == True:   #说明订单成功，需要确认订单即可  此时进入了订单页面，即选择乘客
                                            
                        # 初始化订单数据,获取到REPEAT_SUBMIT_TOKEN,key_check_isChange,leftTicketStr
                        self.__init_dc__()                    
                        
                        # 获取该用户下的乘车人信息
                        self.__get_passenger__()
                        
                        # 进行订单确认
                        if self.__check_order_info__() == True:
                            # 订单检查成功，确认订单
                            # 查询订单队列余票
                            self.__get_queue_count__()
                            # 最后一次确认订单
                            #if True:
                            if self.__confirm_single__() == True:
                                print('购票成功,退出程序!')
                                break                
                                                                    
                        else:
                            # 休眠3秒钟，防止被防刷票封ip
                            sleep(0.2)        
                                
            except Exception as e:
                print("发生异常:{}，继续购买...".format(e))