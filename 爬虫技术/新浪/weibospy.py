# -*- coding: utf-8 -*-
"""
Created on Thu May 24 16:35:36 2018

@author: zy
"""
import os
import time
from selenium import webdriver
#from selenium.common.exceptions import NoSuchElementException  
from bs4 import BeautifulSoup



class WeiboSpyder():
    '''
    python爬虫——基于selenium用火狐模拟登陆爬搜索关键词的微博:https://blog.csdn.net/u010454729/article/details/51225388

    0.安装火狐 
    1.安装selenium，可通过pip安装：pip install selenium     
    （1）、下载geckodriver.exe：下载地址：https://github.com/mozilla/geckodriver/releases请根据系统版本选择下载；（如Windows 64位系统）
    （2）、下载解压后将getckodriver.exe复制到Firefox的安装目录下，
     如（C:\Program Files\Mozilla Firefox），并在环境变量Path中添加路径：C:\Program Files\Mozilla Firefox
    2.程序里面改三处：用户名、密码、搜过的关键词search_text 
    3.需要手动输入验证码，并且验证码大小写敏感，若是输错了，等3秒再输入。 
    4.爬下来了，用BeautifulSoup提取,并且将相同文本的放在一起，并且排序 
     
    时间：5秒一个，不可缩短，不然加载不出来下一页这个按钮，然后程序就挂了，若网速快些，延时可以短些。 在每个点击事件之前或者之后都加time.sleep(2)  
    '''  
    def __init__(self):
        #微博登录用户名，用户命名
        self.username_  = "18151521911"  
        self.password_  = "123456aa"  
        self.href = 'http://s.weibo.com/'  
              
        #获取火狐浏览器对象 
        self.browser = webdriver.Firefox(executable_path = r'D:\FF\geckodriver.exe')     
        
        #https://blog.csdn.net/vicky_lov/article/details/83344708
        #chromedriver下载地址：https://www.cnblogs.com/xiaoqi111/p/6051266.html 需要配置path路径
        #谷歌浏览器：https://www.portablesoft.org/google-chrome-legacy-versions/          
        #self.executable_path = r'C:\Users\Administrator\AppData\Local\Google\Chrome\Application\chromedriver.exe'      
        #option = webdriver.ChromeOptions()
        #option.add_argument('--no-sandbox')
        #option.add_argument('--disable-dev-shm-usage')
        #option.add_argument('--headless')    #可视化显示
        #option.add_argument('--disable-infobars');        
        #option.add_argument('--user-data-dir=C:\\Users\\Administrator\\AppData\\Local\Google\\Chrome\\User Data')
        #self.browser = webdriver.Chrome(chrome_options=option,executable_path = self.executable_path)

        time.sleep(5)  
        #html请求
        self.browser.get(self.href)  
        time.sleep(5)  
              
        #获取网页右上登陆元素 并点击
        login_btn = self.browser.find_element_by_xpath('//a[@node-type="loginBtn"]')  
        login_btn.click()  
        time.sleep(5)  
              
        #获取选择账号登录元素 并点击  
        #name_login = self.browser.find_element_by_xpath('//a[@action-data="tabname=login"]')  
        name_login = self.browser.find_element_by_xpath('//a[@node-type="login_tab"]')  
        name_login.click()  
        time.sleep(5)  
              
        #获取输入用户名,密码元素 并输入用户名和密码
        username = self.browser.find_element_by_xpath('//input[@node-type="username"]')  
        password = self.browser.find_element_by_xpath('//input[@node-type="password"]')  
        username.clear()  
        username.send_keys(self.username_)  
        password.clear()  
        password.send_keys(self.password_)  
              
        #获取提交登陆元素 并点击
        sub_btn = self.browser.find_element_by_xpath('//a[@suda-data="key=tblog_weibologin3&value=click_sign"]')  
        sub_btn.click()  
        time.sleep(5)  
        
        '''
        #下面是验证码部分，如果需要验证码的化
        while True:  
            try:  
                verify_img = browser.find_element_by_xpath('//img[@node-type="verifycode_image"]')  
            except NoSuchElementException:  
                break  
            if verify_img:  
                # 输入验证码  
                verify_code = browser.find_element_by_xpath('//input[@node-type="verifycode"]')  
                verify_code_ = input('verify_code > ')  
                verify_code.clear()  
                verify_code.send_keys(verify_code_)  
          
                # 提交登陆  
                sub_btn = browser.find_element_by_xpath('//a[@suda-data="key=tblog_weibologin3&value=click_sign"]')  
                sub_btn.click()  
                time.sleep(2)  
            else:  
                break  
        '''    
        #获取搜索栏元素
        #self.search_form = self.browser.find_element_by_xpath('//input[@class="searchInp_form"]')        
         
 
    def get_weibo_search(self,search_text,max_length = 20):  
        '''
        在网页中搜索指定信息,搜多到一页信息后，保存，然后搜索下一页信息，直至到max_length页
        默认在当前路径下生成一个  名字为 search_text(去除下划线) 的文件夹，下面存放爬取得网页
        args:
            search_text：需要搜索的内容
            max_length:最大爬取网页个数
        return:
            dst_dir:返回爬取网页所在的文件夹
        ''' 
        #将关键词送到搜索栏中，进行搜索  
        self.search_form = self.browser.find_element_by_xpath('//input[@node-type="text"]')        
        self.search_form.clear() 
        self.search_form.send_keys(search_text)
        time.sleep(5)
        
        #获取搜索按钮元素 只有窗口最大化 才有搜索按钮
        self.search_btn = self.browser.find_element_by_xpath('//button[@class="s-btn-b"]') 
        #点击搜索   
        self.search_btn.click()  
        #进入循环之前，让第一页先加载完全。  
        time.sleep(2)
             
        print('Try download html for : {}'.format(search_text))       
        topics_name = search_text  
        #将名字里面的空格换位_   创建以搜索内容为名字的文件夹，保存爬取下来的网页
        topics_name = topics_name.replace(" ","_")
        os_path = os.getcwd()  
        dst_dir = os.path.join(os_path,topics_name)  
        if not os.path.isdir(dst_dir):  
            os.mkdir(dst_dir)  
        #捕获异常，有的搜索可能没有下一页 遇到错误会跳过
        try:            
            count = 1              
            while count <= max_length:  
                '''
                #保存网页   构建目标文件路径
                '''
                file_name = os.path.join(dst_dir,'{}_{}.html'.format(topics_name, count))  
                #必须指定编码格式 
                with open(file_name, 'w',encoding='utf-8') as f:  
                    f.write(self.browser.page_source)  
                print('Page {}  download  finish!'.format(count))  
              
                time.sleep(3)
                #获取下一页元素
                self.next_page = self.browser.find_element_by_css_selector('a.next')  
                #next_page = browser.find_element_by_xpath('//a[@class="page next S_txt1 S_line1"]')              
                #有的时候需要手动按F5刷新，不然等太久依然还是出不来，程序就会挂，略脆弱。  
                self.next_page.click()
                count += 1  
                #完成一轮之前，保存之前，先让其加载完，再保存   如果报错，可以通过调节时间长度去除错误
                time.sleep(10)
        except Exception as e:
            print('Error:',e)
        return dst_dir
  
    def get_weibo_text(self,file_name):
        '''
        将html文件里面的<p></p>标签的内容提取出来  
        
        args:
            text:返回一个list  每一个元素都是p标签的内容
        args:
            file_name:html文件路径 
        '''        
        text = []  
        soup = BeautifulSoup(open(file_name,encoding='utf-8'),'lxml')          
        #获取tag为div class_="WB_cardwrap S_bg2 clearfix"的所有标签
        items = soup.find_all("div",class_="WB_cardwrap S_bg2 clearfix")  
        if not items:  
            text = []  
        #遍历每一额标签，提取为p的子标签内容
        for item in items:  
            line = item.find("p").get_text()  
            #print line  
            text.append(line)  
        return text  
      
    def get_weibo_all_page(self,path):
        '''
        文件夹下所有文件内容提取出来，然后合并起来
        
        args:
            path:list 每个元素都是一个文件路径，加入我们在新浪搜索内容为火影忍者，则在当前路径下会生成一个为火影忍者的文件夹
                path就是这个文件夹的路径
        return:
            texts_all:返回合并之后的内容 是一个list            
        '''        
        texts_all = []  
        file_names = os.listdir(path)  
        #遍历当前文件夹下每个文件
        for file_name in file_names:              
            #将html文件里面的<p></p>标签的内容提取出来
            texts = self.get_weibo_text(os.path.join(path,file_name))  
            #遍历每一个元素
            for text in texts:  
                text = text.replace("\t","")  
                text = text.strip("\n")  
                text = text.strip(" ")  
                #若是重了，不加入到里面  
                if text in texts_all:
                    pass  
                else:  
                    texts_all.append(text)  
        return texts_all  
      
        
    def get_results_weibo(self,weibos_name):
        '''
        合并若干个文件夹下提取出来的微博  
        
        args：
            weibos_name：list 每一个元素都是一个搜索项提取出来的文本文件
        args:
            
        '''
        texts = []  
        for file_name in weibos_name:  
            with open(file_name,encoding='utf-8') as f:  
                text = f.readlines()  
                for line in text:  
                    line = line.strip("\n")  
                    if line not in texts:  
                        texts.append(line)  
        return texts  




if __name__ == '__main__':
    print('开始搜索')
    search = WeiboSpyder()
    
    #在新浪搜索中需要搜索内容
    searchs_text = ["火影忍者","苍井空","波多野结衣"]  
    
    #遍历要搜索的每一行
    for search_text in searchs_text:  
        path = search.get_weibo_search(search_text,max_length=5)          
        texts_all = search.get_weibo_all_page(path)  
        #文本排序
        texts_all_sorted = sorted(texts_all)  
        weibo_text_name = path + "_weibos.txt"  
        with open(weibo_text_name,"w",encoding='utf-8')  as f:
            #一行一行的写入
            for text in texts_all_sorted:  
                f.write(text + "\n")          


    #将几个_weibos.txt文件合并到一起  
    print("Together:")  
    file_names_weibos = [i for i in os.listdir(os.getcwd()) if i.endswith("_weibos.txt")]  
    texts = search.get_results_weibo(file_names_weibos)  
    with open("results.txt","w",encoding='utf-8')  as f:
        for text in sorted(texts):  
            f.write(text+"\n")      
    print("Together finish!")  
    