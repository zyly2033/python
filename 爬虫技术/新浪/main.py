#-*- coding: utf-8 -*-
import os
from bs4 import BeautifulSoup
import random
from faker import Factory
import queue
import threading
import urllib.request as urllib  
from selenium import webdriver
import time
import logging
from selenium.common.exceptions import NoSuchElementException



def Weibo():
    ''''' 
    python爬虫——基于selenium用火狐模拟登陆爬搜索关键词的微博:https://blog.csdn.net/u010454729/article/details/51225388
    0.安装火狐 
    1.安装selenium，可通过pip安装：pip install selenium 
    2.程序里面改三处：用户名、密码、搜过的关键词search_text 
    3.需要手动输入验证码，并且验证码大小写敏感，若是输错了，等3秒再输入。 
    4.爬下来了，用BeautifulSoup提取,并且将相同文本的放在一起，并且排序 
     
    时间：5秒一个，不可缩短，不然加载不出来下一页这个按钮，然后程序就挂了，若网速快些，延时可以短些。 
    '''  
    username_  = "你自己的微博号"  
    password_  = "密码"  
    #logging.basicConfig(level=logging.DEBUG)  
    logging.basicConfig(level=logging.INFO)  
          
    browser = webdriver.Firefox()  
    href = 'http://s.weibo.com/'  
    browser.get(href)  
    time.sleep(2)  
          
    # 右上登陆  
    login_btn = browser.find_element_by_xpath('//a[@node-type="loginBtn"]')  
    login_btn.click()  
    time.sleep(3)  
          
    # 选择账号登录  
    name_login = browser.find_element_by_xpath('//a[@action-data="tabname=login"]')  
    name_login.click()  
    time.sleep(2)  
          
    # 输入用户名,密码  
    username = browser.find_element_by_xpath('//input[@node-type="username"]')  
    password = browser.find_element_by_xpath('//input[@node-type="password"]')  
    username.clear()  
    username.send_keys(username_)  
    password.clear()  
    password.send_keys(password_)  
          
    # 提交登陆  
    sub_btn = browser.find_element_by_xpath('//a[@suda-data="key=tblog_weibologin3&value=click_sign"]')  
    sub_btn.click()  
          
    while True:  
        try:  
            verify_img = browser.find_element_by_xpath('//img[@node-type="verifycode_image"]')  
        except NoSuchElementException:  
            break  
        if verify_img:  
            # 输入验证码  
            verify_code = browser.find_element_by_xpath('//input[@node-type="verifycode"]')  
            verify_code_ = input('verify_code > ')  
            verify_code.clear()  
            verify_code.send_keys(verify_code_)  
      
            # 提交登陆  
            sub_btn = browser.find_element_by_xpath('//a[@suda-data="key=tblog_weibologin3&value=click_sign"]')  
            sub_btn.click()  
            time.sleep(2)  
        else:  
            break  
    #===============================以上这部分的登陆，放在函数里面返回browser出错，需要单独拿出来========================  
    def get_weibo_search(search_text, browser):  
        search_form.send_keys(search_text.decode('utf-8'))#将关键词送到搜索栏中，进行搜索  
        # 点击搜索  
        search_btn = browser.find_element_by_xpath('//a[@class="searchBtn"]')  
        search_btn.click()  
        time.sleep(3)#进入循环之前，让第一页先加载完全。  
        # 这块可以得到具体的网页信息  
        count = 1  
        logging.info('try download html for : {}'.format(search_text.encode("gbk")))  
          
        topics_name = search_text.encode("gbk")  
        topics_name = topics_name.replace(" ","_")#将名字里面的空格换位_  
        os_path = os.getcwd()  
        key_dir = os.path.join(os_path,topics_name)  
        if not os.path.isdir(key_dir):  
            os.mkdir(key_dir)  
        while True:  
            # 保存网页      
            file_name = topics_name+os.sep+'{}_{}.html'.format(topics_name, count)  
            with open(file_name, 'w') as f:  
                f.write(browser.page_source)  
            logging.info('for page {}'.format(count))  
          
            try:  
                next_page = browser.find_element_by_css_selector('a.next')  
                #next_page = browser.find_element_by_xpath('//a[@class="page next S_txt1 S_line1"]')  
                time.sleep(1)  
                next_page.click()#有的时候需要手动按F5刷新，不然等太久依然还是出不来，程序就会挂，略脆弱。  
                count += 1  
                time.sleep(5)#完成一轮之前，保存之前，先让其加载完，再保存  
            except Exception as e:#异常这里，跑一次，跳出，跑多个query,断了一个query后，下个继续不了，还需优化  
                logging.error(e)  
                break  
    #======================================================  
    def get_weibo_text(file_name):#将html文件里面的<p></p>标签的内容提取出来  
        text = []  
        soup = BeautifulSoup(open(file_name))  
        items = soup.find_all("div",class_="WB_cardwrap S_bg2 clearfix")  
        if not items:  
            text = []  
        for item in items:  
            line = item.find("p").get_text()  
            #print line  
            text.append(line)  
        return text  
          
    def get_weibo_all_page(path, file_names):#将<span style="font-family: Arial, Helvetica, sans-serif;">文件夹下所有文件里提取出来微博的合并起来</span>  
        texts_all = []  
        for file_name in file_names:  
            #print file_name,  
            texts = get_weibo_text(path+os.sep+file_name)  
            #print len(texts)  
            for text in texts:  
                text = text.replace("\t","")  
                text = text.strip("\n")  
                text = text.strip(" ")  
                if text in texts_all:#若是重了，不加入到里面  
                    pass  
                else:  
                    texts_all.append(text)  
        return texts_all  
      
    def get_results_weibo(weibos_name):#合并若干个文件夹下提取出来的微博  
        texts = []  
        for file_name in weibos_name:  
            with open(file_name) as f:  
                text = f.readlines()  
                for line in text:  
                    line = line.strip("\n")  
                    if line not in texts:  
                        texts.append(line)  
        return texts  



def random_proxies(proxy_ips):
    '''
    从proxy_ips中随机选取一个代理ip    
    
    args:
        proxy_ips:list 每个元素都是一个代理ip
    '''
    ip_index = random.randint(0, len(proxy_ips)-1)
    res = { 'http': proxy_ips[ip_index] }
    return res

def fix_characters(s):
    '''
    替换掉s中的一些特殊字符
    
    args:
        s：字符串
    '''
    for c in ['<', '>', ':', '"', '/', '\\', '|', '?', '*']:
        s = s.replace(c, '')
    return s


def getUrlRespHtml(url):
    '''
    爬取静态页面数据
    '''
    # 这里配置可用的代理IP，可以写多个
    proxy_ips = [
                '183.129.151.130' 
            ]
    headers = {'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8', 
            'Accept-Charset':'GB2312,utf-8;q=0.7,*;q=0.7', 
            'Accept-Language':'zh-cn,zh;q=0.5', 
            'Cache-Control':'max-age=0', 
            'Connection':'keep-alive', 
            'Host':'John', 
            'Keep-Alive':'115', 
            'Referer':url, 
            'User-Agent': fake.user_agent()
            #'User-Agent': 'User-Agent:Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36'
            }
    req = urllib.Request(url=url,origin_req_host=random_proxies(proxy_ips),headers = headers)  
    #当该语句读取的返回值是bytes类型时，要将其转换成utf-8才能正常显示在python程序中
    response = urllib.urlopen(req).read()         
    response = response.decode('utf-8')     #需要进行类型转换才能正常显示在python中
    return response


def get_dynamic_url(url):
    '''    
    selenium是一个用于Web应用自动化程序测试的工具，测试直接运行在浏览器中，就像真正的用户在操作一样
    selenium2支持通过驱动真实浏览器（FirfoxDriver，IternetExplorerDriver，OperaDriver，ChromeDriver）
    selenium2支持通过驱动无界面浏览器（HtmlUnit，PhantomJs）
    

    1、下载geckodriver.exe：下载地址：https://github.com/mozilla/geckodriver/releases请根据系统版本选择下载；（如Windows 64位系统）
    2、下载解压后将getckodriver.exe复制到Firefox的安装目录下，
    如（C:\Program Files\Mozilla Firefox），并在环境变量Path中添加路径：C:\Program Files\Mozilla Firefox
    '''         
    browser = webdriver.Firefox(executable_path = r'D:\ff\geckodriver.exe')    
    browser.get(url)    
    html = browser.page_source
    #关闭浏览器
    browser.quit() 
    return html


class Spider(threading.Thread):
    '''
    主要思路分成两部分：
        1.用来发起http请求分析出播放列表然后丢到队列中
        2.在队列中逐条下载文件到本地
        一般分析列表速度较快，下载速度比较慢，可以借助多线程同时进行下载
    '''
    def __init__(self, base_url, releate_urls, queue=None):
        '''
        args:
            base_url:网址
            releate_urls：相对于网址的网页路径  list集合
            queue:队列
        '''
        threading.Thread.__init__(self)
        print('Start spider\n')
        print ('=' * 50)
        
        #保存字段信息
        self.base_url = base_url
        self.queue = queue
        self.vol = '1'
        self.releate_urls = releate_urls

    def run(self):
        #遍历每一个网页 并开始爬取
        for releate_url in self.releate_urls:
            self.spider(releate_url)
        print ('\nCrawl end\n\n')
    
    def spider(self, releate_url):
        '''
        爬取指定网页信息
        
        args:
            releate_url:网页的相对路径
        '''
        url = os.path.join(self.base_url,releate_url)        
        print ('Crawling: ' + url + '\n')

        '''
        解析html  针对不同的网址，解析内容也不一样
        '''
        #使用BeautifulSoup解析这段代码,能够得到一个 BeautifulSoup 的对象,并能按照标准的缩进格式的结构输出:
        response = get_dynamic_url(url)
        #response = getUrlRespHtml(url)
        
        soup = BeautifulSoup(response, 'html.parser')
    
        #print(soup.prettify())
        with open('./read.html','w',encoding='utf-8') as f:
            f.write(str(soup.prettify()))

        '''
        解析该网站某一期的所有音乐信息
        主要包括：期刊信息
                 期刊封面
                期刊描述
                节目清单             
        '''
        
        #<span class="vol-title">曙色初动</span>  解析标题  find每次只返回第一个元素
        title = soup.find('span',attr={'class':'vol-title'})
        '''
        <div class="vol-cover-wrapper" id="volCoverWrapper">
            <img src="http://img-cdn2.luoo.net/pics/vol/554f999074484.jpg!/fwfh/640x452" alt="曙色初动" class="vol-cover">
            <a href="http://www.luoo.net/vol/index/739" class="nav-prev" title="后一期" style="display: inline; opacity: 0;">&nbsp;</a><a href="http://www.luoo.net/vol/index/737" class="nav-next" title="前一期" style="display: inline; opacity: 0;">&nbsp;</a>
        </div>
        解析对应的图片背景
        '''        
        cover = soup.find('img', attrs={'class': 'vol-cover'})
        '''
        <div class="vol-desc">
        本期音乐为史诗氛围类音乐专题。<br><br>史诗音乐的美好之处在于能够让人有无限多的宏伟想象。就像这一首首曲子，用岁月沧桑的厚重之声，铿锵有力的撞击人的内心深处，绽放出人世间的悲欢离合与决绝！<br><br>Cover From Meer Sadi    
        </div>
        解析描述文本
        '''
        desc = soup.find('div', attrs={'class': 'vol-desc'})
        
        '''
        <a href="javascript:;" rel="nofollow" class="trackname btn-play">01. Victory</a>
        <a href="javascript:;" rel="nofollow" class="trackname btn-play">02. Mythical Hero</a>
        
        解析歌单   find_all返回一个列表 所有元素
        '''        
        track_names = soup.find_all('a', attrs={'class': 'trackname'})
        track_count = len(track_names)
        tracks = []
        # 12期前的音乐编号1~9是1位（如：1~9），之后的都是2位 1~9会在左边垫0（如：01~09）
        for track in track_names:            
            _id = str(int(track.text[:2])) if (int(releate_url) < 12) else track.text[:2]  
            _name = fix_characters(track.text[4:])
            tracks.append({'id': _id, 'name': _name})

        phases = {
            'phase': releate_url,                # 期刊编号
            'title': title,                      # 期刊标题
            'cover': cover,                      # 期刊封面
            'desc': desc,                        # 期刊描述
            'track_count': track_count,          # 节目数
            'tracks': tracks                     # 节目清单(节目编号，节目名称)
            }   
        #追加到队列
        self.queue.put(phases)


class Downloader(threading.Thread):    
    '''
     从队列中读取文件信息，并构建下载文件路径，开始下载文件
    下载文件
    '''
    def __init__(self, url, dst, queue=None):
        '''
        args:
            url:下载文件的源路径
            dst：下载下来文件的保存路径
            queue：队列
        '''
        threading.Thread.__init__(self)
        self.url = url
        self.queue = queue
        self.dst = dst
        self.__counter = 0

        
    def run(self):
        '''
        从队列中取数据，并进行下载
        '''
        while True:
            if self.queue.qsize() <= 0:
                pass
            else:
                phases = self.queue.get()
                self.download(phases)

    def download(self, phases):
        '''
        下载文件
        
        args:
            phases:一个期刊的信息
        '''
        #遍历每一个节目清单(节目编号，节目名称)
        for track in phases['tracks']:
            file_url = self.url % (phases['phase'], track['id'])
            local_file_dict = '%s/%s' % (self.dst, phases['phase'])
            if not os.path.exists(local_file_dict):
                os.makedirs(local_file_dict)
            
            local_file = '%s/%s.%s.mp3' % (local_file_dict, track['id'], track['name'])
            if not os.path.isfile(local_file):
                print ('downloading: ' + track['name'])                
                #response = getUrlRespHtml(file_url)
                response = get_dynamic_url(file_url)
                
                with open(local_file, 'wb') as f:
                    f.write(response)                    
                print ('done.\n')
            else:
                print ('break: ' + track['name'])


if __name__ == '__main__':
    
    fake = Factory.create()

    luoo_site = 'http://www.luoo.net/vol/index'
    #音乐文件地址 是一种格式化的字符串
    luoo_site_mp3 = 'http://luoo-mp3.kssws.ks-cdn.com/low/luoo/radio%s/%s.mp3'        

    spider_queue = queue.Queue()

    luoo = Spider(luoo_site, releate_urls=['680'], queue=spider_queue)
    luoo.setDaemon(True)
    luoo.start()

    #下载
    downloader_count = 5
    for i in range(downloader_count):
        luoo_download = Downloader(luoo_site_mp3, './luoo', queue=spider_queue)
        luoo_download.setDaemon(True)
        luoo_download.start()
        
        
    '''    
    try:  
    #browser = login()  
    # 进行搜索  
    search_form = browser.find_element_by_xpath('//input[@class="searchInp_form"]')  
    search_form.clear()  
    searchs_text = ["火影忍者","火影忍者 雏田","火影忍者 小樱"]  
    for search_text in searchs_text:  
        get_weibo_search(search_text, browser)  
        #======  
        path = search_text.encode("gbk")  
        path = path.replace(" ","_")#文件夹路径里面若有空格，替换为下滑线  
          
        file_names = os.listdir(path)  
        texts_all = get_weibo_all_page(path, file_names)  
          
        texts_all_sorted = sorted(texts_all)  
        weibo_text_name = path+"_weibos.txt"  
        f = open(weibo_text_name,"w")  
        for text in texts_all_sorted:  
            f.write(text+"\n")  
        f.close()  
    except :  
        pass
    #============将几个_weibos.txt文件合并到一起  
    print("together:")  
    file_names_weibos = [i for i in os.listdir(os.getcwd()) if i.endswith("_weibos.txt")]  
    texts = get_results_weibo(file_names_weibos)  
    f = open("results.txt","w")  
    for text in sorted(texts):  
        f.write(text+"\n")  
    f.close()  
    '''