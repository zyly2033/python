#  LCD P35参数设定
LCD_WIDTH = 240             # LCD面板的行宽
LCD_HEIGHT = 320            # LCD面板的列宽
VSPW = 1                # 通过计算无效行数垂直同步脉冲宽度决定VSYNC脉冲的高电平宽度
VBPD = 1                # 垂直同步周期后的无效行数
LINEVAL = (LCD_HEIGHT-1)  # LCD的垂直宽度-1
VFPD = 1                # 垂直同步周期前的的无效行数
CLKVAL = 7              # VCLK = HCLK / [(CLKVAL  + 1)  × 2]
HSPW =  9               # 通过计算VCLK的数水平同步脉冲宽度决定HSYNC脉冲的高电平宽度
HBPD  = 19               # 描述水平后沿为HSYNC的下降沿与有效数据的开始之间的VCLK周期数
HOZVAL = (LCD_WIDTH-1)    # LCD的水平宽度-1
HFPD =  9                # 水平后沿为有效数据的结束与HSYNC的上升沿之间的VCLK周期数 *

GPCUP = 0xffffffff
GPCCON = 0xaaaaaaaa
GPDUP = 0xffffffff
GPDCON = 0xaaaaaaaa

# 或运算
GPGUP = (1 << 4)
GPGCON = (3 << 8)
LCDCON5 = (1 << 3)

LCDCON1 = (CLKVAL << 8) | (3 << 5) | (0xC << 1)
LCDCON2 = (VBPD<<24)|(LINEVAL<<14)|(VFPD<<6)|(VSPW)
LCDCON3 = (HBPD << 19) | (HOZVAL << 8) | (HFPD)
LCDCON4 = (HSPW)
LCDCON5  |= ((1<<11) | (1<<10) | (1 << 9) | (1 << 8) | (1 << 0))
#TCONSEL &= ~((1 << 4) | 1)
TPAL = 0x00
LCDCON1 |= 1

if __name__ == '__main__':
    print('LCDCON1=%#x'%LCDCON1)  #
    print('LCDCON2=%#x'%LCDCON2)  #
    print('LCDCON3=%#x'%LCDCON3)  #
    print('LCDCON4=%#x'%LCDCON4)  #
    print('LCDCON5=%#x'%LCDCON5)  #