#装饰器
#通过函数的内部属性 拿到函数的__name__属性
def now():
    print('2017-11-28')
    
now()
print(now.__name__)

#如果我们想在函数调用之前自动打印日期，但是不想修改原函数，早函数运行期间动态增加功能的方法，称之为'装饰器'
def log(func):                              #传入一个函数作为参数
    def wrapper(*args,**kw):                #该函数可以接受任意参数的调用
        print('call %s()'%func.__name__)    #打印函数调用
        return func(*args,**kw)             #函数调用 调用原始函数
    return wrapper                          #返回一个函数

#我们要借助Python的@语法，把decorator置于函数的定义处：
@log                                       #相当于执行了 now = log(now)  即返回了函数wrapper  参数变量func=now
def now():
    print('2017-11-28')
#函数调用
now()
print(now.__name__)                        #wrapper   函数的__name__属性发生改变了


@log
def f():
    pass
#函数调用
now()
print(now.__name__)                        #wrapper   函数的__name__属性发生改变了


#如果decorator本身需要传入参数 重新编写一个返回decorator的
import functools
def log(txt):
    def decrator(func):
        @functools.wraps(func)                       #在wrapper函数之前加上，保证原有__name__属性不改变      
        def wrapper(*args,**kw):                    #该函数可以接受任意参数的调用
            print('%s %s()'%(txt,func.__name__))    #打印函数调用
            return func(*args,**kw)                 #函数调用 调用原始函数
        return wrapper                              #返回一个函数
    return decrator

@log('execute')                                     #相当于执行了 now = log('execute')(now)  即decrator(now)  即wrapper 
def now():
    print('2017-11-28')
#函数调用 
now()                                               #execute now()
print(now.__name__)                                 #now函数的__name__属性没有发生改变了

@log('execute')
def f():
    pass
#函数调用 
f()



#请设计一个decorator，它可作用于任何函数上，并打印该函数的执行时间：
import time, functools
def metric(func):
    @functools.wraps(func)                       #在wrapper函数之前加上，保证原有__name__属性不改变     
    def wrapper(*args,**kw):
        t1 = time.time()                         #保存当前时间
        #print(t1)
        res = func(*args,**kw)                    #函数调用 调用原始函数  并保存返回值                              
        t2 = time.time()                          #保存当前时间
        print('%s executed in' % func.__name__,t2-t1,'ms')
        return res
    return wrapper
# 测试
@metric
def fast(x, y):
    time.sleep(0.0012)
    return x + y;

@metric
def slow(x, y, z):
    time.sleep(0.1234)
    return x * y * z;

f = fast(11, 22)
s = slow(11, 22, 33)
if f != 33:
    print('测试失败!')
elif s != 7986:
    print('测试失败!')
else:
    print('测试成功!')
    

#请编写一个decorator，能在函数调用的前后打印出'begin call'和'end call'的日志。
def decrator(func):
    @functools.wraps(func)                       #在wrapper函数之前加上，保证原有__name__属性不改变     
    def wrapper(*args,**kw):
        print("begin call %s"%func.__name__)
        s1 = func(*args,**kw)        
        print("end call %s"%func.__name__)
        return s1
    return wrapper
@decrator                                        #相当于执行了 now = decrator(now)  即返回了函数wrapper  
def now():
    print('2017-11-28')

#函数调用
now()
print(now.__name__)



    
    

        
