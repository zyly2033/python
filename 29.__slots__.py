##给实例绑定一个属性
class Student(object):
    pass

s = Student()
s.name = '郑洋' 
print(s.name)


#给实例绑定一个方法，对另一个实例是不起作用的
def set_age(self,age):           #定义一个函数作为实例方法
    self.age = age
from types import MethodType
s.set_age = MethodType(set_age,s)   #给实例绑定一个方法
s.set_age(55)
print(s.age)                         #测试


#给class绑定方法，对所有实例均可用

#使用__slots__变量，限制实例能增加的属性有哪些
class Student(object):
    __slots__ = ('name','age')   #用tuple定义运行绑定的属性名称

s = Student()
s.name = '郑洋'
s.age = 24
#s.score = 99
#print(s.score)


#如果子类定义了__slots__，则子类实例允许的属性为自身的__slots__加上父类的__slots__
class SeniorStudent(Student):
    __slots__=('level')

s = SeniorStudent()
s.level = '高一'
print(s.level)
s.name = '黄旭'
#s.score = 88   报错



