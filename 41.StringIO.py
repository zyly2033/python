# -*- coding: utf-8 -*-
"""
Created on Sun Dec  3 15:04:14 2017

@author: zy
"""

#在内存中读取文件
#StringIo在内存中读取str
#写
from io import StringIO
with StringIO() as f:    
    f.write('hello')
    f.write(' ')
    f.write('world')
    print(f.getvalue())

#读
with StringIO('Hello\n World\n Zhengyang') as f: 
    for x in f.readlines():
        print(x.split())
        
#BytesIo在内存中读取bytes
#写
from io import BytesIO
with BytesIO() as f:    
    f.write('中文'.encode('ansi'))   #ansi在这里指的是GB2312
    print(f.getvalue())

#读
from io import BytesIO
with BytesIO(b'\xd6\xd0\xce\xc4') as f:         
    print(f.read())