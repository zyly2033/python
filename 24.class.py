#在Python中，定义类是通过class关键字
#class后面紧接着是类名，即Student，类名通常是大写开头的单词，紧接着是(object)，表示该类是从哪个类继承下来的，继承的概念我们后面再讲，通常，如果没有合适的继承类，就使用object类，这是所有类最终都会继承的类。
class Student(object):
    #特殊的方法，类似C++中的构造函数  __init__方法的第一个参数永远是self，表示创建的实例本身，因此，在__init__方法内部，就可以把各种属性绑定到self，因为self就指向创建的实例本身。
    def __init__(self,name,score):
        self.name=name
        self.score=score

    #数据封装 访问类中的数据，没有必要从外面的函数去访问，可以直接在Student类的内部定义访问数据的函数，这样，就把“数据”给封装起来了。这些封装数据的函数是和Student类本身是关联起来的，我们称之为类的方法：
    def print_score(self):    #通过在实例上调用方法，我们就直接操作了对象内部的数据，但无需知道方法内部的实现细节
        print('%s %s'%(self.name,self.score))

    def get_grade(self):
        if self.score >= 90:
            return 'A'
        elif self.score >= 60:
            return 'B'
        else:
            return 'C'

#创建实例
bar = Student('郑洋',99)
lst = Student('黄旭',100)
bar.age=24          #可以自由地给一个实例变量绑定属性
print(bar.age)
#print(lst.age)   #报错  和静态语言不同，Python允许对实例变量绑定任何数据，也就是说，对于两个实例变量，虽然它们都是同一个类的不同实例，但拥有的变量名称都可能不同：
print(bar.get_grade())
print(lst.get_grade())
bar.print_score()
lst.print_score()



