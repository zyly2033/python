#列表生成式
#生成1-10的列表
print(list(range(1,11)))
#生成[1*1,2*2,3*3...]的列表
#使用常规方法
L = []
for i in range(1,11):
    L.append(i*i)
print(L)

#使用列表生成式生成
print([ i*i for i in range(1,11)])

#for循环后面还可以加上if判断，这样我们就可以筛选出仅偶数的平方：
print([x * x for x in range(1, 11) if x % 2 == 0])

#还可以使用两层循环，可以生成全排列：
print([m + n for m in 'ABC' for n in 'XYZ'])

#列出当前目录下的所有文件和目录名，可以通过一行代码实现：
import os       # 导入os模块，模块的概念后面讲到
print([d for d in os.listdir('.')])   # os.listdir可以列出文件和目录

#for循环其实可以同时使用两个甚至多个变量，比如dict的items()可以同时迭代key和value：
d = {'x': 'A', 'y': 'B', 'z': 'C' }
for k, v in d.items():
    print(k, '=', v)

#因此，列表生成式也可以使用两个变量来生成list：
d = {'x': 'A', 'y': 'B', 'z': 'C' }
print([k + '=' + v for k, v in d.items()])

#转小写
#如果list中既包含字符串，又包含整数，由于非字符串类型没有lower()方法，所以列表生成式会报错：
L=['Hello','Every','Body','Father']
#L1 = ['Hello', 'World', 18, 'Apple', None]
print([s.lower() for s in L])

#使用内建的isinstance函数可以判断一个变量是不是字符串：
print(isinstance('123',str))

#请修改列表生成式，通过添加if语句保证列表生成式能正确地执行：
L1 = ['Hello', 'World', 18, 'Apple', None]
L2 = [s.lower() for s in L1 if isinstance(s,str)]
print(L2)
if L2 == ['hello', 'world', 'apple']:
    print('测试通过!')
else:
    print('测试失败!')

