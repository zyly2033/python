n = abs(-10)      #abs是函数名，函数名也是一个变量
print(n)
#既然变量可以指向函数，函数的参数能接收变量，那么一个函数就可以接收另一个函数作为参数，这种函数就称之为高阶函数。
def add(x,y,f):
    return f(x)+f(y)

n = add(-5,6,abs)
print(n)


#map的使用 map()函数接收两个参数，一个是函数，一个是Iterable，map将传入的函数依次作用到序列的每个元素，并把结果作为新的Iterator返回。
def f(x):
    return x*x
r =  map(f,list(range(1,10)))  #返回的是一个Iterator类型对象
print(r)
lis = list(r)     #通过该函数把Iterator整个序列计算出来，并返回一个list
print(lis)

#把这个list所有数字转为字符串：
s1 = list(map(str, [1, 2, 3, 4, 5, 6, 7, 8, 9]))

#reduce把一个函数作用在一个序列[x1, x2, x3, ...]上，这个函数必须接收两个参数，reduce把结果继续和序列的下一个元素做累积计算，其效果就是：
#reduce(f, [x1, x2, x3, x4]) = f(f(f(x1, x2), x3), x4)
from functools import reduce
def add(x, y):
    return x + y
reduce(add, [1, 3, 5, 7, 9])
#但是如果要把序列[1, 3, 5, 7, 9]变换成整数13579
def fn(x, y):
    return x * 10 + y
reduce(fn, [1, 3, 5, 7, 9])

#如果考虑到字符串str也是一个序列 把str转换为int的函数：
def char2num(s):
    return {'0': 0, '1': 1, '2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9}[s]
num = reduce(fn, map(char2num, '13579'))
print(num)


#字符串转为数字
def str2int(s):
    def fn(x, y):
        return x * 10 + y
    def char2num(s):
        return {'0': 0, '1': 1, '2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9}[s]
    return reduce(fn, map(char2num, s))


#还可以用lambda函数进一步简化成：
#def char2num(s):
#    return {'0': 0, '1': 1, '2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9}[s]
#def str2int(s):
#    return reduce(lambda x, y: x * 10 + y, map(char2num, s))

#利用map()函数，把用户输入的不规范的英文名字，变为首字母大写，其他小写的规范名字。输入：['adam', 'LISA', 'barT']，输出：['Adam', 'Lisa', 'Bart']：
def normalize(s):        #首字母转为大写
    return s[0].upper()+s[1:].lower()
L1 = ['adam', 'LISA', 'barT']
L2 = list(map(normalize, L1))
print(L2)

#Python提供的sum()函数可以接受一个list并求和，请编写一个prod()函数，可以接受一个list并利用reduce()求积：
def prod(L):
    return reduce(lambda x,y:x*y,L)

print('3 * 5 * 7 * 9 =', prod([3, 5, 7, 9]))
if prod([3, 5, 7, 9]) == 945:
    print('测试成功!')
else:
    print('测试失败!')

print('123234'.find('.'))    #返回-1
#利用map和reduce编写一个str2float函数，把字符串'123.456'转换成浮点数123.456：
def str2float(s):    
     loc = s.find('.')         #获取'.'的位置
     s = s.replace('.','')     #把'.'替换掉
     if loc == -1:
         n = 0
     else:
         n = len(s) - loc;        #获取小数点后面小数的位数
     def char2nums(s):        
        return {'0':0,'1':1,'2':2,'3':3,'4':4,'5':5,'6':6,'7':7,'8':8,'9':9}[s]   
     return   reduce(lambda x,y:x*10+y,map(char2nums,s))/pow(10,n)

print(str2float('0.12344'))    
print('str2float(\'123.456\') =', str2float('123.456'))
if abs(str2float('123.456') - 123.456) < 0.00001:
    print('测试成功!')
else:
    print('测试失败!')



    

