#通过列表生成式，我们可以直接创建一个列表。但是，受到内存限制，列表容量肯定是有限的。而且，创建一个包含100万个元素的列表，不仅占用很大的存储空间，如果我们仅仅需要访问前面几个元素，那后面绝大多数元素占用的空间都白白浪费了。
#所以，如果列表元素可以按照某种算法推算出来，那我们是否可以在循环的过程中不断推算出后续的元素呢？这样就不必创建完整的list，从而节省大量的空间。在Python中，这种一边循环一边计算的机制，称为生成器：generator。

#列表生成式
L = [x*x for x in range(10)]
print(L)
#生成器 要创建一个generator，有很多种方法。第一种方法很简单，只要把一个列表生成式的[]改成()，就创建了一个generator：
G = (x*x for x in range(10))
print(G)
#打印数值方式1
print(next(G))     #获取发生器下一个值
print(next(G))
print(next(G))
print(next(G))
#通过迭代遍历
for n in G:
    print(n)


#斐波拉契数列
#使用函数打印前max个数
def fib1(max):   
    n, a, b = 0, 0, 1
    while n < max:
        print(b)
        a, b = b, a + b
        n = n + 1
    return 'done'
fib1(10)
print('\n')

#把fib函数变成generator，只需要把print(b)改为yield b就可以了：
def fib(max):
    n, a, b = 0, 0, 1
    while n < max:
        yield(b)
        a, b = b, a + b
        n = n + 1
    return 'done'
f = fib(6)   #发生器对象
for x in f:
    print(x)

f = fib(6)   #指针已经遍历到最后，重新赋值  发生器对象
#但是用for循环调用generator时，发现拿不到generator的return语句的返回值。如果想要拿到返回值，必须捕获StopIteration错误，返回值包含在StopIteration的value中：
while True:
    try:
        x = next(f)
        print('g:', x)
    except StopIteration as e:
        print('Generator return value:', e.value)
        break

#generator和函数的执行流程不一样。函数是顺序执行，遇到return语句或者最后一行函数语句就返回。而变成generator的函数，在每次调用next()的时候执行，遇到yield语句返回，再次执行时从上次返回的yield语句处继续执行。
def odd():
    print('step1')
    yield 1
    print('step2')
    yield 2
    print('step3')
    yield 3
    
#调用该generator时，首先要生成一个generator对象，然后用next()函数不断获得下一个返回值：
o = odd()
next(o)
next(o)
next(o)
#next(o)   #执行3次yield后，已经没有yield可以执行了，所以，第4次调用next(o)就报错。

#          1
#        1   1
#      1   2   1
#    1   3   3   1
#  1   4   6   4   1
#1   5   10  10  5   1
#把每一行看做一个list，试写一个generator，不断输出下一行的list：

def triangles():
    las = [1]           #上一个    
    while True:
        yield las[:]            #输出  不直接使用yield las因为这可能会出错
        nex = []                #清空当前指
        n = len(las)            #获取列表长度
        i=0
        las.append(0)           #在上一次后面追回加0   
        while n>=i:
            a = las[i-1]        #上一个值
            b = las[i]          #当前值
            nex.append(a+b)
            i += 1
        las = [n for n in nex]    #保存上一次值


def triangles1():
    L = [1]
    while True:
        yield L[:]
        L.append(0)
        L = [L[i - 1] + L[i] for i in range(len(L))]
        
print('\n')
oo = triangles()
print(next(oo))
print(next(oo))
print(next(oo))
print(next(oo))
print(next(oo))
print(next(oo))
print(next(oo))
print('\n')

# 期待输出:
# [1]
# [1, 1]
# [1, 2, 1]
# [1, 3, 3, 1]
# [1, 4, 6, 4, 1]
# [1, 5, 10, 10, 5, 1]
# [1, 6, 15, 20, 15, 6, 1]
# [1, 7, 21, 35, 35, 21, 7, 1]
# [1, 8, 28, 56, 70, 56, 28, 8, 1]
# [1, 9, 36, 84, 126, 126, 84, 36, 9, 1]
n = 0
results = []
for t in triangles():
    print('输出',t)
    results.append(t)
    n = n + 1
    if n == 10:
        break
if results == [
    [1],
    [1, 1],
    [1, 2, 1],
    [1, 3, 3, 1],
    [1, 4, 6, 4, 1],
    [1, 5, 10, 10, 5, 1],
    [1, 6, 15, 20, 15, 6, 1],
    [1, 7, 21, 35, 35, 21, 7, 1],
    [1, 8, 28, 56, 70, 56, 28, 8, 1],
    [1, 9, 36, 84, 126, 126, 84, 36, 9, 1]
]:
    print('测试通过!')
else:
    print('测试失败!')

print(results)



