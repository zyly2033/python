# -*- coding: utf-8 -*-
"""
Created on Sat Dec  2 10:58:17 2017

@author: zy
"""

#首先开看一下动态语言和静态语言的区别，最大的区别就是函数和类定义的时候，不是编译时定义，而是运行时动态创建的
class Hello(object):
    def hello(self,name='world'):
        print('Hello,%s'%name)

#创建实例
h = Hello()
h.hello()
#查看类的类型
print(type(Hello))    #它的类型是type
#查看实例的类型，他的类型是Hello
print(type(h))

#由于Hello的类型是type，我们说class(类)的定义实在运行时创建的，而创建class的方法就是调用type()函数
#type()函数既可以返回一个对象的类型，又可以创建出新的类型，比如，我们可以通过type()函数创建出Hello类，而无需通过class Hello(object)...的定义：
def fn(self,name='world'):
    print('Hello,%s'%name)


#要创建一个class对象，type()函数传入三个参数
#class的名称
#继承的父类集合 注意pythion支持多重继承，如果只有一个父类，别忘了tuple的单元素写法
#class的放大名称与函数绑定    
Hello = type('Hello',(object,),dict(hello=fn))
h = Hello();
#查看类的类型
print(type(Hello))    #它的类型是type
#查看实例的类型，他的类型是Hello
print(type(h))


#元类
#除了使用type()动态创建类以外，要控制类的新建行为，还可以使用metaclass
#先定义metaclass就可以创建类，最后创建类的实例
#metaclass允许你创建类或者修改类，换句话说，你可以把类看做metaclass创建出来的实例
#metaclass必须从'type'类型派生
class MyMetaclass(type):
    def __new__(cls,name,bases,attrs):
        #遍历所有属性和方法
        print('开始遍历所有属性和方法')
        for k,v in attrs.items():
            print('%s==>%s'%(k,v))
        #在创建MyList类时要通过MyMetaclass.__new__()方法来创建
        attrs['add'] = lambda self,value:self.append(value)   #添加一个方法
        return type.__new__(cls,name,bases,attrs)
    
#定义新类 在创建MyList类时要通过MyMetaclass.__new__()方法来创建
class MyList(list,metaclass=MyMetaclass):
    #添加属性和方法
    name = '郑洋'
    def Hello(self,name):
        print('Hello %s',name)

L=MyList()
L.add(10)
print(L[0])

        
        
