# -*- coding: utf-8 -*-
"""
Created on Mon Dec  4 12:26:14 2017

@author: zy
"""
#http://blog.csdn.net/jicahoo/article/details/48064079
#http://blog.csdn.net/liyuanbhu/article/details/28611429
#http://blog.csdn.net/liyuanbhu/article/details/28870439
#NumPy是Python科学计算的基础包。它提供了多维数组对象、基于数组的各种派生对象（例如，masked Array, 矩阵）。除此之外，还提供了各种各样的加快数组操作的例程，包括数学基本计算、逻辑、图形操作、排序、选择、输入输出，离散傅立叶变换、基础线性代数、基础统计操作、随机仿真等等。
#NumPy的核心是ndarray对象。一方面，Ndarray对象封装了可以包含相同数据类型的多维数组；另一方面，为获得更好的性能， 在ndarray上的操作都是在编译过的代码上执行的。此外，和Python自身的序列对象相比，两者之间有如下不同：
#       NumPy数组的大小是固定的。Python的List是可以动态增长的。改变NumPy的大小会重新创建一个新的数组并把原来的删掉。
#       NumPy数组中的元素一定是同一类型的。（相应地，每个元素所占的内存大小也是一样的。）例外情况是：（不是特别理解：one can have arrays of (Python, including NumPy) objects, thereby allowing for arrays of different sized elements.）
#       NumPy数组支持在大量数据上进行数学计算和其他类型的操作。通常情况下，与Python自带的序列类型相比，NumPy数组上的操作执行更高效，代码量也更少。
#       越来越多的Python科学计算包都是用到了NumPy的数组；虽然这些库支持Python序列类型的输入，但是内部操作还是要先将其转换为NumPy的数组类型，而且输出通常就是NumPy数组。所以，如果你想要高效地使用这些Python的科学计算包，仅仅知道Python内建的序列类型是不够的，你还需要知道如何使用NumPy数组。

#NumPy 提供了对多维数组的支持，与Python原生支持的List类型不同，数组的所有元素必须同样的类型。数组的维度被称为axes，维数称为 rank。 
#Numpy的数组类型为 ndarray， ndarray 的重要属性包括: 
#ndarray.ndim：数组的维数，也称为rank
#ndarray.shape：数组各维的大小tuple 类型，对一个n 行m 列的矩阵来说， shape 为 (n,m)。
#ndarray.size：元素的总数。 
#Ndarray.dtype：每个元素的类型，可以是 numpy.int32, numpy.int16, and numpy.float64 等。 
#Ndarray.itemsize：每个元素占用的字节数。
#Ndarray.data：指向数据内存。

#生成数组
#有许多种方法生成数组。比如，可以将Python list 或 tuple 转化为数组，转化后的数组元素的类型由原来的对象的类型来决定。
import numpy as np
a = np.array([1,2,3,4])
print(a)
print(a.dtype)    #每个元素的类型
print(a.shape)    #数组各维的大小
b = np.array([1.2,3.4,5.6,5])
print(b)
print(b.dtype)
#指定数据类型
c = np.array( [ [1,2], [3,4] ], dtype=complex )  
print(c)
print(c.dtype)

print('----------------------------------------------')
#我们无法事先知道数组元素的具体值，但是数组大小是已知的。 这时可以用下面几种方法生成数组。 默认的，生成的数组的元素类型为float64.
#zeros 函数生成元素全部为0的数组，
print(np.zeros((3,4)))
#empty函数生成元素没有赋值的数组，这时元素值由内存中原来的内容决定。 
print(np.empty((2,3)))
#ones函数生成元素全部为1的数组
print(np.ones((2,3,4),dtype = np.int16))

#arange 函数生成的数组的元素按照等比数列排布，类似于 range函数。
print(np.arange( 10, 30, 5 ))  
print(np.arange( 0, 2, 0.3 ))                 # it accepts float arguments  

#linspace 函数有些类似matlab中的同名函数，下面是个例子: 
print(np.linspace( 0, 2, 9 ))                 # 9 numbers from 0 to 2  
x = np.linspace( 0, 2*np.pi, 100 )               # useful to evaluate function at lots of points  
f = np.sin(x) 


#基本的算术运算符都可以应用于数组类型，结果为对应元素之间的运，返回值为一个新的数组。
a = np.array([20,30,40,40])
b = np.arange(4)
print(a-b)
print(b**2)
print(10*np.sin(a))
print(a<35)

#乘法操作符 * 表示的也是元素乘法，如果需要矩阵乘法，可以使用dot函数或者生成一个matrix对象。
A = np.array( [[1,1],[0,1]])
B = np.array( [[2,0],[3,4]])
print(A*B)                  #对应元素相乘
print(np.dot(A,B))             #矩阵乘法
print(np.random.random((2,3)))

#当两个不同元素类型的数组运算时，结果的元素类型为两者中更精确的那个。（类型提升）
a = np.ones(3,dtype=np.int32)
b = np.linspace(0,np.pi,3)
print(b.dtype)
c = a*b
print(c)
print(c.dtype)

#Array类型提供了许多内置的运算方法，比如。
a = np.random.random((2,3))
print(a)
print(a.sum())      #计算所有元素和
print(a.min())      #获取最小的元素
print(a.max())      #获取最大的元素

#默认情况下， 这些方法作用于整个 array，通过指定 axis，可以使其只作用于某一个 axis : 
print(a.sum(axis = 0))  #按列求和
print(a.sum(axis = 1))  #按行求和
print(a.cumsum(axis = 1))  #按行累加

#NumPy 提供了许多常用函数，如sin, cos, and exp. 同样，这些函数作用于数组中每一个元素，返回另一个数组。
print(np.sin(a))
print(np.cos(a))
print(np.sqrt(a))
print(np.exp(a))
print(np.add(a,b))

#索引、切片、和迭代
#与list类似，数组可以通过下标索引某一个元素，也可以切片，可以用迭代器迭代。
a = np.arange(10) ** 3
print(a)
print([2,3,4,5])    #这是列表
print(a[2])
print(a[2:5])
print(a[0:6:-2])
print(a[::-1])      #倒叙排列
for x in a:
    print(x)
    

#多维索引
def f(x,y):  
    return 10*x+y   
b = np.fromfunction(f,(5,4),dtype=int)  
print(b)
print(b[2:3])
print(b[0:5,3])      #0-4行索引第3列
print(b[:,1])        #所有行索引第1列
print(b[-1])         #最后一行

#多维数组迭代时以第一个维度为迭代单位: 
for row in b:
    print(row)
    
#如果我们想忽略维度，将多维数组当做一个大的一维数组也是可以的，下面是例子
for ele in b.flat:
    print(ele)






