#list列表
classmates=['郑洋','黄旭','杨德亮']
print(classmates)
#计算列表长度
print(len(classmates))
#输出第一个元素
print(classmates[0])
#追加一个元素
classmates.append('刘稳')
print(classmates)
#插入一个元素
classmates.insert(1,'郑国光')
print(classmates)
#替换
classmates[1]='查娜'
#删除
classmates.pop(1)
print(classmates)


