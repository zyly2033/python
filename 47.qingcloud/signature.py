"""
青云生成签名：https://docs.qingcloud.com/product/api/common/signature.html#common-signature

@author zy
"""
import base64
import hmac
from hashlib import sha256
import time
from urllib import parse
import requests

'''
全局变量定义
'''
access_key_id = 'GHZFTHHBOGECTJTLIXSS'
secret_access_key = 'JO3HJ9Dcz6L1RmmbOIWlqtPhbMbjW68v3hVSjuMV'


params = {
    "access_key_id": access_key_id,
    "action": "DescribeInstances",
    "signature_method": "HmacSHA256",
    "signature_version": '1',
    "time_stamp": time.strftime('%Y-%m-%dT%H:%M:%SZ', time.localtime(time.time())),
    "version": '1',
    "zone": "pek3c"
}
uri = 'https://api.qingcloud.com/iaas/'

def create_signature():
    # 前面生成的被签名串
    string_to_sign = 'GET\n/iaas/\n'
    for (key, value) in params.items():
        value = str(value)
        value = value.replace(' ', '%20')
        value = value.replace(':', '%3A')
        string_to_sign +=  key + '=' + value + '&'
    string_to_sign = string_to_sign[0:-1]
    nn =  'GET\n/iaas/\naccess_key_id=QYACCESSKEYIDEXAMPLE&action=RunInstances&count=1&image_id=centos64x86a&instance_name=demo&instance_type=small_b&login_mode=passwd&login_passwd=QingCloud20130712&signature_method=HmacSHA256&signature_version=1&time_stamp=2013-08-27T14%3A30%3A10Z&version=1&vxnets.1=vxnet-0&zone=pek3a'
    print('参数', string_to_sign)

    secret_key = secret_access_key.encode()
    total_params = string_to_sign.encode()
    signature = hmac.new(secret_key, total_params, digestmod=sha256).digest()
    signature = base64.b64encode(signature).strip()
    signature = parse.quote_plus(signature)

    return signature

def request(signature):
    url = uri + '?'
    for (key, value) in params.items():
        value = str(value)
        value = value.replace(' ', '%20')
        value = value.replace(':', '%3A')
        url += key + '=' + value + '&'
    url += 'signature'  + '=' + signature
    print('参数', url)
    r = requests.get(url)
    return r.json()


if __name__ == '__main__':
    signature = create_signature()
    print(signature)
    res = request(signature)
    print(res)