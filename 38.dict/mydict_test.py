# -*- coding: utf-8 -*-
"""
Created on Sun Dec  3 11:01:37 2017

@author: zy
"""

#为了编写单元测试，我们需要引入Python自带的unittest模块，编写mydict_test.py如下：
import unittest
#导入第三方模块
from mydict import Dict

class TestDict(unittest.TestCase):  #编写单元测试时，我们需要编写一个测试类，从unittest.TestCase继承。
    #在每个测试方法之前执行
    def setUp(self):
        print('开始测试.....')
        
    #在每个测试方法之后执行
    def tearDown(self):
        print('测试结束...')
        
    #以test开头的方法就是测试方法，不以test开头的方法不被认为是测试方法，测试的时候不会被执行。

    #对每一类测试都需要编写一个test_xxx()方法。由于unittest.TestCase提供了很多内置的条件判断，我们只需要调用这些方法就可以断言输出是否是我们所期望的。
    #最常用的断言就是assertEqual()：
    #另一种重要的断言就是期待抛出指定类型的Erro
    def test__init(self):
        d = Dict(a=1,b='test')
        #测试两个值是不是相等
        self.assertEqual(d.a,1)
        self.assertEqual(d.b,'test')
        self.assertTrue(isinstance(d,dict))
        
    def test_key(self):
        d = Dict()
        d['key'] = 'value'
        self.assertEqual(d.key,'value')
        
    def test_attr(self):
        d= Dict()
        d.key = 'value'
        self.assertEqual(d['key'],'value')
        self.assertTrue('key' in d)
        
    def test_keyerror(self):
        d = Dict()
        #通过d['empty']访问不存在的key时，我们期望抛出KeyError：
        with self.assertRaises(KeyError):
            value = d['empty']
            
    def test_attrerror(self):
        d =Dict()
        with self.assertRaises(AttributeError):
            value = d.empty