# -*- coding: utf-8 -*-
"""
Created on Sun Dec  3 11:02:24 2017

@author: zy
"""

#单元测试是用来对一个模块、一个函数或者一个类来进行正确性检验的测试工作。

#比如对函数abs()，我们可以编写出以下几个测试用例：

#输入正数，比如1、1.2、0.99，期待返回值与输入相同；

#输入负数，比如-1、-1.2、-0.99，期待返回值与输入相反；

#输入0，期待返回0；

#输入非数值类型，比如None、[]、{}，期待抛出TypeError。

#把上面的测试用例放到一个测试模块里，就是一个完整的单元测试。

#如果单元测试通过，说明我们测试的这个函数能够正常工作。如果单元测试不通过，要么函数有bug，要么测试条件输入不正确，总之，需要修复使单元测试能够通过。

#我们来编写一个Dict类
class Dict(dict):
    def __init__(self,**kw):
        super(Dict,self).__init__(**kw)
    
    def __getattr__(self,key):
        try:
            return self[key]
        except KeyError:
            raise AttributeError(r"'Dict' object has no attribute '%s'"%key)
            
    def __setattr__(self,key,value):
        self[key] = value
        
##测试
d = Dict(a=1,b='test')
print(d.a)
print(d['a'])