# -*- coding: utf-8 -*-
"""
Created on Sat Jun  9 18:38:43 2018

@author: zy
"""

'''
python imp模块
https://blog.csdn.net/csujiangyu/article/details/45285897
'''
import imp 

'''
imp.get_suffixes()

返回3元组列表(suffix, mode, type), 
获得特殊模块的描述.s
uffix为文件后缀名;
mode为打开文件模式; 
type为文件类型, 1代表PY_SOURCE, 2代表PY_COMPILED, 3代表C_EXTENSION
'''

#[('.cp36-win_amd64.pyd', 'rb', 3), ('.pyd', 'rb', 3), ('.py', 'r', 1), ('.pyw', 'r', 1), ('.pyc', 'rb', 2)]
print(imp.get_suffixes())



'''
imp.find_module(name[, path])

如果path为空,则按照sys.path路径搜索模块名, 
返回三元组(file, pathname, description).
file为刚打开的模块文件, pathname为模块的路径, description为imp.get_suffixes()返回的元组.
如果模块为包,file返回None, pathname为包路径, description返回的type为PKG_DIRECTORY.
find_module不会处理层次结构的模块名(带’.’号的模块名module.name1.name2).
“path”必须是一个列表.
'''
#(<_io.TextIOWrapper name='.\\24.class.py' mode='r' encoding='utf-8'>, '.\\24.class.py', ('.py', 'r', 1))
file, pathname, desc = imp.find_module('24.class',['.'])
print(file)
print(pathname)
print(desc)


'''
imp.load_module(name, file, pathname, description)

加载一个被find_module找到的模块. 如果模块已经被加载, 等同于reload().
当模块是包或者不从文件加载时, file和pathname可以是None和”.
成功加载后返回一个模块对象,否则抛出 ImportError异常.
需要自己关闭file,最好用try…finally…
'''
my = imp.load_module('sep', file, pathname, desc)    #加载该模块，并执行
print(my)                        #<module 'sep' from '.\\24.class.py'>
s = my.Student('郑洋',99)
print(type(s))     #<class 'sep.Student'>
