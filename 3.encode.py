#字符串和编码问题
s1 = 'ABC'.encode('ascii')    #把'ABC'转换为ascii编码的bytes
s2 = '中文'.encode('utf-8')   #把'中文'转换为以unicode编码的bytes
print(s1,type(s1))    # b'ABC' <class 'bytes'>
print(s2,type(s2))    # b'\xe4\xb8\xad\xe6\x96\x87' <class 'bytes'>
#解码
s3= b'\xe4\xb8\xad\xe6\x96\x87'.decode('utf-8')
print(s3)
#计算字符个数
print(len(s3))

#计算字节个数
print(len('中文'.encode('utf-8')))
print('%.2f'%3.1415926)
#格式换字符串
print('%2d-%02d'%(3,1))
'Hello, {0}, 成绩提升了 {1:.1f}%'.format('小明', 17.125)


data = "5a 5a 02 03 5a"
send_list = []
while data != '':
    num = int(data[0:2], 16)
    data = data[2:].strip()
    send_list.append(num)
byte_array = bytes(send_list)
print(byte_array)    # b'ZZ\x02\x03Z'

# s4= b'\x5a\x5a\x01\x02\x03\x04\xaa\xba'.decode('utf-8') # 会失败，当做常规字节数组处理，每个元素都是一个字节数据
#print(s4)
hex_string = ' '.join(format(x, '02x') for x in byte_array)
print(hex_string)   # 5a 5a 02 03 5a