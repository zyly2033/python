# -*- coding: utf-8 -*-
"""
Created on Sat Dec  2 22:09:35 2017

@author: zy
"""
#try...except...finally...的错误处理机制
try:
    print('try....')
    r = 10 / int('a')
    print('result:',r)
except ValueError as e:
    print('ValueError:',e)
except ZeroDivisionError as e:
    print('ZeroDivisonError:',e)
finally:
    print('finally...')
print('END')


#果错误没有被捕获，它就会一直往上抛，最后被Python解释器捕获，打印一个错误信息，然后程序退出。
def foo(s):
    return 10 / int(s)

def bar(s):
    return foo(s) * 2

def main():
    bar('0')

#main()

#如果不捕获错误，自然可以让Python解释器来打印出错误堆栈，但程序也被结束了。既然我们能捕获错误，就可以把错误堆栈打印出来，然后分析错误原因，同时，让程序继续执行下去。
#Python内置的logging模块可以非常容易地记录错误信息：
import logging

def main():
    try:        
        bar('0')
    except Exception as e:
        logging.exception(e)
main()
print('END')


#因为错误是class，捕获一个错误就是捕获到该class的一个实例。因此，错误并不是凭空产生的，而是有意创建并抛出的。Python的内置函数会抛出很多类型的错误，我们自己编写的函数也可以抛出错误。
#如果要抛出错误，首先根据需要，可以定义一个错误的class，选择好继承关系，然后，用raise语句抛出一个错误的实例：
class FooError(ValueError):
    pass

def foo(s):
    n = int(s)
    if n==0:
        raise FooError('invalid value: %s' % s)
    return 10 / n

#foo('0')
#只有在必要的时候才定义我们自己的错误类型。如果可以选择Python已有的内置的错误类型（比如ValueError，TypeError），尽量使用Python内置的错误类型。最后，我们来看另一种错误处理的方式：

#运行下面的代码，根据异常信息进行分析，定位出错误源头，并修复：
from functools import reduce

def str2num(s):
    #return int(s)
    #修改为
    return float(s)

def calc(exp):
    ss = exp.split('+')
    print('ss:',ss)
    ns = map(str2num, ss)      #返回一个发生器
    #print('ns:',list(ns))
    return reduce(lambda acc, x: acc + x, ns)    #求和

def main():
    try:
        r = calc('100 + 200 + 345')
        print('100 + 200 + 345 =', r)
        r = calc('99 + 88 + 7.6')
        print('99 + 88 + 7.6 =', r)
    except ValueError as e:
        print('ValueError:',e)
    finally:
        print('finally...')
main()

