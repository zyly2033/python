#获取对象的信息  有多中方法
#第一种使用 type() 返回对应的class类型

#判断一个变量的类型
print(type(123))
print(type('abc'))
print(type(None))
print(type(abs))
class Student(object):
    def __init__(self):
        pass
s = Student()
print(type(s))

#判断两个变量的类型是不是相等
print(type(123)==type(456))
print(type(123)==type('456'))
print(type(123)== int)
print(type('123')== str)

#但如果要判断一个对象是否是函数怎么办？可以使用types模块中定义的常量：
import types
def fn():
    pass
print(type(fn) == types.FunctionType)               #True
print(type(abs) == types.BuiltinFunctionType)       #True
print(type(lambda x:x) == types.LambdaType)         #True
print(type(lambda x:x) == types.LambdaType)         #True
print(type(x for x in range(10)) == types.GeneratorType)    #True


#使用isintance() isinstance()判断的是一个对象是否是该类型本身，或者位于该类型的父继承链上。
class Animal(object):
    def run(self):
        print('Animal is running....')

#继承有什么好处？最大的好处是子类获得了父类的全部功能
class Dog(Animal):
    #重写父类方法
    def run(self):
        print('Dog is runing....')

class Cat(Animal):
    #重写父类方法
    def run(self):
        print('Cat is runing....')


a = Animal()
d = Dog()
c = Cat()
#判断一个变量是否是某个类型可以用isinstance()判断： 在继承关系中，如果一个实例的数据类型是某个子类，那它的数据类型也可以被看做是父类。但是，反过来就不行：
print(isinstance(a,Animal))
print(isinstance(d,Animal))
print(isinstance(c,Animal))


#基本类型判断
print(isinstance(123,int))
print(isinstance('123',str))
print(isinstance(b'a',bytes))

#判断一个变量是不是某些类型中的一种
print(isinstance([1,2,3],(list,tuple)))
print(isinstance([1,2,3],(tuple,list)))
print(isinstance('123',(str,tuple)))


#使用dir()
#如果要获得一个对象的所有属性和方法，可以使用dir()函数，它返回一个包含字符串的list，比如，获得一个str对象的所有属性和方法：
print(dir('ABC'))
print(dir(str))
print(dir(234))
#类似__xxx__的属性和方法在Python中都是有特殊用途的，比如__len__方法返回长度。在Python中，如果你调用len()函数试图获取一个对象的长度，实际上，在len()函数内部，它自动去调用该对象的__len__()方法，所以，下面的代码是等价的：
#我们自己写的类，如果也想用len(myObj)的话，就自己写一个__len__()方法：
class MyDog(object):    
    def __len__(self):        
        return 100
dog = MyDog()
print(len(dog))

#仅仅把属性和方法列出来是不够的，配合getattr()、setattr()以及hasattr()，我们可以直接操作一个对象的状态：
class MyObject(object):
    def __init__(self):
        self.x = 9
    def power(self):
       return self.x * self.x

obj = MyObject()
print(hasattr(obj, 'x'))  # 有属性'x'吗？
print(hasattr(obj, 'y'))  # 有属性'y'吗？
setattr(obj, 'y', 19)     # 设置一个属性'y'
print(hasattr(obj, 'y'))  # 有属性'y'吗？
print(getattr(obj, 'y'))  # 获取属性'y'  如果试图获取不存在的属性，会抛出AttributeError的错误：
print(obj.y)
#可以传入一个default参数，如果属性不存在，就返回默认值
print(getattr(obj, 'z',404))

#也可以获得对象的方法
print(hasattr(obj, 'power')) # 有属性'power'吗？)
print(getattr(obj, 'power')) # 获取属性'power'
fn = getattr(obj, 'power')   # 获取属性'power'并赋值到变量fn
print(fn())      # 调用fn()与调用obj.power()是一样的


#一个正确的用法的例子如下：
def readImage(fp):
    if hasattr(fp, 'read'):
        return readData(fp)
    return None
#假设我们希望从文件流fp中读取图像，我们首先要判断该fp对象是否存在read方法，如果存在，则该对象是一个流，如果不存在，则无法读取。hasattr()就派上了用场。
#请注意，在Python这类动态语言中，根据鸭子类型，有read()方法，不代表该fp对象就是一个文件流，它也可能是网络流，也可能是内存中的一个字节流，但只要read()方法返回的是有效的图像数据，就不影响读取图像的功能。
