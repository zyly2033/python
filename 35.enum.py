# -*- coding: utf-8 -*-
"""
Created on Sat Dec  2 21:06:51 2017

@author: zy
"""

#Enum可以把一组相关常量定义在一个class中，但class不可变
from enum import Enum
#@unique                #该装饰符可以保障我们检测没有重复的值
class Weekday(Enum):
    Sun = 0
    Mon = 1
    The = 2
    Wed = 3
    Thu = 4
    Fir = 5
    Sat = 6
    
#通过成员名称获取成员    
print(Weekday['Sun'])
print(Weekday.Sun)
#获取成员的值
print(Weekday.Sun.value)
#通过值获取成员
print(Weekday(1))


#把Student的gender属性改造为枚举类型，可以避免使用字符串：
class Gender(Enum):
    Male = 0
    Female = 1

class Student(object):
    @property    
    def gender(self):   #把getter方法转换为属性  获取属性
        return self.__gender
    
    @gender.setter     #把setter方法转换为属性   设置属性
    def gender(self,value):
        if not isinstance(value,Gender):
            raise ValueError('Gender property please input Gender type!')
        self.__gender = value
    
    def __init__(self, name, gender):
        if not isinstance(gender,Gender):
            raise ValueError('Gender property please input Gender type!')
        self.__name = name
        self.__gender = gender
    
bart = Student('Bart', Gender.Male)
#bart.gender = Gender.Female
if bart.gender == Gender.Male:
    print('测试通过!')
else:
    print('测试失败!')