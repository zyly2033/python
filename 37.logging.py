# -*- coding: utf-8 -*-
"""
Created on Sun Dec  3 09:59:26 2017

@author: zy
"""

#调试
#1.利用print()把一些可能有问题的变量打印出来
def foo(s):
    n = int(s)
    print('n == %s'%n)
    return 10/n

    
#2 利用assert()输出错误信息
def foo(s):
    n = int(s)
    assert n!=0,'n is zero'  
    return 10/n

 
#logging 
import logging
#简单的将日志打印到屏幕
def foo(s):
    n = int(s)    
    #这就是logging的好处，它允许你指定记录信息的级别，有debug，info，warning，error等几个级别，当我们指定level=INFO时，logging.debug就不起作用了。同理，指定level=WARNING后，debug和info就不起作用了。这样一来，你可以放心地输出不同级别的信息，也不用删除，最后统一控制输出哪个级别的信息。
    logging.debug('This is  a  debug message!',n)
    logging.info('This is  a  info message!')
    logging.warning('This is  a  warning message!')
    return 10/n    

#通过logging.baseConfig函数对日志的输出格式及方式做相关的配置
logging.basicConfig(level=logging.DEBUG,
                format='%(asctime)s %(filename)s[line:%(lineno)d] %(levelname)s %(message)s',
                datefmt='%a, %d %b %Y %H:%M:%S',
                filename='log.log',
                filemode='w')
    
logging.debug('This is debug message')
logging.info('This is info message')
logging.warning('This is warning message')

def main():
    foo('0')
    
#main()

#这个方法也是用pdb，但是不需要单步执行，我们只需要import pdb，然后，在可能出错的地方放一个pdb.set_trace()，就可以设置一个断点：
import pdb
s = '0'
n = int(s)
pdb.set_trace() # 运行到这里会自动暂停
print(10 / n)
#运行代码，程序会自动在pdb.set_trace()暂停并进入pdb调试环境，可以用命令p查看变量，或者用命令c继续运行：
