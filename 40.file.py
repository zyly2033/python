# -*- coding: utf-8 -*-
"""
Created on Sun Dec  3 14:42:43 2017

@author: zy
"""

#读写文件是最常见的IO操作。Python内置了读写文件的函数，用法和C是兼容的。
#读取文本文件 默认以utf-8编码读取
with open('./40.uft8.txt','r',encoding='utf-8',errors='ignore') as f:
    print(f.read())
with open('./40.ansi.txt','r',encoding='ansi',errors='ignore') as f:
    print(f.read())
 #默认是是使用ansi编码读取问文件
with open('./40.ansi.txt','r') as f:
    print(f.read())
    
#写入文件
with open('./40.ansi.txt','a+',encoding='ansi',errors='ignore') as f:     #追加方式打开
    #写入换行符
    f.write('\n')
    f.write('新写入一行数据')
    
#创建一个新的文件
with open('./40.test.txt','w+') as f:
    f.write('这是一个新的文件')
#如果需要读取文件二进制文件，比如音乐文件，视频文件，图片文件，需要以二进制方式操作文件，需要在操作方式中加入b
    
#请将本地一个文本文件读为一个str并打印出来：
#使用r忽略转义字符
fpath = r'C:\Windows\system.ini'
with open(fpath, 'r') as f:
    s = f.read()
    print(s)
