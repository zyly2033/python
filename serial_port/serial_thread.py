from PyQt5.QtCore import QThread,pyqtSignal
import serial


class SerialThread(QThread):
    """
    创建一个继承自QThread的SerialThread类，实现串口数据的读取/发送
    """

    # 用于发送串口数据接收信号
    data_received = pyqtSignal(str)
    # 串口打开/接收异常
    serial_error = pyqtSignal(str)

    def __init__(self, port, baud_rate, data_bits, parity_bits, stop_bits, data_format):
        """
        初始化
        :param port: 串口号
        :param baud_rate: 波特率
        :param data_bits: 数据位
        :param stop_bits: 停止位
        :param parity_bits: 奇偶校验位
        """
        super().__init__()
        self.port = port
        self.baud_rate = baud_rate
        self.data_bits = data_bits
        self.stop_bits = stop_bits
        self.parity_bits = parity_bits

        # 串口已经运行标志位
        self.running = False
        # 串口
        self.serial = None
        # 数据格式
        self.__date_format = data_format
        # 发送开启追加换行
        self.__auto_line = True

    @property
    def date_format(self):
        """
        #把一个getter方法变成属性
        :return:
        """
        return self.__date_format

    @date_format.setter
    def date_format(self, value):
        """
        # 负责把一个setter方法变成属性赋值
        :param value:
        :return:
        """
        if not isinstance(value, str):
            raise ValueError('date_format must be an str')
        if value not in ['hex', 'ascii']:
            raise ValueError('date_format must in [hex,ascii]')
        self.__date_format = value

    def run(self):
        """
        打开串口
        :return:
        """
        # 串口已经运行
        if self.running:
            return

        try:
            # 建立一个串口
            with serial.Serial(port=self.port,
                               baudrate=self.baud_rate,
                               parity=self.parity_bits,
                               bytesize=self.data_bits,
                               stopbits=self.stop_bits,
                               timeout=2) as self.serial:
                self.running = True
                while self.running:
                    data = self.__read_data__()
                    if data:
                        self.data_received.emit(data)
        except Exception as e:
            print("打开串口时失败：", e)
            self.serial_error.emit("打开串口时失败！")

    def stop(self):
        """
        线程停止
        :return:
        """
        self.running = False
        self.wait()

    def __read_data__(self):
        """
        按行接收数据
        :return:
        """
        if self.serial is None or not self.serial.isOpen():
            return

        try:
            # 读取串口数据 例如：b'DDR V1.12 52218f4949 cym 23/07/0'
            byte_array = self.serial.readline()
            if len(byte_array) == 0:
                return None
            # ascii显示
            if self.__date_format == 'ascii':
                # 串口接收到的字符串为b'ABC',要转化成unicode字符串才能输出到窗口中去
                data_str = byte_array.decode('utf-8')
            else:
                # 串口接收到的字符串为b'ZZ\x02\x03Z'，要转换成16进制字符串显示
                data_str = ' '.join(format(x, '02x') for x in byte_array)
                if self.__auto_line:
                    data_str += '\r\n'
            return data_str
        except Exception as e:
            print("接收数据异常：", e)
            self.serial_error.emit("接收数据异常！")

    def send_data(self, data: str):
        """
        发送数据
        :return:
        """
        if not self.running:
            self.serial_error.emit("请先打开串口！")
            return

        # hex发送 比如：5a 5a 02 03 5a -> b'ZZ\x02\x03Z'
        if self.__date_format == 'hex':
            data_str = data.strip()
            send_list = []
            while data_str != '':
                try:
                    num = int(data_str[0:2], 16)
                except ValueError:
                    self.serial_error.emit('请输入十六进制数据，以空格分开!')
                    return
                data_str = data_str[2:].strip()
                send_list.append(num)
            if self.__auto_line:
                send_list.append(0x0d)
                send_list.append(0x0a)
            byte_array = bytes(send_list)
        else:
            if self.__auto_line:
                data += '\r\n'
            # ascii发送 比如：'ABC' -> b'ABC'
            byte_array = data.encode('utf-8')

        try:
            self.serial.write(byte_array)
        except Exception as e:
            print("发送失败", e)
            self.serial_error.emit('发送失败!')


