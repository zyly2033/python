本节我们将会通过`PyQt5`实现串口数据采集和实时通信，涉及到的技术栈包括：`Python`、`PyQt5`。

### 一、环境搭建

#### 1.1 `Python 3.X`安装

直接从官网下载安装包：[`https://www.python.org/ftp/python/`](https://www.python.org/ftp/python/)；

这里我下载的包为`https://www.python.org/ftp/python/3.9.6/python-3.9.6-amd64.exe`，安装版本：`python 3.9.6` 。

双击开始安装的时候，一定要把下面的 `Add Path`勾上 （表示添加到环境变量，这样`cmd`也能使用了），其他一路`Next`安装完成。默认会安装一键式工具`pip`。

`pip`工具镜像源配置。配置方法如下：

- 在`cmd`窗口下执行`echo %HOMEPATH%`获取用户`HOME`目录，并在该目录下创建`pip`目录；
- 在`pip`目录下创建`pip.ini`文件。记住，后缀必须是`.ini`格式。并在该文件中写入如下内容;

内容如下：

```ini
[global]
index-url = https://pypi.tuna.tsinghua.edu.cn/simple
[install]
trusted-host = pypi.tuna.tsinghua.edu.cn
```

#### 1.2 安装`Pycharm`

官方网站：[`http://www.jetbrains.com/pycharm/`](http://www.jetbrains.com/pycharm/)，提供以下安装版本：

- `Professional`：专业版（收费，网上一大堆破解方法）
- `Community`：社区版（免费，我用的这个），下载版本为`pycharm-community-2023.3.4.exe`；

<img src="https://gitee.com/zyly2033/blog-pic/raw/master/202403021334017.png" style="zoom: 80%;" />

#### 1.3 `PyQt5`安装

使用`pip`工具安装`PyQt5`工具，执行：

```shell
pip install pyqt5
```

如果慢，用国内源：

```shell
pip install pyqt5 -i https://pypi.tuna.tsinghua.edu.cn/simple
```

使用`pip`工具安装`PyQt5-tools`工具，执行：

```shell
pip install pyqt5-tools
```

如果慢，用国内源：

```shell
pip install pyqt5-tools -i https://pypi.tuna.tsinghua.edu.cn/simple
```

工具安装完成后的路径在`E:\Program Files\Python\Lib\site-packages`。

`PyQt5`主要有三个部分：

- `QtCore`: 包含了核心的非`GUI`的功能。主要和时间、文件与文件夹、各种数据、模型、流、`URLs`、`mime`类文件、进程与线程一起使用；
- `QtGui`: 包含了窗口系统、事件处理、2D图像、基本绘画、字体和文字类；
- `QtWidgets`: 包含了一些创建桌面的`UI`元素和控件；

#### 1.4 环境配置

`PyCharm`是开发`Python`程序主流常用的IDE。为方便调用`Qt Designer`实现界面开发和编译相应完成，可以在`PyCharm`配置`Qt Designer`和`PyUIC`、`Pyrcc`。

其中`Qt Designer`是`Qt` 设计师，`PyUics`是把`UI`界面转换成`py`文件，`Pyrcc`是资源系统转换。

打开`PyCharm`， 新建一个项目，项目名称为`serial_port`。

##### 1.4.1 配置`Qt Designer`

菜单`File` ->`Settings` -> `Tools` -> `External Tools` -> `+`号，进行添加。 参数配置说明：

- `Name`：填入`Qt Designer`，实际可以任意取值；
- `Program`：`designer.exe`程序绝对路径。根据实际安装路径填写，这里我配置的是`E:\Program Files\Python\Lib\site-packages\qt5_applications\Qt\bin\designer.exe`；
-  `Working directory`： 填入`$FileDir$`，固定取值；

具体如下：

<img src="https://gitee.com/zyly2033/blog-pic/raw/master/202403021354539.png" style="zoom: 80%;" />

##### 1.4.2 配置`PyUIC`

该工具是用于将`Qt Designer`工具开发完成的`.ui`文件转化为`.py`文件。配置打开路径同`Qt Designer`，参数配置说明：

- `Name`：填入`PyUIC`，实际可以任意取值。
-  `Program`：`python.exe`程序绝对路径，根据实际安装路径填写，这里我配置的是`E:\Program Files\Python\python.exe`；
- `Arguments`: `-m PyQt5.uic.pyuic $FileName$ -o $FileNameWithoutExtension$.py`；
-  `Working directory`： 填入`$FileDir$`，固定取值；

具体如下：

<img src="https://gitee.com/zyly2033/blog-pic/raw/master/202403021359648.png" style="zoom:80%;" />

##### 1.4.3 配置`Pyqcc`

配置打开路径同`Qt Designer`。参数配置说明：

- `Name`：填入`Pyqcc`，实际可以任意取值。
-  `Program`：这里我配置的是`E:\Program Files\Python\Scripts\pyrcc5.exe`；
- `Arguments`: `$FileName$ -o $FileNameWithoutExtension$_rc.py`；
-  `Working directory`： 填入`$FileDir$`，固定取值；

具体如下：

<img src="https://gitee.com/zyly2033/blog-pic/raw/master/202403021402388.png" style="zoom:80%;" />

#### 1.5 测试

测试`Qt Designer`和`PyUIC`、`Pyqcc`配置是否成功。打开路径：菜单栏`Tools` ->`External Tools` ->`Qt Designer`/`PyUIC`/`Pyqcc`；

<img src="https://gitee.com/zyly2033/blog-pic/raw/master/202403021403381.png"  />

##### 1.5.1 `ui_serial_port.ui`

点击`Qt Designer`，打开`Designer`程序主主界面，会弹出一个窗口，这里一般是选择`Main Window`或者`Widget`，其中`Main Window`继承自`Widget`，添加了一些内容，本质二者差不多。这里选择的是`Main Window`；

将左侧`Widget Box`中`Push button`空间拖到主界面，`Ctrl + S`保存名称`SerialPort.ui`，默认后缀就是`.ui`。

<img src="https://gitee.com/zyly2033/blog-pic/raw/master/202403021405876.png" style="zoom: 67%;" />

##### 1.5.2 `ui_serial_port.py`

同理点击`PyUIC`，自动完成`ui_serial_port.ui`文件的转换，生成文件名为`ui_serial_port.py`。

##### 1.5.3 `serial_port.py`

除`ui`界面代码，还需要有一个逻辑代码，而逻辑代码个人感觉使用类的形式来组织更加方便，也更优雅。

还记得创建`ui`时选择的类吗？是`Widget`还是`Main Window`，逻辑代码类最好是继承这个这个类，即`QWidget`或`QMainWindow`。一般的代码结构如下所示：

```python
from PyQt5.QtWidgets import QMainWindow

# 导入设计的ui界面转换成的py文件
from ui_serial_port import Ui_MainWindow


class SerialPort(QMainWindow):
    """
     串口行为
    """

    def __init__(self):
        # QMainWindow构造函数初始化
        super().__init__()
        self.ui = Ui_MainWindow()
        # 这个函数本身需要传递一个MainWindow类，而该类本身就继承了这个，所以可以直接传入self
        self.ui.setupUi(self)
```

##### 1.5.4 `main.py`

在当前项目下，新建`main.py `文件；

```python
import sys
from serial_port import SerialPort
from PyQt5.QtWidgets import QApplication, QMainWindow

if __name__ == '__main__':
    # 先建立一个app
    app = QApplication(sys.argv)
    # 初始化一个对象，调用init函数，已加载设计的ui文件
    ui = SerialPort()
    # 显示这个ui
    ui.show()
    # 运行界面，响应按钮等操作
    sys.exit(app.exec_())
```

运行程序：

<img src="https://gitee.com/zyly2033/blog-pic/raw/master/202403021458848.png" style="zoom:80%;" />

### 二、程序设计

#### 2.1 需求

客户这里有一款惯性设备，在惯性装置的`AXS31`接口，里面有两路数据，一路称为导航解算，一路称为原始数。我们通过串口读取该设备的数据并在界面显示处理，同时还需要将读取到的数据保存到文本中。

##### 2.1.1 导航解算报文

波特率：230400，数据位8，停止位1，无校验；

| 字节  | 意义                     | 类型 | 所占字节 | 备注                                                         |
| ----- | ------------------------ | ---- | -------- | ------------------------------------------------------------ |
| 1-2   | 报文头                   |      | 2字节    | 5a  5a                                                       |
| 3     | 工作状态                 |      | 1字节    | 0xFF等待对准<br>0x00码头对准<br/>0x01海上对准<br/>0x02牵引对准<br/>0x03 是无阻尼<br/> 0x04是惯导阻尼<br/>0x05 点校<br/>0x06 综合校正<br/>0x07 位置组合 |
| 4     | 参数状态                 |      | 1字节    | B8 =0手动 B8=1自动                                           |
| 5-8   | 运行时间                 |      | 4字节    | 单位0.05s                                                    |
| 9-11  | 纬度                     |      | 3字节    | 量纲93206.75556                                              |
| 12-14 | 经度                     |      | 3字节    | 量纲46603.37778                                              |
| 15-16 | 升沉                     |      | 2字节    | 最小量纲100   m                                              |
| 17-18 | 东速                     |      | 2字节    | 最小量纲100   kn                                             |
| 19-20 | 北速                     |      | 2字节    | 最小量纲100   kn                                             |
| 21-22 | 垂速                     |      | 2字节    | 最小量纲100   m/s                                            |
| 23-25 | 姿态角1                  |      | 3字节    | 最小量纲0.25*93206.75556                                     |
| 26-28 | 姿态角2                  |      | 3字节    | 最小量纲93206.75556                                          |
| 29-31 | 姿态角3                  |      | 3字节    | 最小量纲93206.75556                                          |
| 32-34 | 姿态角速率1(纵摇角速率)  |      | 3字节    | 93206.75556      度每秒                                      |
| 35-37 | 姿态角速率2 (横摇角速率) |      | 3字节    | 93206.75556      度每秒                                      |
| 38-40 | 姿态角速率3(航向角速率)  |      | 3字节    | 93206.75556      度每秒                                      |
| 41    | 故障码                   |      | 1字节    | B0=1  IMU接收错<br/>B1=1  测角采样错<br/>B2=1  接收缓存错<br/>B3=1  测角控制板错<br/>B4=1  驱动错<br/>B5=1 测角错<br/>B6=1 激磁错<br/>B7=1 转台保护错 |
| 42-43 | IMUtime                  |      | 2字节    |                                                              |
| 44-47 | 备用                     |      | 4字节    |                                                              |
| 48    | 应答标志                 |      | 1字节    | 可忽略                                                       |
| 49    | 校验和                   |      | 1字节    | 3-48字节累加和                                               |

 ##### 2.1.2 原始信息报文

波特率：230400，数据位8，停止位1，无校验；

| 字节  | 意义                       | 类型  | 所占字节 | 备注                      |
| ----- | -------------------------- | ----- | -------- | ------------------------- |
| 1-2   | 报文头                     | Int   | 2字节    | 5a  5a                    |
| 3-6   | IMUtime                    | Int   | 4字节    | 整型时戳                  |
| 7-10  | GYROX                      | float | 4字节    | 直接浮点数                |
| 11-14 | GYROY                      | float | 4字节    | 直接浮点数                |
| 15-18 | GYROZ                      | float | 4字节    | 直接浮点数                |
| 19-22 | ACCEX                      | float | 4字节    | 直接浮点数                |
| 23-26 | ACCEY                      | float | 4字节    | 直接浮点数                |
| 27-30 | ACCEZ                      | float | 4字节    | 直接浮点数                |
| 31-34 | 备用                       | Int   | 4字节    |                           |
| 35-37 | 转台角1                    | Int   | 3字节    | 量纲2.330168888888889*e4° |
| 38-40 | 转台角2                    | Int   | 3字节    | 量纲2.330168888888889*e4° |
| 41-43 | GPS经度                    | Int   | 3字节    | 量纲93206.75556           |
| 44-46 | GPS纬度                    | Int   | 3字节    | 量纲46603.37778           |
| 47-48 | Para[4](电磁/牵引时为航向) | Int   | 2字节    |                           |
| 49-50 | Para[2]/para[5]            | Int   | 2字节    |                           |
| 51-52 | Para[3]/para[6]            | Int   | 2字节    |                           |
| 53-54 | Para[7]                    | Int   | 2字节    |                           |
| 55-56 | Para[8]                    | Int   | 2字节    |                           |
| 57    | comdatavalid               | Int   | 1字节    |                           |
| 58    | 校验和                     | Int   | 1字节    | 3-57和校验                |

#### 2.2 界面设计

首先，我们设计一个简单的用户界面，包括：

- 串口配置区域：串口选择、波特率、数据位、停止位的设置，以及一个按钮用于开始打开和关闭串口；
- 接收设置区域：用于设置接收和发送的数据格式，支持`16`进制以及`ASCII`两种格式；这里为了简单起见，程序中发送/接收采用一样的数据格式；
  - 16进制：例如：`5a 5a 02 03 5a`；
  - ASCII格式：例如：`DDR V1.12 52218f4949 cym 23/07/0`；

- 数据发送区域以及数据接收区域；
- 如果接收到的数据时导航解算或者原始信息报文，则将解析后的数据显示在界面；


界面效果如下；

<img src="https://gitee.com/zyly2033/blog-pic/raw/master/202403031852331.png" style="zoom:80%;" />

我们在实现该需求的时候，将界面相关的代码均放置在`serial_port.py`文件中，该文件主要包含了如下功能；

- 界面初始化工作；
- 打开串口；
- 接收数据；
- 发送数据。

##### 2.2.1 初始化工作

初始化工作主要包括：

- 初始化串口设置区域相关控件默认值，比如当前可用串口、以及常用波特率等；
- 初始化串口接收设置区域相关控件，比如处理`Hex`、`ASCII`点击事件；
- 初始化串口数据接收区域相关控件，比如设置为只读，滚动条始终移动到最底层；
- 初始化串口数据发送区域相关控件，比如点击发送触发串口发送操作；
- 初始化报文解析区域相关控件，比如设置导航结算、原始信息区域控件为只读；

##### 2.2.2 打开串口

当在界面点击打开串口时，会创建串口线程，用于处理串口数据的接收和发送，核心代码如下：

```python
   def open_serial_connection(self):
        """
        打开串口
        :return:
        """
        if not self.serial_reader_thread or not self.serial_reader_thread.isRunning():
            # 参数校验
            if not self.__validate_setting__():
                return

            # 建立一个串口
            self.serial_reader_thread = SerialReaderThread(self.ui.cbx_com.currentText(),
                                                           self.ui.cbx_baud_rate.currentData(),
                                                           self.ui.cbx_data_bit.currentData(),
                                                           self.ui.cbx_parity_bit.currentData(),
                                                           self.ui.cbx_stop_bit.currentData(),
                                                           'hex' if self.ui.rbn_data_format_hex.isChecked() else 'ascii')

            self.serial_reader_thread.data_received.connect(self.handle_data_received)
            self.serial_reader_thread.serial_error.connect(self.handler_serial_error)
            self.serial_reader_thread.start()
            self.ui.btx_start.setText("关闭串口")
        else:
            self.serial_reader_thread.stop()
            # 打开串口操作
            self.ui.btx_start.setText("打开串口")
```

##### 2.2.3 接收数据

当串口接收到数据时，将会由`handle_data_received`函数进行处理；

```python
    def handle_data_received(self, data):
        """
        接收数据
        :param data:接收到的数据
        :return:
        """
        # 获取时间
        current_time = QDateTime.currentDateTime().toString("yyyy-MM-dd:mm:ss")

        # 超过5000字符清空
        if len(self.ui.txt_recv_data_viewer.toPlainText()) > 5000:
            self.ui.txt_recv_data_viewer.clear()
        # 更新显示区域中的数据
        if self.ui.cbx_show_time.isChecked():
            self.ui.txt_recv_data_viewer.insertPlainText(f"[{current_time}] {data}")
        else:
            self.ui.txt_recv_data_viewer.insertPlainText(f"[{data}")

        # 必须是hex格式
        if self.ui.rbn_data_format_ascii.isChecked():
            return

        # 假数据，测试
        # data = '5a 5a 03 04 05 06 40 49 0f db 0b 0c 0d 0e 0f 10 11 12 13 14 15 16 17 18 19 1a 1b 1c 1d 1e 1f 20 21 22 23 24 25 26 27 28 29 2a 2b 2c 2d 2e 2f 30 31 32 33 34 35 36 37 38 39 3a'
        # 解析data
        package_parse = PackageParse(data)
        data_format = package_parse.get_data_format()
        if data_format == PackageParse.Type.NAl_SOL:
            # 解析数据
            data = package_parse.get_nav_sol()
            # 写入当前时间，并保存到文件
            data["current_time"] = current_time
            save_csv('nav_data.csv', data)
            # 界面显示
            self.ui.let_nav_header.setText(data["header"])
            self.ui.let_nav_work_state.setText(data["work_state"])
            ......
        if data_format == PackageParse.Type.RAW:
            # 解析数据
            data = package_parse.get_raw()
            # 写入当前时间，并保存到文件
            data["current_time"] = current_time
            save_csv('raw_data.csv', data)
            # 界面显示
            self.ui.let_raw_header.setText(data["header"])
            self.ui.let_raw__imu_time.setText(data["imu_time"])
            ......
```

该函数会将接收到的数据显示在界面的接收区域`QTextEdit`控件中。

同时会对串口接收到的数据进行解析，如果是导航解算和原始信息报文，我们将会将其分别保存到不同文件，同时将数据显示到界面上，

##### 2.2.4 发送数据

当点击发送数据时，会调用`send_serial_data`方法：

```python
 def send_serial_data(self, data: str):
        """
        发送数据
        :return:
        """
        if not self.serial_reader_thread or not self.serial_reader_thread.isRunning():
            QMessageBox.warning(self, "Warning", "请先打开串口！")
            return

        data = self.ui.let_send_data_viewer.text()
        if data != "":
            self.serial_reader_thread.send_data(data)
            self.ui.let_send_data_viewer.clear()
```

这里实际上就是调用串口线程来进行数据发送。

#### 2.3 串口线程

`PyQt`已经为我们提供了串口控件，控件名称为`QtSerialPort`，使用方法比较简单，主要是两个模块：`QSerialPort`, `QSerialPortInfo`，但是这个控件提供的能力有限。

这里我们使用另一个串口包来实现：

```shell
pip install pyserial
```

为了实现串口数据的读取，我们创建了一个继承自`QThread`的`SerialThread`类。在这个类中，我们使用`PySerial`打开串口，并解析串口数据。`serial_thread.py`文件代码如下：

```python
from PyQt5.QtCore import QThread,pyqtSignal
import serial


class SerialThread(QThread):
    """
    创建一个继承自QThread的SerialThread类，实现串口数据的读取/发送
    """

    # 用于发送串口数据接收信号
    data_received = pyqtSignal(str)
    # 串口打开/接收异常
    serial_error = pyqtSignal(str)

    def __init__(self, port, baud_rate, data_bits, parity_bits, stop_bits, data_format):
        """
        初始化
        :param port: 串口号
        :param baud_rate: 波特率
        :param data_bits: 数据位
        :param stop_bits: 停止位
        :param parity_bits: 奇偶校验位
        """
        super().__init__()
        self.port = port
        self.baud_rate = baud_rate
        self.data_bits = data_bits
        self.stop_bits = stop_bits
        self.parity_bits = parity_bits

        # 串口已经运行标志位
        self.running = False
        # 串口
        self.serial = None
        # 数据格式
        self.__date_format = data_format
        # 发送开启追加换行
        self.__auto_line = True

    @property
    def date_format(self):
        """
        #把一个getter方法变成属性
        :return:
        """
        return self.__date_format

    @date_format.setter
    def date_format(self, value):
        """
        # 负责把一个setter方法变成属性赋值
        :param value:
        :return:
        """
        if not isinstance(value, str):
            raise ValueError('date_format must be an str')
        if value not in ['hex', 'ascii']:
            raise ValueError('date_format must in [hex,ascii]')
        self.__date_format = value

    def run(self):
        """
        打开串口
        :return:
        """
        # 串口已经运行
        if self.running:
            return

        try:
            # 建立一个串口
            with serial.Serial(port=self.port,
                               baudrate=self.baud_rate,
                               parity=self.parity_bits,
                               bytesize=self.data_bits,
                               stopbits=self.stop_bits,
                               timeout=2) as self.serial:
                self.running = True
                while self.running:
                    data = self.__read_data__()
                    if data:
                        self.data_received.emit(data)
        except Exception as e:
            print("打开串口时失败：", e)
            self.serial_error.emit("打开串口时失败！")

    def stop(self):
        """
        线程停止
        :return:
        """
        self.running = False
        self.wait()

    def __read_data__(self):
        """
        按行接收数据
        :return:
        """
        if self.serial is None or not self.serial.isOpen():
            return

        try:
            # 读取串口数据 例如：b'DDR V1.12 52218f4949 cym 23/07/0'
            byte_array = self.serial.readline()
            if len(byte_array) == 0:
                return None
            # ascii显示
            if self.__date_format == 'ascii':
                # 串口接收到的字符串为b'ABC',要转化成unicode字符串才能输出到窗口中去
                data_str = byte_array.decode('utf-8')
            else:
                # 串口接收到的字符串为b'ZZ\x02\x03Z'，要转换成16进制字符串显示
                data_str = ' '.join(format(x, '02x') for x in byte_array)
                if self.__auto_line:
                    data_str += '\r\n'
            return data_str
        except Exception as e:
            print("接收数据异常：", e)
            self.serial_error.emit("接收数据异常！")

    def send_data(self, data: str):
        """
        发送数据
        :return:
        """
        if not self.running:
            self.serial_error.emit("请先打开串口！")
            return

        # hex发送 比如：5a 5a 02 03 5a -> b'ZZ\x02\x03Z'
        if self.__date_format == 'hex':
            data_str = data.strip()
            send_list = []
            while data_str != '':
                try:
                    num = int(data_str[0:2], 16)
                except ValueError:
                    self.serial_error.emit('请输入十六进制数据，以空格分开!')
                    return
                data_str = data_str[2:].strip()
                send_list.append(num)
            if self.__auto_line:
                send_list.append(0x0d)
                send_list.append(0x0a)
            byte_array = bytes(send_list)
        else:
            if self.__auto_line:
                data += '\r\n'
            # ascii发送 比如：'ABC' -> b'ABC'
            byte_array = data.encode('utf-8')

        try:
            self.serial.write(byte_array)
        except Exception as e:
            print("发送失败", e)
            self.serial_error.emit('发送失败!')

```

**参考文章**

**[1] [`PyQt5`开发环境搭建和配置](https://www.cnblogs.com/linyfeng/p/11216494.html)**

**[2] [`PyQt5` 从零开始环境搭建](https://blog.csdn.net/u011942101/article/details/123319040)**

**[3] [`PyQt5`入门](https://blog.csdn.net/ZHOU_YONG915/article/details/129400618?spm=1001.2014.3001.5501)**

**[4] [`python`的`pyserial`模块接收串口助手发送HEX过来接收到非16进制乱码问题](https://blog.csdn.net/zx520113/article/details/85471707)**

**[5] [Python串口通信详解：从基础到高级](https://zhuanlan.zhihu.com/p/676880159?utm_id=0)**

**[6] [在`Windows`中使用`PyQt`和`PySerial`实现串口数据读取](https://blog.csdn.net/qq_37037348/article/details/135277597)**