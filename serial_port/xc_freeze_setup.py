"""
xc_freeze制作exe文件： 错误指南：https://www.jianshu.com/p/0babfd73fa95
 1、pip install cx_Freeze -i  https://pypi.tuna.tsinghua.edu.cn/simple
 2. 进入程序所在路径，写setup.py文件内容如下  https://www.jianshu.com/p/0babfd73fa95
 3、cmd程序所在路径，python xc_freeze_setup.py bdist_msi
 4、会在setup.py所在的目录中生成两个文件夹，dist文件夹中包含一个msi文件，点击安装后会生成build文件中的内容，build文件中包含exe文件和所依赖的各种文件。
"""
import os

os.environ['TCL_LIBRARY'] = r"E:\Program Files\Python\tcl\tcl8.6"
os.environ['TK_LIBRARY'] = r"E:\Program Files\Python\tcl\tk8.6"

from cx_Freeze import setup, Executable

options = {'build_exe':
    {
        'excludes': ['gtk', 'Tkinter'],
        # 依赖的包
        'packages': ['PyQt5'],
        'includes': [],
        # 额外添加的文件
        'include_files': ['conf/serial.svg']
    }
}

# 不要出现中文
executables = [Executable(
    # 工程的 入口
    script=r'G:\python\serial_port\main.py',
    base='Win32GUI',
    # exe文件图标
    icon='conf/serial.ico',
    # exe文件名
    target_name='serial.exe')]
setup(name='serial', version='1.0', description='serial', options=options, executables=executables)
