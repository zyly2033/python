import csv
import os

from PyQt5.QtCore import QDateTime

from package_parse import PackageParse


# 导入设计的ui界面转换成的py文件
def save_csv(file_name, dict: {}):
    """
    保存到csv文件
    :param file_name:文件名
    :param dict: 数据，字典格式
    :return:
    """
    file_exists = os.path.exists(file_name)
    # Save data to CSV file
    with open(file_name, 'a', newline='') as csvfile:
        writer = csv.writer(csvfile)
        # 如果文件不存在，则写入列名
        if not file_exists:
            writer.writerow(dict.keys())
        # 写入数据
        writer.writerow(dict.values())


if __name__ == '__main__':
    # 获取时间
    current_time = QDateTime.currentDateTime().toString("yyyy-MM-dd hh:mm:ss")
    # 假数据，测试
    with open('raw_data.txt', 'r') as file:
        # 读取文件所有内容
        data = file.read()
    # 如果报文最后两位是换行符，则移除
    data = data.replace('0d 0a', '')
    data = bytes.fromhex(data.replace(' ', ''))
    # 解析data
    while data is not None:
        package_parse = PackageParse(data)
        data_format = package_parse.get_data_format()
        if data_format == PackageParse.Type.NAl_SOL:
            # 解析数据
            dict_msg = package_parse.get_nav_sol()
            # 写入当前时间，并保存到文件
            dict_msg["current_time"] = current_time
            save_csv('nav_data.csv', dict_msg)

        if data_format == PackageParse.Type.RAW:
            # 解析数据
            data = package_parse.get_raw()
            # 写入当前时间，并保存到文件
            data["current_time"] = current_time
            save_csv('raw_data.csv', data)
        # 一次收到若干个数据包
        if data_format != PackageParse.Type.UNkNOWN and len(data) > data_format.value:
            data = data[data_format.value:]
        else:
            data = None
