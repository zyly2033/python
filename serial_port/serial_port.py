import csv
import os

import serial
import serial.tools.list_ports
from PyQt5.QtCore import QDateTime
from PyQt5.QtGui import QIcon, QTextCursor
from PyQt5.QtWidgets import QComboBox, QMessageBox, QMainWindow

from package_parse import PackageParse
from serial_thread import SerialThread
# 导入设计的ui界面转换成的py文件
from ui.ui_serial_port import Ui_MainWindow


def save_csv(file_name, dict: {}):
    """
    保存到csv文件
    :param file_name:文件名
    :param dict: 数据，字典格式
    :return:
    """
    file_exists = os.path.exists(file_name)
    # Save data to CSV file
    with open(file_name, 'a', newline='') as csvfile:
        writer = csv.writer(csvfile)
        # 如果文件不存在，则写入列名
        if not file_exists:
            writer.writerow(dict.keys())
        # 写入数据
        writer.writerow(dict.values())


class SerialPort(QMainWindow):
    """
     串口行为
    """

    def __init__(self):
        # QMainWindow构造函数初始化
        super().__init__()

        self.ui = Ui_MainWindow()
        # 这个函数本身需要传递一个MainWindow类，而该类本身就继承了这个，所以可以直接传入self
        self.ui.setupUi(self)

        # 设置窗口标题、图标
        self.setWindowTitle("串口数据解析")
        self.setWindowIcon(QIcon('serial.svg'))
        # 禁用窗体拖拽功能
        self.setFixedSize(1004, 978)

        # 初始化串口设置默认参数
        self.__init_serial_setting__()
        # 初始化串口接收设置区域相关控件
        self.__init_recv_setting__()
        # 初始化串口数据接收区域相关控件
        self.__init_recv_data_viewer__()
        # 初始化串口数据发送区域相关控件
        self.__init_send_data_viewer__()
        # 初始化报文解析区域相关控件
        self.__init_package_setting__()

        # 串口接收线程
        self.serial_thread = None

    def __del__(self):
        """
        自动关闭串口
        :return:
        """
        self.serial_thread.stop()

    def __init_serial_setting__(self):
        """
        初始化串口设置相关控件默认参数
        设置下拉列表自动补全  https://blog.csdn.net/xuleisdjn/article/details/51434118
        :return:
        """
        # 加载可用串口
        self.ui.cbx_com.setEditable(False)
        self.ui.cbx_com.setMaxVisibleItems(10)  # 设置最大显示下列项 超过要使用滚动条拖拉
        self.ui.cbx_com.setInsertPolicy(QComboBox.InsertAfterCurrent)  # 设置插入方式\
        port_list = list(serial.tools.list_ports.comports())  # 获取当前的所有串口，得到一个列表
        for port in port_list:
            self.ui.cbx_com.addItem(port.device)

        # 初始化波特率列表
        self.ui.cbx_baud_rate.setEditable(False)
        self.ui.cbx_baud_rate.setMaxVisibleItems(10)  # 设置最大显示下列项 超过要使用滚动条拖拉
        self.ui.cbx_baud_rate.setInsertPolicy(QComboBox.InsertAfterCurrent)  # 设置插入方式
        for baud_rate in [1200, 2400, 4800, 9600, 19200, 384000, 57600, 115200, 460800, 921600, 230400, 1500000]:
            self.ui.cbx_baud_rate.addItem(str(baud_rate), baud_rate)
        # 设置默认值
        self.ui.cbx_baud_rate.setCurrentIndex(7)

        # 初始化校验位列表
        self.ui.cbx_parity_bit.setEditable(False)
        self.ui.cbx_parity_bit.setMaxVisibleItems(10)  # 设置最大显示下列项 超过要使用滚动条拖拉
        self.ui.cbx_parity_bit.setInsertPolicy(QComboBox.InsertAfterCurrent)  # 设置插入方式
        for (key, value) in {'None': serial.PARITY_NONE, 'Odd': serial.PARITY_ODD,
                             'Even': serial.PARITY_EVEN, 'Mark': serial.PARITY_MARK,
                             'Space': serial.PARITY_SPACE}.items():
            self.ui.cbx_parity_bit.addItem(key, value)
        # 设置默认值
        self.ui.cbx_parity_bit.setCurrentIndex(0)

        # 初始化数据位列表
        self.ui.cbx_data_bit.setEditable(False)
        self.ui.cbx_data_bit.setMaxVisibleItems(10)  # 设置最大显示下列项 超过要使用滚动条拖拉
        self.ui.cbx_data_bit.setInsertPolicy(QComboBox.InsertAfterCurrent)  # 设置插入方式
        for data_bit in [serial.FIVEBITS, serial.SIXBITS, serial.SEVENBITS, serial.EIGHTBITS]:
            self.ui.cbx_data_bit.addItem(str(data_bit), data_bit)
        # 设置默认值
        self.ui.cbx_data_bit.setCurrentIndex(3)

        # 初始化停止位列表
        self.ui.cbx_stop_bit.setEditable(False)
        self.ui.cbx_stop_bit.setMaxVisibleItems(10)  # 设置最大显示下列项 超过要使用滚动条拖拉
        self.ui.cbx_stop_bit.setInsertPolicy(QComboBox.InsertAfterCurrent)  # 设置插入方式
        for data_bit in [serial.STOPBITS_ONE, serial.STOPBITS_TWO]:
            self.ui.cbx_stop_bit.addItem(str(data_bit), data_bit)
        # 设置默认值
        self.ui.cbx_stop_bit.setCurrentIndex(0)

        # 设置点击打开串口按钮对应的槽函数
        self.ui.btx_start.clicked.connect(self.open_serial_connection)

    def __init_recv_setting__(self):
        """
        接收设置初始化
        :return:
        """
        self.ui.rbn_data_format_hex.clicked.connect(self.rbn_data_format_hex_clicked)
        self.ui.rbn_data_format_ascii.clicked.connect(self.rbn_data_format_ascii_clicked)

    def __init_package_setting__(self):
        self.ui.let_nav_header.setReadOnly(True)
        self.ui.let_nav_work_state.setReadOnly(True)
        self.ui.let_nav_param_state.setReadOnly(True)
        self.ui.let_nav_run_time.setReadOnly(True)
        self.ui.let_nav_latitude.setReadOnly(True)
        self.ui.let_nav_longitude.setReadOnly(True)
        self.ui.let_nav_heave.setReadOnly(True)
        self.ui.let_nav_east_speed.setReadOnly(True)
        self.ui.let_nav_north_speed.setReadOnly(True)
        self.ui.let_nav_vertical_speed.setReadOnly(True)
        self.ui.let_nav_attitude_angle1.setReadOnly(True)
        self.ui.let_nav_attitude_angle2.setReadOnly(True)
        self.ui.let_nav_attitude_angle3.setReadOnly(True)
        self.ui.let_nav_attitude_velocity1.setReadOnly(True)
        self.ui.let_nav_attitude_velocity2.setReadOnly(True)
        self.ui.let_nav_attitude_velocity3.setReadOnly(True)
        self.ui.let_nav_fault_code.setReadOnly(True)
        self.ui.let_nav_imu_time.setReadOnly(True)
        self.ui.let_nav_reserved.setReadOnly(True)
        self.ui.let_nav_response_flag.setReadOnly(True)
        self.ui.let_nav_check_sum.setReadOnly(True)
        self.ui.let_raw_header.setReadOnly(True)
        self.ui.let_raw__imu_time.setReadOnly(True)
        self.ui.let_raw_gyrox.setReadOnly(True)
        self.ui.let_raw_gyroy.setReadOnly(True)
        self.ui.let_raw_gyroz.setReadOnly(True)
        self.ui.let_raw_accex.setReadOnly(True)
        self.ui.let_raw_accey.setReadOnly(True)
        self.ui.let_raw_accez.setReadOnly(True)
        self.ui.let_raw_reserved.setReadOnly(True)
        self.ui.let_raw_turntable_angle1.setReadOnly(True)
        self.ui.let_raw_turntable_angle2.setReadOnly(True)
        self.ui.let_raw_longitude.setReadOnly(True)
        self.ui.let_raw_latitude.setReadOnly(True)
        self.ui.let_raw_para4.setReadOnly(True)
        self.ui.let_raw_para2_5.setReadOnly(True)
        self.ui.let_raw_para3_6.setReadOnly(True)
        self.ui.let_raw_para7.setReadOnly(True)
        self.ui.let_raw_para8.setReadOnly(True)
        self.ui.let_raw_comdata_valid.setReadOnly(True)
        self.ui.let_raw_check_sum.setReadOnly(True)

    def rbn_data_format_hex_clicked(self):
        """
        接收数据格式发生变化
        :return:
        """
        if self.ui.rbn_data_format_hex.isChecked():
            self.ui.rbn_data_format_ascii.setChecked(False)
            if self.serial_thread:
                self.serial_thread.date_format = 'hex'

    def rbn_data_format_ascii_clicked(self):
        """
        接收数据格式发生变化
        :return:
        """
        if self.ui.rbn_data_format_ascii.isChecked():
            self.ui.rbn_data_format_hex.setChecked(False)
            if self.serial_thread:
                self.serial_thread.date_format = 'ascii'

    def __init_recv_data_viewer__(self):
        """
        初始化串口数据接收区域
        :return:
        """
        self.ui.txt_recv_data_viewer.setReadOnly(True)
        self.ui.txt_recv_data_viewer.textChanged.connect(
            lambda: self.ui.txt_recv_data_viewer.moveCursor(QTextCursor.End))

    def __init_send_data_viewer__(self):
        """
        初始化串口数据发送区域
        :return:
        """
        self.ui.btn_send.clicked.connect(self.send_serial_data)

    def __validate_setting__(self):
        """
        校验串口设置参数
        :return:
        """
        # 参数校验
        if self.ui.cbx_com.currentIndex() == -1:
            QMessageBox.warning(self, "Warning", "请选择串口！")
            return False
        if self.ui.cbx_baud_rate.currentIndex() == -1:
            QMessageBox.warning(self, "Warning", "请选择波特率！")
            return False
        if self.ui.cbx_parity_bit.currentIndex() == -1:
            QMessageBox.warning(self, "Warning", "请选择校验位！")
            return False
        if self.ui.cbx_data_bit.currentIndex() == -1:
            QMessageBox.warning(self, "Warning", "请选择数据位！")
            return False
        if self.ui.cbx_data_bit.currentIndex() == -1:
            QMessageBox.warning(self, "Warning", "请选择停止位！")
            return False

        return True

    def open_serial_connection(self):
        """
        打开串口
        :return:
        """
        if not self.serial_thread or not self.serial_thread.isRunning():
            # 参__validate_setting__数校验
            if not self.__validate_setting__():
                return

            # 建立一个串口
            self.serial_thread = SerialThread(self.ui.cbx_com.currentText(),
                                                     self.ui.cbx_baud_rate.currentData(),
                                                     self.ui.cbx_data_bit.currentData(),
                                                     self.ui.cbx_parity_bit.currentData(),
                                                     self.ui.cbx_stop_bit.currentData(),
                                                           'hex' if self.ui.rbn_data_format_hex.isChecked() else 'ascii')

            self.serial_thread.data_received.connect(self.handle_data_received)
            self.serial_thread.serial_error.connect(self.handler_serial_error)
            self.serial_thread.start()
            self.ui.btx_start.setText("关闭串口")
        else:
            self.serial_thread.stop()
            # 打开串口操作
            self.ui.btx_start.setText("打开串口")

    def handle_data_received(self, data):
        """
        接收数据
        :param data:接收到的数据
        :return:
        """
        # 获取时间
        current_time = QDateTime.currentDateTime().toString("yyyy-MM-dd hh:mm:ss")

        # 超过5000字符清空
        if len(self.ui.txt_recv_data_viewer.toPlainText()) > 5000:
            self.ui.txt_recv_data_viewer.clear()
        # 更新显示区域中的数据
        if self.ui.cbx_show_time.isChecked():
            self.ui.txt_recv_data_viewer.insertPlainText(f"[{current_time}] {data}")
        else:
            self.ui.txt_recv_data_viewer.insertPlainText(f"[{data}")

        # 必须是hex格式
        if self.ui.rbn_data_format_ascii.isChecked():
            return

        # 假数据，测试
        #with open('raw_data.txt', 'r') as file:
            # 读取文件所有内容
            #data = file.read()
        # 如果报文最后两位是换行符，则移除
        data = data.replace('0d 0a', '')
        data = bytes.fromhex(data.replace(' ', ''))
        # 解析data
        while data is not None:
            package_parse = PackageParse(data)
            data_format = package_parse.get_data_format()
            if data_format == PackageParse.Type.NAl_SOL:
                # 解析数据
                dict_msg = package_parse.get_nav_sol()
                # 写入当前时间，并保存到文件
                dict_msg["current_time"] = current_time
                save_csv('nav_data.csv', dict_msg)
                # 界面显示
                self.ui.let_nav_header.setText(dict_msg["header"])
                self.ui.let_nav_work_state.setText(dict_msg["work_state"])
                self.ui.let_nav_param_state.setText(dict_msg["param_state"])
                self.ui.let_nav_run_time.setText(str(dict_msg["run_time"]))
                self.ui.let_nav_latitude.setText(str(dict_msg["latitude"]))
                self.ui.let_nav_longitude.setText(str(dict_msg["longitude"]))
                self.ui.let_nav_heave.setText(str(dict_msg["heave"]))
                self.ui.let_nav_east_speed.setText(str(dict_msg["east_speed"]))
                self.ui.let_nav_north_speed.setText(str(dict_msg["north_speed"]))
                self.ui.let_nav_vertical_speed.setText(str(dict_msg["vertical_speed"]))
                self.ui.let_nav_attitude_angle1.setText(str(dict_msg["attitude_angle1"]))
                self.ui.let_nav_attitude_angle2.setText(str(dict_msg["attitude_angle2"]))
                self.ui.let_nav_attitude_angle3.setText(str(dict_msg["attitude_angle3"]))
                self.ui.let_nav_attitude_velocity1.setText(str(dict_msg["attitude_velocity1"]))
                self.ui.let_nav_attitude_velocity2.setText(str(dict_msg["attitude_velocity2"]))
                self.ui.let_nav_attitude_velocity3.setText(str(dict_msg["attitude_velocity3"]))
                self.ui.let_nav_fault_code.setText(dict_msg["fault_code"])
                self.ui.let_nav_imu_time.setText(dict_msg["imu_time"])
                self.ui.let_nav_reserved.setText(dict_msg["reserved"])
                self.ui.let_nav_response_flag.setText(dict_msg["response_flag"])
                self.ui.let_nav_check_sum.setText(dict_msg["check_sum"])
            if data_format == PackageParse.Type.RAW:
                # 解析数据
                data = package_parse.get_raw()
                # 写入当前时间，并保存到文件
                data["current_time"] = current_time
                save_csv('raw_data.csv', data)
                # 界面显示
                self.ui.let_raw_header.setText(dict_msg["header"])
                self.ui.let_raw__imu_time.setText(dict_msg["imu_time"])
                self.ui.let_raw_gyrox.setText(dict_msg["gyrox"])
                self.ui.let_raw_gyroy.setText(dict_msg["gyroy"])
                self.ui.let_raw_gyroz.setText(dict_msg["gyroz"])
                self.ui.let_raw_accex.setText(dict_msg["accex"])
                self.ui.let_raw_accey.setText(dict_msg["accey"])
                self.ui.let_raw_accez.setText(dict_msg["accez"])
                self.ui.let_raw_reserved.setText(dict_msg["reserved"])
                self.ui.let_raw_turntable_angle1.setText(dict_msg["turntable_angle1"])
                self.ui.let_raw_turntable_angle2.setText(dict_msg["turntable_angle2"])
                self.ui.let_raw_longitude.setText(dict_msg["longitude"])
                self.ui.let_raw_latitude.setText(dict_msg["latitude"])
                self.ui.let_raw_para4.setText(dict_msg["para4"])
                self.ui.let_raw_para2_5.setText(dict_msg["para2_5"])
                self.ui.let_raw_para3_6.setText(dict_msg["para3_6"])
                self.ui.let_raw_para7.setText(dict_msg["para7"])
                self.ui.let_raw_para8.setText(dict_msg["para8"])
                self.ui.let_raw_comdata_valid.setText(dict_msg["comdata_valid"])
                self.ui.let_raw_check_sum.setText(dict_msg["check_sum"])
            # 一次收到若干个数据包
            if data_format != PackageParse.Type.UNkNOWN and len(data) > data_format.value:
                data = data[data_format.value:]
            else:
                data = None

    def handler_serial_error(self, error):
        """
        串口接收线程异常
        :param error:
        :return:
        """
        QMessageBox.critical(self, '错误', error)

    def send_serial_data(self, data: str):
        """
        发送数据
        :return:
        """
        if not self.serial_thread or not self.serial_thread.isRunning():
            QMessageBox.warning(self, "Warning", "请先打开串口！")
            return

        data = self.ui.let_send_data_viewer.text()
        if data != "":
            self.serial_thread.send_data(data)
            self.ui.let_send_data_viewer.clear()
