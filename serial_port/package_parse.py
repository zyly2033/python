import unittest
from enum import Enum


def translate(hex_array: [], unit=None):
    """
    将字节数组转换为16进制字符串  [0x0a,0x04]-> 0x0a 0x04
    :param hex_array: 字节数组
    :param unit 量纲
    :return:
    """
    if unit is not None:
        # 将字节数组按照小端格式转换为数字
        num = int.from_bytes(hex_array, byteorder='little')
        return num / unit
    return ' '.join(f'0x{val:02X}' for val in hex_array)


class PackageParse(object):
    """
    报文解析工具
    """

    class Type(Enum):
        """
        报文类型
        """
        NAl_SOL = 49  # 导航解算
        RAW = 58  # 原始信息
        UNkNOWN = 0

    def __init__(self, data: []):
        """
        传入需要解析的报文
        :param data: 字符数组，比如[61 62 63 64 65 66]
        """
        self.__byte_array = data

    def validate_nal_sol(self):
        """
        校验和
            导航结算：第49个字节为3-48字节累加和
        """
        # 取出第3到第48字节
        selected_bytes = self.__byte_array[2:48]

        # 计算累加和并只保留低两位数字
        checksum = sum(selected_bytes) & 0xFF

        return checksum == self.__byte_array[48]

    def validate_raw(self):
        """
        校验和
            原始信息：第58个字节为3-57字节累加和
        """
        # 取出第3到第48字节
        selected_bytes = self.__byte_array[2:57]

        # 计算累加和并只保留低两位数字
        checksum = sum(selected_bytes) & 0xFF

        return checksum == self.__byte_array[57]

    def get_data_format(self):
        """
        返回数据类型：在惯性装置的AXS31接口，里面有两路数据，一路称为导航解算，一路称为原始数
        :return: 0: 导航解算   1:原始信息  2：非法数据
        """
        # 原始信息报文
        if (len(self.__byte_array) >= 58 and self.__byte_array[0] == 0x5a and self.__byte_array[1] == 0x5a
                and self.validate_raw()):
            return PackageParse.Type.RAW
        # 导航解算的报文内容
        if (len(self.__byte_array) >= 49 and self.__byte_array[0] == 0x5a and self.__byte_array[1] == 0x5a
                and self.validate_nal_sol()):
            return PackageParse.Type.NAl_SOL
        return PackageParse.Type.UNkNOWN

    def get_nav_sol(self):
        """
        获取导航解算数据
        :return:
        """
        if self.get_data_format() != PackageParse.Type.NAl_SOL:
            print("数据格式错误，非导航解算报文！")
            return None

        message_dict = {}

        # 解析报文头
        message_dict['header'] = translate(self.__byte_array[0:2])

        # 解析工作状态
        message_dict['work_state'] = translate(self.__byte_array[2:3])

        # 解析参数状态
        message_dict['param_state'] = translate(self.__byte_array[3:4])

        # 解析运行时间
        message_dict['run_time'] = translate(self.__byte_array[4:8], 20)

        # 解析纬度
        message_dict['latitude'] = translate(self.__byte_array[8:11], 93206.75556)

        # 解析经度
        message_dict['longitude'] = translate(self.__byte_array[11:14], 46603.37778)

        # 解析升沉
        message_dict['heave'] = translate(self.__byte_array[14:16], 100)

        # 解析东速
        message_dict['east_speed'] = translate(self.__byte_array[16:18], 100)

        # 解析北速
        message_dict['north_speed'] = translate(self.__byte_array[18:20], 100)

        # 解析垂速
        message_dict['vertical_speed'] = translate(self.__byte_array[20:22], 100)

        # 解析姿态角1
        message_dict['attitude_angle1'] = translate(self.__byte_array[22:25], 0.25 * 93206.75556)

        # 解析姿态角2
        message_dict['attitude_angle2'] = translate(self.__byte_array[25:28], 93206.75556)

        # 解析姿态角3
        message_dict['attitude_angle3'] = translate(self.__byte_array[28:31], 93206.75556)

        # 姿态角速率1
        message_dict['attitude_velocity1'] = translate(self.__byte_array[31:34], 93206.75556)

        # 姿态角速率2
        message_dict['attitude_velocity2'] = translate(self.__byte_array[34:37], 93206.75556)

        # 姿态角速率3
        message_dict['attitude_velocity3'] = translate(self.__byte_array[37:40], 93206.75556)

        # 故障码
        message_dict['fault_code'] = translate(self.__byte_array[40:41])

        # IMU时间
        message_dict['imu_time'] = translate(self.__byte_array[41:43])

        # 备用
        message_dict['reserved'] = translate(self.__byte_array[43:47])

        # 应答标志
        message_dict['response_flag'] = translate(self.__byte_array[47:48])

        # 校验和
        message_dict['check_sum'] = translate(self.__byte_array[48:49])

        return message_dict

    def get_raw(self):
        """
        获取原始信息报文
        :return:
        """
        if self.get_data_format() != PackageParse.Type.RAW:
            print("数据格式错误，非原始信息报文！")
            return None

        message_dict = {}

        # 解析报文头
        message_dict['header'] = translate(self.__byte_array[0:2])

        # IMUtime
        message_dict['imu_time'] = translate(self.__byte_array[2:6])

        # GYROX
        message_dict['gyrox'] = translate(self.__byte_array[6:10])

        # GYROY
        message_dict['gyroy'] = translate(self.__byte_array[10:14])

        # GYROZ
        message_dict['gyroz'] = translate(self.__byte_array[14:18])

        # ACCEX
        message_dict['accex'] = translate(self.__byte_array[18:22])

        # ACCEY
        message_dict['accey'] = translate(self.__byte_array[22:26])

        # ACCEZ
        message_dict['accez'] = translate(self.__byte_array[26:30])

        # 备用
        message_dict['reserved'] = translate(self.__byte_array[30:34])

        # 转台角1
        message_dict['turntable_angle1'] = translate(self.__byte_array[34:37])

        # 转台角2
        message_dict['turntable_angle2'] = translate(self.__byte_array[37:40])

        # GPS经度
        message_dict['longitude'] = translate(self.__byte_array[40:43])

        # GPS纬度
        message_dict['latitude'] = translate(self.__byte_array[43:46])

        # Para[4](电磁/牵引时为航向)
        message_dict['para4'] = translate(self.__byte_array[46:48])

        # Para[2]/para[5]
        message_dict['para2_5'] = translate(self.__byte_array[48:50])

        # Para[3]/para[6]
        message_dict['para3_6'] = translate(self.__byte_array[50:52])

        # Para[7]
        message_dict['para7'] = translate(self.__byte_array[52:54])

        # Para[8]
        message_dict['para8'] = translate(self.__byte_array[54:56])

        # comdatavalid
        message_dict['comdata_valid'] = translate(self.__byte_array[56:57])

        # 校验和
        message_dict['check_sum'] = translate(self.__byte_array[57:58])

        return message_dict


# 编写单元测试类
class TestPackageParse(unittest.TestCase):
    def test_data_format(self):
        data = '5a 5a 03 04 05 06 07 08 09 0a 0b 0c 0d 0e 0f 10 11 12 13 14 15 16 17 18 19 1a 1b 1c 1d 1e 1f 20 21 22 23 24 25 26 27 28 29 2a 2b 2c 2d 2e 2f 30 31'
        package = PackageParse(data)
        data_format = package.get_data_format()
        self.assertEqual(data_format, PackageParse.Type.NAl_SOL)

    def test_get_nav_sol(self):
        data = '5a 5a 04 bf b9 f8 0e 01 3e 49 37 d4 83 56 00 00 01 00 00 00 00 00 d9 1d 0b 28 73 ff f3 12 00 9c fc ff f4 00 00 42 ff ff 00 b7 14 00 00 00 00 00 25'
        package = PackageParse(data)
        data = package.get_nav_sol()
        print("解析数据", data)
        self.assertEqual(data['header'], '0x5A 0x5A')
        self.assertEqual(data['work_state'], '0x04')
        self.assertEqual(data['param_state'], '0xBF')
        self.assertAlmostEqual(data['run_time'], 355167860.0, places=2)
        self.assertAlmostEqual(data['latitude'], 38.87304067426333, places=2)
        self.assertAlmostEqual(data['longitude'], 121.66165351287547, places=2)
        self.assertAlmostEqual(data['heave'], 0.0, places=2)
        self.assertAlmostEqual(data['east_speed'], 0.01, places=2)
        self.assertAlmostEqual(data['north_speed'], 0.0, places=2)
        self.assertAlmostEqual(data['vertical_speed'], 0.0, places=2)
        self.assertAlmostEqual(data['attitude_angle1'], 31.265416143833853, places=2)
        self.assertAlmostEqual(data['attitude_angle2'], 179.6131610784715, places=2)
        self.assertAlmostEqual(data['attitude_angle3'], 0.052045583722493856, places=2)
        self.assertAlmostEqual(data['attitude_velocity1'], 179.99068736171765, places=2)
        self.assertAlmostEqual(data['attitude_velocity2'], 0.002617835998410328, places=2)
        self.assertAlmostEqual(data['attitude_velocity3'], 179.9979615125657, places=2)
        self.assertEqual(data['fault_code'], '0x00')
        self.assertEqual(data['imu_time'], '0xB7 0x14')
        self.assertEqual(data['reserved'], '0x00 0x00 0x00 0x00')
        self.assertEqual(data['response_flag'], '0x00')
        self.assertEqual(data['check_sum'], '0x25')

    def test_get_raw(self):
        data = '5a 5a 03 04 05 06 40 49 0f db 0b 0c 0d 0e 0f 10 11 12 13 14 15 16 17 18 19 1a 1b 1c 1d 1e 1f 20 21 22 23 24 25 26 27 28 29 2a 2b 2c 2d 2e 2f 30 31 32 33 34 35 36 37 38 39 3a'
        package = PackageParse(data)
        data = package.get_raw()
        print("解析数据", data)
        self.assertEqual(data['header'], '0x5A 0x5A')
        self.assertEqual(data['imu_time'], '0x03 0x04 0x05 0x06')
        self.assertEqual(data['reserved'], '0x1F 0x20 0x21 0x22')
        self.assertEqual(data['turntable_angle1'], '0x23 0x24 0x25')
        self.assertEqual(data['turntable_angle2'], '0x26 0x27 0x28')
        self.assertEqual(data['longitude'], '0x29 0x2A 0x2B')
        self.assertEqual(data['latitude'], '0x2C 0x2D 0x2E')
        self.assertEqual(data['para4'], '0x2F 0x30')
        self.assertEqual(data['para2_5'], '0x31 0x32')
        self.assertEqual(data['para3_6'], '0x33 0x34')
        self.assertEqual(data['para7'], '0x35 0x36')
        self.assertEqual(data['para8'], '0x37 0x38')
        self.assertEqual(data['comdata_valid'], '0x39')
        self.assertEqual(data['check_sum'], '0x3A')
