#继承和多态

#在OOP程序设计中，当我们定义一个class的时候，可以从某个现有的class继承，新的class称为子类（Subclass），而被继承的class称为基类、父类或超类（Base class、Super class）。
class Animal(object):
    def run(self):
        print('Animal is running....')

#继承有什么好处？最大的好处是子类获得了父类的全部功能
class Dog(Animal):
    #重写父类方法
    def run(self):
        print('Dog is runing....')

class Cat(Animal):
    #重写父类方法
    def run(self):
        print('Cat is runing....')


a = Animal()
d = Dog()
c = Cat()
#判断一个变量是否是某个类型可以用isinstance()判断： 在继承关系中，如果一个实例的数据类型是某个子类，那它的数据类型也可以被看做是父类。但是，反过来就不行：
print(isinstance(a,Animal))
print(isinstance(d,Animal))
print(isinstance(c,Animal))


#对于一个变量，我们只需要知道它是Animal类型，无需确切地知道它的子类型，就可以放心地调用run()方法，而具体调用的run()方法是作用在Animal、Dog、Cat还是Tortoise对象上，由运行时该对象的确切类型决定，这就是多态真正的威力：调用方只管调用，不管细节，而当我们新增一种Animal的子类时，只要确保run()方法编写正确，不用管原来的代码是如何调用的。这就是著名的“开闭”原则：
#对扩展开放：允许新增Animal子类；
#对修改封闭：不需要修改依赖Animal类型的run_twice()等函数。
def run_twice(animal):
    if hasattr(animal,'run'):        
        animal.run()        

#对于Python这样的动态语言来说，则不一定需要传入Animal类型。我们只需要保证传入的对象有一个run()方法就可以了：
#这就是动态语言的“鸭子类型”，它并不要求严格的继承体系，一个对象只要“看起来像鸭子，走起路来像鸭子”，那它就可以被看做是鸭子。
    
run_twice(a)
run_twice(d)
run_twice(c)
