#汉诺塔的移动可以用递归函数非常简单地实现。
#请编写move(n, a, b, c)函数，它接收参数n，表示3个柱子A、B、C中第1个柱子A的盘子数量，然后打印出把所有盘子从A借助B移动到C的方法，例如：
def move(n,a,b,c):   #将A柱n个盘子从a->c    
    if n == 1:        
        print('%d号盘子：%s->%s'%(n,a,c))      #将A柱下盘子移动到C
        return
    else:
        move(n-1,a,c,b)          #将A柱n-1盘子从A->B
        print('%d号盘子：%s->%s'%(n,a,c))      #将A柱下盘子移动到C
        move(n-1,b,a,c)          #将B柱n-1盘子从B->C


move(4,'A','B','C')        
        
                  
        
        
