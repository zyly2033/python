#sorted函数 对list进行排序
L=[36,5,-12,9,-1]
print(sorted(L))

#sorted()函数也是一个高阶函数，它还可以接收一个key函数来实现自定义的排序，例如按绝对值大小排序：
print(sorted(L,key=abs))  #key指定的函数将作用于list的每一个元素上，并根据key函数返回的结果进行排序

S = ['bob', 'about', 'Zoo', 'Credit']
print(sorted(S))

#现在，我们提出排序应该忽略大小写，按照字母序排序。要实现这个算法，不必对现有代码大加改动，只要我们能用一个key函数把字符串映射为忽略大小写排序即可。忽略大小写来比较两个字符串，实际上就是先把字符串都变成大写（或者都变成小写），再比较。
print(sorted(S,key=str.lower))


#下面意义一样
print(str.upper('Abc'))
print('Abc'.upper())

#要进行反向排序，不必改动key函数，可以传入第三个参数reverse=True：
print(sorted(S,key=str.lower,reverse=True))


#假设我们用一组tuple表示学生名字和成绩：
#请用sorted()对上述列表分别按名字排序：
L = [('Bob', 75), ('Adam', 92), ('Bart', 66), ('Lisa', 88)]
def by_name(t):
    return t[0]
L2 = sorted(L, key=by_name)
print(L2)

