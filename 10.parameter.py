#https://www.liaoxuefeng.com/wiki/0014316089557264a6b348958f449949df42a6d3a2e542c000/001431752945034eb82ac80a3e64b9bb4929b16eeed1eb9000
#函数参数类型
##################################  位置参数
print('位置参数')
def power(x,n):    
    sum = 1
    while(n>0):
        sum = sum*x
        n = n -1
    return sum;
print(power(2,5),'\n')

##################################   默认参数
print('默认参数')
def person(name,sex,age=6,city='北京'): 
    s1 = '我的名字是%s,性别%s,年龄%d,来自%s'%(name,sex,age,city)
    return s1
print(person('郑洋','男',24))


def add_end(L=[]):
    L.append('END')
    return L               #返回L变量指向的对象，L是可变对象，因为会出现
                           #下面意想不到的问题 

l1 = add_end()   #输出['END']
print(l1)
l2 = add_end()   #输出['END','END']
print(l2)
    


#改进
def add_end_new(L=None):
    if L is None:
        L=[]    
    L.append('END')
    return L
l1 = add_end_new()   #输出['END']
print(l1)
l2 = add_end_new()   #输出['END']
print(l2,'\n')


#################################### 可变参数
print('可变参数')
#计算a^2 + b^2 + c^2 +....
#不使用可变参数  使用list或者tuple列表
def calc(numbers):      #传入一个列表
    sum = 0
    for n in numbers:
        sum = sum + n*n
    return sum

#传入list
nums = [1,2,3]
print(calc(nums))
#传入tiple
nums = (1,2,3)
print(calc(nums))

#利用可变参数计算
def my_calc(*numbers):    #参数args接收到的是一个tuple
    sum = 0
    for n in numbers:
        sum = sum + n * n
    return sum
print(my_calc(1,2,3,4))   #可变参数在函数调用时，自动组装成一个元组
print(my_calc(*nums))     #加*把nums编程可变参传进去
print('\n')


################################ 关键字参数 穿入含有参数名的参数
print('关键字参数参数')
def person1(name,age,**kw):
    print('name:',name,'age:',age,'other:',kw)

print(person1('郑洋',30))
print(person1('郑洋',30,city='北京',job='学生'))
#dict字典
extra ={'city':'北京','job':'学生'}
person1('郑洋',24,city=extra['city'])  
person1('郑洋',24,**extra)   #把dict所有key-value用关键字参数传入带参数**kw参数
print('\n')


############################### 命名关键字参数  限制关键字参数的名字
print('命名关键字参数')
def person2(name,age,*,city,job):  #和关键字参数**kw不同，命名关键字参数需要一个特殊分隔符*，*后面的参数被视为命名关键字参数。
    print(name,age,city,job)

person2('郑洋',24,city='北京',job='工程师')
#person2('郑洋',24,city2='北京',job='工程师')     #报错

#如果函数定义中已经有了一个可变参数，后面跟着的命名关键字参数就不再需要一个特殊分隔符*了：
def person3(name, age, *args, city, job):
    print(name, age, args, city, job)
person3('郑洋',24,'可变参数1','可变参数2',city='北京',job='工程师')     

print('\n')


############################ 组合参数
def f1(a, b, c=0, *args, **kw):
    print('a =', a, 'b =', b, 'c =', c, 'args =', args, 'kw =', kw)

def f2(a, b, c=0, *, d, **kw):
    print('a =', a, 'b =', b, 'c =', c, 'd =', d, 'kw =', kw)

f1(1, 2)
f1(1, 2, c=3)
f1(1, 2, 3, 'a', 'b')
f1(1, 2, 3, 'a', 'b', x=99)
f2(1, 2, d=99, ext=None)
args = (1, 2, 3, 4)
kw = {'d': 99, 'x': '#'}
f1(*args, **kw)
args = (1, 2, 3)
kw = {'d': 88, 'x': '#'}
f2(*args, **kw)


################################ 练习
#以下函数允许计算两个数的乘积，请稍加改造，变成可接收一个或多个数并计算乘积：
def prod(x, y):
    return x * y

def product(*args):
    if len(args)==0:
        raise TypeError('tuple len is not 0')
    sum = 1
    for n in args:
        sum = sum*n
    return sum
# 测试
print('product(5) =', product(5))
print('product(5, 6) =', product(5, 6))
print('product(5, 6, 7) =', product(5, 6, 7))
print('product(5, 6, 7, 9) =', product(5, 6, 7, 9))
if product(5) != 5:
    print('测试失败!')
elif product(5, 6) != 30:
    print('测试失败!')
elif product(5, 6, 7) != 210:
    print('测试失败!')
elif product(5, 6, 7, 9) != 1890:
    print('测试失败!')
else:
    try:
        product()
        print('测试失败!')
    except TypeError:
        print('测试成功!')


########  递归函数
def fact(n):
    if n== 1:
        return 1
    else:
        return n*fact(n-1)

#计算10！
print(fact(10))
#fact(1000)     #栈溢出


###尾递归优化
#使用递归函数的优点是逻辑简单清晰，缺点是过深的调用会导致栈溢出。
#针对尾递归优化的语言可以通过尾递归防止栈溢出。尾递归事实上和循环是等价的，没有循环语句的编程语言只能通过尾递归实现循环。
#Python标准的解释器没有针对尾递归做优化，任何递归函数都存在栈溢出的问题。
def fact1(n):
    return fact_iter(n, 1)

def fact_iter(num, product):
    if num == 1:
        return product
    return fact_iter(num - 1, num * product)
#fact1(1000)     #栈溢出


    













    
