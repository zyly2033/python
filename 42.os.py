# -*- coding: utf-8 -*-
"""
Created on Sun Dec  3 16:02:39 2017

@author: zy
"""

#对文件和目录进行操作
import os
#获取当前系统
if os.name == 'posix':
    print('操作系统为:linux,unix或Max OS X')
    #打印详细信息，windows下不支持
    print(os.uname())
elif os.name == 'nt':
    print('windows操作系统')
else:
    pass
    
#获取当前系统的所有环境变量
print(os.environ)
print('\n\n')
#要获取某个环境变量的值，可以调用os.environ.get('key')：
print(os.environ.get('path','不存在'))
 

#操作文件或者目录
#获取当前工作目录
print(os.path.abspath('.'))
#把两个路径合成一个时，不要直接拼字符串，而要通过os.path.join()函数，这样可以正确处理不同操作系统的路径分隔符。在Linux/Unix/Mac下，os.path.join()返回这样的字符串：
#part-1/part-2
#而Windows下会返回这样的字符串：
#part-1\part-2


#递归删除一个目录
def removeDir(dirPath):
    if not os.path.isdir(dirPath):
        return
    #获取制定目录下的所有文件
    files = os.listdir(dirPath)
    try:
        #遍历文件名称
        for file in files:
            #获取全路径
            filePath = os.path.join(dirPath,file)
            #删除文件
            if os.path.isfile(filePath):
                os.remove(filePath)
            #递归删除目录
            elif os.path.isdir(filePath):
                 removeDir(filePath)
        #删除该目录
        os.rmdir(dirPath)
    except Exception as e:
        print('Exception:',e)



path = os.path.join('.','42.test')
#删掉一个目录
if os.path.isdir(path):    #目录存在
    print('准备删除...')
    #os.rmdir(path)
    removeDir(path)
#创建一个目录
if not os.path.isdir(path):    #目录不存在
    print('准备创建目录:',path)
    os.mkdir(path)
#同样的道理，要拆分路径时，也不要直接去拆字符串，而要通过os.path.split()函数，这样可以把一个路径拆分为两部分，后一部分总是最后级别的目录或文件名：
print(os.path.split(path))
#os.path.splitext()可以直接让你得到文件扩展名，很多时候非常方便
print(os.path.splitext(path))


#创建一个新文件
newfile = os.path.join(path,'new.txt')
with open(newfile,'w+') as f:
    print('创建新文件:',newfile)
    f.write('这是一个新文件')
#文件重命名
from shutil import *
if os.path.isfile(newfile):
    print('文件重命名:',newfile)
    newName = os.path.join(path,'file.txt')
    #把新文件重命名
    os.rename(newfile,newName)
    #复制文件
    copyfile(newName,newfile)    
    #删除文件
    os.remove(newName)

    
#过滤当前路径下的所有目录文件
L = [x for x  in os.listdir('.') if os.path.isdir(x)]
print('过滤当前路径下的所有目录文件',L)
L = [x for x  in os.listdir('.') if os.path.isfile(x) and os.path.splitext(x)[1] == '.py']
print('过滤当前路径下的所有py文件',sorted(L))

#利用os模块编写一个能实现dir -l输出的程序。

#编写一个程序，能在当前目录以及当前目录的所有子目录下查找文件名包含指定字符串的文件，并打印出相对路径。
def search(dirPath,string):  
    if not os.path.isdir(dirPath):
        print('请输入一个正确的目录')
        return    
    #保存所有找到的文件
    L = []    
    #获取制定目录下的所有文件
    files = os.listdir(dirPath)
    try:
        #遍历文件名称
        for file in files:            
            #获取全路径
            filePath = os.path.join(dirPath,file)            
            if os.path.isfile(filePath):              
                #判断file文件名是都包含string
                if file.find(string) != -1:
                     L.append(filePath)                     
                     #print(filePath)
            #递归删除目录
            elif os.path.isdir(filePath):
                 for x in search(filePath,string):
                     L.append(x)
        return L              
    except Exception as e:
        print('Exception:',e)    
        
print('开始查找文件')
L = search(r'F:\python','new') 
print(stra)
print(L)
    

    


    