#https://www.liaoxuefeng.com/wiki/0014316089557264a6b348958f449949df42a6d3a2e542c000/001431756919644a792ee4ead724ef7afab3f7f771b04f5000
#切片 用于从list和tuple取部分元素
L=['郑洋','黄旭','朱圆圆','刘慧','刘稳']
#print(L[-8])  #越界

#取前三个元素
print(L[0:3])
#取后从倒数第二个开始之后的所有元素
print(L[-2:])
#取后从倒数第二个开始之后的一个元素
print(L[-2:-1])

#print(L[-2:0])  无效
#获取0-100的有序列表
L=list(range(101))
#取前十个元素
print(L[:10])
#每间隔5个数取一次
print(L[::5])
print(L[0:0])   #[]
#tuple也是一种list，唯一区别是tuple不可变。因此，tuple也可以用切片操作，只是操作的结果仍是tuple：
print((0, 1, 2, 3, 4, 5)[:3])
#字符串'xxx'也可以看成是一种list，每个元素就是一个字符。因此，字符串也可以用切片操作，只是操作结果仍是字符串：
s1 = 'ABCDEFG'
print(s1[:3])
print(s1[::2])
#利用切片操作，实现一个trim()函数，去除字符串首尾的空格，注意不要调用str的strip()方法：
def trim(s):
    if s is None:
        raise TypeError('请输入一个字符串')
    start = 0                               #记录第一个不是空格的索引
    end = len(s)                            #长度  用于记录最后一个空格的索引   
    while start<end and s[start] == ' ':    #从头开始遍历   过滤首部的空格
        start += 1    
    while end > start and s[end-1] == ' ':  #从结尾开始遍历  过滤尾部的空格         
        end -= 1    
    return   s[start:end]

s1 = trim('   abc   ased  ')
s2 = trim('abc   ased  ')
s3 = trim('abc   ased')
s4 = trim('')
s5 = trim('     ')
print('%s->去空格成功'%s1)
print('%s->去空格成功'%s2)
print('%s->去空格成功'%s3)
print('%s->去空格成功'%s4)
print('%s->去空格成功'%s5)
# 测试:
if trim('hello  ') != 'hello':
    print('测试失败!')
elif trim('  hello') != 'hello':
    print('测试失败!')
elif trim('  hello  ') != 'hello':
    print('测试失败!')
elif trim('') != '':
    print('测试失败!')
elif trim('    ') != '':
    print('测试失败!')
else:
    print('测试成功!')


