﻿#和map()类似，filter()也接收一个函数和一个序列。和map()不同的是，filter()把传入的函数依次作用于每个元素，然后根据返回值是True还是False决定保留还是丢弃该元素。
def is_odd(n):
    return n % 2 == 1    
print(list(filter(is_odd, [1, 2, 4, 5, 6, 9, 10, 15])))


#把一个序列中的空字符串删掉，可以这么写：
print(' 256  6  '.strip())            #去除前后的空格
def not_empty(s):
    return s and s.strip()            
print(list(filter(not_empty, ['A', '', 'B', None, 'C', '  '])))


#用filter求素数







#定义一个生成器，不断返回下一个素数
def primes():
    #构造一个从3开始的奇数序列
    def _odd_iter():
        n =1
        while True:
            n = n+2
            yield n
    #nn = _odd_iter()
    #print(next(nn))   #3
    #print(next(nn))   #5
    #print(next(nn))   #7
    #定义一个筛选函数
    def _not_divisible(n):
        return lambda x:x%n>0
    yield 2
    it = _odd_iter()                        #初始化序列
    while True:
        n = next(it)                        #返回序列的下一个数
        yield n
        it = filter(_not_divisible(n),it)   #构造新序列

#由于primes()也是一个无限序列，所以调用时需要设置一个退出循环的条件：

# 打印1000以内的素数:
#for n in primes():
#    if n < 1000:
#        print(n)
#    else:
#        break



#回数是指从左向右读和从右向左读都是一样的数，例如12321，909。请利用filter()筛选出回数：
def is_palindrome(n):
    def inverse(s):     #把字符串翻转 s[::-1]也是取反
        L=''
        for n in s:        
            L='%s%s'%(n,L)
        return L    
    s = str(n)                     #把n转换成字符串         
    return s == inverse(s)

def is_palindrome1(n):
    return str(n)[::-1]==str(n)

output = filter(is_palindrome, range(1, 1000))
print('1~1000:', list(output))
if list(filter(is_palindrome, range(1, 200))) == [1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 22, 33, 44, 55, 66, 77, 88, 99, 101, 111, 121, 131, 141, 151, 161, 171, 181, 191]:
    print('测试成功!')
else:
    print('测试失败!')

                            


    

