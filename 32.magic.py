# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

#看到类似__slots__这种形如__xxx__的变量或者函数名就要注意，这些在Python中是有特殊用途的。
#__slots__我们已经知道怎么用了，__len__()方法我们也知道是为了能让class作用于len()函数。
#除此之外，Python的class中还有许多这样有特殊用途的函数，可以帮助我们定制类。

#__str__（）  当我们打印一个实例的时候调用
#__repr__()  当直接输出实例的时候调用
#__iter__() 当用循环遍历时调用 如果一个类想被用于for ... in循环，类似list或tuple那样，就必须实现一个_iter__()方法，该方法返回一个迭代对象，然后，Python的for循环就会不断调用该迭代对象的__next__()方法拿到循环的下一个值，直到遇到StopIteration错误时退出循环。
#__getitem__() 使实例可以向list那样按照下表取元素  当使用下表取元素时调用
#__getattr__() 当调用不存在属性时，调用
#__call__() 直接对实例调用
class Student(object):
    def __init__(self,name):
        self.__name = name
    def __str__(self):
        return '\'Student\' object (name:%s)'%self.__name
    def __repr__(self):    
        return '当直接输出实例的时候调用'

s = Student('郑洋')
print(s)              #调用__str__()函数 
s

class Fib(object):
    def __init__(self):
        self.__a,self.__b = 0,1
    def __iter__(self):        #实例本身就是迭代对象
        return self
    def __next__(self):
        self.__a,self.__b = self.__b,self.__a + self.__b
        if(self.__a > 100):
            raise StopIteration()
        return self.__a        #返回下一个值
    #使实例可以向list那样按照下表取元素  当使用下表取元素时调用
    def __getitem__(self,n):
        if isinstance(n,int):       #判断传入的是索引还是切片
            a,b=1,1
            for x in range(n):
                a,b = b,a+b
            return a
        if isinstance(n,slice):    #切片 
            start = n.start
            stop = n.stop
            if start == None:
                start = 0 
            a,b = 1,1
            L= []
            for x in range(stop):
                if x > start:
                    L.append(a)
                a,b = b,a+b
            return L

        
        
    
 #循环遍历   
for x in Fib():
    print(x)
    
#Fib实例虽然能作用于for循环，看起来和list有点像，但是，把它当成list来使用还是不行，比如，取第5个元素：
f = Fib()
print(f[4])
print(f[4:10])

class Student(object):          
    def __getattr__(self,attr):
        if attr=='score':               #返回一个属性
            return 99
        elif attr=='age':                 #返回一个方法
            return lambda:25
        else:
            raise AttributeError('调用了一个不存在的属性或者方法!')
    def __call__(self,name):
        print('输入的参数是%s'%name)
        

s = Student()
s.gender = '男'
print(s.score)
print(s.age())
s('郑洋')
#判断一个对象是否能被调用，能被调用的对象就是一个Callable对象，比如函数和我们上面定义的带有__call__()的类实例
print(callable(s))
print(callable(max))
print(callable([1,2,3]))
print(callable('234'))

print(dir(s))

#s.pp()


